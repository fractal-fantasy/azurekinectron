var animateLights = false, strobeLights = false;
var ambientLight, lights = [], lightHelpers = [];
var lightsStrobeTimer, lightsStrobeTimerHold;
var strobeLightObj =    {   now: 0,
                            elapsed: 0,
                            then: 0,
                            fpsInterval: 0
                        };
        
var lightsObject = new THREE.Object3D();

function addLights() {

     ambientLight = new THREE.AmbientLight(0x777777);
    //ambientLight = new THREE.DirectionalLight(0xffffff, 0.8);
    //ambientLight.position.set(0, 0, 150);
    //ambientLight.rotation.set(1, 1, 1);
    lightsObject.add( ambientLight );


    lights[0] = new THREE.PointLight(0x1500ff, 1, 0);
    lights[1] = new THREE.PointLight(0x00b8ff, 1, 0);


    lights[0].position.set(-800, 600, 1000);
    lights[1].position.set(800, 200, 1000);


    lightsObject.add( lights[0] );
    lightsObject.add( lights[1] );

    lights[0].animate = false;
    lights[1].animate = false;


    lightHelpers[0] = new THREE.PointLightHelper( lights[0], 2 );
    lightHelpers[1] = new THREE.PointLightHelper( lights[1], 2 );
    scene.add( lightHelpers[0] );
    scene.add( lightHelpers[1] );

    lightsObject.add( ambientLight );

    scene.add( lightsObject );
    // sun = new THREE.DirectionalLight(0xffffff, 1);
    // sun.position.set(300, 2000, 500);
    // scene.add(sun);

}

function setLightsVisibility( value ) {

    lightsObject.visible = value;

}

function animatePointLights() {

    var properties = params.program.objects.lights.properties;

    if ( lights[0].animate ) {
        lights[0].position.x = properties.animateRadius.value * Math.sin( time * properties.animateSpeed.value );
        lights[0].position.z = properties.animateRadius.value * Math.cos( time * properties.animateSpeed.value );
        lights[0].position.y = properties.animateYamount.value * Math.sin( time * properties.animateYspeed.value );
    }

    if ( lights[1].animate ) {
        lights[1].position.x = properties.animateRadius.value * Math.sin( Math.PI/1 + time * properties.animateSpeed.value );
        lights[1].position.z = properties.animateRadius.value * Math.cos( Math.PI/1 + time * properties.animateSpeed.value );
        lights[1].position.y = properties.animateYamount.value * Math.cos( time * properties.animateYspeed.value );
    }

}

function startStrobeLights() {

    //var properties = params.program.objects.lights.properties;

    // strobeLightObj.fpsInterval = 1000 / properties.strobeFrequency.value;
    lights[0].then = Date.now();
    lights[1].then = Date.now();

    strobeLights = true;

    //animateStrobe();

    // lightsStrobeTimer = setInterval( function () {

    //     //lights[0].visible = true;
    //     //lights[1].visible = !lights[0].visible;

    //     //lights[0].intensity = properties.pointLight1Intensity.value;
    //     lights[0].color.set( 0xffffff );

    //     lights[0].timeout = setTimeout( function() {

    //         //lights[0].intensity = 0;
    //         lights[0].color.set( 0x000000 );

    //     }, properties.strobeHold.value );
       

    // }, 1000 / properties.strobeFrequency.value );

}

function animatePontLightsStrobe() {

    //if ( requestID % params.program.objects.lights.properties.strobeFrequency.value == 0 ) lights[0].color.set( 0xffffff );

    //if ( requestID % params.program.objects.lights.properties.strobeHold.value == 0 ) lights[0].color.set( 0x000000 );

    //strobeLightObj.strobeFrameID = requestAnimationFrame( animateStrobe );

    lights[0].now = Date.now();
    lights[1].now = Date.now() - 1000 / params.program.objects.lights.properties.strobeFrequency.value / 2;
    lights[0].elapsed = lights[0].now - lights[0].then;
    lights[1].elapsed = lights[1].now - lights[1].then;

    lights[0].fpsInterval = 1000 / params.program.objects.lights.properties.strobeFrequency.value;
    lights[1].fpsInterval = 1000 / params.program.objects.lights.properties.strobeFrequency.value;

    // if enough time has elapsed, draw the next frame

    if (lights[0].elapsed > lights[0].fpsInterval) {

        // Get ready for next frame by setting then=now, but also adjust for your
        // specified fpsInterval not being a multiple of RAF's interval (16.7ms)
        lights[0].then = lights[0].now - (lights[0].elapsed % lights[0].fpsInterval);


        lights[0].color.set( "#" + params.program.objects.lights.properties.pointLight1Color.value );

        lights[0].timeout = setTimeout( function() {

            lights[0].color.set( 0x000000 );

        }, 1000 / params.program.objects.lights.properties.strobeHold.value );

    }

    if (lights[1].elapsed > lights[1].fpsInterval) {

        // Get ready for next frame by setting then=now, but also adjust for your
        // specified fpsInterval not being a multiple of RAF's interval (16.7ms)
        lights[1].then = lights[1].now - (lights[1].elapsed % lights[1].fpsInterval);


        lights[1].color.set( "#" + params.program.objects.lights.properties.pointLight2Color.value );

        lights[1].timeout = setTimeout( function() {

            lights[1].color.set( 0x000000 );

        }, 1000 / params.program.objects.lights.properties.strobeHold.value );

    }
}

function stopStrobeLights() {

    //cancelAnimationFrame( strobeLightObj.strobeFrameID );

    //clearInterval( lightsStrobeTimer );
    clearTimeout( lights[0].timeout );
    clearTimeout( lights[1].timeout );

    //lightsStrobeTimer = undefined;
    
    strobeLights = false;

    lights[0].color.set( "#" + params.program.objects.lights.properties.pointLight1Color.value );
    lights[1].color.set( "#" + params.program.objects.lights.properties.pointLight2Color.value );
    // console.log('stop strobes');

    //lights[0].visible = params.program.objects.lights.properties.pointLight1On.value;
    //lights[1].visible = params.program.objects.lights.properties.pointLight2On.value;
    
}

function setLightsParameter( param, paramKey ) {

    if (paramKey !== "visibility")
        params.program.objects.lights.properties[ paramKey ].value = param.value;

    switch (paramKey) {

        case "visibility":
            setLightsVisibility( param.value );
            break;

        case "ambientLightOn":
            ambientLight.visible = param.value;
            break;

        case "ambientLightColor":
            var newColorHex = "#" + param.value;
            ambientLight.color.set( newColorHex );
            break;
        
        case "ambientLightIntensity":
            ambientLight.intensity = param.value;
            break;

        case "pointLight1On":
            lights[0].visible = param.value;
            break;

        case "pointLight1Color":
            var newColorHex = "#" + param.value;
            lights[0].color.set( newColorHex );
            break;
        
        case "pointLight1Intensity":
            lights[0].intensity = param.value;
            break;

        case "pointLight2On":
            lights[1].visible = param.value;
            break;

        case "pointLight2Color":
            var newColorHex = "#" + param.value;
            lights[1].color.set( newColorHex );
            break;
        
        case "pointLight2Intensity":
            lights[1].intensity = param.value;
            break;

        case "pointLight1Position_X":
            lights[0].position.x = param.value;
            break;
        case "pointLight1Position_Y":
            lights[0].position.y = param.value;
            break;
        case "pointLight1Position_Z":
            lights[0].position.z = param.value;
            break;
        
        case "pointLight2Position_X":
            lights[1].position.x = param.value;
            break;
        case "pointLight2Position_Y":
            lights[1].position.y = param.value;
            break;
        case "pointLight2Position_Z":
            lights[1].position.z = param.value;
            break;

        // case "animatePointLights":
        //     animateLights = param.value;
        //     break;

        case "showLightHelpers":

            lightHelpers[0].visible = param.value;
            lightHelpers[1].visible = param.value;
            break;

        case "animatePointLight1":

            lights[0].animate = param.value;
            animateLights = param.value || lights[1].animate;
            break;

        case "animatePointLight2":

            lights[1].animate = param.value;
            animateLights = param.value || lights[0].animate;
            break;

        case "strobePointLights":

            if (param.value) {

                lights[0].color.set( 0x000000 );
                lights[1].color.set( 0x000000 );

                startStrobeLights();
            } else {
                stopStrobeLights();

                

                var stopAgain = setTimeout( function() {

                    stopStrobeLights();
        
                }, 100);
            }

            break;

        case "strobeFrequency":

            if (strobeLights) { // if strobe is on
                //stopStrobeLights();
                //startStrobeLights();
            }
            break;

        default:


    }



}