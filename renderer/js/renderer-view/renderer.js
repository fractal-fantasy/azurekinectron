/**
 * @author mikkamikka / https://mikkamikka.com
 */



const KinectAzure = require("kinect-azure");


const { remote, ipcRenderer } = require('electron');
let prefsWindow = remote.getGlobal('prefsWindow');

const { dialog } = require('electron').remote;
const fs = require('fs');
const path = require('path');

// save file
function saveFile(path, data) {

    return fs.writeFile(path, data,

        (err) => {
            if (!err) {
                console.log("File saved at path:", path);
            }

            else
                console.error(err);
        })
}

class Parentview {
    postMessage( arg1 ) {
        if (prefsWindow) prefsWindow.webContents.send('message', arg1 );
    }
}


var colorRampActive = false;

// ------ Default params  -----/

var SCREEN_WIDTH = window.innerWidth,
    SCREEN_HEIGHT = window.innerHeight;

// WFOV_2X2BINNED


// WFOV_UNBINNED
// const DEPTH_WIDTH = 1024;  
// const DEPTH_HEIGHT = 1024;

const DEPTH_TO_COLOR_NFOV_WIDTH = 640;  
const DEPTH_TO_COLOR_NFOV_HEIGHT = 576;

// NFOV_UNBINNED
const DEPTH_WIDTH_NFOV_UNBINNED = 640;  
const DEPTH_HEIGHT_NFOV_UNBINNED = 576;


const TEXTUREFILTER = THREE.LinearFilter;
//  const TEXTUREFILTER = THREE.NearestFilter;
const ANISOTROPY = 1;

const COLOR_WIDTH = 1280;
const COLOR_HEIGHT = 720;

const IR_TO_COLOR_WIDTH = 1280;
const IR_TO_COLOR_HEIGHT = 720;

const DEPTH_TO_COLOR_WIDTH = 1280;
const DEPTH_TO_COLOR_HEIGHT = 720;

var limitFPS = false;
var maxFPS = 60;

var sceneFlowTextureActive = false;






var COLOR_TO_DEPTH_WIDTH = 640;
var COLOR_TO_DEPTH_HEIGHT = 576;
var DEPTH_WIDTH = 640;  
var DEPTH_HEIGHT = 576;
var IR_WIDTH = 640;
var IR_HEIGHT = 576;

////TO GET WFOV COLOR TO DEPTH WORKING (KILLS NFOV COLOR)
// COLOR_TO_DEPTH_WIDTH = 512
// COLOR_TO_DEPTH_HEIGHT = 512;
// DEPTH_WIDTH = 512;  
// DEPTH_HEIGHT = 512;
// IR_WIDTH = 512;
// IR_HEIGHT = 512;



const transformationType = {
    depthToColor: 0,
    colorToDepth: 1
}
const depthType = {   
     WFOV: KinectAzure.K4A_DEPTH_MODE_WFOV_2X2BINNED,
    //WFOV: KinectAzure.K4A_DEPTH_MODE_WFOV_UNBINNED,
    NFOV: KinectAzure.K4A_DEPTH_MODE_NFOV_UNBINNED 
};

var transformationMode = transformationType.depthToColor;
var depthMode = depthType.WFOV;

const MATERIAL_MAP_IR = 0, 
    MATERIAL_MAP_DEPTH = 1, 
    MATERIAL_MAP_RGB = 2, 
    MATERIAL_MAP_COLORRAMP = 3, 
    MATERIAL_MAP_NORMAL = 4;


var envMapTexturesListObj = { Null: null };
var panoMapTexturesListObj = {};


var params = {}



// ------ Default params done-----/





const parentview = new Parentview();

var sceneObjects = {

    depthMesh: null,
    pointcloud: null,
    instanced: null,
    backgroundPlane: null,
    backgroundSphere: null,
    handGeometry: null

}

var currentProgram = 0;
var activeSceneObject;
var autoRotateScene = false;

var autoRotateVector = new THREE.Vector3();
var autoRotateVector2 = new THREE.Vector3();
var autoRotateSpeed = 0;
        
var debugMidi = false;

var requestID = 0;

var doCaptureRGB = false;

var kinectIsListening = false;

const statsRenderer = new Stats();
statsRenderer.dom.style.cssText = '';
document.getElementById('statsRenderer').appendChild( statsRenderer.dom );
const statsKinect = new Stats();
statsKinect.dom.style.cssText = '';
document.getElementById('statsKinect').appendChild( statsKinect.dom );

// const KinectAzure = require('kinect-azure');
// const kinect = new KinectAzure();

var kinectState = "stopped";



process.dlopen = () => {
    throw new Error('Load native module is not safe')
}

        let kinectWorker = new Worker('js/renderer-view/kinectWorker.js');

        kinectWorker.onmessage = function(e) {

            if (e.data[0] === "msg" ) {

                console.log('Message received from worker: ', e.data[1]);

                if ( e.data[1] === "kinect started listening" ) {

                    kinectIsListening = true;

                    parentview.postMessage( { type: "kinectStartedListening" } );

                }

                if ( e.data[1] === "kinect stopped listening" ) {

                    kinectIsListening = false;

                    parentview.postMessage( { type: "kinectStoppedListening" } );

                }

                if ( e.data[1] === "kinect closed" ) {

                    //ipcRenderer.sendSync('synchronous-message', 'kinect closed');
                    ipcRenderer.send('asynchronous-message', 'kinect closed');   // close renderer window

                    kinectState = "stopped";

                    kinectIsListening = false;

                    // parentview.postMessage( { type: "kinectStoppedListening" } );

                }

                if ( e.data[1] === "kinect started" ) {

                    kinectState = "camera";

                    kinectIsListening = true;

                    parentview.postMessage( { type: "kinectStartedListening" } );

                    swapTransformationModeMeshes();

                }

                if ( e.data[1] === "playback started" ) {

                    kinectState = "playback";

                    kinectIsListening = true;

                    parentview.postMessage( { type: "kinectStartedListening" } );

                    swapTransformationModeMeshes();

                }

            } else {                

                switch (e.data[0]) {

                    case "got depth frame":

                        // depthPostFX.fxComposer.render();
                        // dynamicsPostFX.fxComposer.render();
                            
                        // composerDynamics.render();
                            
                        // if(sceneFlowActive){
                            //composerDynamics.render();
                            
                            // composerVelocity.render();
                        // }
                        // if(liquidActive){
                        //     
                        // }

                        processDepthFrame( e.data[1] );

                        statsKinect.update();

                       
                        break;

                    case "got color frame":

                        processColorFrame( e.data[1] );
                        break;

                    case "got ir frame":

                        processIRFrame( e.data[1] );
                        break;   

                    case "got body frame":

                        processBodyFrame( e.data[1] );          

                }
            }

        }


        

        const canvas = document.getElementById('outputCanvas');
        canvas.width = canvas.clientWidth;
        canvas.height = canvas.clientHeight;

        const container = canvas;

        var context = canvas.getContext( 'webgl2', { alpha: false } );
        var renderer = new THREE.WebGLRenderer( { canvas: canvas, context: context } );
        renderer.sortObjects = true;
        renderer.setClearColor(0x000000);
        renderer.setPixelRatio(window.devicePixelRatio);

        const scene = new THREE.Scene();

        var depthPostFX,dynamicsPostFX;
        var echoPostFX_IR, echoPostFX_DEPTH, echoPostFX_COLOR;
        

        var time, myTime = 0;
        var timeCamera = 0;



        const camera = new THREE.PerspectiveCamera( 30, canvas.clientWidth / canvas.clientHeight, 1, 10000 );
     //  const cubeCamera =  new HalfCubeCamera(1, 10000, 2880); // Near, far, and resolution
       var cubeCamera = new HalfCubeCamera(0.1, 10000, 2880, {}, false);  // Without bottom camera

        cubeCamera.position.set(0, 50, 600);
        cubeCamera.position.set(0, 0, 0);
        cubeCamera.renderBottom = false;
       
        const cameraDome = new THREE.OrthographicCamera(
            -SCREEN_WIDTH / 2,
            SCREEN_WIDTH / 2,
            SCREEN_HEIGHT / 2,
            -SCREEN_HEIGHT / 2,
            1, 1000);

            cameraDome.position.z = 1100;
            cameraDome.position.x = 4000;

        //cameraDome.antialias = true;
       // const quadGeometry  = new THREE.PlaneBufferGeometry(window.innerHeight,  window.innerHeight);
        
        const quadMaterial = new THREE.ShaderMaterial({
            uniforms: {
                cubeTexture: { value: cubeCamera.renderTarget.texture }
            },
            vertexShader: `
                varying vec2 vUv;
                
                void main()  {
                    vUv = vec2(1.0 - uv.x, uv.y); // Adjust UV flipping if needed
                    gl_Position = projectionMatrix * modelViewMatrix * vec4(position, 1.0);
                }
            `,
            fragmentShader: `
                precision mediump float;
                
                uniform samplerCube cubeTexture;
                
                varying vec2 vUv;
                
                const float PI = 3.14159265359;
                const float TWO_PI = 2.0 * PI;
                
                void main()  {
                    vec2 uv = vUv;
        
                    // Ensure UV.x wraps correctly
                    float longitude = mod(uv.x * TWO_PI, TWO_PI);
                    float latitude = uv.y * PI - PI * 0.5;
        
                    vec3 dir = vec3(
                        cos(latitude) * sin(longitude), 
                        sin(latitude),
                        cos(latitude) * cos(longitude)
                    );
                    
                    gl_FragColor = textureCube(cubeTexture, dir);
                }
            `,
            side: THREE.DoubleSide
        });
        const quadMaterialSpite = new THREE.ShaderMaterial({
            uniforms: {
                cubeTexture: { value: cubeCamera.renderTarget.texture }
            },
            vertexShader: `
                varying vec2 vUv;
                
                void main()  {
                    vUv = vec2(1. - uv.x, 1. - uv.y); // Flipping both x and y for correct orientation
                    gl_Position = projectionMatrix * modelViewMatrix * vec4(position, 1.0);
                }
            `,
            fragmentShader: `
                precision mediump float;
                
                uniform samplerCube cubeTexture;
                
                varying vec2 vUv;
                
                #define M_PI 3.1415926535897932384626433832795
                
                void main()  {
                    vec2 uv = vUv;
                    
                    float longitude = uv.x * 2. * M_PI - M_PI; // Adjusted to map from [0, 1] to [-PI, PI]
                    float latitude = uv.y * M_PI; // Map from [0, 1] to [0, PI]
                    
                    vec3 dir = vec3(
                        -sin(longitude) * sin(latitude),
                        cos(latitude),
                        -cos(longitude) * sin(latitude)
                    );
                    dir = normalize(dir); // Normalize the direction vector
                    
                    gl_FragColor = textureCube(cubeTexture, dir);
                }
            `,
            side: THREE.DoubleSide
        });
        const domeMaterial = new THREE.ShaderMaterial({
            uniforms: {
                cubeTexture: { value: null }, // Remember to set this correctly in your render loop
                domeDegrees: { value: 210.0 }
            },
            vertexShader: `
                varying vec2 vUv;
                
                void main() {
                    vUv = uv;
                    gl_Position = projectionMatrix * modelViewMatrix * vec4(position, 1.0);
                }
            `,
            fragmentShader: `
                uniform samplerCube cubeTexture;
                uniform float domeDegrees;
                varying vec2 vUv;
                
                const float PI = 3.14159265359;
                
                void main() {
                    float maxTheta = domeDegrees / 180.0 * PI; // Convert domeDegrees to radians
                    float cosMaxTheta = cos(maxTheta); // Pre-calculate cos(maxTheta)
                
                    // Convert UV to normalized coordinates (-1 to 1)
                    vec2 xy = 2.0 * vUv - 1.0;
                    float radiusSquared = dot(xy, xy);
                    
                    // Discard the fragment if it's outside the circle
                    if (radiusSquared > 1.0) {
                        discard;
                    }
                    
                    float radius = sqrt(radiusSquared);
                    float theta = radius * maxTheta; // Theta goes from 0 to maxTheta as radius goes from 0 to 1
                
                    // Spherical to Cartesian coordinates conversion
                    vec3 dir = vec3(
                        sin(theta) * cos(atan(xy.y, xy.x)),
                        cos(theta), // Assuming this maps to the zenith
                        sin(theta) * sin(atan(xy.y, xy.x))
                    );
                
                    // Discard the fragment if it's below the horizon
                    if (dir.y < cosMaxTheta) {
                        discard;
                    }
                
                    vec4 color = textureCube(cubeTexture, dir);
                    gl_FragColor = color;
                }
            `,
            side: THREE.DoubleSide,
            depthWrite: false,
            depthTest: false,
            minFilter: THREE.LinearFilter, // Use linear filtering for smoother interpolation
            magFilter: THREE.LinearFilter // Use linear filtering for smoother interpolation
        });

        const domeMaterialOld = new THREE.ShaderMaterial({
            uniforms: {
                cubeTexture: { value: null }, // Remember to set this correctly in your render loop
                domeDegrees: {value: 210.0}
            },
            vertexShader: `
                varying vec2 vUv;
                
                void main() {
                    vUv = uv;
                    gl_Position = projectionMatrix * modelViewMatrix * vec4(position, 1.0);
                }
            `,
            fragmentShader: `
                uniform samplerCube cubeTexture;
                uniform float domeDegrees;
                varying vec2 vUv;
                
                const float PI = 3.14159265359;
                
                void main() {

                    float MAX_THETA = domeDegrees / 180.0 * PI; // Maximum theta value (210 degrees)
                float COS_MAX_THETA = cos(MAX_THETA); // Pre-calculate cos(MAX_THETA)
            
                    // Convert UV to normalized coordinates (-1 to 1)
                    vec2 xy = 2.0 * vUv - 1.0;
                    float radiusSquared = dot(xy, xy);
                    
                    // Discard the fragment if it's outside the circle
                    if (radiusSquared > 1.0) {
                        discard;
                    }
                    
                    float radius = sqrt(radiusSquared);
                    float phi = atan(xy.y, xy.x);
                    float theta = radius * PI; // Theta goes from 0 to PI as radius goes from 0 to 1
            
                    // Spherical to Cartesian coordinates conversion
                    vec3 dir = vec3(
                        sin(theta) * cos(phi),
                        cos(theta), // Assuming this maps to the zenith
                        sin(theta) * sin(phi)
                    );
            
                    // Discard the fragment if it's below MAX_THETA
                    if (dir.y < COS_MAX_THETA) {
                        discard;
                    }
            
                    vec4 color = textureCube(cubeTexture, dir);
                    gl_FragColor = color;
                }
            `,
            side: THREE.DoubleSide,
            depthWrite: false,
            depthTest: false,
            minFilter: THREE.LinearFilter, // Use linear filtering for smoother interpolation
            magFilter: THREE.LinearFilter // Use linear filtering for smoother interpolation
        });

        const quadGeometry  = new THREE.PlaneBufferGeometry(1550,  1550);
        const quadMesh = new THREE.Mesh(quadGeometry, domeMaterial);
        quadMesh.position.set(4000, 0, 200);
      //  quadMesh.position.x = ;
        //quadMesh.renderOrder = 999
        scene.add(cubeCamera);
       scene.add(quadMesh);
       
    //    cubeCamera.renderTarget.texture.ANISOTROPY = 2;
    //    cubeCamera.renderTarget.texture.minFilter = Three.LinearFilter;
    //    cubeCamera.renderTarget.texture.magFilter = Three.LinearFilter;

        

        const cameraOrtho = new THREE.OrthographicCamera(
            -SCREEN_WIDTH / 2,
            SCREEN_WIDTH / 2,
            SCREEN_HEIGHT / 2,
            -SCREEN_HEIGHT / 2,
            1, 5100);
        cameraOrtho.position.z = 700;
        // cameraOrtho.frustumCulled = false;

        const controls = new THREE.OrbitControls(camera, renderer.domElement);

        controls.autoRotate = false;
        controls.autoRotateSpeed = 2;
        controls.refreshAutoRotationAngle();

        var object;


        // init commonly used kinect textures

        var depth_texture, color_texture, depth_colorramp_texture, normal_texture;
        // var depthPixels = null, colorPixels = null, depthColorrampPixels = null;
        
        ////==================== DEPTH TO COLOR ========================////

        var depthToColor = {
            sceneObjects: {},
            depthPixels: new Float32Array(DEPTH_TO_COLOR_WIDTH * DEPTH_TO_COLOR_HEIGHT * 4),
            depthColorrampPixels: new Uint8Array(DEPTH_TO_COLOR_WIDTH * DEPTH_TO_COLOR_HEIGHT * 4),
            colorPixels: new Uint8Array(COLOR_WIDTH * COLOR_HEIGHT * 4),
            irPixels: new Uint8Array(IR_TO_COLOR_WIDTH * IR_TO_COLOR_HEIGHT * 4)
        }

        // var depth_textureOG, depthPixelsOG;
        // var depth_textureOG2, depthPixelsOG2;
        // depthPixelsOG = new Float32Array(DEPTH_TO_COLOR_WIDTH * DEPTH_TO_COLOR_HEIGHT * 4);
        // depth_textureOG = new THREE.DataTexture(depthPixelsOG, COLOR_WIDTH, COLOR_HEIGHT, THREE.RGBAFormat);
        // depth_textureOG.type = THREE.FloatType;
        // depth_textureOG.flipY = true;


        // depthPixelsOG2 = new Float32Array(DEPTH_TO_COLOR_WIDTH * DEPTH_TO_COLOR_HEIGHT * 4);
        // depth_textureOG2 = new THREE.DataTexture(depthPixelsOG2, COLOR_WIDTH, COLOR_HEIGHT, THREE.RGBAFormat);
        // depth_textureOG2.type = THREE.FloatType;
        // depth_textureOG2.flipY = true;


        // Integer type texture DEPTH
        depthToColor.rawDepthPixels = new Uint8Array( DEPTH_TO_COLOR_WIDTH * DEPTH_TO_COLOR_HEIGHT * 2 );
        depthToColor.raw_depth_texture = new THREE.DataTexture( depthToColor.rawDepthPixels, DEPTH_TO_COLOR_WIDTH, DEPTH_TO_COLOR_HEIGHT, THREE.RGIntegerFormat );
        depthToColor.raw_depth_texture.flipY = true;
        depthToColor.raw_depth_texture.internalFormat = 'RG8UI';

        // Integer type texture IR
        depthToColor.rawIRPixels = new Uint8Array( DEPTH_TO_COLOR_WIDTH * DEPTH_TO_COLOR_HEIGHT * 2 );
        depthToColor.raw_ir_texture = new THREE.DataTexture( depthToColor.rawIRPixels, DEPTH_TO_COLOR_WIDTH, DEPTH_TO_COLOR_HEIGHT, THREE.RGIntegerFormat );
        depthToColor.raw_ir_texture.flipY = true;
        depthToColor.raw_ir_texture.internalFormat = 'RG8UI';

        // Integer type texture COLOR
        depthToColor.raw_color_texture = new THREE.DataTexture( depthToColor.colorPixels, COLOR_WIDTH, COLOR_HEIGHT, THREE.RGBAIntegerFormat );
        depthToColor.raw_color_texture.flipY = true;
        depthToColor.raw_color_texture.internalFormat = 'RGBA8UI';

        var raw_depth_texture = depthToColor.raw_depth_texture;
        var raw_ir_texture = depthToColor.raw_ir_texture;
        var raw_color_texture = depthToColor.raw_color_texture;

        //// Render targets for kinect texture processing

        var depthProcRTMaterials = [];
        
        depthToColor.camera_RT = new THREE.OrthographicCamera(DEPTH_TO_COLOR_WIDTH / -2,
                DEPTH_TO_COLOR_WIDTH / 2,
                DEPTH_TO_COLOR_HEIGHT / 2,
                DEPTH_TO_COLOR_HEIGHT / -2, -1000, 1000);

        //===== depth ======//
        depthToColor.glslProcRenderTarget_depth = new THREE.WebGLRenderTarget( DEPTH_TO_COLOR_WIDTH, DEPTH_TO_COLOR_HEIGHT, 
            { 
                
                magFilter: THREE.NearestFilter, 
                magFilter: THREE.NearestFilter, 
                format: THREE.RedFormat, //THREE.RGBAFormat.
                type: THREE.FloatType, //THREE.HalfFloatType


            //     format: THREE.RGBAFormat, //THREE.RGBAFormat
            //     magFilter: THREE.LinearFilter,
            //     minFilter: THREE.LinearFilter, //THREE.LinearMipmapLinearFilter,THREE.LinearFilter, 
            //    // minFilter: THREE.LinearMipmapLinearFilter, //THREE.LinearFilter, //THREE.NearestFilter, 
            //     anisotropy:16,
            } );

       // console.log("anisotropy", renderer.getMaxAnisotropy()); 
        depthToColor.scene_RT_depth = new THREE.Scene();
        depthToColor.material_RT_depth = new THREE.RawShaderMaterial({
            uniforms: {
                "in_texture": { value: depthToColor.raw_depth_texture },
                "minDepth": { value: 100 },
                "maxDepth": { value: 2000 },
                "depthProc_gradientEnable": { value: false },
                "depthProc_uvScale": { value: 1.0 },
                "depthProc_gradThreshold": { value: 0.1 }
            },
            // uniforms: rawDepthProcShader.uniforms,
            vertexShader:   rawDepthProcShader.vertexShader,
            fragmentShader: rawDepthProcShader.fragmentShader
        });

        // depthToColor.material_RT_depth.uniforms["in_texture"].value = depthToColor.raw_depth_texture;

        depthToColor.geometry_RT_depth = new THREE.PlaneBufferGeometry(DEPTH_TO_COLOR_WIDTH, DEPTH_TO_COLOR_HEIGHT);
        depthToColor.mesh_RT_depth = new THREE.Mesh( depthToColor.geometry_RT_depth, depthToColor.material_RT_depth );
        depthToColor.mesh_RT_depth.position.z = -100;
        depthToColor.scene_RT_depth.add( depthToColor.mesh_RT_depth );

        //depth_texture = glslProcRenderTarget_depth.texture;
        //depth_texture = depthPostFX.kinectTexture;

        //===== IR ======//
        depthToColor.glslProcRenderTarget_ir = new THREE.WebGLRenderTarget( DEPTH_TO_COLOR_WIDTH, DEPTH_TO_COLOR_HEIGHT, 
                            { 
                                minFilter: THREE.LinearFilter, 
                                magFilter: THREE.LinearFilter, 
                                format: THREE.RGBAFormat 
                            } );        
        depthToColor.scene_RT_ir = new THREE.Scene();
        depthToColor.material_RT_ir = new THREE.RawShaderMaterial({
            uniforms: {
                "in_texture": { value: depthToColor.raw_ir_texture },
                "minDepth": { value: 100 },
                "maxDepth": { value: 2000 },
                "minIR": { value: 100 },
                "maxIR": { value: 2000 },
                "barrelAlpha": { value: 0.0 },
                "barrelScale": { value: 1.0 }
                
            },
            vertexShader:   rawIRProcShader.vertexShader,
            fragmentShader: rawIRProcShader.fragmentShader
        });        
        depthToColor.geometry_RT_ir = new THREE.PlaneBufferGeometry(DEPTH_TO_COLOR_WIDTH, DEPTH_TO_COLOR_HEIGHT);
        depthToColor.mesh_RT_ir = new THREE.Mesh( depthToColor.geometry_RT_ir, depthToColor.material_RT_ir );
        depthToColor.mesh_RT_ir.position.z = -100;
        depthToColor.scene_RT_ir.add( depthToColor.mesh_RT_ir );

        
        //===== color ======//
        depthToColor.glslProcRenderTarget_color = new THREE.WebGLRenderTarget( COLOR_WIDTH, COLOR_HEIGHT, 
                            { 
                                minFilter: THREE.LinearFilter, 
                                magFilter: THREE.LinearFilter, 
                                format: THREE.RGBAFormat 
                            } );
        depthToColor.scene_RT_color = new THREE.Scene();
        depthToColor.material_RT_color = new THREE.RawShaderMaterial({
            uniforms: {
                "in_texture": { value: depthToColor.raw_color_texture },
                "barrelAlpha": { value: 0.0 },
                "barrelScale": { value: 1.0 }
            },
            vertexShader:   rawColorProcShader.vertexShader,
            fragmentShader: rawColorProcShader.fragmentShader
        });        
        depthToColor.geometry_RT_color = new THREE.PlaneBufferGeometry(DEPTH_TO_COLOR_WIDTH, DEPTH_TO_COLOR_HEIGHT);
        depthToColor.mesh_RT_color = new THREE.Mesh( depthToColor.geometry_RT_color, depthToColor.material_RT_color );
        depthToColor.mesh_RT_color.position.z = -100;
        depthToColor.scene_RT_color.add( depthToColor.mesh_RT_color );
        depthToColor.color_texture = depthToColor.glslProcRenderTarget_color.texture;

        //===== color capture RGB envMap ======//
        depthToColor.glslProcRenderTarget_color_envMap = new THREE.WebGLRenderTarget( COLOR_WIDTH, COLOR_HEIGHT, 
                            { 
                                minFilter: THREE.LinearFilter, 
                                magFilter: THREE.LinearFilter, 
                                format: THREE.RGBAFormat 
                            } );
        depthToColor.color_texture_envmap = depthToColor.glslProcRenderTarget_color_envMap.texture;

        //===== colorramp ======//
        depthToColor.glslProcRenderTarget_colorramp = new THREE.WebGLRenderTarget( DEPTH_TO_COLOR_WIDTH, DEPTH_TO_COLOR_HEIGHT, 
                            { 
                                minFilter: THREE.LinearFilter, 
                                magFilter: THREE.LinearFilter, 
                                format: THREE.RGBAFormat 
                            } );        
        depthToColor.scene_RT_colorramp = new THREE.Scene();
        depthToColor.material_RT_colorramp = new THREE.RawShaderMaterial({
            uniforms: {
                "in_texture": { value: depthToColor.raw_depth_texture },
                "minDepth": { value: 100 },
                "maxDepth": { value: 2000 },
                "colorStart": { value: 0.9},
                "colorEnd": { value: 1.0 },

            },
            vertexShader:   rawColorrampProcShader.vertexShader,
            fragmentShader: rawColorrampProcShader.fragmentShader
        });        
        depthToColor.geometry_RT_colorramp = new THREE.PlaneBufferGeometry(DEPTH_TO_COLOR_WIDTH, DEPTH_TO_COLOR_HEIGHT);
        depthToColor.mesh_RT_colorramp = new THREE.Mesh( depthToColor.geometry_RT_colorramp, depthToColor.material_RT_colorramp );
        depthToColor.mesh_RT_colorramp.position.z = -100;
        depthToColor.scene_RT_colorramp.add( depthToColor.mesh_RT_colorramp );
        depthToColor.depth_colorramp_texture = depthToColor.glslProcRenderTarget_colorramp.texture;


        //===== normal ======//
        depthToColor.glslProcRenderTarget_normal = new THREE.WebGLRenderTarget( DEPTH_TO_COLOR_WIDTH, DEPTH_TO_COLOR_HEIGHT, 
                            { 
                                minFilter: THREE.LinearFilter, 
                                magFilter: THREE.LinearFilter, 
                                format: THREE.RGBAFormat,
                                type: THREE.FloatType 
                            } );        
        depthToColor.scene_RT_normal = new THREE.Scene();
        depthToColor.material_RT_normal = new THREE.ShaderMaterial({
            uniforms: {
                "bumpMap": { value: depthToColor.glslProcRenderTarget_depth.texture },
                //"bumpMap": { value: depth_texture },
                "bumpScale": { value: 100 },
                "uvScale": { value: 2 }
            },
            vertexShader:   rawNormalProcShader.vertexShader,
            fragmentShader: rawNormalProcShader.fragmentShader,
            precision: "mediump"
        });        
        depthToColor.geometry_RT_normal = new THREE.PlaneBufferGeometry(DEPTH_TO_COLOR_WIDTH, DEPTH_TO_COLOR_HEIGHT);
        depthToColor.mesh_RT_normal = new THREE.Mesh( depthToColor.geometry_RT_normal, depthToColor.material_RT_normal );
        // depthToColor.mesh_RT_normal.position.z = -100;
        depthToColor.scene_RT_normal.add( depthToColor.mesh_RT_normal );
        depthToColor.normal_texture = depthToColor.glslProcRenderTarget_normal.texture;

        depthProcRTMaterials.push( depthToColor.material_RT_depth );
        depthProcRTMaterials.push( depthToColor.material_RT_ir );
        depthProcRTMaterials.push( depthToColor.material_RT_color );
        depthProcRTMaterials.push( depthToColor.material_RT_colorramp );
        depthProcRTMaterials.push( depthToColor.material_RT_normal );


        ////==================== COLOR TO DEPTH ========================////

        var colorToDepth = {
            sceneObjects: {},
            depthPixels: new Float32Array(COLOR_TO_DEPTH_WIDTH * COLOR_TO_DEPTH_HEIGHT * 4),
            depthColorrampPixels: new Uint8Array(COLOR_TO_DEPTH_WIDTH * COLOR_TO_DEPTH_HEIGHT * 4),
            colorPixels: new Uint8Array(COLOR_TO_DEPTH_WIDTH * COLOR_TO_DEPTH_HEIGHT * 4),
            irPixels: new Uint8Array(COLOR_TO_DEPTH_WIDTH * COLOR_TO_DEPTH_HEIGHT * 4)
        }

        // Integer type texture test DEPTH
        colorToDepth.rawDepthPixels = new Uint8Array( COLOR_TO_DEPTH_WIDTH * COLOR_TO_DEPTH_HEIGHT * 2 );
        colorToDepth.raw_depth_texture = new THREE.DataTexture( colorToDepth.rawDepthPixels, COLOR_TO_DEPTH_WIDTH, COLOR_TO_DEPTH_HEIGHT, THREE.RGIntegerFormat );
        colorToDepth.raw_depth_texture.flipY = true;
        colorToDepth.raw_depth_texture.internalFormat = 'RG8UI';

        // Integer type texture test IR
        colorToDepth.rawIRPixels = new Uint8Array( COLOR_TO_DEPTH_WIDTH * COLOR_TO_DEPTH_HEIGHT * 2 );
        colorToDepth.raw_ir_texture = new THREE.DataTexture( colorToDepth.rawIRPixels, COLOR_TO_DEPTH_WIDTH, COLOR_TO_DEPTH_HEIGHT, THREE.RGIntegerFormat );
        colorToDepth.raw_ir_texture.flipY = true;
        colorToDepth.raw_ir_texture.internalFormat = 'RG8UI';
        
        colorToDepth.raw_color_texture = new THREE.DataTexture( colorToDepth.colorPixels, COLOR_TO_DEPTH_WIDTH, COLOR_TO_DEPTH_HEIGHT, THREE.RGBAIntegerFormat );
        colorToDepth.raw_color_texture.flipY = true;
        colorToDepth.raw_color_texture.internalFormat = 'RGBA8UI';

        //// Render targets for kinect texture processing

        // var depthProcRTMaterials = [];
        
        colorToDepth.camera_RT = new THREE.OrthographicCamera(COLOR_TO_DEPTH_WIDTH / -2,
            COLOR_TO_DEPTH_WIDTH / 2,
            COLOR_TO_DEPTH_HEIGHT / 2,
            COLOR_TO_DEPTH_HEIGHT / -2, -1000, 1000);

        //===== depth ======//
        colorToDepth.glslProcRenderTarget_depth = new THREE.WebGLRenderTarget( COLOR_TO_DEPTH_WIDTH, COLOR_TO_DEPTH_HEIGHT, 
                            { 
                                minFilter: THREE.NearestFilter, 
                                magFilter: THREE.NearestFilter, 
                                format: THREE.RedFormat,
                                type: THREE.HalfFloatType
                            } );
        colorToDepth.scene_RT_depth = new THREE.Scene();
        colorToDepth.material_RT_depth = new THREE.RawShaderMaterial({
            uniforms: {
                "in_texture": { value: colorToDepth.raw_depth_texture },
                "minDepth": { value: 100 },
                "maxDepth": { value: 2000 },
                "depthProc_gradientEnable": { value: false },
                "depthProc_uvScale": { value: 1.0 },
                "depthProc_gradThreshold": { value: 0.1 }                
            },
            vertexShader:   rawDepthProcShader.vertexShader,
            fragmentShader: rawDepthProcShader.fragmentShader
        });

        colorToDepth.geometry_RT_depth = new THREE.PlaneBufferGeometry(COLOR_TO_DEPTH_WIDTH, COLOR_TO_DEPTH_HEIGHT);
        colorToDepth.mesh_RT_depth = new THREE.Mesh( colorToDepth.geometry_RT_depth, colorToDepth.material_RT_depth );
        colorToDepth.mesh_RT_depth.position.z = -100;
        colorToDepth.scene_RT_depth.add( colorToDepth.mesh_RT_depth );


        //===== IR ======//
        colorToDepth.glslProcRenderTarget_ir = new THREE.WebGLRenderTarget( IR_WIDTH, IR_HEIGHT, 
                            { 
                                minFilter: THREE.LinearFilter, 
                                magFilter: THREE.LinearFilter, 
                                format: THREE.RGBAFormat 
                            } );        
        colorToDepth.scene_RT_ir = new THREE.Scene();
        colorToDepth.material_RT_ir = new THREE.RawShaderMaterial({
            uniforms: {
                "in_texture": { value: colorToDepth.raw_ir_texture },
                "minDepth": { value: 100 },
                "maxDepth": { value: 2000 },
                "minIR": { value: 100 },
                "maxIR": { value: 2000 },
                "barrelAlpha": { value: 0.0 },
                "barrelScale": { value: 1.0 }
            },
            vertexShader:   rawIRProcShader.vertexShader,
            fragmentShader: rawIRProcShader.fragmentShader
        });        
        colorToDepth.geometry_RT_ir = new THREE.PlaneBufferGeometry(IR_WIDTH, IR_HEIGHT);
        colorToDepth.mesh_RT_ir = new THREE.Mesh( colorToDepth.geometry_RT_ir, colorToDepth.material_RT_ir );
        colorToDepth.mesh_RT_ir.position.z = -100;
        colorToDepth.scene_RT_ir.add( colorToDepth.mesh_RT_ir );

        
        //===== color ======//
        colorToDepth.glslProcRenderTarget_color = new THREE.WebGLRenderTarget( COLOR_TO_DEPTH_WIDTH, COLOR_TO_DEPTH_HEIGHT, 
                            { 
                                minFilter: THREE.LinearFilter, 
                                magFilter: THREE.LinearFilter, 
                                format: THREE.RGBAFormat 
                            } );
        colorToDepth.scene_RT_color = new THREE.Scene();
        colorToDepth.material_RT_color = new THREE.RawShaderMaterial({
            uniforms: {
                "in_texture": { value: colorToDepth.raw_color_texture },
                // "barrelAlpha": { value: 0.0 },
                // "barrelScale": { value: 1.0 }
            },
            vertexShader:   rawColorProcShader.vertexShader,
            fragmentShader: rawColorProcShader.fragmentShader
        });        
        colorToDepth.geometry_RT_color = new THREE.PlaneBufferGeometry(COLOR_TO_DEPTH_WIDTH, COLOR_TO_DEPTH_HEIGHT);
        colorToDepth.mesh_RT_color = new THREE.Mesh( colorToDepth.geometry_RT_color, colorToDepth.material_RT_color );
        colorToDepth.mesh_RT_color.position.z = -100;
        colorToDepth.scene_RT_color.add( colorToDepth.mesh_RT_color );
        colorToDepth.color_texture = colorToDepth.glslProcRenderTarget_color.texture;

        //===== color capture RGB envMap ======//
        colorToDepth.glslProcRenderTarget_color_envMap = new THREE.WebGLRenderTarget( COLOR_TO_DEPTH_WIDTH, COLOR_TO_DEPTH_HEIGHT, 
                            { 
                                minFilter: THREE.LinearFilter, 
                                magFilter: THREE.LinearFilter, 
                                format: THREE.RGBAFormat 
                            } );
        colorToDepth.color_texture_envmap = colorToDepth.glslProcRenderTarget_color_envMap.texture;

        //===== colorramp ======//
        colorToDepth.glslProcRenderTarget_colorramp = new THREE.WebGLRenderTarget( COLOR_TO_DEPTH_WIDTH, COLOR_TO_DEPTH_HEIGHT, 
                            { 
                                minFilter: THREE.LinearFilter, 
                                magFilter: THREE.LinearFilter, 
                                format: THREE.RGBAFormat 
                            } );
        colorToDepth.scene_RT_colorramp = new THREE.Scene();
        colorToDepth.material_RT_colorramp = new THREE.RawShaderMaterial({
            uniforms: {
                "in_texture": { value: colorToDepth.raw_depth_texture },
                "minDepth": { value: 100 },
                "maxDepth": { value: 2000 }
            },
            vertexShader:   rawColorrampProcShader.vertexShader,
            fragmentShader: rawColorrampProcShader.fragmentShader
        });        
        colorToDepth.geometry_RT_colorramp = new THREE.PlaneBufferGeometry(COLOR_TO_DEPTH_WIDTH, COLOR_TO_DEPTH_HEIGHT);
        colorToDepth.mesh_RT_colorramp = new THREE.Mesh( colorToDepth.geometry_RT_colorramp, colorToDepth.material_RT_colorramp );
        colorToDepth.mesh_RT_colorramp.position.z = -100;
        colorToDepth.scene_RT_colorramp.add( colorToDepth.mesh_RT_colorramp );
        colorToDepth.depth_colorramp_texture = colorToDepth.glslProcRenderTarget_colorramp.texture;


        //===== normal ======//
        colorToDepth.glslProcRenderTarget_normal = new THREE.WebGLRenderTarget( DEPTH_WIDTH, DEPTH_HEIGHT, 
                            { 
                                minFilter: THREE.LinearFilter, 
                                magFilter: THREE.LinearFilter, 
                                format: THREE.RGBAFormat,
                                type: THREE.FloatType 
                            } );        
        colorToDepth.scene_RT_normal = new THREE.Scene();
        colorToDepth.material_RT_normal = new THREE.ShaderMaterial({
            uniforms: {
                "bumpMap": { value: colorToDepth.glslProcRenderTarget_depth.texture },
                //"bumpMap": { value: depth_texture },
                "bumpScale": { value: 100 },
                "uvScale": { value: 2 }
            },
            vertexShader:   rawNormalProcShader.vertexShader,
            fragmentShader: rawNormalProcShader.fragmentShader,
            precision: "mediump"
        });        
        colorToDepth.geometry_RT_normal = new THREE.PlaneBufferGeometry(DEPTH_WIDTH, DEPTH_HEIGHT);
        colorToDepth.mesh_RT_normal = new THREE.Mesh( colorToDepth.geometry_RT_normal, colorToDepth.material_RT_normal );
        colorToDepth.mesh_RT_normal.position.z = -100;
        colorToDepth.scene_RT_normal.add( colorToDepth.mesh_RT_normal );
        colorToDepth.normal_texture = colorToDepth.glslProcRenderTarget_normal.texture;

        depthProcRTMaterials.push( colorToDepth.material_RT_depth );
        depthProcRTMaterials.push( colorToDepth.material_RT_ir );
        depthProcRTMaterials.push( colorToDepth.material_RT_color );
        depthProcRTMaterials.push( colorToDepth.material_RT_colorramp );
        depthProcRTMaterials.push( colorToDepth.material_RT_normal );


        ////==================== default RTs =======================////


        var glslProcRenderTarget_depth = depthToColor.glslProcRenderTarget_depth;
        var glslProcRenderTarget_ir = depthToColor.glslProcRenderTarget_ir;
        var glslProcRenderTarget_color = depthToColor.glslProcRenderTarget_color;
        var glslProcRenderTarget_color_envMap = depthToColor.glslProcRenderTarget_color_envMap;
        var glslProcRenderTarget_colorramp = depthToColor.glslProcRenderTarget_colorramp;
        var glslProcRenderTarget_normal = depthToColor.glslProcRenderTarget_normal;

        

        var camera_RT = depthToColor.camera_RT;
        var scene_RT_depth = depthToColor.scene_RT_depth;
        var scene_RT_ir = depthToColor.scene_RT_ir;
        var scene_RT_color = depthToColor.scene_RT_color;
        var scene_RT_colorramp = depthToColor.scene_RT_colorramp;
        var scene_RT_normal = depthToColor.scene_RT_normal; //normalNeed

//==================================================================


        const resize = () => {

            SCREEN_WIDTH = window.innerWidth;
            SCREEN_HEIGHT = window.innerHeight;

            camera.aspect = SCREEN_WIDTH / SCREEN_HEIGHT;

            cameraOrtho.left = SCREEN_WIDTH / -2;
            cameraOrtho.right = SCREEN_WIDTH / 2;
            cameraOrtho.top = SCREEN_HEIGHT / 2;
            cameraOrtho.bottom = SCREEN_HEIGHT / -2;

            cameraDome.left = SCREEN_WIDTH / -2;
            cameraDome.right = SCREEN_WIDTH / 2;
            cameraDome.top = SCREEN_HEIGHT / 2;
            cameraDome.bottom = SCREEN_HEIGHT / -2;

            renderer.setSize(SCREEN_WIDTH, SCREEN_HEIGHT);
            if ( composer ) {

                composer.setSize(SCREEN_WIDTH, SCREEN_HEIGHT);
            }
            
            camera.updateProjectionMatrix();
      
            cameraOrtho.updateProjectionMatrix();
            cameraDome.updateProjectionMatrix();
           // console.log(SCREEN_WIDTH, SCREEN_HEIGHT);
        };
        window.addEventListener('resize', resize);



        

        
        let clock = new THREE.Clock();
        let deltaTime = 0;
        let intervalTime = 1 / maxFPS;
        
        const animate = (now) => {           




            time = Date.now() * 0.002;          
            myTime += 0.05;


            deltaTime += clock.getDelta();
          

            if (animateLights) animatePointLights();

            if (strobeLights) animatePontLightsStrobe();
            
            if (animateCamera) rotateCamera();

            if (autoRotateScene) {              

                // // autoRotateVector.y += autoRotateSpeed; 
                // // autoRotateVector2.y -= autoRotateSpeed; 
                // // scene.rotation.setFromVector3( autoRotateVector );
                // // sceneObjects.depthMesh.rotation.setFromVector3( autoRotateVector2 );

                // controls.update();

                // lightsObject.rotation.y -= controls.autoRotationAngle;
                
                // sceneObjects.depthMesh.rotation.y -= controls.autoRotationAngle;
                // sceneObjects.pointcloud.rotation.y -= controls.autoRotationAngle;
                // sceneObjects.instanced.rotation.y -= controls.autoRotationAngle;

                sceneObjects.backgroundSphere.rotation.y -= autoRotateSpeed * 0.1;

                sceneObjects.depthMesh.children[0].material.uniforms.envRotation.value = sceneObjects.backgroundSphere.rotation.y - Math.PI;;
                sceneObjects.depthMesh.children[1].material.uniforms.envRotation.value = sceneObjects.backgroundSphere.rotation.y - Math.PI;;

            }

            
            //for ( var objectKey in sceneObjects ) {

                // var object = sceneObjects[objectKey];

                //if ( object != null && object.material.uniforms !== undefined && object.material.uniforms.time !== undefined ) 
                sceneObjects.depthMesh.children[0].material.uniforms.time.value = myTime;
                sceneObjects.depthMesh.children[1].material.uniforms.time.value = myTime;
                dynamicsPostFX.vectorDisplacementMap.uniforms['time'].value = myTime; 
               
                // cloudStars.uniforms['time'].value = myTime; 

            //}

            if ( kinectIsListening )


            if (limitFPS){

                if (deltaTime  > intervalTime) {
                    render();
                    
                    deltaTime = deltaTime % intervalTime;
                    statsRenderer.update();
                } 

            } else {

                render();
                // render();
                // render();
                // render();
     
                // statsRenderer.update();
                statsRenderer.update();

            }

            
            requestID = requestAnimationFrame( animate );

        }

        function render() {

            
           

            renderer.clear();
               //FRUSTRUM
             //  scene.traverse(obj => obj.frustumCulled = false);

            if (!echoPostFX_DEPTH.doRender) {
                renderer.setRenderTarget( glslProcRenderTarget_depth );
                renderer.render( scene_RT_depth, camera_RT );
            }

            if (!echoPostFX_IR.doRender) {
                renderer.setRenderTarget( glslProcRenderTarget_ir );
                renderer.render( scene_RT_ir, camera_RT );
            }

            if (!echoPostFX_COLOR.doRender) {
                renderer.setRenderTarget( glslProcRenderTarget_color );
                renderer.render( scene_RT_color, camera_RT );
            }

            // capture RGB envMap (a method for invisible effect that mika tried)
            // if (doCaptureRGB) {

            //     renderer.setRenderTarget( glslProcRenderTarget_color_envMap );
            //     renderer.render( scene_RT_color, camera_RT );

            //     color_texture_envmap = glslProcRenderTarget_color_envMap.texture;

            //     doCaptureRGB = false;
            //    console.log("doCaptureRGB");

            // }

            // renderer.render( scene_RT_colorramp, camera_RT );
            if (colorRampActive){
                renderer.setRenderTarget( glslProcRenderTarget_colorramp );
                renderer.render( scene_RT_colorramp, camera_RT );
                
               // console.log('color ramp');
            }
            renderer.setRenderTarget( glslProcRenderTarget_normal );
            renderer.render( depthToColor.scene_RT_normal, camera_RT );  //TypeError: Failed to execute 'texImage2D' on 'WebGL2RenderingContext': No function was found that matched the signature provided.\n    
            
          
            renderer.setRenderTarget( null );


            if (echoPostFX_DEPTH.doRender) {
                renderer.setRenderTarget( echoPostFX_DEPTH.renderTarget );
                renderer.render( echoPostFX_DEPTH.scene, echoPostFX_DEPTH.camera );
                renderer.setRenderTarget( null );
            }

            if (echoPostFX_IR.doRender) {
                renderer.setRenderTarget( glslProcRenderTarget_ir );
                renderer.render( echoPostFX_IR.scene, echoPostFX_IR.camera );
                renderer.setRenderTarget( null );
            }

            if (echoPostFX_COLOR.doRender) {
                renderer.setRenderTarget( glslProcRenderTarget_color );
                renderer.render( echoPostFX_COLOR.scene, echoPostFX_COLOR.camera );
                renderer.setRenderTarget( null );
            }

            


            /// reaction-diffusion render passes
            if (preprocessDepthWithReactionDiffusion) {
          
                // render and swap buffers
                for (var i = 0; i < rdNumIterations; i++) {

                    if (!rdBufferToggled) {
                        rdMaterial.uniforms["tDiffuse1"].value = rdRenderTarget1.texture;
                        renderer.setRenderTarget( rdRenderTarget2 );
                        renderer.render(rdScene, rdCamera);
                        renderer.setRenderTarget( null );
                        backTexture = rdRenderTarget2.texture;
                    }
                    else {

                        rdMaterial.uniforms["tDiffuse1"].value = rdRenderTarget2.texture;
                        renderer.setRenderTarget( rdRenderTarget1 );
                        renderer.render(rdScene, rdCamera);
                        renderer.setRenderTarget( null );
                        backTexture = rdRenderTarget1.texture;
                    }

                    rdBufferToggled = !rdBufferToggled;

                }

                depthPostFX.texturePass.map = backTexture;

            }
            /// end of reaction-diffusion render passes    

            
            

            renderer.setRenderTarget(null);

            //IF DOME ACTIVE RENDER cubecamera
            
            
            composer.render(); // TypeError: Failed to execute 'texImage2D' on 'WebGL2RenderingContext': No function was found that matched the signature provided.
            depthPostFX.fxComposer.render(); 
            dynamicsPostFX.fxComposer.render(); //THREE.WebGLProgram: gl.getProgramInfoLog() C:\fakepath(133,11-73): warning X3571: pow(f, e) will not work for negative f, use abs(f) or conditionally handle negative values if you expect them

            
            if(params.camera.dome.value){
           //     renderer.clippingPlanes = cubeCamera.clippingPlanes;

                // Update the cube camera and render the scene to the cubemap
                cubeCamera.update(renderer, scene);

                // Remove clipping planes after rendering
              //  renderer.clippingPlanes = [];
                // cubeCamera.update(renderer, scene);
                quadMesh.material.uniforms.cubeTexture.value = cubeCamera.renderTarget.texture;
            }

            // if(displacementActive){
            //     composerDisplacement.render();
            // }

        }




    function handleMessage( data ) {

        msg = {};
        msg.data = data;

        switch (msg.data.type) {

            case "keystroke":
                onKeystroke(msg.data.key);
                break;
                
            case "updateOSCParams":
                oscParams = msg.data.message;
                console.log("updateOSCParams: ", oscParams);
                updateUdpPortOptions();
                break;

            case "allowSendOSC":
                skeleton_setAllowSenOSCHand(msg.data.message);
                break;

            case "updateMidiMap":
                //updateMidiMap(msg.data.message);
                break;
            case "setMidiInPortByPortId":
                setMidiInPortByPortId(msg.data.message);
                break;
            case "setMidiOutPortByPortId":
                setMidiOutPortByPortId(msg.data.message);
                break;
            case "setMidiInChannel":
                midiInChannel = Number(msg.data.message);
                break;
            case "setMidiOutChannel":
                midiOutChannel = Number(msg.data.message);
                break;
            case "setMidiInPortMuteState":
                // setMidiInPortMuteState(msg.data.message);
                break;

            case "setBTJointControlLeft":
                setBTJointControlLeft(msg.data.message);
                break;
            case "setBTJointControlRight":
                setBTJointControlRight(msg.data.message);
                break;

            case "setBTSpaceType":
                btSpace = msg.data.message;
                break;
            case "setTemporalSmoothing":
                kinectWorker.postMessage( [ "setTemporalSmoothing", Number(msg.data.message) ] );
                break;
            case "setJointConstraintBorder":
                jointMapBorderOffset.x = Number(msg.data.message.x);
                jointMapBorderOffset.Ytop = Number(msg.data.message.Ytop);
                jointMapBorderOffset.Ybottom = Number(msg.data.message.Ybottom);
                setJointMapConstraints();
             //   console.log("joint constraints set:", jointMapBorderOffset);
                break;
            case "setBTProcessingMode":
                kinectWorker.postMessage( [ "setBTProcessingMode", Number(msg.data.message) ] );
                break;
            case "setBTModelPath":
                kinectWorker.postMessage( [ "setBTModelPath", Number(msg.data.message) ] );
                break;


            case "loadAssets":

                loadAssets(msg.data.message);

                break;

            case "setAllParams":
                
                params = msg.data.message;

                parentview.postMessage( { type: "gotAllParams" } );

                //console.logconsole.log( "got params from control" )
                //console.log( params );

                break;

            case "setPlaybackMkvFilePath":
                kinectWorker.postMessage( [ "setPlaybackMkvFilePath", msg.data.message ] );
                break;

            case "setLogoFilePath":
                setLogoImage( msg.data.message );
                break;

            case "action":
                switch (msg.data.message) {

                    // case "setParamsAndAnimate":  // pass preset params object to this view

                    //     params = msg.data.message;
                    //     break;

                    case "makeScreenshot":

                        var canvas = document.getElementById("outputCanvas");

                        composer.render();

                        var dataURL = canvas.toDataURL();

                            dialog.showSaveDialog({
                                filters: [{
                                    name: 'image',
                                    extensions: ['png']
                                }] }
                                ).then(result => {

                                    //console.log(result.filePath);

                                    saveFilePath = result.filePath;

                                    var data = dataURL.replace(/^data:image\/\w+;base64,/, "");

                                    var buf = new Buffer.from(data, 'base64');

                                    fs.writeFile(saveFilePath, buf,
                                        (err) => {
                                            if (!err) {
                                                console.log("File saved at path:", saveFilePath);
                                                parentview.postMessage({ type: "screenshotSaved", message: { path: saveFilePath, name: saveFilePath } });
                                            }
                                            else
                                                console.error(err);
                                    })

                                    

                                }).catch(err => {
                                    console.error(err);
                                })

                        break;

                    case "captureRGBenvMap":

                        doCaptureRGB = true;
                        break;

                    case "init":

                        init();

                        //console.log( "got init message from control" )

                        break;  

                    case "startAnimation":
                        startAnimation();
                        break;

                    case "startKinect":
                        kinectWorker.postMessage( "start kinect" );    // send start kinect message
                        break;

                    case "reinitKinect":
                        kinectWorker.postMessage( "restart kinect" );    // send re-init kinect message
                        break;

                    case "startPlayback":
                        kinectWorker.postMessage( "start playback" );
                        break;

                    case "stopPlayback":
                        kinectWorker.postMessage( "stop playback" );
                        break;

                    case "pauseKinect":
                        kinectWorker.postMessage( "pause kinect" );
                        break;
                    
                    case "resumeKinect":
                        kinectWorker.postMessage( "resume kinect" );
                        break;
                  

                    case "reset":
                        reset();
                        break;

                    case "resetOrbitControls":
                        resetOrbitControls(msg.data.camY,msg.data.camDistance,msg.data.camFOV);
                        break;

                    case "resetSceneAutoRotation":

                        resetSceneAutoRotation();
                        break;

                    case "resetReactionDiffusion":
                        echoPostFX_DEPTH.cleanTextureArray();
                        resetReactionDiffusion(depthPostFX.width,depthPostFX.height);
                        // console.log(,, depthPostFX);
                        break;
                    // case "resetRDparams":
                    //     resetRDparams();
                    //     break;
                    case "showCalibrationGrid":
                        showCalibrationGrid();
                        break;
                    case "hideCalibrationGrid":
                        hideCalibrationGrid();
                        break;
                    case "showBlackout":
                        showBlackout();
                        break;
                    case "hideBlackout":
                        hideBlackout();
                        break;
                    case "toggleHandMarkersVisibility":
                        toggleHandMarkersVisibility();
                        break;
                    case "setMarkersScale_L":
                        setMarkersScale_L();
                        break;
                    case "setMarkersScale_R":
                        setMarkersScale_R();
                        break;
                    case "setMarkersScale_U":
                        setMarkersScale_U();
                        break;
                    case "setMarkersScale_D":
                        setMarkersScale_D();
                        break;
                    case "setMarkersOffset_Plus":
                        setMarkersOffset_Plus();
                        break;
                    case "setMarkersOffset_Minus":
                        setMarkersOffset_Minus();
                        break;
                }
                break;
            

            case "paramChange":

                setParameter(msg.data.message.param, msg.data.message.paramKey, msg.data.message.needUpdateParams);

                break;


            case "programChange__Prepare":

                currentProgram = Number( msg.data.message );
                break;

            case "getOrbitControlsState":

                var orbitControlsState = controls.getState();
                parentview.postMessage({ type: "OrbitControlsState", message: JSON.stringify( orbitControlsState ) });
                
                break;

            case "setOrbitControlsState":

                setOrbitControlsState( msg.data.message );
                break;

            case "programChange":

                switchProgram(msg.data.message);
                break;

            case "loadNewTexture":

            console.log('loadNewTexture');

                loadTextureFromFile( msg.data.message.param );

                var param =  msg.data.message.param;

                setMaterialEnvMapProperty( param.object, param.userLoadedEnvMapNameList[ param.userLoadedEnvMapNameList.length - 1 ].name );

                break;

            // postprocessing param toggle
            case "postprocessingParamToggle":

                postprocessingParamToggle(msg.data.message);
                break;
            // postprocessing parameter change
            case "postprocessingParamChange":

                postprocessingParamChange(msg.data.message);
                break;

            // skeleton hands mapping settings sent from control view
            case "setSoloModeHandController":
                skeleton_setSoloModeHandController(msg.data.message);
                break;

            case "setHandController":
                skeleton_setHandController(msg.data.message);
                break;

            case "setAllowSendMidiHand":
                skeleton_setAllowSendMidiHand(msg.data.message);
                break;

            case "enableSendHandCoordinates":
                skeleton_enableSendHandCoordinates(msg.data.message);
                break;

        }

    }

    function reset() {

        // restore default camera position and object rotation for presets 
        for (var i = 0; i < object.length; i++) {

            object[i].rotation.y = 0;

            if (object[i].homeCameraPosition !== undefined) {
                object[i].homeCameraPosition.copy(homeCameraPosition);
            }
        }
    }




    function setParameter(param, paramKey, needUpdateParams) {


        var objectSelector = param.object;

        var object = sceneObjects[ objectSelector ];

        //console.log( "object:", objectSelector );

        var parentBlockId = needUpdateParams ? needUpdateParams.parentBlockId : undefined;
        //console.log( "parentBlockId:", parentBlockId );


        if (param.object !== undefined) {

            if ( param.object === "depthPostprocessing" ) {

                if(params.depthPostprocessing[ paramKey ] == undefined) return;

                params.depthPostprocessing[ paramKey ].value = param.value;
                depthPostFX.setDepthPostprocessingParameter( param, paramKey );
                
                return;
            }
            
            if ( param.object === "depthFX" ) {
                
                if(params.depthFX[ paramKey ] == undefined) return;
                //console.log(paramKey, params.depthFX[ paramKey ].value);
                params.depthFX[ paramKey ].value = param.value;
               // console.log(param.name, param.value );
                depthPostFX.setDepthPostprocessingParameter( param, paramKey );
               
                return;
            }

            if ( param.object === "lights" ) {
                setLightsParameter( param, paramKey );

                // setTimeout( function() {

                //     setLightsParameter( param, paramKey );
    
                // }, 100);
                return;
            }

            if ( param.object === "camera" ) {
                setCameraParameter( param, paramKey );
                return;
            }

            if ( param.object === "backgroundSphere" ) {

                switch (paramKey) {

                    case "scale":
                        sceneObjects.backgroundSphere.scale.set( param.value, param.value, param.value );
                        break;
                    case "rotationY":
                        sceneObjects.backgroundSphere.rotation.y = - Math.PI + param.value;
                        sceneObjects.backgroundSphere.userData.rotationYorigin = - Math.PI + param.value;

                        sceneObjects.depthMesh.children[0].material.uniforms.envRotation.value = sceneObjects.backgroundSphere.rotation.y - Math.PI;
                        sceneObjects.depthMesh.children[1].material.uniforms.envRotation.value = sceneObjects.backgroundSphere.rotation.y - Math.PI;
                        //console.log("sphere rotation y", sceneObjects.backgroundSphere.rotation.y);
                        break;
                    case "spherePositionY":
                        sceneObjects.backgroundSphere.position.y = param.value;
                        //console.log("sphere position y", sceneObjects.backgroundSphere.position.y);
                        break;
                    case "domeDegrees":
                        domeMaterial.uniforms.domeDegrees.value = param.value;
                       // console.log("sphere position y", sceneObjects.backgroundSphere.position.y);
                        break;
                    case "sphereOpacity":
                        sceneObjects.backgroundSphere.material.opacity = param.value;
                        //console.log("sphere opacity ", sceneObjects.backgroundSphere.material.opacity);
                        break;   
                    case "rotationX":
                        sceneObjects.backgroundSphere.rotation.x = param.value;
                        break;
                    case "autoRotation":

                        // if (param.value == true) controls.saveState();

                        // reset cam position only if auto-rotation state changed
                        if ( autoRotateScene === true && param.value === false) {

                            resetSceneAutoRotation();  // renderer-camera.js

                        }

                        if ( autoRotateScene === false && param.value === true) {

                            resetSceneAutoRotation();  // renderer-camera.js

                        }

                        autoRotateScene = param.value;

                        controls.autoRotate = param.value;

                        

                        break;
                    case "autoRotationSpeed":

                        // autoRotateVector.y = param.value * 0.1;
                        autoRotateSpeed = param.value * 0.1;

                        controls.autoRotateSpeed = param.value;
                        controls.refreshAutoRotationAngle();
                        break;
                    case "opacity":

                        sceneObjects.backgroundSphere.material.opacity = param.value;
                        break;
                    case "bgSpherePlay":
                        console.log("bgSpherePlay", param.value);
                            //i need to check if the video is already playing and if it's a video texture vs image texture
                        if (sceneObjects.backgroundSphere.material.map === null) return;
                        if (sceneObjects.backgroundSphere.material.map.image.type === "video"){
                            if (param.value){   
                                if (sceneObjects.backgroundSphere.material.map.image.readyState === 4) { // HAVE_ENOUGH_DATA
                                    if (sceneObjects.backgroundSphere.material.map.image.paused){
                                        
                                        sceneObjects.backgroundSphere.material.map.image.play();
                                    }
                                }
                            } else {
                                sceneObjects.backgroundSphere.material.map.image.currentTime = 0;
                                sceneObjects.backgroundSphere.material.map.image.pause();
                            }
                        }
                        
    
                    break;
                }

            }

            if ( param.object === "handGeometry" ) {

                switch (paramKey) {

                    case "size":
                        markerLeft.scale.set( param.value, param.value, param.value );
                        markerRight.scale.set( param.value, param.value, param.value );
                        break;
                    case "positionZ":
                        jointMapConstraints_3D.offsetVector.z = param.value;
                        jointMapConstraints_2D["2"].offsetVector.z = param.value;
                        jointMapConstraints_2D["3"].offsetVector.z = param.value;
                        break;
                    case "positionX":
                        jointMapConstraints_3D.offsetVector.x = param.value;
                        jointMapConstraints_2D["2"].offsetVector.x = param.value;
                        jointMapConstraints_2D["3"].offsetVector.x = param.value;
                    break;
                    case "positionY":
                        jointMapConstraints_3D.offsetVector.y = param.value;
                        jointMapConstraints_2D["2"].offsetVector.y = param.value;
                        jointMapConstraints_2D["3"].offsetVector.y = param.value;
                    break;
                    case "enableBT":

                        //if ( kinectIsListening === false ) break;

                        if (param.value) {
                            kinectWorker.postMessage( [ "startBodyTracking", param.value ] );
                        } else {
                            kinectWorker.postMessage( [ "stopBodyTracking", param.value ] );
                        }
                        
                        break;
                }

            }

            // if ( param.object === "pointcloud" ) {

            //     switch (paramKey) {

            //         // case "autorotate":
            //         //     setPointcloudAutorotation(param.value);
            //         //     break;

            //         case "positionZ":
            //             setPointcloudPositionZ(param.value);
            //             break;

            //     }

            // }
            
        }

        //if ( parentBlockId === "lights" ) {
            
            //return;
        //}


        switch (paramKey) {

            // case "showNormalMap":

            //     if (activeSceneObject.material.uniforms && activeSceneObject.material.uniforms.showNormalMap) {

            //         activeSceneObject.material.uniforms.showNormalMap.value = param.value;
            //     }
            //     break;

            case "meshRotationX":

              //sceneObjects.depthMesh.children[0].rotation.x = param.value;
                //    console.log(sceneObjects.depthMesh.children[0].rotation.x,sceneObjects.depthMesh.children[0].rotation.x,sceneObjects.depthMesh.children[0].rotation.z,)
             //  console.log(sceneObjects.depthMesh.children[0].position.x,sceneObjects.depthMesh.children[0].position.y,sceneObjects.depthMesh.children[0].position.z);
               setObjectRotationX( param );
              break;

            case "positionZ":
                //sceneObjects.depthMesh.children[0].position.z = param.value;
               // console.log(sceneObjects.depthMesh.children[0].position.x,sceneObjects.depthMesh.children[0].position.y,sceneObjects.depthMesh.children[0].position.z);
                setObjectPositionZ( param );
                break;

            case "flipX":
                flipProjectionX(param.value);
                break;
            case "midiActive":
                // flipProjectionX(param.value);
                if (param.value){
                    getMidiAccess( function(){} );
                }
                break;

            case "visibility":

                object.visible = param.value;

                break;

            case "midiInMessageLimit":
                midiBro.setMidiInMessageLimit(param.value);
                break;

            case "colorStart":

                depthToColor.material_RT_colorramp.uniforms[ "colorStart" ].value = Number( param.value );
                break;
            
            case "colorEnd":

                depthToColor.material_RT_colorramp.uniforms[ "colorEnd" ].value = Number( param.value );
                break;
            
            case "oldMaterial":


                if (param.value){
                 
                    //  depthPostFX.texturePass.map = depth_textureOG;
                    //edgePostFX.texturePass.map = depth_textureOG;
                    // sceneObjects.depthMesh.children[0].material = smoothLightsMaterial2;
                    // sceneObjects.depthMesh.children[0].material.needsUpdate = true;
                    // sceneObjects.depthMesh.children[0].material.displacementMap = depthPostFX.kinectTexture;
                   
                    // sceneObjects.depthMesh.children[0].material.uniforms['displacementScale'].value = 512;
                    // sceneObjects.depthMesh.children[0].material.uniforms['showNormalMap'].value = true;
                    
                }else{
                     // depth_texture = depthPostFX.kinectTexture;


                    // sceneObjects.depthMesh.children[0].material = smoothLightsMaterial;
                    // sceneObjects.depthMesh.children[0].material.needsUpdate = true;


                    //depthPostFX.texturePass.map = glslProcRenderTarget_depth.texture;
                }   
          
                console.log(sceneObjects);
                // object.children[0].material.needsUpdate = true;
            break;

            case "depthLimit":
                params.depth.depthLimit.value = param.value;

                params.depth.minDepth.value = Math.max( params.depth.depthLimit.value - params.depth.halfRange.value, 100 );

                setDepthProcRTMaterialsUniform( "minDepth", params.depth.minDepth );

                params.depth.maxDepth.value = params.depth.depthLimit.value + params.depth.halfRange.value;
                setDepthProcRTMaterialsUniform( "maxDepth", params.depth.maxDepth );

                

                break;
            case "meshScale":
       
                sceneObjects.depthMesh.scale.set(param.value, param.value,param.value);

                break;
            case "halfRange":
                params.depth.halfRange.value = param.value;

                params.depth.minDepth.value = Math.max( params.depth.depthLimit.value - params.depth.halfRange.value, 100 );

                setDepthProcRTMaterialsUniform( "minDepth", params.depth.minDepth );

                params.depth.maxDepth.value = params.depth.depthLimit.value + params.depth.halfRange.value;
                setDepthProcRTMaterialsUniform( "maxDepth", params.depth.maxDepth );
                
                break;

            case "minDepth":
                params.depth.minDepth.value = param.value;

                setDepthProcRTMaterialsUniform( paramKey, param );

                break;

            case "maxDepth":
                params.depth.maxDepth.value = param.value;

                setDepthProcRTMaterialsUniform( paramKey, param );

                break;

            case "minIR":                
                params.depth.minIR.value = param.value;

                setDepthProcRTMaterialsUniform( paramKey, param );

                echoPostFX_IR.material.uniforms.minIR.value = param.value;

                break;

            case "maxIR":
                params.depth.maxIR.value = param.value;

                setDepthProcRTMaterialsUniform( paramKey, param );

                echoPostFX_IR.material.uniforms.maxIR.value = param.value;

                break;

            case "depthMode":

                // if ( kinectIsListening === false ) break;

                if ( params.depth.depthMode.value === param.value ) break;

                // if ( param.value === "2" ) {
                //     transformationMode = transformationType.colorToDepth;
                // } else {
                //     transformationMode = transformationType.depthToColor;
                // }

                // swapTransformationModeMeshes();

                kinectWorker.postMessage( [ "set depth mode", param.value ] );

                //if ( kinectState === "camera" ) kinectWorker.postMessage( "restart kinect" );

                if ( kinectState === "playback" ) kinectWorker.postMessage( "restart playback" );

                params.depth.depthMode.value = param.value;

                switch (param.value) {

                    case "0":
                        depthMode = depthType.WFOV;
                        // fpsMode = fpsType.WFOV;
                        transformationMode = transformationType.depthToColor;
                        break;
                    case "1":
                        depthMode = depthType.NFOV;
                        // fpsMode = fpsType.NFOV;
                        transformationMode = transformationType.depthToColor;
                        break;
                    case "2":
                        depthMode = depthType.NFOV;
                        // fpsMode = fpsType.NFOV;
                        transformationMode = transformationType.colorToDepth;
                        break;
                    // case "3":
                    //     depthMode = depthType.WFOV;
                    //     // fpsMode = fpsType.WFOV;
                    //     transformationMode = transformationType.colorToDepth;
                    //     break;
                    case "3":
                        depthMode = depthType.WFOV_UNBINNED;
                        fpsMode = fpsType.WFOV_UNBINNED;
                        transformationMode = transformationType.depthToColor;
                        break;

                }

                break;

            case "bumpScale":

                setDepthProcRTMaterialsUniform( paramKey, param );

                sceneObjects[ param.object ].children[0].material.bumpScale = param.value;
                sceneObjects[ param.object ].children[1].material.bumpScale = param.value;

                sceneObjects[ param.object ].children[0].material.needsUpdate = true;
                sceneObjects[ param.object ].children[1].material.needsUpdate = true;

                
                break;
            
            case "uvScale":

                setDepthProcRTMaterialsUniform( paramKey, param );
            
                break;

            case "depthProc_gradientEnable":

                setDepthProcRTMaterialsUniform( paramKey, param );
            
                break;

            case "depthProc_uvScale":

                setDepthProcRTMaterialsUniform( paramKey, param );
            
                break;

            case "depthProc_gradThreshold":

                setDepthProcRTMaterialsUniform( paramKey, param );
            
                break;

            
            case "spheretest":
                params.common.spheretest.value = param.value;
                sphere2.visible = param.value;
                break;

            case "geometryDensity":
                setPointcloudGeometryDensity( param.value );
                break;


            case "ballRadius":
                //primitive.radius = param.value;

                // setInstancedOffsetsAttribute( object, param.value);

                // if (object.children[0].material.uniforms !== undefined) {

                //     if (object.children[0].material.uniforms[paramKey]) {
                //         object.children[0].material.uniforms[paramKey].value = param.value;
                //         object.children[1].material.uniforms[paramKey].value = param.value;

                //     }

                // }                          

                if ( object.isMesh ) {

                    if (object.material.uniforms !== undefined) {

                        if (object.material.uniforms[paramKey]) {
                            object.material.uniforms[paramKey].value = param.value;                                                    }
                    }

                } else {

                    for ( var i = 0; i < object.children.length; i++ ) {

                        setInstancedOffsetsAttribute( object.children[i], param.value);

                        if (object.children[i].material.uniforms !== undefined) {

                            if (object.children[i].material.uniforms[paramKey]) {
                                object.children[i].material.uniforms[paramKey].value = param.value;
                            }

                        }         
                    }
                }


                break;


            case "panoramaTexture":

                //setPanoramaTexture(Math.round(param.value) - 1);

                break;



            /////==== MATERIAL ================/////////

            case "doubleSide":
               // console.log("doubleSide", param.value);
                var material = sceneObjects[ param.object ].children[1].material;
                var material2 = sceneObjects[ param.object ].children[0].material;
                
                // var material = sceneObjects[ param.object ].children[1].material;

                if (param.value){
                //    console.log("setting DoubleSided");
                    // materialSide = 2;
                    // materialSide2 = 2;
                    material.side = THREE.DoubleSide;
                    material2.side = THREE.DoubleSide;
                    // params.program.objects[ param.object ].material.side.value =1;
                    
                }else{
                    //console.log("setting FrontSide");
                    // materialSide = 0;
                    // materialSide2 = 0;
                    material.side = THREE.FrontSide;
                    material2.side = THREE.FrontSide;
                    // params.program.objects[ param.object ].material.side.value =0;
                };
                // console.log( {materialSide});
                // console.log( {materialSide2});

                material.needsUpdate = true;
                material2.needsUpdate = true;

                break;

            case "refraction":

                setMaterialRefractionMode( param );
                break;

            case "flatEnvMap":

                setMaterialFlatEnvMapMode( param );
                break;

            case "cutBackground":

                params.program.objects[ param.object ].material.cutBackground.value = param.value;
                var material = sceneObjects[ param.object ].children[0].material;
                if ( material.hasOwnProperty("uniforms") ) {
                    material.defines.CUT_BACKGROUND = param.value;
                }
                material.needsUpdate = true;
                material = sceneObjects[ param.object ].children[1].material;
                if ( material.hasOwnProperty("uniforms") ) {
                    material.defines.CUT_BACKGROUND = param.value;
                }
                material.needsUpdate = true;

                break;

            case "map":

                setMaterialMapProperty( param );
                break;

            case "envMap":

                setMaterialEnvMapProperty( param.object, param.optionsList[ Number( param.value ) ] );
                break;

            case "diffuse":

                setMaterialDiffuseProperty( param );
                break;
            case "diffuseExposure":
                var material = sceneObjects[ param.object ].children[1].material;
                var material2 = sceneObjects[ param.object ].children[0].material;
                
                if ( material.hasOwnProperty("uniforms") ) {
                    material.uniforms.diffuseExposure.value = param.value;
                }

                if ( material2.hasOwnProperty("uniforms") ) {
                    material2.uniforms.diffuseExposure.value = param.value;
                }


                material.needsUpdate = true;
                material2.needsUpdate = true;
                
                break;
            case "colorOffset":
                var material = sceneObjects[ param.object ].children[1].material;
                var material2 = sceneObjects[ param.object ].children[0].material;
                
                if ( material.hasOwnProperty("uniforms") ) {
                    material.uniforms.colorOffset.value = param.value;
                }

                if ( material2.hasOwnProperty("uniforms") ) {
                    material2.uniforms.colorOffset.value = param.value;
                }


                material.needsUpdate = true;
                material2.needsUpdate = true;
                
                break;

            case "normalMap":

                setMaterialNormalMapProperty( param, paramKey );
                break;
            
            case "normalScale":

                setMaterialNormalMapProperty( param, paramKey );
                break;
            
            case "bumpMap":

                setMaterialBumpMapProperty( param, paramKey );
                break;
            
            case "bumpScale":

                setMaterialBumpMapProperty( param, paramKey );
                break;

            case "showHandMarkers":
                //setHandMarkersVisibility(param.value);
                break;

            case "rotX":
                object.geometry.applyMatrix(new THREE.Matrix4().makeRotationX(param.value));
                break;
            case "rotY":
                object.geometry.applyMatrix(new THREE.Matrix4().makeRotationY(param.value));                
                break;
            case "rotZ":
                object.geometry.applyMatrix(new THREE.Matrix4().makeRotationZ(param.value));                
                break;

            case "lightness":
                setScreenMaterialLightness(param.value);
                break;

            case "strobe_frequency":
                params.strobe.strobe_frequency.value = param.value;
                break;
            case "strobe_hold":
                params.strobe.strobe_hold.value = param.value;
                break;

            case "strobe_toggle":

                if (param.value === true) {
                    startStrobe();
                } else {
                    stopStrobe();
                } 

                setTimeout( function() {

                    if (param.value === true) {
                        startStrobe();
                    } else {
                        stopStrobe();
                    } 
    
                }, 100);
                break;

            case "flipLogoX":
                flipLogoX(param.value);
                break;

            case "clippingPlaneConstant":
                clippingPlane.constant = param.value;
                break;
            

            default:

                if (object == undefined) break;

                if ( object.isMesh ) {


                    if (object.material == undefined) break;

                    if (object.material[paramKey] !== undefined) {

                            object.material[paramKey] = param.value;                           
                    } 

                    if (object.material.uniforms !== undefined) {

                        if (object.material.uniforms[paramKey]) {
                            object.material.uniforms[paramKey].value = param.value;                                                    }
                    }


                } else {

                    for ( var i = 0; i < object.children.length; i++ ) {

                        if (object.children[i].material == undefined) break;

                        if (object.children[i].material[paramKey] !== undefined)
                                object.children[i].material[paramKey] = param.value;                        

                        if (object.children[i].material.uniforms !== undefined) {

                            if (object.children[i].material.uniforms[paramKey])
                                object.children[i].material.uniforms[paramKey].value = param.value;                            
                        }

                    }



                }
                

        }



        if (paramKey.indexOf('screen') !== -1) {

            if (object.isUsingReactionDiffusion) {
                if (object.material.uniforms !== undefined && object.material.uniforms[paramKey.substring(7)] !== undefined) {
                    object.material.uniforms[paramKey.substring(7)].value = param.value;
                }
            }
                
        }

    }


//new 

function setMaterialMapProperty( param ) {

    var object = sceneObjects[ param.object ];

    if ( object.isMesh || object.isPoints ) {

        defineMaterialMapPerMesh( object );

    } else {
    
        defineMaterialMapPerMesh( object.children[0], "depthToColor" );
        defineMaterialMapPerMesh( object.children[1], "colorToDepth" );     

    }
    
    function defineMaterialMapPerMesh( object, transMode ) {

        var glslProcRenderTarget_ir, glslProcRenderTarget_color, glslProcRenderTarget_colorramp,glslProcRenderTarget_normal;

        if (transMode === "depthToColor") {

            glslProcRenderTarget_ir = depthToColor.glslProcRenderTarget_ir;
            glslProcRenderTarget_color = depthToColor.glslProcRenderTarget_color;
            glslProcRenderTarget_colorramp = depthToColor.glslProcRenderTarget_colorramp;
            glslProcRenderTarget_normal = depthToColor.glslProcRenderTarget_normal;

        } else {

            glslProcRenderTarget_ir = colorToDepth.glslProcRenderTarget_ir;
            glslProcRenderTarget_color = colorToDepth.glslProcRenderTarget_color;
            glslProcRenderTarget_colorramp = colorToDepth.glslProcRenderTarget_colorramp;
            glslProcRenderTarget_normal = colorToDepth.glslProcRenderTarget_normal;

        }      

        if (param.value == 3){
            // console.log("colorRamp bittchh");
            colorRampActive = true;
        } else {
            colorRampActive = false;
        }
        
        sceneFlowTextureActive = (Number( param.value) == 5);

        depthPostFX.setSceneFlow(sceneFlowTextureActive||liquidActive);

        if ( param.object === "depthMesh" || param.object === "pointcloud" ) {
            var texture;
            var useSingleChannelColorMap = false;
            
            switch ( Number( param.value ) ) {

                case 0: texture = glslProcRenderTarget_ir.texture;
                    break;
                case 1: 
                    texture = depthPostFX.kinectTexture;
                    //texture = depth_textureOG;
                    useSingleChannelColorMap = false;
                    break;
                case 2: 
                    // texture = color_texture;
                    texture = glslProcRenderTarget_color.texture;
                    break;
                case 3:                 
                    texture = glslProcRenderTarget_colorramp.texture;                  
                    break;
                case 4:
                    // texture = normal_texture;
                    texture = glslProcRenderTarget_normal.texture;
                   // texture = dynamicsPostFX.precomputedNormalsRenderTarget.texture;
                    // texture = dynamicsPostFX.precomputedNormalsRenderTarget.texture;
                    // texture = depthPostFX.edgeRenderTarget.texture;
                    break;
                case 5: 
                    //texture = dynamicsPostFX.sceneFlowRenderTarget.texture;
                    texture = dynamicsPostFX.flowRenderTarget;
                    break;
                case 6: 
                    texture = dynamicsPostFX.renderTarget.texture;
                    break;
                case 7: 
                    texture = undefined;
                    break;

            }

            object.material.uniforms['map'].value = texture;
            object.material.uniforms['useSingleChannelColorMap'].value = useSingleChannelColorMap;
            object.material.map = texture;
            object.material.needsUpdate = true;


        }
        else 
        if ( param.object === "backgroundSphere" ) {

            // sceneObjects.backgroundSphere.material.needsUpdate = true;

            // currentPanoramaTexture = panoramaTextures[ Number(param.value) ];

        } 
        else 
        if ( param.object === "backgroundPlane" ) {

            var useSingleChannelColorMap = false;
            
            switch ( Number( param.value ) ) {

                case 0: texture = glslProcRenderTarget_ir.texture;
                    break;
                case 1: 
                    texture = depth_texture;     
                    //texture = depth_textureOG;      
                    useSingleChannelColorMap = false;
                    texture.name = "depth";
                    break;
                case 2: texture = glslProcRenderTarget_color.texture;
                    break;
                case 3: 
                    texture = glslProcRenderTarget_colorramp.texture;
                    break;
                case 4: 
                  //  texture = normal_texture;
                    texture = glslProcRenderTarget_normal.texture;
                   // texture = precomputedNormalsRenderTarget.texture;
                    //texture =depthPostFX.edgeRenderTarget.texture;
                  //  texture = dynamicsPostFX.precomputedNormalsRenderTarget.texture;
                    break;
                case 5: 
                   // texture = dynamicsPostFX.flowRenderTarget;
                    
                    // texture = dynamicsPostFX.sceneFlowRenderTarget.texture;
                    texture = dynamicsPostFX.flowRenderTarget;
                    break;
                case 6: 
                    texture = dynamicsPostFX.renderTarget.texture;
                    break;
                case 7: 
                    texture = undefined;
                    break;


            }
            
            var customShader = object.material.userData.shader;
            if ( customShader ) { 
            
               customShader.uniforms['useSingleChannelColorMap'].value = useSingleChannelColorMap;

               customShader.uniforms['map'].value = texture;

            }
            object.material.userData.useSingleChannelColorMap = useSingleChannelColorMap;

            object.material.map = texture;

            object.material.userData.colorMap = texture;  // fix for null envMap

   

        }

    }
    
    

}


function setMaterialEnvMapProperty( objectID, textureID ) {

    var object = sceneObjects[ objectID ];

    var envMapTexture = envMapTexturesListObj[ textureID ];

    if (object.material != undefined){
        if (object.material.map instanceof THREE.VideoTexture) {
            object.material.map.image.pause();
        }
    }
    if ( objectID === "backgroundSphere" ) {

        if ( envMapTexture !== undefined ) {

            object.material.map = envMapTexture;
            object.material.needsUpdate = true;

            if (topSphereActive){
                sceneObjects.topSphere.material.envMap = envMapTexture;
                sceneObjects.topSphere.material.needsUpdate = true;
            }

        }

    } else if ( objectID === "backgroundPlane" ) {

        if ( envMapTexture !== undefined ) {

            var useSingleChannelColorMap = false;

            if (envMapTexture === null) {    // if "envMap" dropdown set to Null set the "color map" texture
 
                envMapTexture = object.children[0].material.userData.colorMap;

                if ( envMapTexture !== null && envMapTexture.name === "depth" ) useSingleChannelColorMap = true;
            }


            for ( var i = 0; i < object.children.length; i++ ) {

                var customShader = object.children[i].material.userData.shader;
                if ( customShader ) { 

                    customShader.uniforms['useSingleChannelColorMap'].value = useSingleChannelColorMap;

                    customShader.uniforms['map'].value = envMapTexture;

                }
                object.children[i].material.userData.useSingleChannelColorMap = useSingleChannelColorMap;
                
                object.children[i].material.map = envMapTexture;

                // console.log("new envMap", object.children[i].material.map);
                
            }
        }

    } else {

        if ( envMapTexture != undefined ) {
            if ( params.program.objects[ objectID ].material.refraction.value ) {
                envMapTexture.mapping = THREE.EquirectangularRefractionMapping;
            } else {
                envMapTexture.mapping = THREE.EquirectangularReflectionMapping;
            }
        }

        if ( object.isMesh ) {

            if ( object.hasOwnProperty("material") ) {

                if ( object.material.hasOwnProperty("uniforms") ) {

                    object.material.uniforms['envMap'].value = envMapTexture;
                    object.material.envMap = envMapTexture;
                    object.material.needsUpdate = true;

                } else {

                    object.material.envMap = envMapTexture;
                    object.material.needsUpdate = true;

                }
            }

        } else {

            for ( var i = 0; i < object.children.length; i++ ) {

                if ( object.children[i].hasOwnProperty("material") ) {

                    if ( object.children[i].material.hasOwnProperty("uniforms") ) {

                        object.children[i].material.uniforms['envMap'].value = envMapTexture;
                        object.children[i].material.envMap = envMapTexture;
                        object.children[i].material.needsUpdate = true;

                    } else {

                        object.children[i].material.envMap = envMapTexture;
                        object.children[i].material.needsUpdate = true;

                    }

                }

            }
            
        }        

    }    

}


function setMaterialNormalMapProperty( param, paramKey ) {

    var object = sceneObjects[ param.object ];
 
    // if ( param.object === "depthMesh" ) {

            if ( paramKey === "normalMap" ) {
            
                if ( param.value ) {

                    // object.children[0].material.uniforms['normalMap'].value = depthToColor.glslProcRenderTarget_normal.texture;
                    // object.children[0].material.normalMap = depthToColor.glslProcRenderTarget_normal.texture;

                    // object.children[1].material.uniforms['normalMap'].value = colorToDepth.glslProcRenderTarget_normal.texture;
                    // object.children[1].material.normalMap = colorToDepth.glslProcRenderTarget_normal.texture;


                    object.children[0].material.uniforms['normalMap'].value = dynamicsPostFX.precomputedNormalsRenderTarget.texture;
                    object.children[0].material.normalMap = dynamicsPostFX.precomputedNormalsRenderTarget.texture;

                    object.children[1].material.uniforms['normalMap'].value = dynamicsPostFX.precomputedNormalsRenderTarget.texture;
                    object.children[1].material.normalMap = dynamicsPostFX.precomputedNormalsRenderTarget.texture;

                } else {

                    object.children[0].material.uniforms['normalMap'].value = null;
                    object.children[1].material.uniforms['normalMap'].value = null;

                    object.children[0].material.normalMap = null;
                    object.children[1].material.normalMap = null;

                }

                object.children[0].material.needsUpdate = true;
                object.children[1].material.needsUpdate = true;

            }
            else 
            if ( paramKey === "normalScale" ) {

                object.children[0].material.uniforms['normalScale'].value = new THREE.Vector2( Number( param.value ), Number( param.value ) );
                object.children[1].material.uniforms['normalScale'].value = new THREE.Vector2( Number( param.value ), Number( param.value ) );            
            }

    // }

}

function setMaterialBumpMapProperty( param, paramKey ) {

    var object = sceneObjects[ param.object ];
 
    if ( param.object === "depthMesh" ) {

        for ( var i = 0; i < object.children.length; i++ ) {

            if ( paramKey === "bumpMap" ) {
        
                if ( param.value ) {

                    object.children[i].material.uniforms['bumpMap'].value = depthPostFX.kinectTexture;
                    object.children[i].material.bumpMap = true;
                    

                } else {

                    object.children[i].material.uniforms['bumpMap'].value = null;
                    object.children[i].material.bumpMap = false;

                }

                object.children[i].material.needsUpdate = true;

            }
            else 
            if ( paramKey === "bumpScale" ) {

                object.children[i].material.uniforms['bumpScale'].value = Number( param.value );
            
            }

        }     

        
    } 
}


function flipProjectionX( value ) {

    if (value) {
        container.style["transform"] = "scaleX(-1)";
        container.style["-ms-filter"] = "FlipH";
    } else {
        container.style["transform"] = "scaleX(1)";
        container.style["-ms-filter"] = "FlipH";
    }
}

function flipLogoX(value) {

        effectStrobe.uniforms["flipLogoX"].value = value;
    
}


function setObjectPositionZ( param ) {

    switch (param.object) {

        case "depthMesh":
            sceneObjects.depthMesh.position.z = Number( param.value );
           // sceneObjects.depthMesh.children[0].position.z = param.value;
            break;
        case "pointcloud":
            sceneObjects.pointcloud.position.z = Number( param.value );
            break;
        case "instanced":
            sceneObjects.instanced.position.z = Number( param.value );
            break;

    }

}

function setObjectRotationX( param ) {

    switch (param.object) {

        case "depthMesh":
            //sceneObjects.depthMesh.children[0].rotation.x = param.value;
            sceneObjects.depthMesh.rotation.x = Number( param.value );
            break;
        case "pointcloud":
            sceneObjects.pointcloud.rotation.x = Number( param.value );
            break;
        case "instanced":
            sceneObjects.instanced.rotation.x = Number( param.value );
            break;

    }

}

function setScreenMaterialLightness(newValue) {

    if (activeSceneObject.material.uniforms && activeSceneObject.material.uniforms["diffuse"]) {

        var hsl = activeSceneObject.material.uniforms["diffuse"].value.getHSL();
        activeSceneObject.material.uniforms["diffuse"].value.setHSL(hsl.h, hsl.s, newValue);

    }

}

function defineRefractionModePerMesh( mesh, param ) {

    if ( mesh.material.envMap != undefined ) {

        if ( param.value )  {
            mesh.material.envMap.mapping = THREE.EquirectangularRefractionMapping;
        } else {
            mesh.material.envMap.mapping = THREE.EquirectangularReflectionMapping;
        }

    }
    mesh.material.needsUpdate = true;
    
}

function setMaterialRefractionMode( param ) {

    params.program.objects[ param.object ].material.refraction.value = param.value;

    var object = sceneObjects[ param.object ];

    if (object.isMesh) {

        defineRefractionModePerMesh( object, param );


    } else {

        for ( var i = 0, il = object.children.length; i < il; i ++ ) {

            var child = object.children[i];
            if ( child && child.isMesh ) defineRefractionModePerMesh( child, param );

        }

    }    

}

function setMaterialFlatEnvMapMode( param ) {

    params.program.objects[ param.object ].material.flatEnvMap.value = param.value;

    var object = sceneObjects[ param.object ];

    defineMaterialFlatEnvMapModePerMesh( object.children[0] );
    defineMaterialFlatEnvMapModePerMesh( object.children[1] );

    function defineMaterialFlatEnvMapModePerMesh( object ) {

        if ( object.material.hasOwnProperty("uniforms") ) {

            object.material.defines.ENVMAP_FLAT = param.value;

        }

        object.material.needsUpdate = true;
    }

}


function setMaterialDiffuseProperty( param ) {

    var object = sceneObjects[ param.object ];

    var newColorHex = "#" + param.value;

    if ( object.isMesh ) {

        if ( object.hasOwnProperty("material") ) {

            if ( object.material.hasOwnProperty("uniforms") ) {

                object.material.uniforms['diffuse'].value = new THREE.Color( newColorHex );

            } else {

                object.material.diffuse = new THREE.Color( newColorHex );

            }

        }

    } else {

        for ( var i = 0; i < object.children.length; i++ ) {

            if ( object.children[i].hasOwnProperty("material") ) {

                if ( object.children[i].material.hasOwnProperty("uniforms") ) {

                    object.children[i].material.uniforms['diffuse'].value = new THREE.Color( newColorHex );

                } else {

                    object.children[i].material.diffuse = new THREE.Color( newColorHex );

                }

            }          
        }
        
    }

    

}








function setDepthProcRTMaterialsUniform( paramKey, param ) {

    for (var i = 0; i < depthProcRTMaterials.length; i++ ) {

        if ( depthProcRTMaterials[i].uniforms.hasOwnProperty( paramKey ) ) {

            depthProcRTMaterials[i].uniforms[ paramKey ].value = Number( param.value );
        
        }

    }

    //if (echoPostFX_DEPTH.doRender) {

        if ( echoPostFX_DEPTH.material.uniforms.hasOwnProperty( paramKey ) ) {
         
            echoPostFX_DEPTH.material.uniforms[ paramKey ].value = Number( param.value );

        }

    //}

    for ( var i = 0; i < sceneObjects.pointcloud.children.length; i++ ){

        if ( sceneObjects.pointcloud.children[i].material.uniforms.hasOwnProperty( paramKey ) ) {

            sceneObjects.pointcloud.children[i].material.uniforms[ paramKey ].value = Number( param.value );

        }

    }

    

}



function swapTransformationModeMeshes() {

    if ( transformationMode === transformationType.colorToDepth ) {

        depthPostFX.setSize( COLOR_TO_DEPTH_WIDTH, COLOR_TO_DEPTH_HEIGHT );
        dynamicsPostFX.setSize( COLOR_TO_DEPTH_WIDTH, COLOR_TO_DEPTH_HEIGHT );
        setReactionDiffusionDimensions(COLOR_TO_DEPTH_WIDTH, COLOR_TO_DEPTH_HEIGHT );

        glslProcRenderTarget_depth = colorToDepth.glslProcRenderTarget_depth;
        glslProcRenderTarget_ir = colorToDepth.glslProcRenderTarget_ir;
        glslProcRenderTarget_color = colorToDepth.glslProcRenderTarget_color;
        glslProcRenderTarget_color_envMap = colorToDepth.glslProcRenderTarget_color_envMap;
        glslProcRenderTarget_colorramp = colorToDepth.glslProcRenderTarget_colorramp;
        glslProcRenderTarget_normal = colorToDepth.glslProcRenderTarget_normal;

        raw_depth_texture = colorToDepth.raw_depth_texture;
        raw_ir_texture = colorToDepth.raw_ir_texture;
        raw_color_texture = colorToDepth.raw_color_texture;

        camera_RT = colorToDepth.camera_RT;
        scene_RT_depth = colorToDepth.scene_RT_depth;
        scene_RT_ir = colorToDepth.scene_RT_ir;
        scene_RT_color = colorToDepth.scene_RT_color;
        scene_RT_colorramp = colorToDepth.scene_RT_colorramp;
        scene_RT_normal = colorToDepth.scene_RT_normal;

        for ( var object in sceneObjects ) {

            if ( sceneObjects[object].children.length === 2 ) {
                sceneObjects[object].children[0].visible = false;
                sceneObjects[object].children[1].visible = true;
            }

        }

    } else {

        depthPostFX.setSize( DEPTH_TO_COLOR_WIDTH, DEPTH_TO_COLOR_HEIGHT );
        dynamicsPostFX.setSize( DEPTH_TO_COLOR_WIDTH, DEPTH_TO_COLOR_HEIGHT );
        setReactionDiffusionDimensions( DEPTH_TO_COLOR_WIDTH, DEPTH_TO_COLOR_HEIGHT );

        glslProcRenderTarget_depth = depthToColor.glslProcRenderTarget_depth;
        glslProcRenderTarget_ir = depthToColor.glslProcRenderTarget_ir;
        glslProcRenderTarget_color = depthToColor.glslProcRenderTarget_color;
        glslProcRenderTarget_color_envMap = depthToColor.glslProcRenderTarget_color_envMap;
        glslProcRenderTarget_colorramp = depthToColor.glslProcRenderTarget_colorramp;
        glslProcRenderTarget_normal = depthToColor.glslProcRenderTarget_normal;

        raw_depth_texture = depthToColor.raw_depth_texture;
        raw_ir_texture = depthToColor.raw_ir_texture;
        raw_color_texture = depthToColor.raw_color_texture;
        
        camera_RT = depthToColor.camera_RT;
        scene_RT_depth = depthToColor.scene_RT_depth;
        scene_RT_ir = depthToColor.scene_RT_ir;
        scene_RT_color = depthToColor.scene_RT_color;
        scene_RT_colorramp = depthToColor.scene_RT_colorramp;
        scene_RT_normal = depthToColor.scene_RT_normal;

        for ( var object in sceneObjects ) {

            if ( sceneObjects[object].children.length === 2 ) {
                sceneObjects[object].children[0].visible = true;
                sceneObjects[object].children[1].visible = false;
            }

        }

    }

    //depthPostFX.texturePass.map = glslProcRenderTarget_depth.texture;
    //depthPostFX.texturePass.map = depth_textureOG;
    depthPostFX.texturePass.map = glslProcRenderTarget_depth.texture;
    // dynamicsPostFX.texturePass.map = glslProcRenderTarget_depth.texture;

    echoPostFX_DEPTH.setTransformationMode( transformationMode );
    echoPostFX_IR.setTransformationMode( transformationMode );
    echoPostFX_COLOR.setTransformationMode (transformationMode );

    // force to update depthPostFX input texture
    depthPostFX.setDepthPostprocessingParameter( params.depthFX.preprocessDepthWithEcho, "preprocessDepthWithEcho" );

    //sceneObjects.depthMesh.children[0].material.uniforms['normalMap'].value = glslProcRenderTarget_normal.texture;
    //sceneObjects.depthMesh.children[1].material.uniforms['normalMap'].value = glslProcRenderTarget_normal.texture;

    rdMaterial.uniforms["tDiffuse2"].value = glslProcRenderTarget_depth.texture;


}

//------- KINECT PROCESSOR --------- //

//Mapping to a range
// const map = (value, inputMin, inputMax, outputMin, outputMax) => {
//     return (value - inputMin) * (outputMax - outputMin) / (inputMax - inputMin) + outputMin;
// };

function processDepthFrame(dataArray) {

    if ( dataArray.length == 0 ) return;

    raw_depth_texture.image.data.set(dataArray);
    raw_depth_texture.needsUpdate = true;

    echoPostFX_DEPTH.processEchoEffect( raw_depth_texture, dataArray );

    
    // var w = DEPTH_TO_COLOR_WIDTH, h = DEPTH_TO_COLOR_HEIGHT;
    // var outputStride = DEPTH_TO_COLOR_WIDTH * 4;
    // var inputStride = DEPTH_TO_COLOR_WIDTH * 2;
    // var minDepth = params.depth.minDepth.value, maxDepth = params.depth.maxDepth.value;
    // //var MapDepthToByte = maxDepth / 255;
    // for (var y = 0; y < h; y++) {
    //     for (var x = 0; x < w; x++) {
    //         const i = y * outputStride + x * 4;
    //         const depthI = y * inputStride + x * 2;
    //         var byte1 = dataArray[ depthI + 1 ];
    //         var byte2 = dataArray[ depthI ];
    //         const depth = ( byte1 << 8 ) + byte2;
    //         //const mappedDepth = depth / MapDepthToByte;
    //         //var bIntensity = depth >= minDepth && depth <= maxDepth ?(1.0 - mappedDepth / 255.0) : 0.0;
    //         var bIntensity = depth >= minDepth && depth <= maxDepth ? 1.0 - map( depth, minDepth, maxDepth, 0, 1.0 ) : 0;
    //         depth_textureOG.image.data[ i ] = bIntensity;
    //         depth_textureOG.image.data[ i + 1 ] = bIntensity;
    //         depth_textureOG.image.data[ i + 2 ] = bIntensity;
    //         depth_textureOG.image.data[ i + 3 ] = 0xff;

    //     }
    // }            
    // depth_textureOG.needsUpdate = true;


    // depth_textureOG2.image.data.set(dataArray);
    // depth_textureOG2.needsUpdate = true;



}

function processColorFrame(dataArray) {

    if ( dataArray.length == 0 ) return;
    
    raw_color_texture.image.data.set(dataArray);
    raw_color_texture.needsUpdate = true;

    echoPostFX_COLOR.processEchoEffect( raw_color_texture, dataArray );
    
}

function processIRFrame(dataArray) {

    if ( dataArray.length == 0 ) return;

    raw_ir_texture.image.data.set(dataArray);
    raw_ir_texture.needsUpdate = true;

    echoPostFX_IR.processEchoEffect( raw_ir_texture, dataArray );

}



//------- MENUBAR --------- //

(function () {
      
    const remote = require('electron').remote; 
    
    function initMenu() { 
      document.getElementById("min-btn").addEventListener("click", function (e) {
        const window = remote.getCurrentWindow();
        window.minimize(); 
      });
      
      document.getElementById("max-btn").addEventListener("click", function (e) {
        const window = remote.getCurrentWindow();
        if (!window.isMaximized()) {
          window.maximize();
        } else {
          window.unmaximize();
        }	 
      });
      
      document.getElementById("close-btn").addEventListener("click", function (e) {
        const window = remote.getCurrentWindow();
        window.hide();
      }); 
    }; 
    
    document.onreadystatechange = function () {
      if (document.readyState == "complete") {
        initMenu(); 
      }
    };
})();


