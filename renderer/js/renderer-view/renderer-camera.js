var animateCamera = false;
var homeCameraPosition = new THREE.Vector3();
var lastCameraPosition = new THREE.Vector3();

function setCameraParameter( param, paramKey ) {

    params.program.camera[ paramKey ] = param.value;

    switch (paramKey) {


        case "camDistance":

            params.camera.camDistance.value = param.value;
            //composer.renderModelPass.camera.zoom = zoom;
            //composer.renderModelPass.camera.updateProjectionMatrix();
            //camera.zoom = zoom;
            // cameraOrtho.zoom = zoom;
            // cameraDome.zoom = zoom;
            camera.position.z = param.value;
           
            controls.position0.z = param.value;

            //cameraOrtho.position.z = param.value;
           // var zoom = (params.camera.camDistance.value + 1000) / (param.value);
            var zoom = (params.camera.camDistance.value + 580) / (param.value);
         //   var zoom = (params.camera.camDistance.value + 5000) / (param.value);
            cameraOrtho.zoom = zoom;
            cameraDome.zoom = zoom;

            camera.updateProjectionMatrix();
            cameraOrtho.updateProjectionMatrix();
            //cameraDome.position.z = param.value;
            
            cameraDome.updateProjectionMatrix();
            
            // console.log(cameraDome.position.z);
            
            //console.log(composer.renderModelPass.camera, param.value, zoom, composer.renderModelPass.camera.fov,composer.renderModelPass.camera.zoom)
            
            // camera.position.z = param.value;
            // params.camera.camDistance.value = param.value;
            // homeCameraPosition.z = param.value;
           // camera.updateProjectionMatrix();
            

            //OLD
            // if (params.camera.ortho.value === true) {
            //     var zoom = (params.camera.camDistance.value + 500) / (param.value);
            //     cameraOrtho.zoom = zoom;
            //     cameraOrtho.updateProjectionMatrix();

            //     // params.camera.camDistance.value = param.value;

            // } else {
            //     camera.position.z = param.value;
            // }
            
            // console.log("camera position Z:", camera.position.z);
           // cameraOrtho.lookAt( scene.position );
           // camera.lookAt( scene.position );
            
            break;

        case "camFOV":
            camera.fov = param.value;
            camera.updateProjectionMatrix();
            break;
        case "ortho":
            params.camera.ortho.value = param.value;
            if (param.value) {

                composer.renderModelPass.camera = cameraOrtho;                
                controls.object = cameraOrtho;
                

            } else {

                composer.renderModelPass.camera = camera;
                controls.object = camera;

                // camera.position.z = homeCameraPosition.z;
                camera.updateProjectionMatrix();

            }
            break;
        case "dome":
            params.camera.dome.value = param.value;
            if (param.value) {
                composer.renderModelPass.camera = cameraDome;
            
                // var zoom = (params.camera.camDistance.value + 1000) / (params.camera.camDistance.value);
                // composer.renderModelPass.camera.zoom = zoom;
                var zoom = (params.camera.camDistance.value + 580) / (param.value);
                //camera.position.z = params.camera.camDistance.value;
                composer.renderModelPass.camera.updateProjectionMatrix();
               // controls.object = cameraDome;

                
                // homeCameraPosition.z = params.camera.camDistance.value;
                // controls.position0.z = params.camera.camDistance.value;
               // cubeCamera.position.y = params.program.camera.camY;

            } else {
                composer.renderModelPass.camera = camera;
                controls.object = camera;

                camera.position.z = homeCameraPosition.z;
                camera.updateProjectionMatrix();
                console.log("domeCam",param.value);
            }
            break;
        case "camY":
            //params.camera.camY.value = param.value;
            // if (params.camera.dome.value === true) {
            //    cubeCamera.position.y=param.value;
            //    cubeCamera.updateProjectionMatrix();
            //    console.log("cubeCamY",param.value);
            // } else if (params.camera.dome.ortho === true) {
            //     cameraOrtho.position.y=param.value;
            //     console.log("orthoCamY",param.value);
            // }else{
            //     camera.position.y = param.value;
            //     console.log("cameraCamY",param.value);
            //     //camera.position.y = param.value;
            // }

            cubeCamera.position.y=param.value;
            cameraOrtho.position.y=param.value;
            camera.position.y = param.value;
            // cubeCamera.updateProjectionMatrix();
            // console.log("cubeCamY",param.value);




            
            // camera.lookAt( scene.position );
            // console.log("camera position Y:", camera.position.y);

           // camera.updateProjectionMatrix();
            break;
        case "domeCamZ":
          
            if (params.camera.dome.value){
                console.log("domeCamZ",param.value);
               cubeCamera.position.z=param.value;
            } else if (params.camera.dome.ortho){

            }else{

            }
            

            break;

        case "camRotate":
          
            if (params.camera.dome.value){
                // cubeCamera.position.y=param.value;
                cubeCamera.rotation.y=param.value;
            } 
            // camera.lookAt( scene.position );
            // console.log("camera position Y:", camera.position.y);

            //camera.updateProjectionMatrix();
            break;
        case "camRotateX":
        
        if (params.camera.dome.value){
            // cubeCamera.position.y=param.value;
            cubeCamera.rotation.x=param.value;
        } 
        // camera.lookAt( scene.position );
        // console.log("camera position Y:", camera.position.y);

        //camera.updateProjectionMatrix();
        break;

        // case "camRotationX":
        //     camera.rotation.x = param.value;
        //     //camera.updateProjectionMatrix();
        //     break;
           

        case "animateCamera":
            timeCamera = 0;
            animateCamera = param.value;
            break;
        case "limitFPS":
           limitFPS = param.value;
            break;
    

        case "showStats":
            if (param.value) {
                document.getElementById('statsContainer').style.display = "block";
            } else {
                document.getElementById('statsContainer').style.display = "none";
            }                
            break;
                
        default:

            params.camera[ paramKey ].value = param.value;

    }

}

function rotateCamera() {

    timeCamera += 0.01;

    //var properties = params.camera;

    var properties = params.program.camera;
   
    if (!params.camera.ortho.value) {

        // camera.position.x = properties.camDistance.value * Math.sin( timeCamera * properties.animateSpeed.value );
        // camera.position.z = properties.camDistance.value * Math.cos( timeCamera * properties.animateSpeed.value );
        // camera.position.y = properties.animateZamount.value * Math.sin( timeCamera * properties.animateZspeed.value ); 

        camera.position.x = properties.camDistance * Math.sin( timeCamera * properties.animateSpeed );
        camera.position.z = properties.camDistance * Math.cos( timeCamera * properties.animateSpeed );
        camera.position.y = properties.camY+properties.animateZamount * Math.sin( timeCamera * properties.animateZspeed ); 
        //camera.position.y += properties.camY;
        camera.lookAt( scene.position );

    } else  {

        // cameraOrtho.position.x = properties.camDistance.value * Math.sin( timeCamera * properties.animateSpeed.value );
        // cameraOrtho.position.z = properties.camDistance.value * Math.cos( timeCamera * properties.animateSpeed.value );
        // cameraOrtho.position.y = properties.animateZamount.value * Math.sin( timeCamera * properties.animateZspeed.value ); 

        composer.renderModelPass.camera.position.x = properties.camDistance * Math.sin( timeCamera * properties.animateSpeed );
        composer.renderModelPass.camera.position.z = properties.camDistance * Math.cos( timeCamera * properties.animateSpeed );
        composer.renderModelPass.camera.position.y = properties.camY +properties.animateZamount * Math.sin( timeCamera * properties.animateZspeed ); 
        //camera.position.y += properties.camY;
        composer.renderModelPass.camera.lookAt( scene.position );
    }

}

function setOrbitControlsState( orbitControlsState ) {

    controls.setState( orbitControlsState );

    //console.log("orbit controls set:", controls);

}

// function resetCamera() {

//     camera.position.x = homeCameraPosition.x;
//     camera.position.y = homeCameraPosition.y;
//     camera.position.z = homeCameraPosition.z;
//     camera.lookAt( scene.position );

//     cameraOrtho.position.x = homeCameraPosition.x;
//     cameraOrtho.position.y = homeCameraPosition.y;
//     cameraOrtho.position.z = homeCameraPosition.z;
//     cameraOrtho.lookAt( scene.position );

//     var zoom = params.camera.camDistance.value / homeCameraPosition.z;
//     cameraOrtho.zoom = zoom;
//     cameraOrtho.updateProjectionMatrix();

//     console.log("camera position:", camera.position);

// }

function resetOrbitControls(camY,camDistance,camFOV) {

    console.log({camY,camDistance,camFOV});

    controls.reset();

    // console.log("resetOrbitControls", camY,camDistance,camFOV);

    // console.log(params);
    if (camY !== undefined){
        camera.position.y = camY;
    }

    if (camDistance !== undefined){
        var zoom = (params.camera.camDistance.value + 580) / (camDistance);
        composer.renderModelPass.camera.zoom = zoom;
        composer.renderModelPass.camera.updateProjectionMatrix();
        camera.position.z = camDistance;
        homeCameraPosition.z = camDistance;
      //  console.log("camera position Z:", camera.position.z);
        controls.position0.z = camDistance;
    }

    if (camFOV !== undefined){
        camFOV
        camera.fov = camFOV;
        console.log("camera FOV:", camFOV, camera.fov);
        camera.updateProjectionMatrix();
    }

    // camera.position.y = params.program.camera.camY;
    


}

function resetSceneAutoRotation() {

    autoRotateScene = false;

    //resetCamera();
    // controls.reset();

    // lightsObject.rotation.y = 0;
    
    // sceneObjects.depthMesh.rotation.y = 0;
    // sceneObjects.pointcloud.rotation.y = 0;
    // sceneObjects.instanced.rotation.y = 0;

    sceneObjects.backgroundSphere.rotation.y = sceneObjects.backgroundSphere.userData.rotationYorigin;

    sceneObjects.depthMesh.children[0].material.uniforms.envRotation.value = sceneObjects.backgroundSphere.rotation.y - Math.PI;;
    sceneObjects.depthMesh.children[1].material.uniforms.envRotation.value = sceneObjects.backgroundSphere.rotation.y - Math.PI;;

}