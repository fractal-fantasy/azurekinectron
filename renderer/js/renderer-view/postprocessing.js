var composer, effectEdge, effectSave, effectBlend, effectBloom,
    effectStrobe, effectBlack, effectStrobeTimer, effectStrobeTimerHold, effectBlackTimer, effectStrobeState = 0,
    effectBlackState = 0, isStrobeOn = false, isBlackOn = false;    

var backColorHSL = { h: 0, s: 0, l: 0 }, backColor = new THREE.Color();

var effectReactionDiffusion, effectSave_RD;
var renderTarget; // postprocessing render target
var renderTargetDepthPreprocess = null;
var effectSaveRenderTarget = null;

var liquidActive, sceneFlowTextureActive;
var logoFilePath = 'images/ff_logo.png';


function startStrobe() {

    effectStrobe.enabled = true;

    isStrobeOn = true;
    effectStrobe.uniforms['state'].value = 1;

    function swapStrobeState() {
        effectStrobeState = 1 - effectStrobeState;
        effectStrobe.uniforms['state'].value = effectStrobeState;
    }

    function repeat() {

        swapStrobeState();

        effectStrobeTimer = setTimeout( 
            repeat, 
            1000 / params.strobe.strobe_frequency.value
        )
    }

    repeat();

    // effectStrobeTimer = setInterval(function () {

    //    effectStrobeState = 1 - effectStrobeState;
    //    effectStrobe.uniforms['state'].value = effectStrobeState;

    // }, 1000 / params.strobe.strobe_frequency.value );

    // effectStrobeTimerHold = setTimeout(function () {

    //    stopStrobe();

    // }, params.strobe.strobe_hold.value );
}


function stopStrobe() {

    clearTimeout(effectStrobeTimer);
    //clearTimeout(effectStrobeTimerHold);

    effectStrobe.uniforms['state'].value = 0;
    isStrobeOn = false;

    if (effectStrobe.uniforms['logoON'].value === 0 && !isBlackOn) effectStrobe.enabled = false;

}

function startBlack(freq) {

    effectStrobe.enabled = true;
    effectStrobe.uniforms['blackoutON'].value = 1;
    isBlackOn = true;

}


function stopBlack() {

    effectStrobe.uniforms['blackoutON'].value = 0;
   // effectStrobe.enabled = false;
    isBlackOn = false;

}


function startShowingLogo() {
    effectStrobe.enabled = true;
    effectStrobe.uniforms["logoON"].value = 1;
}

function stopShowingLogo() {
    effectStrobe.uniforms["logoON"].value = 0;
    if (!isStrobeOn && !isBlackOn) effectStrobe.enabled = false;
}

function setLogoImage( filepath ) {

    var textureLoader = new THREE.TextureLoader();
    textureLoader.load(
        filepath,
        function (texture) {
            texture.generateMipmaps = false;
            texture.minFilter = THREE.LinearFilter;
            texture.magFilter = THREE.LinearFilter;
            effectStrobe.uniforms["logo"].value = texture;
        }
    );

}




 function initPostprocessing() {

        var renderTargetParameters = {
            minFilter: THREE.LinearFilter,
            magFilter: THREE.LinearFilter,
            format: THREE.RGBAFormat,
            anisotropy: 2.0,
            stencilBuffer: false
        };

        renderTarget = new THREE.WebGLRenderTarget(SCREEN_WIDTH, SCREEN_HEIGHT, renderTargetParameters);

        // edge detection
        effectEdge = new THREE.ShaderPass(THREE.EdgeShader2, "tDiffuse");


        // motion blur
        effectSaveRenderTarget = new THREE.WebGLRenderTarget(SCREEN_WIDTH, SCREEN_HEIGHT, renderTargetParameters);
        effectSave = new THREE.SavePass(effectSaveRenderTarget);
        effectBlend = new THREE.ShaderPass(THREE.BlendShader, "tDiffuse1");      
        effectBlend.uniforms['tDiffuse2'].value = effectSave.renderTarget.texture;
        //effectBlend.uniforms['mixRatio'].value = 0.95;

        // unreal bloom
        ////                                            function ( resolution, strength, radius, threshold )
        //effectBloom = new THREE.UnrealBloomPass(new THREE.Vector2(SCREEN_WIDTH, window.SCREEN_HEIGHT), 1, 1, 0.37);
        effectBloom = new THREE.UnrealBloomPass(new THREE.Vector2(SCREEN_WIDTH, SCREEN_HEIGHT), 1, 1, 0.37);

        effectStrobe = new THREE.ShaderPass(THREE.StrobeShader, "tDiffuse");

        var textureLoader = new THREE.TextureLoader();
        textureLoader.load(
	        logoFilePath,
	        function (texture) {
	            texture.generateMipmaps = false;
	            texture.minFilter = THREE.LinearFilter;
	            texture.magFilter = THREE.LinearFilter;
	            effectStrobe.uniforms["logo"].value = texture;
	            effectStrobe.uniforms["flipLogoX"].value = params.strobe.flipLogoX.value;
	        }
        );

        composer = new THREE.EffectComposer(renderer, renderTarget);

        composer.camera = camera;

        var renderModelPass = new THREE.RenderPass(scene, composer.camera);

        composer.renderModelPass = renderModelPass;

        var copyShader = new THREE.ShaderPass(THREE.CopyShader);

        brightnessPass = new THREE.ShaderPass(THREE.BrightnessShader); // brightness pass

        copyShader.renderToScreen = true;        

        composer.addPass(renderModelPass);

        //effects passes adding

        composer.addPass(effectEdge);

        //composer.addPass(effectReactionDiffusion);
        //composer.addPass(effectSave_RD);
         
        composer.addPass(effectBlend);     
        composer.addPass(effectSave);

        composer.addPass(brightnessPass);
        //composer.addPass(effectBlack);
     
        composer.addPass(effectEdge);
        composer.addPass(effectBloom);
        composer.addPass(effectStrobe);  
        composer.addPass(copyShader);

        
        // all off on start
        effectEdge.enabled = false;

    

        //effectReactionDiffusion.enabled = true;
        //effectSave_RD.enabled = true;

        effectBlend.enabled = false;
        effectSave.enabled = false;
        effectStrobe.enabled = false
        //effectBlack.enabled = false
        effectBloom.enabled = false;
        brightnessPass.enabled = true;

        testParams = {
            amount: 2.0
        }

    }


    function postprocessingParamToggle(msg) {

        var needUpdateParams = msg.needUpdateParams;

        // update preset parameter in params object :: follows the manual parmeter adjustment in control view
        if (needUpdateParams !== undefined && needUpdateParams.value === true) {

            if (needUpdateParams.parentBlockId !== undefined) {  // if parameter is global update preset's super config

                if (params.program[needUpdateParams.parentBlockId] === undefined) {

                    params.program[needUpdateParams.parentBlockId] = {};

                }

                params.program[needUpdateParams.parentBlockId][msg.effectKey] = msg.active;

            }
        }

        switch (msg.effectKey) {
            case "motionBlur":
                effectSave.enabled = msg.active;
                effectBlend.enabled = msg.active;
                break;
            // case "liquid":
            //     sceneFlowActive = msg.active;
            //     dynamicsPostFX.effectSceneFlowDynamic.enabled =  msg.active;
            //     dynamicsPostFX.effectSaveFlowDynamic.enabled =  msg.active;
            //     dynamicsPostFX.effectSceneFlowDynamicCorrector.enabled =  msg.active;
            //     dynamicsPostFX.effectJiggle.enabled =  msg.active;
            //     dynamicsPostFX.effectSaveJiggle.enabled =  msg.active;
            //     dynamicsPostFX.effectSaveSmooth.enabled = msg.active;
            //     dynamicsPostFX.effectTemporalSmoothDynamic.enabled = msg.active;
            //     dynamicsPostFX.hblurDynamic.enabled =  msg.active;
            //     dynamicsPostFX.vblurDynamic.enabled =  msg.active;
            //     // console.log("sceneFlowDynamic");

            //     liquidActive = msg.active;
            //     dynamicsPostFX.effectComputeLiquidDisplacement.enabled = msg.active;
            //     dynamicsPostFX.effectSaveLiquidDisplacement.enabled = msg.active;
            //     dynamicsPostFX.effectComputeLiquidVelocity.enabled = msg.active;
            //     dynamicsPostFX.effectSaveLiquidVelocity.enabled = msg.active;

            
            // break;  
            case "unrealBloom":
                effectBloom.enabled = msg.active;
                break;
            case "edgeDetection":
                effectEdge.enabled = msg.active;
                break;
            case "background":
                brightnessPass.enabled = msg.active;
                break;
            
            case "BLACK":
                if (msg.param === true) startBlack(30); else stopBlack();
                break;

            // case "liquid":
            //     liquidActive = msg.active;
            //     effectComputeLiquidDisplacement.enabled = msg.active;
            //     effectSaveLiquidDisplacement.enabled = msg.active;
            //     console.log("liquid", msg.active)
            //     effectComputeLiquidVelocity.enabled = msg.active;
            //     effectSaveLiquidVelocity.enabled = msg.active;
        
            //     break;
            case "noiseVariations":
                vectorDisplacementMap.uniforms['noiseVariations'].value = msg.active;
    
                break;
            // case "fixEdgeNormals":
            // vectorDisplacementMap.uniforms['fixEdgeNormals'].value = msg.active;

            // break;
            case "useNoiseToSmearDots":
            vectorDisplacementMap.uniforms['useNoiseToSmearDots'].value = msg.active;

            break;
        }
    }

    function postprocessingParamChange(msg) {

        var needUpdateParams = msg.needUpdateParams;

        // update preset parameter in params object :: follows the manual parmeter adjustment in control view
        if (needUpdateParams !== undefined && needUpdateParams.value === true) {

            if (needUpdateParams.parentBlockId !== undefined) {  // if parameter is global update preset's super config

                if (params.program[needUpdateParams.parentBlockId] === undefined) {

                    params.program[needUpdateParams.parentBlockId] = {};

                }

                params.program[needUpdateParams.parentBlockId][msg.paramKey] = msg.param.value;

            }
        }


        switch (msg.effectKey) {

            case "motionBlur":
              
                effectBlend.uniforms["mixRatio"].value = msg.param.value;
                break;

            case "temporalSmoothDynamic":
                
                break;
            
            case "blurDynamic":
                hblurDynamic.uniforms.h.value = msg.param.value / DEPTH_TO_COLOR_WIDTH;
                vblurDynamic.uniforms.v.value = msg.param.value / DEPTH_TO_COLOR_HEIGHT;
                break;
            
    

            case "unrealBloom":

                effectBloom[msg.paramKey] = msg.param.value;
                break;

            case "edgeDetection":
                effectEdge.uniforms[msg.paramKey].value = new THREE.Vector2(msg.param.value, msg.param.value);
                break;
            case "displacement":{
                switch (msg.paramKey) {
                    case "displacementScaleDepth":
                        dynamicsPostFX.vectorDisplacementMap.uniforms.depthScale.value = msg.param.value;
                        dynamicsPostFX.smoothLightsMaterial.uniforms.depthScale.value = msg.param.value;
            
                        break;
                    case "normalZ":
                        dynamicsPostFX.precomputeNormals.uniforms['normalZ'].value = msg.param.value;
            
                        break;
                    case "normalsRatio":
                        dynamicsPostFX.precomputeNormals.uniforms['normalsRatio'].value = msg.param.value;
                        dynamicsPostFX.smoothLightsMaterial.uniforms.normalsRatio.value = msg.param.value;
            
                        break;
                    case "lookupRadius":
                        dynamicsPostFX.precomputeNormals.uniforms['lookupRadius'].value = msg.param.value;
                        dynamicsPostFX.smoothLightsMaterial.uniforms.lookupRadius.value = msg.param.value;
            
                        break;
                }

            }
           
            // case "liquid":
            //     switch (msg.paramKey) {

            //         case "depthVelocityThreshold":
            //             dynamicsPostFX.effectSceneFlowDynamic.uniforms['depthVelocityThreshold'].value = msg.param.value;
            //         break;

            //         case "tension":
            //             dynamicsPostFX.effectJiggle.uniforms['tension'].value = msg.param.value;
            //         break;

            //         case "sourceVelocityScale":
            //             dynamicsPostFX.effectJiggle.uniforms['sourceVelocityScale'].value = msg.param.value;
            //         break;

            //         case "velocityScale":
            //             var material = sceneObjects.depthMesh.children[1].material;
            //             var material2 = sceneObjects.depthMesh.children[0].material;
                        
            //             if ( material.hasOwnProperty("uniforms") ) {
            //                 material.uniforms.velocityScale.value = msg.param.value;
            //             }

            //             if ( material2.hasOwnProperty("uniforms") ) {
            //                 material2.uniforms.velocityScale.value = msg.param.value;
            //             }

            //             material.needsUpdate = true;
            //             material2.needsUpdate = true;
            //         break;

            //         case "mixRatio":
            //             dynamicsPostFX.effectTemporalSmoothDynamic.uniforms["mixRatio"].value = msg.param.value;
            //            // console.log("mixRatio",effectTemporalSmoothDynamic.uniforms["mixRatio"].value);
            //             break;

            //         case "blurDynamicAmount":
            //             dynamicsPostFX.hblurDynamic.uniforms.h.value = msg.param.value / DEPTH_TO_COLOR_WIDTH;
            //             dynamicsPostFX.vblurDynamic.uniforms.v.value = msg.param.value / DEPTH_TO_COLOR_HEIGHT;
            //             dynamicsPostFX.hblurTilt.uniforms.h.value = msg.param.value / DEPTH_TO_COLOR_WIDTH;
            //             dynamicsPostFX.vblurTilt.uniforms.v.value = msg.param.value / DEPTH_TO_COLOR_HEIGHT;
            //             break;
            //         case "liquidSpeed":
            //             dynamicsPostFX.effectComputeLiquidVelocity.uniforms.speed.value = msg.param.value;
            //             break;
            //         case "liquidDamp":
            //             dynamicsPostFX.effectComputeLiquidVelocity.uniforms.damp.value = msg.param.value;
            //             break;
            //         case "liquidTension":
            //             dynamicsPostFX.effectComputeLiquidVelocity.uniforms.tension.value = msg.param.value;
            //             break;
            //         case "liquidBlur":
            //             dynamicsPostFX.hblurVelocity.uniforms.h.value = msg.param.value / DEPTH_TO_COLOR_WIDTH;
            //             dynamicsPostFX.vblurVelocity.uniforms.v.value = msg.param.value / DEPTH_TO_COLOR_HEIGHT;
            //             break;
            //         case "liquidGravity":
            //             dynamicsPostFX.effectComputeLiquidVelocity.uniforms.gravity.value = msg.param.value;
            //             // vblurVelocity.uniforms.v.value = msg.param.value / DEPTH_TO_COLOR_HEIGHT;
            //             break;
            //         case "maxGravity":
            //             dynamicsPostFX.effectComputeLiquidVelocity.uniforms.maxGravity.value = msg.param.value;
            //             // vblurVelocity.uniforms.v.value = msg.param.value / DEPTH_TO_COLOR_HEIGHT;
            //             break;
            //     }
            // break;
            case "background": 

                switch (msg.paramKey) {

                    case "backColorHue":
                        backColorHSL.h = msg.param.value / 360;
                        backColor.setHSL(backColorHSL.h, backColorHSL.s, backColorHSL.l);
                        renderer.setClearColor(backColor);
                        break;
                    case "backColorSaturation":
                        backColorHSL.s = msg.param.value / 100;
                        backColor.setHSL(backColorHSL.h, backColorHSL.s, backColorHSL.l);
                        renderer.setClearColor(backColor);
                        break;
                    case "backColorLightness":
                        backColorHSL.l = msg.param.value / 100;
                        backColor.setHSL(backColorHSL.h, backColorHSL.s, backColorHSL.l);
                        renderer.setClearColor(backColor);
                        break;
                    case "brightness":
                        //params.depth.brightness.value = param.value;
                        brightnessPass.uniforms["amount"].value = msg.param.value;
                        break;

                }

                break;

            

            case "LOGO":
                if (msg.param === 1) startShowingLogo(); else stopShowingLogo();
                break;

            case "STROBE":
                if (msg.param === 1) {
                    startStrobe();
                } else {
                    stopStrobe();
                } 
                break;          

            case "BLACK":
                if (msg.param === 1) startBlack(30); else stopBlack();
                break;
        }
    }





