const shader_pointcloud = {

    vertex: `

        #version 300 es

        // RDInteger type texture read and processing
        // uniform usampler2D displacementMap;

        // Float type texture read
        uniform sampler2D displacementMap;

        uniform float displacementScale;
        uniform sampler2D map;
        uniform float pointSize;
        uniform float depthCutLevel;
        varying float vDepth;
        varying vec4 vColor;
        varying float drawMode;
        varying vec2 vUv;

        uniform float minDepth;
        uniform float maxDepth;

        uniform bool useSingleChannelColorMap;

        float scale( in float value, in float inputMin, in float inputMax, in float outputMin, in float outputMax ){
                return (value - inputMin) * (outputMax - outputMin) / (inputMax - inputMin) + outputMin;
            }

        void main() {

            vUv = uv;

            // RDInteger type texture read and processing
            // float depthf = 0.0;
            // uint byteX = texture2D( displacementMap, vUv ).x;
            // uint byteY = texture2D( displacementMap, vUv ).y;
            // uint depthu = ( byteY << 8u ) + byteX;
            // depthf = float(depthu);
            // float depth = depthf >= minDepth && depthf <= maxDepth ? 1.0 - scale( depthf, minDepth, maxDepth, 0.0, 1.0 ) : 0.0;

            // Float type texture read
            float depth = texture2D( displacementMap, vUv ).x;

            vDepth = depth;          

            float z = depth * displacementScale;

            vec4 color = texture2D( map, vUv );
            if ( useSingleChannelColorMap ) color.rgb = color.rrr;

            vColor = color;

            vec3 pos = vec3( position.x, position.y, position.z + z);

            if ( depth < depthCutLevel ) drawMode = 0.0;
            else drawMode = 1.0;

            gl_PointSize = pointSize;
            gl_Position = projectionMatrix * modelViewMatrix * vec4( pos, 1.0 );

        }
        `,

    fragment: `

        #version 300 es

        varying float vDepth;
        varying vec4 vColor;
        varying vec2 vUv;
        varying float drawMode;
        uniform bool useColorRamp;
        uniform float colorShift;
        uniform float colorWidth;  
        uniform float brightness;       
        uniform float saturation;         

        out vec4 out_FragColor;

        vec3 rgb2hsv(vec3 c)
        {
            vec4 K = vec4(0.0, -1.0 / 3.0, 2.0 / 3.0, -1.0);
            vec4 p = mix(vec4(c.bg, K.wz), vec4(c.gb, K.xy), step(c.b, c.g));
            vec4 q = mix(vec4(p.xyw, c.r), vec4(c.r, p.yzx), step(p.x, c.r));

            float d = q.x - min(q.w, q.y);
            float e = 1.0e-10;
            return vec3(abs(q.z + (q.w - q.y) / (6.0 * d + e)), d / (q.x + e), q.x);
        }

        float hue2rgb(float f1, float f2, float hue) {
            if (hue < 0.0)
                hue += 1.0;
            else if (hue > 1.0)
                hue -= 1.0;
                float res;
                if ((6.0 * hue) < 1.0)
                res = f1 + (f2 - f1) * 6.0 * hue;
            else if ((2.0 * hue) < 1.0)
                res = f2;
            else if ((3.0 * hue) < 2.0)
                res = f1 + (f2 - f1) * ((2.0 / 3.0) - hue) * 6.0;
            else
                res = f1;
            return res;
        }

        vec3 hsl2rgb(vec3 hsl) {
            vec3 rgb;

            if (hsl.y == 0.0) {
                rgb = vec3(hsl.z); // Luminance
            } else {
                float f2;

                if (hsl.z < 0.5)
                    f2 = hsl.z * (1.0 + hsl.y);
                else
                    f2 = hsl.z + hsl.y - hsl.y * hsl.z;

                float f1 = 2.0 * hsl.z - f2;

                rgb.r = hue2rgb(f1, f2, hsl.x + (1.0/3.0));
                rgb.g = hue2rgb(f1, f2, hsl.x);
                rgb.b = hue2rgb(f1, f2, hsl.x - (1.0/3.0));
            }
            return rgb;
        }

        vec3 hsl2rgb(float h, float s, float l) {
            return hsl2rgb(vec3(h, s, l));
        }

        vec3 hsv2rgb(vec3 c)
        {
            vec4 K = vec4(1.0, 2.0 / 3.0, 1.0 / 3.0, 3.0);
            vec3 p = abs(fract(c.xxx + K.xyz) * 6.0 - K.www);
            return c.z * mix(K.xxx, clamp(p - K.xxx, 0.0, 1.0), c.y);
        }

        vec3 hsv2rgb(float h, float s, float v) {
            return hsv2rgb(vec3(h, s, v));
        }

        vec3 hue_to_rgb(float hue)
        {
            float R = abs(hue * 6.0 - 3.0) - 1.0;
            float G = 2.0 - abs(hue * 6.0 - 2.0);
            float B = 2.0 - abs(hue * 6.0 - 4.0);
            //return saturate(vec3(R,G,B));
            return vec3(R,G,B);
        }

        void main() {                                          
           
            vec3 fragRGB = vec3(1.0);
            vec3 fragHSV = rgb2hsv(fragRGB).xyz;
            fragHSV.x += vDepth * colorWidth + colorShift;
            fragHSV.yz = vec2(saturation, brightness);
            fragRGB = hsv2rgb(fragHSV);        
        
            out_FragColor = useColorRamp ? vec4( fragRGB, 1.0 ) : vec4( vColor.xyz, vDepth );        

            if (drawMode == 0.0) discard;
            
        }
    `
}

