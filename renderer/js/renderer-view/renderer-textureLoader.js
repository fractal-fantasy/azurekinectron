

// preload envmaps

function loadAssets(envMapTexturePathList) {
    var textureLoader = new THREE.TextureLoader();

    console.log("params.envMapTexturePathList:", envMapTexturePathList);

    for (var name in envMapTexturePathList) {
        var url = envMapTexturePathList[name];
        var texture;

        // Check if the file is a video (MP4)
        if (url.toLowerCase().endsWith('.mp4')) {
            // Create a video element
            var video = document.createElement('video');
            video.src = url;
            video.muted = true;
            video.autoplay = false;
            video.loop = true;
            video.type = 'video';
            video.crossOrigin = 'anonymous'; // Set cross-origin attribute if needed

            // Create a video texture from the video element
            texture = new THREE.VideoTexture(video);
        } else {
            // Load image texture
            texture = textureLoader.load(url);
        }

        texture.format = THREE.RGBFormat;
        texture.minFilter = THREE.LinearFilter;
        texture.magFilter = THREE.LinearFilter;
        texture.mapping = THREE.EquirectangularReflectionMapping;
        texture.wrapS = THREE.RepeatWrapping;
        texture.wrapT = THREE.RepeatWrapping;

        envMapTexturesListObj[name] = texture;

        //console.log("texture loaded: ", url);
    }
}


function loadTextureFromFile(param) {
    console.log("loadTextureFromFile", param);
    // Update current preset param object
    params.program.objects[param.object].material.envMapLoader = param;

    var loader = new THREE.TextureLoader();
    var name = param.userLoadedEnvMapNameList[param.userLoadedEnvMapNameList.length - 1].name;
    var path = param.userLoadedEnvMapNameList[param.userLoadedEnvMapNameList.length - 1].path;

    var texture;

    // Check if the file is a video (MP4)
    if (path.toLowerCase().endsWith('.mp4')) {
        // Create a video element
        var video = document.createElement('video');
        video.src = path;
        video.autoplay = false;
        video.muted = true;
        video.loop = true;
        video.type = 'video';
        video.crossOrigin = 'anonymous'; // Set cross-origin attribute if needed

        // Create a video texture from the video element
        texture = new THREE.VideoTexture(video);
    } else {
        // Load image texture
        texture = loader.load(path);
    }

    texture.format = THREE.RGBFormat;
    texture.minFilter = THREE.LinearFilter;
    texture.magFilter = THREE.LinearFilter;
    texture.wrapS = THREE.RepeatWrapping;
    texture.wrapT = THREE.RepeatWrapping;

    if (param.object === "backgroundSphere" || param.object === "backgroundPlane") {
        texture.mapping = THREE.UVMapping;
    } else {
        if (params.program.objects[param.object].material.refraction.value) {
            texture.mapping = THREE.EquirectangularRefractionMapping;
        } else {
            texture.mapping = THREE.EquirectangularReflectionMapping;
        }
    }

    envMapTexturesListObj[name] = texture;

    console.log("texture loaded:", param);
    console.log("envMapTexturesListObj:", envMapTexturesListObj);
}





//function to load texture from a file OLD

// function loadTextureFromFile( param ) {
//     console.log("loadTextureFromFile", param);
//     // update current preset param object
//     params.program.objects[ param.object ].material.envMapLoader = param;

//     if (param.userLoadedEnvMapNameList[1]
  
//     var loader = new THREE.TextureLoader();
//     var name = param.userLoadedEnvMapNameList[ param.userLoadedEnvMapNameList.length - 1 ].name;
//     var path = param.userLoadedEnvMapNameList[ param.userLoadedEnvMapNameList.length - 1 ].path;

//     var texture = loader.load( path );

//     texture.format = THREE.RGBFormat;
//     texture.minFilter = THREE.LinearFilter;
//     texture.magFilter = THREE.LinearFilter;
//     // texture.mapping = THREE.EquirectangularReflectionMapping;
//     texture.wrapS = THREE.RepeatWrapping;
//     texture.wrapT = THREE.RepeatWrapping;



//     if ( param.object === "backgroundSphere" ) {

//         texture.mapping = THREE.UVMapping;


//     } else if ( param.object === "backgroundPlane" ) {

//         texture.mapping = THREE.UVMapping;

//     } else {

//         if ( params.program.objects[ param.object ].material.refraction.value ) {
//             texture.mapping = THREE.EquirectangularRefractionMapping;
//         } else {
//             texture.mapping = THREE.EquirectangularReflectionMapping;
//         }

//     }

    
//     envMapTexturesListObj[ name ] = texture;

//     // params.program.objects[ param.object ].material.needsUpdate = true;

//     console.log("texture loaded:", param);
//     console.log("envMapTexturesListObj:", envMapTexturesListObj);

// }