﻿/**
 * @author mikkamikka / https://mikkamikka.com
 */

var trackedEntities = [];

var isAnybody = false, prevIsAnybody = false;

let K4ABT_SPACE_CAMERA = 0, K4ABT_SPACE_DEPTH = 1;

var btSpace = K4ABT_SPACE_DEPTH;

var leftHand, rightHand;
var leftControlJointType, rightControlJointType;

var debugHands = true;

var leftHandPositionShort, rightHandPositionShort;

function getClosestBody(trackedEntities) {

    var closestDist = 10000, closestBody;

    for (var i = 0; i < trackedEntities.length; i++) {        

        var positionZ = trackedEntities[i].skeleton.joints[KinectAzure.K4ABT_JOINT_PELVIS].cameraZ;

        if (positionZ < closestDist) {

            closestDist = positionZ;

            closestBody = trackedEntities[i];

        }

    }

    return closestBody;

}


function processBodyFrame(bodyFrame) {

    trackedEntities = bodyFrame.bodies;

    if (trackedEntities.length > 0) {

        isAnybody = true;

        if (prevIsAnybody === false) sendMidiMessage("CC", 119, 127);


        if (trackedEntities.length > 1) {

            var closestBody = getClosestBody(trackedEntities);
            processBody(closestBody.skeleton.joints);

        } else {

            processBody(trackedEntities[0].skeleton.joints);

        }


    } else {

        isAnybody = false;

        if (prevIsAnybody === true) sendMidiMessage("CC", 119, 0);

    }

    prevIsAnybody = isAnybody;

}



var markerLeft = new THREE.Mesh(
   new THREE.TetrahedronBufferGeometry(15, 1),
   new THREE.MeshStandardMaterial({ color: 0xff0000, metalness: 0.50, roughness: 0.3, opacity: 1.0, flatShading: true, transparent: true }));
markerLeft.position.x = -50;
markerLeft.visible = true;


var markerRight = new THREE.Mesh(
    new THREE.TetrahedronBufferGeometry(15, 1),
    new THREE.MeshStandardMaterial({ color: 0x0000ff, metalness: 0.50, roughness: 0.3, opacity:1.0, flatShading: true, transparent: true }));
markerRight.position.x = 50;
markerRight.visible = true;

var jointMapBorderOffset = { x: 40, Ytop: 70, Ybottom: 70 };

var jointMapConstraints_2D = {
    /// KinectAzure.K4A_DEPTH_MODE_WFOV_2X2BINNED:
    3: {
        left: DEPTH_WIDTH - jointMapBorderOffset.x, right: jointMapBorderOffset.x, 
        top: jointMapBorderOffset.Ytop, bottom: DEPTH_HEIGHT - jointMapBorderOffset.Ybottom, 
        centerX: DEPTH_WIDTH / 2, centerY: DEPTH_HEIGHT / 2, 
        offsetVector: new THREE.Vector3(-DEPTH_WIDTH / 2, -DEPTH_HEIGHT / 2, 0), 
        scaleVector: new THREE.Vector3(3, -3, 1) 
       },
    /// KinectAzure.K4A_DEPTH_MODE_NFOV_UNBINNED:
    2: {
        left: DEPTH_WIDTH_NFOV_UNBINNED - jointMapBorderOffset.x, right: jointMapBorderOffset.x, 
        top: jointMapBorderOffset.Ytop, bottom: DEPTH_HEIGHT_NFOV_UNBINNED - jointMapBorderOffset.Ybottom,
        centerX: DEPTH_WIDTH_NFOV_UNBINNED / 2, centerY: DEPTH_HEIGHT_NFOV_UNBINNED / 2,  
        offsetVector: new THREE.Vector3(-DEPTH_WIDTH_NFOV_UNBINNED / 2, -DEPTH_HEIGHT_NFOV_UNBINNED / 2, 0),
        scaleVector: new THREE.Vector3(1.3, -1.3, 1)
       }
}

var jointMapConstraints_3D = {
    left: 700, right: -700,
    top: -500, bottom: 250, 
    centerX: 0, centerY: 0, 
    back: 700, front: 1200,
    offsetVector: new THREE.Vector3(0, 0, -800), 
    scaleVector: new THREE.Vector3(1.00, -1.00, -1.00)
}

function setJointMapConstraints() {

    jointMapConstraints_2D = {
    /// KinectAzure.K4A_DEPTH_MODE_WFOV_2X2BINNED:
    3: {
        left: DEPTH_WIDTH - jointMapBorderOffset.x, right: jointMapBorderOffset.x, 
        top: jointMapBorderOffset.Ytop, bottom: DEPTH_HEIGHT - jointMapBorderOffset.Ybottom, 
        centerX: DEPTH_WIDTH / 2, centerY: DEPTH_HEIGHT / 2, 
        offsetVector: new THREE.Vector3(-DEPTH_WIDTH / 2, -DEPTH_HEIGHT / 2, 0), 
        scaleVector: new THREE.Vector3(3, -3, 1) 
       },
    /// KinectAzure.K4A_DEPTH_MODE_NFOV_UNBINNED:
    2: {
        left: DEPTH_WIDTH_NFOV_UNBINNED - jointMapBorderOffset.x, right: jointMapBorderOffset.x, 
        top: jointMapBorderOffset.Ytop, bottom: DEPTH_HEIGHT_NFOV_UNBINNED - jointMapBorderOffset.Ybottom,
        centerX: DEPTH_WIDTH_NFOV_UNBINNED / 2, centerY: DEPTH_HEIGHT_NFOV_UNBINNED / 2,  
        offsetVector: new THREE.Vector3(-DEPTH_WIDTH_NFOV_UNBINNED / 2, -DEPTH_HEIGHT_NFOV_UNBINNED / 2, 0),
        scaleVector: new THREE.Vector3(1.3, -1.3, 1)
       }
}

    jointMapConstraints_3D = {
        left: 700, right: -700,
        top: -500, bottom: 250, 
        centerX: 0, centerY: 0, 
        back: 700, front: 1200,
        offsetVector: new THREE.Vector3(0, 0, -800), 
        scaleVector: new THREE.Vector3(1.00, -1.00, -1.00)
    }

}

function setMarkersScale_L() {
    jointMapConstraints_2D["2"].scaleVector.x -= 0.1;
    jointMapConstraints_2D["3"].scaleVector.x -= 0.1;
}
function setMarkersScale_R() {
    jointMapConstraints_2D["2"].scaleVector.x += 0.1;
    jointMapConstraints_2D["3"].scaleVector.x += 0.1;
}
function setMarkersScale_U() {
    jointMapConstraints_2D["2"].scaleVector.y += 0.1;
    jointMapConstraints_2D["3"].scaleVector.y += 0.1;
}
function setMarkersScale_D() {
    jointMapConstraints_2D["2"].scaleVector.y -= 0.1;
    jointMapConstraints_2D["3"].scaleVector.y -= 0.1;
}
function setMarkersOffset_Plus() {
    jointMapConstraints_2D["2"].offsetVector.z += 5;
    jointMapConstraints_2D["3"].offsetVector.z += 5;
}
function setMarkersOffset_Minus() {
    jointMapConstraints_2D["2"].offsetVector.z -= 5;
    jointMapConstraints_2D["3"].offsetVector.z -= 5;
}



var controlValueHandLeft_midi = new THREE.Vector3();
var controlValueHandRight_midi = new THREE.Vector3();
var controlValueHandLeft_osc = new THREE.Vector3();
var controlValueHandRight_osc = new THREE.Vector3();

var allowSendHandCoordinatesToControlView = false;

var allowSendMIDI = {
    left: false,
    right:false
}

var allowSendOSC = {
    left: false,
    right: false
};

var handController = {
    left: { x: 0, y: 0, z: 0 },
    right: { x: 0, y: 0, z: 0 }
}

var soloModeHandController = {
    left: { x: false, y: false, z: false },
    right: { x: false, y: false, z: false }
}

var soloModeOn = false;



function skeleton_setSoloModeHandController(controller) {

    soloModeHandController = controller;

    updateSoloModeState();

}

function skeleton_setHandController(controller) {

    handController = controller;

}

function skeleton_setAllowSendMidiHand(allowObj) {

    allowSendMIDI = allowObj;

}

function skeleton_setAllowSenOSCHand(allowObj) {

    allowSendOSC = allowObj;

}

function skeleton_enableSendHandCoordinates(enabled) {

    allowSendHandCoordinatesToControlView = enabled;

}


function updateSoloModeState() {

    soloModeOn = soloModeHandController.left.x || soloModeHandController.left.y || soloModeHandController.left.z ||
                 soloModeHandController.right.x || soloModeHandController.right.y || soloModeHandController.right.z;

}


function setBTJointControlLeft(jointType) {

    leftControlJointType = jointType;

    console.log( "Left joint selected: ", leftControlJointType );

}

function setBTJointControlRight(jointType) {

    rightControlJointType = jointType;

    console.log( "Right joint selected: ", rightControlJointType );

}

function processBody(joints) {

    leftHand = joints[leftControlJointType];
    rightHand = joints[rightControlJointType];


    // if (activeSceneObject.isLiquidWall === true || activeSceneObject.isUsingReactionDiffusion) {

    //     //var projectionL = leftHand.projectOntoFrame;
    //     //var projectionR = rightHand.projectOntoFrame;

    //     var projectionL = poses[25];
    //     var projectionR = poses[26];

    //     rdMaterial.uniforms["mouse_left"].value = new THREE.Vector2(projectionL.x / 512.0, 1.0 - projectionL.y / 424.0);
    //     rdMaterial.uniforms["mouse_right"].value = new THREE.Vector2(projectionR.x / 512.0, 1.0 - projectionR.y / 424.0);
        
    // }            

    if (debugHands && leftHand !== undefined && rightHand !== undefined) {

        markerLeft.visible = true;
        markerRight.visible = true;
        
        if (btSpace === K4ABT_SPACE_CAMERA) {

            markerLeft.position.set(leftHand.cameraX, leftHand.cameraY, leftHand.cameraZ);
            markerRight.position.set(rightHand.cameraX, rightHand.cameraY, rightHand.cameraZ);   

            markerLeft.position.add(jointMapConstraints_3D.offsetVector);
            markerLeft.position.multiply(jointMapConstraints_3D.scaleVector);

            markerRight.position.add(jointMapConstraints_3D.offsetVector);
            markerRight.position.multiply(jointMapConstraints_3D.scaleVector);

        } else {

            markerLeft.position.set(leftHand.depthX, leftHand.depthY, 0);
            markerRight.position.set(rightHand.depthX, rightHand.depthY, 0);

            markerLeft.position.add(jointMapConstraints_2D[depthMode].offsetVector);
            markerLeft.position.multiply(jointMapConstraints_2D[depthMode].scaleVector);

            markerRight.position.add(jointMapConstraints_2D[depthMode].offsetVector);
            markerRight.position.multiply(jointMapConstraints_2D[depthMode].scaleVector);

        }        
        

        // markerLeft.material.opacity = leftHand.positionStatus / 2;
        // markerLeft.quaternion.copy(leftHand.orientation);       
        

        // markerRight.material.opacity = rightHand.positionStatus / 2;
        // markerRight.quaternion.copy(rightHand.orientation);

        if (btSpace === K4ABT_SPACE_CAMERA) {
            controlValueHandLeft_midi.x = convertToMidiValue(leftHand.cameraX, jointMapConstraints_3D.centerX, jointMapConstraints_3D.left);
            controlValueHandRight_midi.x = convertToMidiValue(rightHand.cameraX, jointMapConstraints_3D.centerX, jointMapConstraints_3D.right);
            // controlValueHandLeft_midi.x = convertToMidiValue(leftHand.cameraX, jointMapConstraints_3D.right, jointMapConstraints_3D.left);
            // controlValueHandRight_midi.x = convertToMidiValue(rightHand.cameraX, jointMapConstraints_3D.left, jointMapConstraints_3D.right);

            controlValueHandLeft_midi.y = convertToMidiValue(leftHand.cameraY, jointMapConstraints_3D.bottom, jointMapConstraints_3D.top);
            controlValueHandRight_midi.y = convertToMidiValue(rightHand.cameraY, jointMapConstraints_3D.bottom, jointMapConstraints_3D.top);

            controlValueHandLeft_midi.z = convertToMidiValue(leftHand.cameraZ, jointMapConstraints_3D.back, jointMapConstraints_3D.front);
            controlValueHandRight_midi.z = convertToMidiValue(rightHand.cameraZ, jointMapConstraints_3D.back, jointMapConstraints_3D.front);
        } else {

            controlValueHandLeft_midi.x = convertToMidiValue(leftHand.depthX, jointMapConstraints_2D[depthMode].centerX, jointMapConstraints_2D[depthMode].left);
            controlValueHandRight_midi.x = convertToMidiValue(rightHand.depthX, jointMapConstraints_2D[depthMode].centerX, jointMapConstraints_2D[depthMode].right);

            controlValueHandLeft_midi.y = convertToMidiValue(leftHand.depthY, jointMapConstraints_2D[depthMode].bottom, jointMapConstraints_2D[depthMode].top);
            controlValueHandRight_midi.y = convertToMidiValue(rightHand.depthY, jointMapConstraints_2D[depthMode].bottom, jointMapConstraints_2D[depthMode].top);           

            controlValueHandLeft_midi.z = 0;
            controlValueHandRight_midi.z = 0;
        }

        // Send tracked value to control windows - MIDI/OSC Settings
        if (allowSendHandCoordinatesToControlView === true) {

            if (btSpace === K4ABT_SPACE_CAMERA) {
                        
                leftHandPositionShort = {
                    x: leftHand.cameraX.toFixed(0),
                    y: leftHand.cameraY.toFixed(0),
                    z: leftHand.cameraZ.toFixed(0)
                }

                rightHandPositionShort = {
                    x: rightHand.cameraX.toFixed(0),
                    y: rightHand.cameraY.toFixed(0),
                    z: rightHand.cameraZ.toFixed(0)
                }

            } else {

                leftHandPositionShort = {
                    x: leftHand.depthX.toFixed(0),
                    y: leftHand.depthY.toFixed(0),
                    z: 0
                }

                rightHandPositionShort = {
                    x: rightHand.depthX.toFixed(0),
                    y: rightHand.depthY.toFixed(0),
                    z: 0
                }

            }

            parentview.postMessage({ type: "handPositionLeft", message: JSON.stringify(leftHandPositionShort) });

            parentview.postMessage({ type: "handPositionRight", message: JSON.stringify(rightHandPositionShort) });

        }

        // Send MIDI messages

        if (soloModeOn === false) {

            if (allowSendMIDI.left === true) {
                if (leftHand.confidence !== 0) {
                    if (handController.left.x > 0) processMidiBodyEvent(handController.left.x, controlValueHandLeft_midi.x);
                    if (handController.left.y > 0) processMidiBodyEvent(handController.left.y, controlValueHandLeft_midi.y);
                    if (handController.left.z > 0) processMidiBodyEvent(handController.left.z, controlValueHandLeft_midi.z);
                }
            }

            if (allowSendMIDI.right === true) {
                if (rightHand.confidence !== 0) {
                    if (handController.right.x > 0) processMidiBodyEvent(handController.right.x, controlValueHandRight_midi.x);
                    if (handController.right.y > 0) processMidiBodyEvent(handController.right.y, controlValueHandRight_midi.y);
                    if (handController.right.z > 0) processMidiBodyEvent(handController.right.z, controlValueHandRight_midi.z);
                }
            }
        }
        else {

            if (allowSendMIDI.left === true) {
                if (leftHand.confidence !== 0) {

                    if (handController.left.x > 0 && soloModeHandController.left.x) processMidiBodyEvent(handController.left.x, controlValueHandLeft_midi.x);
                    if (handController.left.y > 0 && soloModeHandController.left.y) processMidiBodyEvent(handController.left.y, controlValueHandLeft_midi.y);
                    if (handController.left.z > 0 && soloModeHandController.left.z) processMidiBodyEvent(handController.left.z, controlValueHandLeft_midi.z);
                }
            }

            if (allowSendMIDI.right === true) {
                if (rightHand.confidence !== 0) {

                    if (handController.right.x > 0 && soloModeHandController.right.x) processMidiBodyEvent(handController.right.x, controlValueHandRight_midi.x);
                    if (handController.right.y > 0 && soloModeHandController.right.y) processMidiBodyEvent(handController.right.y, controlValueHandRight_midi.y);
                    if (handController.right.z > 0 && soloModeHandController.right.z) processMidiBodyEvent(handController.right.z, controlValueHandRight_midi.z);
                }
            }

        }

        // Send OSC messages

        if (soloModeOn === false) {
        
            if (allowSendOSC.left) {

                if (btSpace === K4ABT_SPACE_CAMERA) {
                     controlValueHandLeft_osc.x = convertToOSCValue(leftHand.cameraX, -400, 800);
                    //controlValueHandLeft_osc.x = convertToOSCValue(leftHand.cameraX, 0, 2000);
                    controlValueHandLeft_osc.y = convertToOSCValue(leftHand.cameraY, 600, -600);
                    controlValueHandLeft_osc.z = convertToOSCValue(leftHand.cameraZ, 700, 1200);
                } else {
                    controlValueHandLeft_osc.x = convertToOSCValue(leftHand.depthX, jointMapConstraints_2D[depthMode].centerX, jointMapConstraints_2D[depthMode].left);
                   // controlValueHandLeft_osc.x = convertToOSCValue(leftHand.depthX, jointMapConstraints_2D[depthMode].right, jointMapConstraints_2D[depthMode].left);
                    controlValueHandLeft_osc.y = convertToOSCValue(leftHand.depthY, jointMapConstraints_2D[depthMode].bottom, jointMapConstraints_2D[depthMode].top);
                    controlValueHandLeft_osc.z = 0;
                }
                
                processOSCBodyEvent( "left", "x", controlValueHandLeft_osc.x );
                processOSCBodyEvent( "left", "y", controlValueHandLeft_osc.y );
                processOSCBodyEvent( "left", "z", controlValueHandLeft_osc.z );

            }

            if (allowSendOSC.right) {

                if (btSpace === K4ABT_SPACE_CAMERA) {
                     controlValueHandRight_osc.x = convertToOSCValue(rightHand.cameraX, 400, -800);
                    //controlValueHandRight_osc.x = convertToOSCValue(rightHand.cameraX, 0, -2000);
                    controlValueHandRight_osc.y = convertToOSCValue(rightHand.cameraY, 600, -600);
                    controlValueHandRight_osc.z = convertToOSCValue(rightHand.cameraZ, 700, 1200);
                } else {
                    //controlValueHandRight_osc.x = convertToOSCValue(rightHand.depthX, jointMapConstraints_2D[depthMode].left, jointMapConstraints_2D[depthMode].right);
                    controlValueHandRight_osc.x = convertToOSCValue(rightHand.depthX, jointMapConstraints_2D[depthMode].centerX, jointMapConstraints_2D[depthMode].right);
                    controlValueHandRight_osc.y = convertToOSCValue(rightHand.depthY, jointMapConstraints_2D[depthMode].bottom, jointMapConstraints_2D[depthMode].top);
                    controlValueHandRight_osc.z = 0;
                }

                processOSCBodyEvent( "right", "x", controlValueHandRight_osc.x );
                processOSCBodyEvent( "right", "y", controlValueHandRight_osc.y );
                processOSCBodyEvent( "right", "z", controlValueHandRight_osc.z );

            }

        } else {

            if (allowSendOSC.left) {

                if (btSpace === K4ABT_SPACE_CAMERA) {
                    //change these for body tracking constraints (match with numbers in gui)
                    controlValueHandLeft_osc.x = convertToOSCValue(leftHand.cameraX, -400, 800);
                    controlValueHandLeft_osc.y = convertToOSCValue(leftHand.cameraY, 600, -600);
                    controlValueHandLeft_osc.z = convertToOSCValue(leftHand.cameraZ, 700, 1200);
                } else {
                    //controlValueHandLeft_osc.x = convertToOSCValue(leftHand.depthX, jointMapConstraints_2D[depthMode].right, jointMapConstraints_2D[depthMode].left);
                    controlValueHandLeft_osc.x = convertToOSCValue(leftHand.depthX, jointMapConstraints_2D[depthMode].centerX, jointMapConstraints_2D[depthMode].left);
                    controlValueHandLeft_osc.y = convertToOSCValue(leftHand.depthY, jointMapConstraints_2D[depthMode].bottom, jointMapConstraints_2D[depthMode].top);
                    controlValueHandLeft_osc.z = 0;
                }
                
                if (soloModeHandController.left.x) processOSCBodyEvent( "left", "x", controlValueHandLeft_osc.x );
                if (soloModeHandController.left.y) processOSCBodyEvent( "left", "y", controlValueHandLeft_osc.y );
                if (soloModeHandController.left.z) processOSCBodyEvent( "left", "z", controlValueHandLeft_osc.z );

            }

            if (allowSendOSC.right) {

                if (btSpace === K4ABT_SPACE_CAMERA) {
                    //change these for body tracking limits
                    controlValueHandRight_osc.x = convertToOSCValue(rightHand.cameraX, 400, -800);
                    controlValueHandRight_osc.y = convertToOSCValue(rightHand.cameraY, 600, -600);
                    controlValueHandRight_osc.z = convertToOSCValue(rightHand.cameraZ, 700, 1200);
                } else {
                    //controlValueHandRight_osc.x = convertToOSCValue(rightHand.depthX, jointMapConstraints_2D[depthMode].left, jointMapConstraints_2D[depthMode].right);
                    controlValueHandRight_osc.x = convertToOSCValue(rightHand.depthX, jointMapConstraints_2D[depthMode].centerX, jointMapConstraints_2D[depthMode].right);
                    controlValueHandRight_osc.y = convertToOSCValue(rightHand.depthY, jointMapConstraints_2D[depthMode].bottom, jointMapConstraints_2D[depthMode].top);
                    controlValueHandRight_osc.z = 0;
                }

                if (soloModeHandController.right.x) processOSCBodyEvent( "right", "x", controlValueHandRight_osc.x );
                if (soloModeHandController.right.y) processOSCBodyEvent( "right", "y", controlValueHandRight_osc.y );
                if (soloModeHandController.right.z) processOSCBodyEvent( "right", "z", controlValueHandRight_osc.z );

            }

        }

    }

}

function processMidiBodyEvent(controller, value) {

    sendMidiMessage("CC", controller, value);

}


var leftHandDebug = { position: new THREE.Vector3() };

function emulateHandTrackingInputPosX(control) {

    leftHandDebug.position.x = Number(control.value) / 500;

    var posXMidiValue = convertToMidiValue( leftHandDebug.position.x, 0, 1) ;

    processMidiBodyEvent( handController.left.x, posXMidiValue );

    if (allowSendHandCoordinatesToControlView === true) {                        
        var leftHandPositionShort = {
            x: leftHandDebug.position.x.toFixed(2),
            y: leftHandDebug.position.y.toFixed(2),
            z: leftHandDebug.position.z.toFixed(2)
        }  
        parentview.postMessage({ type: "handPositionLeft", message: JSON.stringify(leftHandPositionShort) } );
    }

    // send OSC message
    processOSCBodyEvent( "left", "x", leftHandDebug.position.x );

}

function emulateHandTrackingInputPosY(control) {

    leftHandDebug.position.y = Number(control.value) / 500;

    var posYMidiValue = convertToMidiValue( leftHandDebug.position.y, 0, 1) ;

    processMidiBodyEvent( handController.left.y, posYMidiValue );

    if (allowSendHandCoordinatesToControlView === true) {                        
        var leftHandPositionShort = {
            x: leftHandDebug.position.x.toFixed(2),
            y: leftHandDebug.position.y.toFixed(2),
            z: leftHandDebug.position.z.toFixed(2)
        }  
        parentview.postMessage({ type: "handPositionLeft", message: JSON.stringify(leftHandPositionShort) } );
    }

    // send OSC message
    processOSCBodyEvent( "left", "y", leftHandDebug.position.y );

}

function emulateHandTrackingInputPosZ(control) {

    leftHandDebug.position.z = Number(control.value) / 500;

    var posZMidiValue = convertToMidiValue( leftHandDebug.position.z, 0, 1) ;

    processMidiBodyEvent( handController.left.z, posZMidiValue );

    if (allowSendHandCoordinatesToControlView === true) {                        
        var leftHandPositionShort = {
            x: leftHandDebug.position.x.toFixed(2),
            y: leftHandDebug.position.y.toFixed(2),
            z: leftHandDebug.position.z.toFixed(2)
        }  
        parentview.postMessage({ type: "handPositionLeft", message: JSON.stringify(leftHandPositionShort) } );
    }

    // send OSC message
    processOSCBodyEvent( "left", "z", leftHandDebug.position.z );

}





function convertToMidiValue(in_value, baseMin, baseMax) {

    if (baseMin === undefined) baseMin = 0.0;
    if (baseMax === undefined) baseMax = 1.0;

    return scaleInt(in_value, baseMin, baseMax, 0, 127);

}

function convertToOSCValue(in_value, baseMin, baseMax) {

    if (baseMin === undefined) baseMin = 0.0;
    if (baseMax === undefined) baseMax = 1.0;

    return scaleFloat(in_value, baseMin, baseMax, 0, 1);

}

function scaleInt(valueIn, baseMin, baseMax, limitMin, limitMax) {

    if (baseMin > baseMax) {
        baseMin = -baseMin;
        baseMax = -baseMax;
        valueIn = -valueIn;
    }

    var newVal = parseInt(((limitMax - limitMin) * (valueIn - baseMin) / (baseMax - baseMin)) + limitMin);

    if (newVal < limitMin) return limitMin;
    if (newVal > limitMax) return limitMax;
    return newVal;
}

function scaleFloat(valueIn, baseMin, baseMax, limitMin, limitMax) {

    if (baseMin > baseMax) {
        baseMin = -baseMin;
        baseMax = -baseMax;
        valueIn = -valueIn;
    }

    var newVal = ((limitMax - limitMin) * (valueIn - baseMin) / (baseMax - baseMin)) + limitMin;


    // if (newVal < limitMin) return limitMin;
    // if (newVal > limitMax) return limitMax;
    return newVal;
}

var MIDISettingsPopupIsActive = false;