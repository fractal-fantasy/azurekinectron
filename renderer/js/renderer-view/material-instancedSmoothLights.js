﻿/**
 * Created by mika on 22.07.2017.
 */

var my_uniforms_instanced = THREE.UniformsUtils.merge([
        THREE.UniformsUtils.clone(THREE.ShaderLib.physical.uniforms),
        {
            "resolution": { value: new THREE.Vector2(512,424) },
            //"lookupRadius": { value: 1.0 },
            //"normalsRatio": { value: 1.0 },
            "time": { value: 0.0 },
            "ballRadius": { value: 1.0 },
            "ballRadiusPower": { value: 1.0 },
            "useGeoMod": { value: false },
            
            "frenelBias": { value: 0.0 },
            "frenelPower": { value: 1.0 },
            "frenelScale": { value: 0.2 }

        }
]
    );

var smoothLightsMaterialInstancedTemplate = new THREE.ShaderMaterial({

    uniforms: my_uniforms_instanced,

    vertexShader: [

        "#define PHYSICAL",

        "varying vec3 vViewPosition;",

        "attribute vec3 offset;",

        "#ifndef FLAT_SHADED",

        "varying vec3 vNormal;",

        "#endif",

        "varying vec3 vReflect;",

        THREE.ShaderChunk["common"],
        THREE.ShaderChunk["uv_pars_vertex"],
        THREE.ShaderChunk["uv2_pars_vertex"],
        THREE.ShaderChunk["displacementmap_pars_vertex"],
        THREE.ShaderChunk["color_pars_vertex"],
        //THREE.ShaderChunk[ "morphtarget_pars_vertex" ],
        //THREE.ShaderChunk[ "skinning_pars_vertex" ],
        //THREE.ShaderChunk[ "shadowmap_pars_vertex" ],
        // THREE.ShaderChunk["specularmap_pars_fragment"],
        //THREE.ShaderChunk[ "logdepthbuf_pars_vertex" ],
        //THREE.ShaderChunk[ "clipping_planes_pars_vertex" ],
        "varying float drawMode;",
        "uniform vec2 resolution;",
        //"uniform float lookupRadius;",
        //"uniform float normalsRatio;",
        //"uniform float amplitude;",

        //"uniform float time;",
        //"uniform float rippleFrequency;",
        //"uniform float rippleSpeed;",
        //"uniform float rippleAmplitude;",

        "attribute vec3 displacement;",
        "uniform float time;",
        "uniform float ballRadius;",
        "uniform float ballRadiusPower;",
        "uniform bool useGeoMod;",


        //"uniform sampler2D indexTexture;",

        "void main() {",

            THREE.ShaderChunk["uv_vertex"],
            //#if defined( USE_MAP ) || defined( USE_BUMPMAP ) || defined( USE_NORMALMAP ) || defined( USE_SPECULARMAP ) || defined( USE_ALPHAMAP ) || defined( USE_EMISSIVEMAP ) || defined( USE_ROUGHNESSMAP ) || defined( USE_METALNESSMAP )
	        //    vUv = ( uvTransform * vec3( uv, 1 ) ).xy;
            //#endif
            THREE.ShaderChunk["uv2_vertex"],
            THREE.ShaderChunk["color_vertex"],

            THREE.ShaderChunk["beginnormal_vertex"],
            //THREE.ShaderChunk["morphnormal_vertex"],
            //THREE.ShaderChunk[ "skinbase_vertex" ],
            //THREE.ShaderChunk[ "skinnormal_vertex" ],

            
            THREE.ShaderChunk["defaultnormal_vertex"],  ////vec3 transformedNormal = normalMatrix * objectNormal;

            "#ifndef FLAT_SHADED // Normal computed with derivatives when FLAT_SHADED",

            "   vNormal = normalize( transformedNormal );",
            
            "#endif",

            THREE.ShaderChunk["begin_vertex"],

          

            //THREE.ShaderChunk["displacementmap_vertex"],
            // modified for use with instanced geometry
            //"#ifdef USE_DISPLACEMENTMAP",
                
                "vec2 tc = offset.xy / vec2( resolution.x, resolution.y ) + 0.5;",

                "float depth = texture2D( displacementMap, tc ).x;",

                //"transformed *= sin(time) * ballRadius;",  // you can play around with sin(time*speed)*amplitude here as well

                "float ballRadiusMod = mod(abs(offset.x), ballRadius);",
                "ballRadiusMod *= mod(abs(offset.y), ballRadius);",

                //"transformed *= ballRadiusMod;",
                "transformed *= useGeoMod ? pow( pow(ballRadiusMod,0.5), ballRadiusPower) : pow(ballRadius, ballRadiusPower);",

                "transformed += offset;",

                "transformed.z += depth * displacementScale;",

                "drawMode = offset.z > 10000.0 ? 0.0 : 1.0;",

	            //"transformed += normal * ( texture2D( displacementMap, tc ).x * displacementScale + displacementBias );",
            //"#endif",
             
            //THREE.ShaderChunk[ "morphtarget_vertex" ],
            //THREE.ShaderChunk[ "skinning_vertex" ],

              
             // "transformed += deltaNormal * ( sin ( uv[0] * rippleFrequency + rippleSpeed * time) * rippleAmplitude);",

             // "transformed += deltaNormal * ( sin ( uv[1] * rippleFrequency + rippleSpeed * time) * rippleAmplitude);",

               //  DIAGONAL
             // "transformed += deltaNormal * ( sin (( uv[1] + uv[0] )  * rippleFrequency + rippleSpeed * time) * rippleAmplitude);",

            THREE.ShaderChunk["project_vertex"],
            //THREE.ShaderChunk[ "logdepthbuf_vertex" ],
            //THREE.ShaderChunk[ "clipping_planes_vertex" ],
                      

            "vViewPosition = - mvPosition.xyz;",
            THREE.ShaderChunk["worldpos_vertex"],
            //THREE.ShaderChunk[ "shadowmap_vertex" ],
            

           // "transformed += normalize( objectNormal ) * 20.0;",
      

           // "vNormal += deltaNormal;",

            //cut off background plane
            //"float index_x = texture2D( indexTexture, uv ).x;",

            //"float depth_pixel_x = texture2D( displacementMap, tc ).x;",

            "drawMode = depth < 0.1 ? drawMode = 0.0 : 1.0;",
            
            //additional cut off
            //"if (dx1.x == 0.0 || dx2.x==0.0 || dy1.x==0.0 || dy2.x==0.0) drawMode = 0.0;",

        "}"
    ].join("\n"),

    fragmentShader: [

        "#define STANDARD",

        `
        #ifdef PHYSICAL
            #define REFLECTIVITY
            #define CLEARCOAT
            #define TRANSPARENCY
        #endif
        `,

        "uniform vec3 diffuse;",
        "uniform vec3 emissive;",
        "uniform float roughness;",
        "uniform float metalness;",
        "uniform float opacity;",

        "uniform float frenelBias;",
        "uniform float frenelPower;",
        "uniform float frenelScale;",

        `
        #ifdef TRANSPARENCY
            uniform float transparency;
        #endif

        #ifdef REFLECTIVITY
            uniform float reflectivity;
        #endif

        #ifdef CLEARCOAT
            uniform float clearcoat;
            uniform float clearcoatRoughness;
        #endif

        #ifdef USE_SHEEN
            uniform vec3 sheen;
        #endif
        `,

        "varying vec3 vViewPosition;",

        "#ifndef FLAT_SHADED",

            "varying vec3 vNormal;",       

        "#endif",


        THREE.ShaderChunk["common"],
        THREE.ShaderChunk["packing"],
        THREE.ShaderChunk["color_pars_fragment"],
        THREE.ShaderChunk["uv_pars_fragment"],
        THREE.ShaderChunk["uv2_pars_fragment"],
        THREE.ShaderChunk["map_pars_fragment"],

        THREE.ShaderChunk["lightmap_pars_fragment"],
        THREE.ShaderChunk["emissivemap_pars_fragment"],


        THREE.ShaderChunk["bsdfs"],
        THREE.ShaderChunk["cube_uv_reflection_fragment"],

        THREE.ShaderChunk["envmap_common_pars_fragment"],
        //THREE.ShaderChunk["envmap_physical_pars_fragment"],

        `
        #if defined( USE_ENVMAP )

            #ifdef ENVMAP_MODE_REFRACTION
                uniform float refractionRatio;
            #endif

            vec3 getLightProbeIndirectIrradiance( /*const in SpecularLightProbe specularLightProbe,*/ const in GeometricContext geometry, const in int maxMIPLevel ) {

                vec3 worldNormal = inverseTransformDirection( geometry.normal, viewMatrix );

                #ifdef ENVMAP_TYPE_CUBE

                    vec3 queryVec = vec3( flipEnvMap * worldNormal.x, worldNormal.yz );

                    // TODO: replace with properly filtered cubemaps and access the irradiance LOD level, be it the last LOD level
                    // of a specular cubemap, or just the default level of a specially created irradiance cubemap.

                    #ifdef TEXTURE_LOD_EXT

                        vec4 envMapColor = textureCubeLodEXT( envMap, queryVec, float( maxMIPLevel ) );

                    #else

                        // force the bias high to get the last LOD level as it is the most blurred.
                        vec4 envMapColor = textureCube( envMap, queryVec, float( maxMIPLevel ) );

                    #endif

                    envMapColor.rgb = envMapTexelToLinear( envMapColor ).rgb;

                #elif defined( ENVMAP_TYPE_CUBE_UV )

                    vec4 envMapColor = textureCubeUV( envMap, worldNormal, 1.0 );

                #else

                    vec4 envMapColor = vec4( 0.0 );

                #endif

                return PI * envMapColor.rgb * envMapIntensity;

            }

            // Trowbridge-Reitz distribution to Mip level, following the logic of http://casual-effects.blogspot.ca/2011/08/plausible-environment-lighting-in-two.html
            float getSpecularMIPLevel( const in float roughness, const in int maxMIPLevel ) {

                float maxMIPLevelScalar = float( maxMIPLevel );

                float sigma = PI * roughness * roughness / ( 1.0 + roughness );
                float desiredMIPLevel = maxMIPLevelScalar + log2( sigma );

                // clamp to allowable LOD ranges.
                return clamp( desiredMIPLevel, 0.0, maxMIPLevelScalar );

            }

            vec3 getLightProbeIndirectRadiance( /*const in SpecularLightProbe specularLightProbe,*/ const in vec3 viewDir, const in vec3 normal, const in float roughness, const in int maxMIPLevel ) {

                #ifdef ENVMAP_MODE_REFLECTION

                vec3 reflectVec = reflect( -viewDir, normal );

                // Mixing the reflection with the normal is more accurate and keeps rough objects from gathering light from behind their tangent plane.
                reflectVec = normalize( mix( reflectVec, normal, roughness * roughness) );

                #else

                vec3 reflectVec = refract( -viewDir, normal, refractionRatio );

                #endif

                reflectVec = inverseTransformDirection( reflectVec, viewMatrix );

                float specularMIPLevel = getSpecularMIPLevel( roughness, maxMIPLevel );

                #ifdef ENVMAP_TYPE_CUBE

                    vec3 queryReflectVec = vec3( flipEnvMap * reflectVec.x, reflectVec.yz );

                    #ifdef TEXTURE_LOD_EXT

                        vec4 envMapColor = textureCubeLodEXT( envMap, queryReflectVec, specularMIPLevel );

                    #else

                        vec4 envMapColor = textureCube( envMap, queryReflectVec, specularMIPLevel );

                    #endif

                    envMapColor.rgb = envMapTexelToLinear( envMapColor ).rgb;

                #elif defined( ENVMAP_TYPE_CUBE_UV )

                    vec4 envMapColor = textureCubeUV( envMap, reflectVec, roughness );

                #elif defined( ENVMAP_TYPE_EQUIREC )

                    vec2 sampleUV;
                    sampleUV.y = asin( clamp( reflectVec.y, - 1.0, 1.0 ) ) * RECIPROCAL_PI + 0.5;
                    sampleUV.x = atan( reflectVec.z, -reflectVec.x ) * RECIPROCAL_PI2 + 0.5;

                    #ifdef TEXTURE_LOD_EXT

                        vec4 envMapColor = texture2DLodEXT( envMap, sampleUV, specularMIPLevel );

                    #else

                        vec4 envMapColor = texture2D( envMap, sampleUV, specularMIPLevel );

                    #endif

                    envMapColor.rgb = envMapTexelToLinear( envMapColor ).rgb;

                #elif defined( ENVMAP_TYPE_SPHERE )

                    vec3 reflectView = normalize( ( viewMatrix * vec4( reflectVec, 0.0 ) ).xyz + vec3( 0.0,0.0,1.0 ) );

                    #ifdef TEXTURE_LOD_EXT

                        vec4 envMapColor = texture2DLodEXT( envMap, reflectView.xy * 0.5 + 0.5, specularMIPLevel );

                    #else

                        vec4 envMapColor = texture2D( envMap, reflectView.xy * 0.5 + 0.5, specularMIPLevel );

                    #endif

                    envMapColor.rgb = envMapTexelToLinear( envMapColor ).rgb;

                #endif

                return envMapColor.rgb * envMapIntensity;

            }

        #endif


        `,

        THREE.ShaderChunk["lights_pars_begin"],
        THREE.ShaderChunk["lights_physical_pars_fragment"],

        THREE.ShaderChunk["bumpmap_pars_fragment"],

        THREE.ShaderChunk["normalmap_pars_fragment" ],

        THREE.ShaderChunk["clearcoat_pars_fragment" ],

        THREE.ShaderChunk["roughnessmap_pars_fragment"],
        THREE.ShaderChunk["metalnessmap_pars_fragment"],

        // custom fresnel/ chromatic aberration --- mika            

        "#if defined( USE_ENVMAP ) && !defined( ENVMAP_MODE_REFLECTION )",           

            "float fast_fresnel(vec3 I, vec3 N, vec3 fresnelValues)",
            "{",
            "    float bias = fresnelValues.x;",
            "    float power = fresnelValues.y;",
            "    float scale = 1.0 - bias;",
            "    return bias + pow(1.0 - dot(I, N), power) * scale;",
            "}",

            "vec3 getFresnel( sampler2D envMap, vec3 normal, vec3 eye ) {",

                //------ normalize incoming vectors
                //
                "vec3 N = normalize(normal);",
                "vec3 I = normalize(eye);",

                //------ Find the reflection
                //
                //"vec3 reflVec = normalize( reflect(-I, N) );",

                //"vec2 sampleUV;",
                //"sampleUV.y = saturate( reflVec.y * 0.5 + 0.5 );",
                //"sampleUV.x = atan( reflVec.z, reflVec.x ) * RECIPROCAL_PI2 + 0.5;",

                //"vec3 reflectColor = texture2D(envMap, sampleUV).xyz;",

                //------ Find the refraction
                //
                "vec3 refractColor;",
                //"vec3 IoR_Values = vec3(refractionRatio, refractionRatio * 0.99, refractionRatio * 0.98);",

                "vec3 IoR_Values = vec3(1.0 - frenelPower * 0.02, 1.0 - frenelPower * 0.01 , 1.0);",

                "IoR_Values *= refractionRatio;",

                //"IoR_Values *= frenelPower;",

                "vec3 refractVec_r = refract(-I, N, IoR_Values.x);",
                "vec3 refractVec_g = refract(-I, N, IoR_Values.y);",
                "vec3 refractVec_b = refract(-I, N, IoR_Values.z);",

                "vec2 refractUV_r, refractUV_g, refractUV_b;",
                "refractUV_r.y = saturate( refractVec_r.y * 0.5 + 0.5 );",
                "refractUV_r.x = atan( refractVec_r.z, refractVec_r.x ) * RECIPROCAL_PI2 + 0.5;",
                //"refractUV_r.x = normal.x * IoR_Values.x * 0.1;",

                "refractUV_g.y = saturate( refractVec_g.y * 0.5 + 0.5 );",
                "refractUV_g.x = atan( refractVec_g.z, refractVec_g.x ) * RECIPROCAL_PI2 + 0.5;",
                //"refractUV_g.x = normal.x * IoR_Values.y * 0.1;",

                "refractUV_b.y = saturate( refractVec_b.y * 0.5 + 0.5 );",
                "refractUV_b.x = atan( refractVec_b.z, refractVec_b.x ) * RECIPROCAL_PI2 + 0.5;",
                //"refractUV_b.x = normal.x * IoR_Values.z *0.1;",

                "refractColor.x = texture2D(envMap, refractUV_r).x;",
                "refractColor.y = texture2D(envMap, refractUV_g).y;",
                "refractColor.z = texture2D(envMap, refractUV_b).z;",


                //------ Find the Fresnel term
                //
                // bias, power
                //"vec3 fresnelValues = vec3(frenelBias, frenelPower, frenelScale);",

                //"float fresnelTerm = fast_fresnel(I, N, fresnelValues);",

                ////"float vReflectionFactor = frenelBias + frenelScale * pow( 1.0 + dot( I, N ), frenelPower );",

            // "vec3 fresnelColor = mix(refractColor, reflectColor, fresnelTerm);",
                ////"vec3 fresnelColor = mix(refractColor, reflectColor, clamp(vReflectionFactor, 0.0, 1.0));",

                //"return fresnelColor;",
                "return refractColor;",
            "}",

        "#endif",



        "varying float drawMode;",

        "void main() {",

            //THREE.ShaderChunk[ "clipping_planes_fragment" ],

            "vec4 diffuseColor = vec4( diffuse, opacity );",
            "ReflectedLight reflectedLight = ReflectedLight( vec3( 0.0 ), vec3( 0.0 ), vec3( 0.0 ), vec3( 0.0 ) );",
            "vec3 totalEmissiveRadiance = emissive;",

            //THREE.ShaderChunk["map_fragment"],
            "#ifdef USE_MAP",

	            "vec4 texelColor = texture2D( map, vec2(vUv.x, vUv.y) );",

                "texelColor = mapTexelToLinear( texelColor );",
                "diffuseColor *= texelColor;",

            "#endif",

            THREE.ShaderChunk["color_fragment"],
            THREE.ShaderChunk["alphatest_fragment"],

            THREE.ShaderChunk["roughnessmap_fragment"],
            THREE.ShaderChunk["metalnessmap_fragment"],
            THREE.ShaderChunk["normal_flip"],


            THREE.ShaderChunk["normal_fragment_begin"],
            THREE.ShaderChunk["normal_fragment_maps"],

            THREE.ShaderChunk["clearcoat_normal_fragment_begin"],
            THREE.ShaderChunk["clearcoat_normal_fragment_maps"],

            THREE.ShaderChunk["emissivemap_fragment"],

            // accumulation
            THREE.ShaderChunk["lights_physical_fragment"],
            THREE.ShaderChunk["lights_fragment_begin"],
            THREE.ShaderChunk["lights_fragment_maps"],
            THREE.ShaderChunk["lights_fragment_end"],


            //  custom fresnel/ chromatic aberration --- mika
            "#if defined( USE_ENVMAP ) && !defined( ENVMAP_MODE_REFLECTION )",

                "radiance = getFresnel( envMap, normal, vViewPosition );",            
                "reflectedLight.indirectSpecular = mix(reflectedLight.indirectSpecular, radiance, frenelScale);",


            "#endif",

            // modulation
            //THREE.ShaderChunk[ "aomap_fragment" ],

            "vec3 outgoingLight = reflectedLight.directDiffuse + reflectedLight.indirectDiffuse + reflectedLight.directSpecular + reflectedLight.indirectSpecular + totalEmissiveRadiance;",

            //"outgoingLight = mix(outgoingLight, radiance, frenelScale);",

            //THREE.ShaderChunk["envmap_fragment"],


            "gl_FragColor = vec4( outgoingLight, diffuseColor.a );",

            THREE.ShaderChunk["premultiplied_alpha_fragment"],
            //THREE.ShaderChunk[ "tonemapping_fragment" ],
            THREE.ShaderChunk["encodings_fragment"],
            //THREE.ShaderChunk[ "fog_fragment" ],

            //"if (drawMode == 0.0) gl_FragColor.a = 0.0;",

            "if (drawMode == 0.0) discard;",


        "}"
    ].join("\n"),

    //wireframe: false,
    lights: true,
    transparent: true,
    //depthWrite: false,
    //depthTest: false

});

smoothLightsMaterialInstancedTemplate.extensions.derivatives = true;
smoothLightsMaterialInstancedTemplate.precision = "mediump";
