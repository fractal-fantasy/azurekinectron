class EchoPostFX {

    depthToColor = {
        renderTarget: null,
        textureArray: [],
        zeroDataArray: null,
        zeroInputTexture: null
    }
    colorToDepth = {
        renderTarget: null,
        textureArray: [],
        zeroDataArray: null,
        zeroInputTexture: null
    }

    textureArray = [];
    zeroDataArray = null;
    zeroInputTexture = null;

    shader = null;
    textureArrayMaxSize = 32;
    
    indexOfTextureToPutDataInto = 0;

    numSplits = 6;
    echoIndex = [];

    scene = null;
    camera = null; 
    renderTarget = null;
    material = null;
    renderTargetParameters = null;

    doRender = false;

    dataTextureParams = null;


    processEchoEffect( raw_texture, dataArray ) {           // called from kinectProcessor.js


        if (this.doRender) {

            this.material.uniforms["tInput1"].value = raw_texture;
            
            this.textureArray[this.indexOfTextureToPutDataInto].image.data.set( dataArray );

            this.textureArray[this.indexOfTextureToPutDataInto].needsUpdate = true;

            this.indexOfTextureToPutDataInto = (this.indexOfTextureToPutDataInto + 1) % this.textureArrayMaxSize;

            this.updateEchoEffectShader();

        }

    }

    updateEchoEffectShader() {

        for (var i = 0; i < this.numSplits-1; i++) {

            var jndex = 'tInput' + (i + 2);

            this.material.uniforms[jndex].value = this.textureArray[this.echoIndex[i]];
            
            this.echoIndex[i] = (this.echoIndex[i] + 1) % this.textureArrayMaxSize;
        }
    }
    

    init( shader, renderTargetParameters, dataTextureParams ) {

        this.shader = shader;

        this.renderTargetParameters = renderTargetParameters;

        this.dataTextureParams = dataTextureParams;

        this.textureArrayMaxSize = params.depthFX.maxBufferSize.value;
        this.numSplits = params.depthFX.numSplits.value;

        this.depthToColor.renderTarget = new THREE.WebGLRenderTarget( DEPTH_TO_COLOR_WIDTH, DEPTH_TO_COLOR_HEIGHT, this.renderTargetParameters);
        this.colorToDepth.renderTarget = new THREE.WebGLRenderTarget( DEPTH_WIDTH, DEPTH_HEIGHT, this.renderTargetParameters);

        this.prepareTextureArray( 128 );

        this.setSplits(this.numSplits, this.textureArrayMaxSize);      

        this.depthToColor.scene = new THREE.Scene();
        this.colorToDepth.scene = new THREE.Scene();
        
        this.material = new THREE.ShaderMaterial({
            uniforms: this.shader.uniforms,
            vertexShader: this.shader.vertexShader,
            fragmentShader: this.shader.fragmentShader
        });      

        this.depthToColor.zeroDataArray = new Uint8Array( DEPTH_TO_COLOR_WIDTH * DEPTH_TO_COLOR_HEIGHT * dataTextureParams.bytesPerPixel );
        this.colorToDepth.zeroDataArray = new Uint8Array( DEPTH_WIDTH * DEPTH_HEIGHT * dataTextureParams.bytesPerPixel );

        this.depthToColor.zeroInputTexture = new THREE.DataTexture( this.depthToColor.zeroDataArray, DEPTH_TO_COLOR_WIDTH, DEPTH_TO_COLOR_HEIGHT, this.dataTextureParams.format );
        this.depthToColor.zeroInputTexture.internalFormat = this.dataTextureParams.internalFormat;
        this.depthToColor.zeroInputTexture.flipY = true;

        this.colorToDepth.zeroInputTexture = new THREE.DataTexture( this.colorToDepth.zeroDataArray, DEPTH_WIDTH, DEPTH_HEIGHT, this.dataTextureParams.format );
        this.colorToDepth.zeroInputTexture.internalFormat = this.dataTextureParams.internalFormat;
        this.colorToDepth.zeroInputTexture.flipY = true;

        this.depthToColor.camera = new THREE.OrthographicCamera(DEPTH_TO_COLOR_WIDTH / -2,
                DEPTH_TO_COLOR_WIDTH / 2,
                DEPTH_TO_COLOR_HEIGHT / 2,
                DEPTH_TO_COLOR_HEIGHT / -2, -1000, 1000);

        this.depthToColor.geometry = new THREE.PlaneBufferGeometry(DEPTH_TO_COLOR_WIDTH, DEPTH_TO_COLOR_HEIGHT, 2, 2);
        this.depthToColor.mesh = new THREE.Mesh( this.depthToColor.geometry, this.material);
        this.depthToColor.mesh.position.z = -100;
        this.depthToColor.scene.add(this.depthToColor.mesh);

        this.colorToDepth.camera = new THREE.OrthographicCamera(DEPTH_WIDTH / -2,
                DEPTH_WIDTH / 2,
                DEPTH_HEIGHT / 2,
                DEPTH_HEIGHT / -2, -1000, 1000);

        this.colorToDepth.geometry = new THREE.PlaneBufferGeometry(DEPTH_WIDTH, DEPTH_HEIGHT, 2, 2);
        this.colorToDepth.mesh = new THREE.Mesh( this.colorToDepth.geometry, this.material);
        this.colorToDepth.mesh.position.z = -100;
        this.colorToDepth.scene.add(this.colorToDepth.mesh);

    }

    setTransformationMode( mode ) {

        if ( mode === transformationType.depthToColor ) {
    
            this.renderTarget = this.depthToColor.renderTarget;
            this.textureArray = this.depthToColor.textureArray;
            this.zeroDataArray = this.depthToColor.zeroDataArray;
            this.zeroInputTexture = this.depthToColor.zeroInputTexture;

            this.camera = this.depthToColor.camera;
            this.scene = this.depthToColor.scene;


        } else {

            this.renderTarget = this.colorToDepth.renderTarget;
            this.textureArray = this.colorToDepth.textureArray;
            this.zeroDataArray = this.colorToDepth.zeroDataArray;
            this.zeroInputTexture = this.colorToDepth.zeroInputTexture;

            this.camera = this.colorToDepth.camera;
            this.scene = this.colorToDepth.scene;

        }

        // init uniforms with explicitly defined empty textures
        for (var i = 1; i <= 8; i++) {
            var jndex = 'tInput' + i;
            this.material.uniforms[jndex].value = this.zeroInputTexture;
        }


    }

    setSplits(numSplits, textureArraySize) {

        this.echoIndex = [];
        for (var i = 0; i < numSplits-1; i++) {

            this.echoIndex[i] = Math.floor( (textureArraySize / numSplits) * (i + 1) );

        }
    }

    prepareTextureArray(size) {

        if (this.textureArray.length < size) {
            for (var i = 0; i < size; i++) {

                var newArray = new Uint8Array( DEPTH_TO_COLOR_WIDTH * DEPTH_TO_COLOR_HEIGHT * this.dataTextureParams.bytesPerPixel );
                var dataTexture = new THREE.DataTexture( newArray, DEPTH_TO_COLOR_WIDTH, DEPTH_TO_COLOR_HEIGHT, this.dataTextureParams.format );
                dataTexture.internalFormat = this.dataTextureParams.internalFormat;
                dataTexture.flipY = true;
                this.depthToColor.textureArray.push(dataTexture);

                newArray = new Uint8Array( DEPTH_WIDTH * DEPTH_HEIGHT * this.dataTextureParams.bytesPerPixel );
                dataTexture = new THREE.DataTexture( newArray, DEPTH_WIDTH, DEPTH_HEIGHT, this.dataTextureParams.format );
                dataTexture.internalFormat = this.dataTextureParams.internalFormat;
                dataTexture.flipY = true;
                this.colorToDepth.textureArray.push(dataTexture);
            }
        }
        
    }

    replaceTextureArray() {
            for (var i = 0; i < this.textureArray.length; i++) {

                var newArray = new Uint8Array( DEPTH_TO_COLOR_WIDTH * DEPTH_TO_COLOR_HEIGHT * this.dataTextureParams.bytesPerPixel );
                var dataTexture = new THREE.DataTexture( newArray, DEPTH_TO_COLOR_WIDTH, DEPTH_TO_COLOR_HEIGHT, this.dataTextureParams.format );
                dataTexture.internalFormat = this.dataTextureParams.internalFormat;
                dataTexture.flipY = true;
                this.depthToColor.textureArray[i].dispose();
                this.depthToColor.textureArray[i] = dataTexture;

                newArray = new Uint8Array( DEPTH_WIDTH * DEPTH_HEIGHT * this.dataTextureParams.bytesPerPixel );
                dataTexture = new THREE.DataTexture( newArray, DEPTH_WIDTH, DEPTH_HEIGHT, this.dataTextureParams.format );
                dataTexture.internalFormat = this.dataTextureParams.internalFormat;
                dataTexture.flipY = true;
                this.colorToDepth.textureArray[i].dispose();
                this.colorToDepth.textureArray[i] = dataTexture;
            }
        
    }

    cleanTextureArray() {
    
            for (var i = this.numSplits + 1; i <= 8; i++) {
    
                var jndex = 'tInput' + i;
    
                this.material.uniforms[jndex].value = this.zeroInputTexture;
    
            }
    }

}