var blurDepth = false;
var denoiseDepthNew = false;
var echoNeedsInit = true;
class DepthPostFX {

    width = DEPTH_TO_COLOR_WIDTH;
    height = DEPTH_TO_COLOR_HEIGHT;


    
    hblur = new THREE.ShaderPass( THREE.HorizontalBlurShader1ChannelFF );
    vblur = new THREE.ShaderPass( THREE.VerticalBlurShader1ChannelFF );
    hblurTilt = new THREE.ShaderPass( THREE.HorizontalTiltShiftShader );
    vblurTilt = new THREE.ShaderPass( THREE.VerticalTiltShiftShader );
    // denoiseNew = new THREE.ShaderPass( THREE.DenoiseDepthNew );
    
    glslEdgeFF = new THREE.ShaderPass( glslEdgeCuttingShaderFF );
    //glslSharpEdge = new THREE.ShaderPass( glslSharpEdgeShader); 

    // glslHoleFillingHorizontal = new THREE.ShaderPass( glslHoleFillingHorizontalShader );
    glslHoleFillingRay = new THREE.ShaderPass( glslHoleFillingRayShader );
    // glslHoleFillingCircle = new THREE.ShaderPass( glslHoleFillingCircleShader );


    // glslHoleFilling = new THREE.ShaderPass( glslHoleFillingShader );
    glslHoleFillingConv = new THREE.ShaderPass( glslHoleFillingConv5x5Shader );

    texturePass = null;

    //barrelDistortionCorrection = new THREE.ShaderPass( THREE.BarrelDistortionCorrectionShader );
    fxaa = new THREE.ShaderPass( THREE.FXAAShader );
   // fxaa2 = new THREE.ShaderPass( THREE.FXAAShader )


    // ssaaRenderPassP = new THREE.SSAARenderPass( scene, camera );
				
    copyPass = new THREE.ShaderPass(THREE.CopyShader);


    // renderTargetParameters = {
    //     minFilter: THREE.NearestFilter,
    //     magFilter: THREE.NearestFilter,
    //     format: THREE.RedFormat,
    //     type: THREE.FloatType
    // };

    // mika's good old and working
    // renderTargetParameters = {
    //     minFilter: THREE.NearestFilter,
    //     magFilter: THREE.NearestFilter,
    //     format: THREE.RGBAFormat,
    //     type: THREE.FloatType
    // };

    // AQ
    // renderTargetParameters = {
    //     minFilter: THREE.NearestFilter,
    //     magFilter: THREE.NearestFilter,
    //     format: THREE.RedFormat,
    //     stencilBuffer: false,
    //     // type: THREE.FloatType
    //     type: THREE.HalfFloatType
    // };

   //SMOOTH BUT STRAFING SIDES
    renderTargetParameters = {
        minFilter: THREE.LinearFilter,
        magFilter: THREE.NearestFilter,
        format: THREE.RGBAFormat,
        stencilBuffer: false,
        type: THREE.FloatType,
        generateMipmaps: false,
        anisotropy:ANISOTROPY,

    };
   

    renderTarget = new THREE.WebGLRenderTarget( this.width, this.height, this.renderTargetParameters );
    renderTarget2 = new THREE.WebGLRenderTarget( this.width, this.height, this.renderTargetParameters );

    fxComposer = null;
    // fxComposer2 = null;

    
    init( in_texture_depth, renderer, params ) {
        
        this.texturePass = new THREE.TexturePass( in_texture_depth );

        this.fxComposer = new THREE.EffectComposer( renderer , this.renderTarget ); 

        this.fxComposer.renderToScreen = false;
        
        this.fxComposer.setSize( this.width, this.height);

        this.renderTargetParameters = {
            minFilter: THREE.LinearFilter,
            magFilter:  THREE.NearestFilter,
            format: THREE.RGBAFormat,
            // depthBuffer: false,
            // stencilBuffer: false,
            type: THREE.FloatType
        };

        this.renderTargetParameters2 = {
            minFilter: THREE.LinearFilter,
            magFilter: THREE.LinearFilter,
            format: THREE.RGBAFormat,
            // depthBuffer: false,
            // stencilBuffer: false,
            type: THREE.FloatType,
            anisotropy:1
        };


        //Edge Mask
        this.preEdgeRenderTarget = new THREE.WebGLRenderTarget(this.width, this.height, this.renderTargetParameters);
        this.savePreEdgeRenderTarget = new THREE.SavePass(this.preEdgeRenderTarget);

        this.edgeMask = new THREE.ShaderPass( edgeMaskShader ); //pass in raw depth
        this.edgeBlurH = new THREE.ShaderPass( THREE.HorizontalBlurShader1ChannelFF );
        this.edgeBlurV = new THREE.ShaderPass( THREE.VerticalBlurShader1ChannelFF );
        this.edgeMaskReset = new THREE.ShaderPass( edgeMaskResetShader ); //pass in edgeMask and depthMap 
        this.edgeMaskReset.uniforms.rawDepth.value = this.preEdgeRenderTarget.texture;

        this.edgeBlurH2 = new THREE.ShaderPass( THREE.HorizontalBlurShader1ChannelFF );
        this.edgeBlurV2 = new THREE.ShaderPass( THREE.VerticalBlurShader1ChannelFF );
        this.edgeMaskReset2 = new THREE.ShaderPass( edgeMaskResetShader ); //pass in edgeMask and depthMap 
        this.edgeMaskReset2.uniforms.rawDepth.value = this.preEdgeRenderTarget.texture;

        
        this.edgeExtrude = new THREE.ShaderPass( edgeExtrudeShader ); // extrude depth by edgemask
        this.edgeExtrude.uniforms.rawDepth.value = this.preEdgeRenderTarget.texture;
        
        this.edgeRenderTarget = new THREE.WebGLRenderTarget(this.width, this.height, this.renderTargetParameters);
        this.saveEdgeRenderTarget = new THREE.SavePass(this.edgeRenderTarget);
        
        
       // motion blur depth
        this.temporalSmoothRenderTarget = new THREE.WebGLRenderTarget(this.width, this.height, this.renderTargetParameters);
        this.effectSave = new THREE.SavePass(this.temporalSmoothRenderTarget);
        this.effectTemporalSmooth = new THREE.ShaderPass(THREE.temporalSmoothShader, "rawDepth");//current      
        this.effectTemporalSmooth.uniforms['rawDepthPrev'].value = this.temporalSmoothRenderTarget.texture;//previous
        
        
        // this.denoiseNew.uniforms.sampleRadius.value = params.depthPostprocessing.sampleRadius.value ;
        // this.denoiseNew.uniforms.amount.value = params.depthPostprocessing.denoiseAmount.value ;

        
        this.fxComposer.addPass( this.texturePass );


        // this.fxComposer.addPass( this.glslHoleFillingCircle ); //
        this.fxComposer.addPass( this.glslHoleFillingRay ); //

        // this.fxComposer.addPass( this.glslHoleFillingHorizontal ); //

        for(var i = 0; i< 6; i++){
            this.fxComposer.addPass( this.glslHoleFillingConv ); //check
        }

        this.fxComposer.addPass( this.glslEdgeFF ); // check
        // this.fxComposer.addPass( this.glslSharpEdge ); // check




        this.fxComposer.addPass(this.effectTemporalSmooth); // check     
        this.fxComposer.addPass(this.effectSave); // check


        //EDGEFIX 
        this.fxComposer.addPass( this.savePreEdgeRenderTarget ); // check
        this.fxComposer.addPass( this.edgeMask ); // check
        for(var i = 0; i< 2; i++){
            this.fxComposer.addPass( this.edgeBlurH2 ); // check
            this.fxComposer.addPass( this.edgeBlurV2 ); // check
            this.fxComposer.addPass( this.edgeMaskReset2 ); // check 
            
        }
        this.fxComposer.addPass( this.edgeBlurH ); // check 
        this.fxComposer.addPass( this.edgeBlurV ); // check 
        this.fxComposer.addPass( this.edgeMaskReset );// check
        this.fxComposer.addPass( this.saveEdgeRenderTarget ); // check
        this.fxComposer.addPass( this.edgeExtrude ); // check
        //EDGEFIX END

        this.fxComposer.addPass( this.hblur ); // check
        this.fxComposer.addPass( this.vblur ); // check
        this.fxComposer.addPass( this.hblurTilt ); // check
        this.fxComposer.addPass( this.vblurTilt ); // check

        // this.fxComposer.addPass( this.denoiseNew ); // check

        for(var i = 0; i< 8; i++){
            this.fxComposer.addPass( this.fxaa ); // check
        }

       // this.fxComposer.addPass( this.barrelDistortionCorrection ); // check

        this.fxComposer.addPass( this.copyPass );

        
         this.kinectTexture = this.renderTarget.texture;

    }

    setSize( width, height ) {
        this.width = width;
        this.height = height;
        
        this.preEdgeRenderTarget.setSize( this.width , this.height );
        this.temporalSmoothRenderTarget.setSize( this.width , this.height );
        this.edgeRenderTarget.setSize( this.width , this.height );
        
        this.edgeBlurH.uniforms.h.value = params.depthPostprocessing.blurEdgeAmount.value / this.width;
        this.edgeBlurV.uniforms.v.value = params.depthPostprocessing.blurEdgeAmount.value / this.height;
        this.hblur.uniforms.h.value = params.depthPostprocessing.blurAmount.value / this.width;
        this.vblur.uniforms.v.value = params.depthPostprocessing.blurAmount.value / this.height;
        this.hblurTilt.uniforms.h.value = params.depthPostprocessing.tiltBlurAmount.value / this.width;
        this.vblurTilt.uniforms.v.value = params.depthPostprocessing.tiltBlurAmount.value / this.height;

        //this.glslHoleFillingHorizontal.uniforms[ "step" ].value.set( 1.0 / this.width, 1.0 / this.height );
        this.glslHoleFillingRay.uniforms[ "step" ].value.set( 1.0 / this.width, 1.0 / this.height );
        //this.glslHoleFillingCircle.uniforms[ "step" ].value.set( 1.0 / this.width, 1.0 / this.height );  
        this.glslHoleFillingConv.uniforms[ "step" ].value.set( 1.0 / this.width, 1.0 / this.height );

        this.fxaa.uniforms[ "resolution" ].value.set( 1.0 / this.width, 1.0 / this.height );
        this.glslEdgeFF.uniforms[ "resolution" ].value.set(this.width, this.height );
        //this.glslSharpEdge.uniforms[ "resolution" ].value.set(this.width, this.height );
        // this.denoiseNew.uniforms[ "resolution" ].value.set(this.width, this.height );

        //this.glslHoleFillingHorizontal.uniforms[ "step" ].value.set( 1.0 / this.width, 1.0 / this.height );
        //this.glslHoleFilling.uniforms[ "step" ].value.set( 1.0 / this.width, 1.0 / this.height );
        //this.glslHoleFillingCircle.uniforms[ "step" ].value.set( 1.0 / this.width, 1.0 / this.height );
        //this.glslHoleFillingRay.uniforms[ "step" ].value.set( 1.0 / this.width, 1.0 / this.height );
        //this.glslHoleFilling.uniforms[ "step" ].value.set( 1.0 / width, 1.0 / height );

        this.fxaa.uniforms[ "resolution" ].value.set( 1.0 / this.width , 1.0 / this.height );
        this.renderTarget.setSize( width, height );

        this.fxComposer.setSize( width, height );

    }

    setSceneFlow(bool){
        dynamicsPostFX.effectTemporalSmoothDynamic.enabled = bool; 
        dynamicsPostFX.effectSaveSmooth.enabled = bool;

        dynamicsPostFX.effectSceneFlowDynamic.enabled =  bool;
        dynamicsPostFX.effectSaveFlowDynamic.enabled =  bool; 
        dynamicsPostFX.effectSceneFlowDynamicCorrector.enabled =  bool;


    }

    setDepthPostprocessingParameter( param, paramKey ) {

        var newValue = Number( param.value );

       // 

        switch (paramKey) {

            // case "fixEdgeNormals":
            //     vectorDisplacementMap.uniforms['fixEdgeNormals'].value = param.value;
            // break;

            //LIQUID START
            case "liquid":
                liquidActive = param.value;

           

                this.setSceneFlow(sceneFlowTextureActive||liquidActive);

                
                dynamicsPostFX.hblurDynamic.enabled =  param.value;
                dynamicsPostFX.vblurDynamic.enabled =  param.value;
                dynamicsPostFX.hblurTilt.enabled =  param.value;
                dynamicsPostFX.vblurTilt.enabled =  param.value;
                
                dynamicsPostFX.effectJiggle.enabled =  param.value;
                dynamicsPostFX.effectSaveJiggle.enabled =  param.value;
                
                dynamicsPostFX.effectComputeLiquidDisplacement.enabled = param.value;
                dynamicsPostFX.effectSaveLiquidDisplacement.enabled = param.value;

                dynamicsPostFX.hblurVelocity.enabled =  param.value;
                dynamicsPostFX.vblurVelocity.enabled =  param.value;

                dynamicsPostFX.effectSaveSceneFlow.enabled =  param.value;

                dynamicsPostFX.effectComputeLiquidVelocity.enabled = param.value;
                dynamicsPostFX.effectSaveLiquidVelocity.enabled = param.value;

                dynamicsPostFX.effectSaveVelocity.enabled =  param.value;

                dynamicsPostFX.vectorDisplacementMap.uniforms['liquid'].value = param.value;

            break;  
            case "depthVelocityThreshold":
                dynamicsPostFX.effectSceneFlowDynamic.uniforms['depthVelocityThreshold'].value = param.value;
            break;

            case "tension":
                dynamicsPostFX.effectJiggle.uniforms['tension'].value = param.value;
            break;

            case "sourceVelocityScale":
                dynamicsPostFX.effectJiggle.uniforms['sourceVelocityScale'].value = param.value;
            break;

            case "velocityScale":
                var material = sceneObjects.depthMesh.children[1].material;
                var material2 = sceneObjects.depthMesh.children[0].material;
                
                if ( material.hasOwnProperty("uniforms") ) {
                    material.uniforms.velocityScale.value = param.value;
                }

                if ( material2.hasOwnProperty("uniforms") ) {
                    material2.uniforms.velocityScale.value = param.value;
                }

                material.needsUpdate = true;
                material2.needsUpdate = true;
            break;

            case "mixRatio":
                dynamicsPostFX.effectTemporalSmoothDynamic.uniforms["mixRatio"].value = param.value;
            break;

            case "blurDynamicAmount":
                dynamicsPostFX.hblurDynamic.uniforms.h.value = param.value / DEPTH_TO_COLOR_WIDTH;
                dynamicsPostFX.vblurDynamic.uniforms.v.value = param.value / DEPTH_TO_COLOR_HEIGHT;
                dynamicsPostFX.hblurTilt.uniforms.h.value = param.value / DEPTH_TO_COLOR_WIDTH;
                dynamicsPostFX.vblurTilt.uniforms.v.value = param.value/ DEPTH_TO_COLOR_HEIGHT;
            break;

            case "liquidSpeed":
                dynamicsPostFX.effectComputeLiquidVelocity.uniforms.speed.value = param.value;
            break;

            case "liquidDamp":
                dynamicsPostFX.effectComputeLiquidVelocity.uniforms.damp.value = param.value;
            break;

            case "liquidTension":
                dynamicsPostFX.effectComputeLiquidVelocity.uniforms.tension.value = param.value;
            break;

            case "liquidBlur":
               // console.log(param.name,param.value);
                dynamicsPostFX.hblurVelocity.uniforms.h.value = param.value / DEPTH_TO_COLOR_WIDTH;
                dynamicsPostFX.vblurVelocity.uniforms.v.value = param.value / DEPTH_TO_COLOR_HEIGHT;
                //console.log(dynamicsPostFX.hblurVelocity.uniforms.h.value*DEPTH_TO_COLOR_WIDTH, dynamicsPostFX.vblurVelocity.uniforms.v.value*DEPTH_TO_COLOR_HEIGHT);
            break;

            case "liquidGravity":
                dynamicsPostFX.effectComputeLiquidVelocity.uniforms.gravity.value = param.value;
            break;

            case "maxGravity":
                dynamicsPostFX.effectComputeLiquidVelocity.uniforms.maxGravity.value = param.value;
            break;
            //LIQUID END


            case "rippleDepthAmplitude":
                dynamicsPostFX.vectorDisplacementMap.uniforms['rippleAmplitude'].value = param.value;
            break;
            case "depthFXOnBackground":
                dynamicsPostFX.vectorDisplacementMap.uniforms['depthFXOnBackground'].value = param.value;
            break;
            case "rippleDepthFrequency":
                dynamicsPostFX.vectorDisplacementMap.uniforms['rippleFrequency'].value = param.value;
            break;


            case "rippleDepthSpeed":
                dynamicsPostFX.vectorDisplacementMap.uniforms['rippleSpeed'].value = param.value;
            break;

            case "dotAmplitude":
                dynamicsPostFX.vectorDisplacementMap.uniforms['dotAmplitude'].value = param.value;
            break;

            case "dotSize":
                dynamicsPostFX.vectorDisplacementMap.uniforms['dotSize'].value = param.value;
            break;

            case "dotFrequency":
                dynamicsPostFX.vectorDisplacementMap.uniforms['dotFrequency'].value = param.value;
            break;

            case "dotExponent":
                dynamicsPostFX.vectorDisplacementMap.uniforms['dotExponent'].value = param.value;
            break;

            case "staggerDots":
                dynamicsPostFX.vectorDisplacementMap.uniforms['staggerDots'].value = param.value;
            break;

            case "quantizeDotsNormal":
                dynamicsPostFX.vectorDisplacementMap.uniforms['quantizeDotsNormal'].value = param.value;
            break;

            case "displaceAlongZ":
                dynamicsPostFX.vectorDisplacementMap.uniforms['displaceAlongZ'].value = param.value;
            break;

            case "useDotsForDepth":
                dynamicsPostFX.vectorDisplacementMap.uniforms['useDotsForDepth'].value = param.value;
            break;

            case "displacementFatness":
                dynamicsPostFX.vectorDisplacementMap.uniforms['displacementFatness'].value = param.value;
            break;

            case "quantizeDepth":
                dynamicsPostFX.vectorDisplacementMap.uniforms['quantizeDepth'].value = param.value;
            break;

            case "quantizeDepthFrequency":
                dynamicsPostFX.vectorDisplacementMap.uniforms['quantizeDepthFrequency'].value = param.value;
            break;

            case "displacementFatness":
                dynamicsPostFX.vectorDisplacementMap.uniforms['displacementFatness'].value = param.value;
            break;

            case "noiseFrequency":
                dynamicsPostFX.vectorDisplacementMap.uniforms['noiseFrequency'].value = param.value;
            break;

            case "noiseAmplitude":
                dynamicsPostFX.vectorDisplacementMap.uniforms['noiseAmplitude'].value = param.value;
            break;
            case "maskFrequency":
                dynamicsPostFX.vectorDisplacementMap.uniforms['maskFrequency'].value = param.value;
            break;

            case "maskThreshold":
                dynamicsPostFX.vectorDisplacementMap.uniforms['maskThreshold'].value = param.value;
            break;
            case "noiseMask":
                dynamicsPostFX.vectorDisplacementMap.uniforms['noiseMask'].value = param.value;
            break;
            case "noiseVariations":
                dynamicsPostFX.vectorDisplacementMap.uniforms['noiseVariations'].value = param.value;
            break;

            case "smearFrequency":
                dynamicsPostFX.vectorDisplacementMap.uniforms['smearFrequency'].value = param.value;
            break;

            case "smearAmplitude":
                dynamicsPostFX.vectorDisplacementMap.uniforms['smearAmplitude'].value = param.value;
            break;

            case "noiseSpeed":
                dynamicsPostFX.vectorDisplacementMap.uniforms['noiseSpeed'].value = param.value;
            break;

            case "noiseWormAmplitude":
                dynamicsPostFX.vectorDisplacementMap.uniforms['noiseWormAmplitude'].value = param.value;
            break;

            case "noiseCellAmplitude":
                dynamicsPostFX.vectorDisplacementMap.uniforms['noiseCellAmplitude'].value = param.value;
            break;

            case "noiseRodAmplitude":
                dynamicsPostFX.vectorDisplacementMap.uniforms['noiseRodAmplitude'].value = param.value;
            break;

            case "FXScale":
                dynamicsPostFX.vectorDisplacementMap.uniforms['FXScale'].value = param.value;
            break;

            case "maskSpeed":
                dynamicsPostFX.vectorDisplacementMap.uniforms['maskSpeed'].value = param.value;
            break;


            case "displacementScaleDepth":
                dynamicsPostFX.vectorDisplacementMap.uniforms.depthScale.value = param.value;
                // smoothLightsMaterial.uniforms.displacementScale.value = param.value;
                sceneObjects.depthMesh.children[0].material.uniforms['displacementScale'].value = param.value;
                sceneObjects.depthMesh.children[1].material.uniforms['displacementScale'].value = param.value;

            break;
            case "normalZ":
                dynamicsPostFX.precomputeNormals.uniforms['normalZ'].value = param.value;

            break;
            case "normalBias":
                dynamicsPostFX.vectorDisplacementMap.uniforms['normalBias'].value = param.value;

            break;
            case "normalBiasShading":
                sceneObjects.depthMesh.children[0].material.uniforms['normalBiasShading'].value = param.value;
                sceneObjects.depthMesh.children[1].material.uniforms['normalBiasShading'].value = param.value;

            break;
            case "normalsRatio":
                dynamicsPostFX.precomputeNormals.uniforms['normalsRatio'].value = param.value;          
                sceneObjects.depthMesh.children[0].material.uniforms['normalsRatio'].value = param.value;
                sceneObjects.depthMesh.children[1].material.uniforms['normalsRatio'].value = param.value;
           // console.log(smoothLightsMaterial.uniforms.normalsRatio.value);
            break;
            case "normalsRatioOld":
                dynamicsPostFX.precomputeNormals.uniforms['normalsRatioOld'].value = param.value;          
                sceneObjects.depthMesh.children[0].material.uniforms['normalsRatioOld'].value = param.value;
               // console.log(smoothLightsMaterial.uniforms.normalsRatio.value);
            break;

            case "cutSidePixels":
                dynamicsPostFX.precomputeNormals.uniforms['cutSidePixels'].value = param.value;          
                sceneObjects.depthMesh.children[0].material.uniforms['cutSidePixels'].value = param.value;
                // console.log(smoothLightsMaterial.uniforms.normalsRatio.value);
            break;
            case "refine_cutSidePixels":
                dynamicsPostFX.precomputeNormals.uniforms['refine_cutSidePixels'].value = param.value;          
                sceneObjects.depthMesh.children[0].material.uniforms['refine_cutSidePixels'].value = param.value;
                // console.log(smoothLightsMaterial.uniforms.normalsRatio.value);
            break;
            case "oldNormals":
                dynamicsPostFX.precomputeNormals.uniforms['oldNormals'].value = param.value;       
                dynamicsPostFX.vectorDisplacementMap.uniforms['oldNormals'].value = param.value;   
                sceneObjects.depthMesh.children[0].material.uniforms['oldNormals'].value = param.value;
                sceneObjects.depthMesh.children[1].material.uniforms['oldNormals'].value = param.value;
                // console.log(smoothLightsMaterial.uniforms.normalsRatio.value);
            break;

            case "lookupRadius":
                dynamicsPostFX.precomputeNormals.uniforms['lookupRadius'].value = param.value;
                sceneObjects.depthMesh.children[0].material.uniforms['lookupRadius'].value = param.value;
                sceneObjects.depthMesh.children[1].material.uniforms['lookupRadius'].value = param.value;
            break;

            

            case "filter": 
                var filter;

                if(param.value){
                    filter = THREE.NearestFilter;
                } else {
                    filter = THREE.LinearFilter;
                }

                this.renderTarget.texture.minFilter = filter;
                this.renderTarget.texture.needsUpdate = true;

                dynamicsPostFX.renderTarget.texture.minFilter = filter;
                dynamicsPostFX.renderTarget.texture.needsUpdate = true;
    
            break;

            case "temporalSmoothing": 
                this.effectSave.enabled = param.value;
                this.effectTemporalSmooth.enabled = param.value;
                //console.log(this.effectSave.enabled,this.effectTemporalSmooth.enabled);
            break;

            case "temporalMixRatio":     
                this.effectTemporalSmooth.uniforms['mixRatio'].value = param.value;   
            break;

            case "temporalThreshold":      
                this.effectTemporalSmooth.uniforms['temporalThreshold'].value = param.value;       
            break;
            case "temporalThresholdRange":      
                this.effectTemporalSmooth.uniforms['temporalThresholdRange'].value = param.value;
            break;


            case "magFilter": 

                var filter;

                switch (param.value) {

                    case "0":
                        filter = THREE.NearestFilter;
                        break;
                    case "1":
                        filter = THREE.LinearFilter;
                        break;         

                }
                
                dynamicsPostFX.renderTarget.texture.magFilter = filter;
                dynamicsPostFX.renderTarget.texture.needsUpdate = true;

                this.renderTarget.texture.magFilter = filter;
                this.renderTarget.texture.needsUpdate = true;

            break;

            case "mipmaps": 

                this.renderTarget.texture.generateMipmaps = param.value;
                this.renderTarget.texture.needsUpdate = true;
            break;

            case "anisotropy": 

                this.renderTarget.texture.anisotropy = param.value;
                this.renderTarget.texture.needsUpdate = true;

                dynamicsPostFX.renderTarget.texture.anisotropy = param.value;
                dynamicsPostFX.renderTarget.texture.needsUpdate = true;

                dynamicsPostFX.precomputedNormalsRenderTarget.texture.anisotropy = param.value;
                dynamicsPostFX.precomputedNormalsRenderTarget.texture.needsUpdate = true;


                // this.preEdgeRenderTarget.texture.anisotropy = param.value;
                // this.preEdgeRenderTarget.texture.texture.needsUpdate = true;

                this.edgeRenderTarget.texture.anisotropy = param.value;
                this.edgeRenderTarget.texture.needsUpdate = true;

            break;

            case "minFilter": 

                var filter;

                switch (param.value) {

                    case "0":
                        filter = THREE.NearestFilter;
                        break;
                    case "1":
                        filter = THREE.NearestMipmapNearestFilter;
                        break;       
                    case "2":
                        filter = THREE.NearestMipmapLinearFilter;
                        break;    
                    case "3":
                        filter = THREE.LinearFilter;
                        break;    
                    case "4":
                        filter = THREE.LinearMipmapNearestFilter;
                        break;   
                    case "5":
                        filter = THREE.LinearMipmapLinearFilter;
                        break;

                }
                this.renderTarget.texture.minFilter = filter;
                this.renderTarget.texture.needsUpdate = true;

                dynamicsPostFX.renderTarget.texture.minFilter = filter;
                dynamicsPostFX.renderTarget.texture.needsUpdate = true;

            break;

            case "encoding": 
                switch (param.value) {

                    case "0":
                        this.renderTarget.texture.encoding = THREE.LinearEncoding;
                        break;
                    case "1":
                        this.renderTarget.texture.encoding = THREE.sRGBEncoding;
                        break;       
                    case "2":
                        this.renderTarget.texture.encoding = THREE.BasicDepthPacking ;
                        break;    
                    case "3":
                        this.renderTarget.texture.encoding = THREE.RGBADepthPacking;
                        break;    
             
                }

                this.renderTarget.texture.needsUpdate = true;

            break;

            case "format": 
                switch (param.value) {

                    case "0":
                        this.renderTarget.texture.format = THREE.AlphaFormat;
                        break;
                    case "1":
                        this.renderTarget.texture.format = THREE.RedFormat;
                        break;       
                    case "2":
                        this.renderTarget.texture.format = THREE.RedIntegerFormat ;
                        break;    
                    case "3":
                        this.renderTarget.texture.format = THREE.RGFormat;
                        break;   
                    case "4":
                        this.renderTarget.texture.format = THREE.RGIntegerFormat;
                        break;
                    case "5":
                        this.renderTarget.texture.format = THREE.RGBAFormat;
                        break;       
                    case "6":
                        this.renderTarget.texture.format = THREE.LuminanceFormat ;
                        break;    
                    case "7":
                        this.renderTarget.texture.format = THREE.LuminanceAlphaFormat;
                        break;  
                    case "8":
                        this.renderTarget.texture.format = THREE.DepthFormat ;
                        break;    
                    case "9":
                        this.renderTarget.texture.format = THREE.DepthStencilFormat;
                        break;  
             
                }

                this.renderTarget.texture.needsUpdate = true;

            break;

            case "blurDepth": 

                this.setBlurDepthForMaterials( param );
                break;
            
            case "fxaaDepth": 

            this.fxaa.enabled = param.value;
            break;

            case "reduceMul": 

            this.fxaa.uniforms.reduceMul.value = param.value;
            break;

            case "reduceMin": 

            this.fxaa.uniforms.reduceMin.value = param.value;

            break;

            case "resMult": 

            this.fxaa.uniforms[ "resMult" ].value.set(param.value,param.value );
            break;
            
            case "denoiseDepthNew": 

                // denoiseDepthNew = param.value;

                // if ( denoiseDepthNew ) {
                //     this.denoiseNew.enabled = true;
                // } else {
                //     this.denoiseNew.enabled = false;
                // }
             
            break;
            
            case "sharpEdgeWidth": 

                this.glslSharpEdge.uniforms[ "sharpEdgeWidth" ].value = param.value;
            break;

            case "sharpEdge":

                this.glslSharpEdge.enabled = param.value;
            break;

            case "edge":

                this.glslEdgeFF.enabled = param.value;
               // edgeCutDisplacement.enabled = param.value;
            break;

            

            case "edgeCleanWidth": 

                this.glslEdgeFF.uniforms[ "edgeCleanWidth" ].value = param.value;
                //edgeCutDisplacement.uniforms[ "edgeCleanWidth" ].value = param.value;

            break;

            case "edgeThreshold": 

                this.glslEdgeFF.uniforms[ "edgeThreshold" ].value = param.value;
                //edgeCutDisplacement.uniforms[ "edgeThreshold" ].value = param.value;
            break;
            
            case "maxThresholdFactor": 

                this.glslEdgeFF.uniforms[ "maxThresholdFactor" ].value = param.value;
               // edgeCutDisplacement.uniforms[ "maxThresholdFactor" ].value = param.value;
            break;

            case "maxSteps": 

                this.glslEdgeFF.uniforms[ "maxSteps" ].value = param.value;
               // edgeCutDisplacement.uniforms[ "maxSteps" ].value = param.value;
            break;
            
            
            case "holeFillingRay":

                this.glslHoleFillingRay.enabled = param.value;
                break;
           
            // case "holeFillingCircle":

            //     this.glslHoleFillingCircle.enabled = param.value;
            // break;
            
            case "hf_sampleCount":
                // this.glslHoleFillingCircle.uniforms[ "hf_sampleCount" ].value = param.value;
                this.glslHoleFillingRay.uniforms[ "hf_sampleCount" ].value = param.value;
            break;
                
            case "hf_circleRadius":
               // this.glslHoleFillingCircle.uniforms[ "hf_circleRadius" ].value = param.value;
                this.glslHoleFillingRay.uniforms[ "hf_circleRadius" ].value = param.value;
            break;

            case "hf_pixelsPerStep":
                this.glslHoleFillingRay.uniforms[ "hf_pixelsPerStep" ].value = param.value;
            break;
            // case "holeFilling":

            //     this.glslHoleFilling.enabled = param.value;
            //     break;
            // case "holeFilling5x5":

            //     this.glslHoleFilling.uniforms[ "hf_mode5x5" ].value = param.value;
            // break;
                
            // case "holeFillingHorizontal":

            //     this.glslHoleFillingHorizontal.enabled = param.value;
            //     break;
            

            // case "hf_mode5x5":

            //     this.glslHoleFilling.uniforms.hf_mode5x5.value = param.value;
                
            //     break;
            
            // case "hf_radius":

            //     // this.glslHoleFilling.uniforms.hf_radius.value = param.value;
            //     this.glslHoleFillingHorizontal.uniforms[ "hf_radius" ].value = param.value;
            //     console.log(param.value);
            //     break;
            
            
            case "holeFillingConv":

                this.glslHoleFillingConv.enabled = param.value;
                break;

            case "hf_denominator":

                this.glslHoleFillingConv.uniforms.hf_denominator.value = newValue;
                break;

            case "hf_offset":

                this.glslHoleFillingConv.uniforms.hf_offset.value = newValue;
                break;
            
            case "hf_min_defined":

                this.glslHoleFillingConv.uniforms.hf_min_defined.value = newValue;
                break;

            case "blurAmount":
                this.hblur.uniforms.h.value = newValue / this.width;
                this.vblur.uniforms.v.value = newValue / this.height;

                // if (newValue < 0.01) this.hblur.enabled = false; else this.hblur.enabled = true;
                // if (newValue < 0.01) this.vblur.enabled = false; else this.vblur.enabled = true;

                break;

            case "edgeExtrude":

               // console.log("edgeExtrude on");
                this.savePreEdgeRenderTarget.enabled = param.value;
                this.saveEdgeRenderTarget.enabled = param.value;

                this.edgeMask.enabled = param.value;

                this.edgeExtrude.enabled = param.value;
                this.edgeMaskReset.enabled = param.value;
            
                
                this.edgeBlurH.enabled = param.value;
                this.edgeBlurV.enabled = param.value;
                sceneObjects.depthMesh.children[0].material.uniforms['edgeExtrude'].value = param.value;
                sceneObjects.depthMesh.children[1].material.uniforms['edgeExtrude'].value = param.value;

             
                if (param.value && params.depthPostprocessing.edgeExtrudeXtra.value){
                  //  console.log('edgeExtrudeExtra');
                    this.edgeMaskReset2.enabled = true;
                    this.edgeBlurH2.enabled = true;
                    this.edgeBlurV2.enabled = true;
                } else {
                    this.edgeMaskReset2.enabled = false;
                    this.edgeBlurH2.enabled = false;
                    this.edgeBlurV2.enabled = false;
                }
                
            break;
            case "edgeExtrudeXtra":

            

                if (param.value && this.edgeExtrude.enabled){
                    this.edgeMaskReset2.enabled = param.value;
                    this.edgeBlurH2.enabled = param.value;
                    this.edgeBlurV2.enabled = param.value;
                } else {
                    this.edgeMaskReset2.enabled = false;
                    this.edgeBlurH2.enabled = false;
                    this.edgeBlurV2.enabled = false;
                }
        
            break;

            case "extrudeDepth":
                this.edgeExtrude.uniforms['extrudeDepth'].value = param.value;
            break;
            case "blurEdgeAmount":
                this.edgeBlurH.uniforms[ "h" ].value = newValue / this.width;
                this.edgeBlurV.uniforms[ "v" ].value = newValue / this.height;
                this.edgeBlurH2.uniforms[ "h" ].value = newValue / this.width;
                this.edgeBlurV2.uniforms[ "v" ].value = newValue / this.height;


            break;
            case "tiltBlurAmount":
                this.hblurTilt.uniforms.h.value = newValue / this.width;
                this.vblurTilt.uniforms.v.value = newValue / this.height;

                // if (newValue < 0.01) this.hblurTilt.enabled = false; else this.hblurTilt.enabled = true;
                // if (newValue < 0.01) this.vblurTilt.enabled = false; else this.vblurTilt.enabled = true;
                
                break;

            case "tiltBlurRadius":
                this.hblurTilt.uniforms.r.value = this.vblurTilt.uniforms.r.value = newValue;
                break;

            case "barrelAlpha":
                //this.barrelDistortionCorrection.uniforms.alpha.value = newValue;
                //setDepthProcRTMaterialsUniform( "barrelAlpha", { "value": newValue } );
                break;

            case "barrelScale":
                //this.barrelDistortionCorrection.uniforms.scale.value = newValue;
                //setDepthProcRTMaterialsUniform( "barrelScale", { "value": newValue } );
                break;
   
            case "preprocessDepthWithEcho":
                // if(param.value && !echoPostFX_DEPTH.doRender ){
                //    echoPostFX_DEPTH.replaceTextureArray();
                //    echoPostFX_IR.replaceTextureArray();
                //    echoPostFX_COLOR.replaceTextureArray();
                // } 

                if(param.value && !echoPostFX_DEPTH.doRender ){
                    echoPostFX_DEPTH.replaceTextureArray();
                    echoPostFX_IR.replaceTextureArray();
                    echoPostFX_COLOR.replaceTextureArray();

                    echoPostFX_DEPTH.cleanTextureArray();
                    echoPostFX_IR.cleanTextureArray();
                    echoPostFX_COLOR.cleanTextureArray();       

                 } 
 

                echoPostFX_DEPTH.doRender = param.value;

                // route echoPostFX_DEPTH.texture as input to depthPostFX first pass
                
                if (echoPostFX_DEPTH.doRender) {
                                       

                    if (preprocessDepthWithReactionDiffusion) {

                        rdMaterial.uniforms["tDiffuse2"].value = echoPostFX_DEPTH.renderTarget.texture;

                        depthPostFX.texturePass.map = rdRenderTarget2.texture;

                    } else {

                        depthPostFX.texturePass.map = echoPostFX_DEPTH.renderTarget.texture;

                    }

                } else {

                    if (preprocessDepthWithReactionDiffusion) {

                        rdMaterial.uniforms["tDiffuse2"].value = glslProcRenderTarget_depth.texture;

                        depthPostFX.texturePass.map = rdRenderTarget2.texture;

                    } else {

                        depthPostFX.texturePass.map = glslProcRenderTarget_depth.texture;

                    }

                }                

                break;

            case "preprocess_IR_WithEcho":

                echoPostFX_IR.doRender = param.value;

                break;

            case "preprocess_Color_WithEcho":

                echoPostFX_COLOR.doRender = param.value;

                break;

            case "preprocessDepthWithReactionDiffusion":


                preprocessDepthWithReactionDiffusion = param.value; 

                // route renderTargetDepthEchoPreprocess.texture as input to depthPostFX first pass
                
                if (preprocessDepthWithReactionDiffusion) {
                    
                    depthPostFX.texturePass.map = rdRenderTarget2.texture;

                    if (echoPostFX_DEPTH.doRender) {

                        rdMaterial.uniforms["tDiffuse2"].value = echoPostFX_DEPTH.renderTarget.texture;
                    
                    } else {

                        rdMaterial.uniforms["tDiffuse2"].value = glslProcRenderTarget_depth.texture;

                    }    

                    
                } else {

                    if (echoPostFX_DEPTH.doRender) {
                    
                        depthPostFX.texturePass.map = echoPostFX_DEPTH.renderTarget.texture;

                    } else {

                        depthPostFX.texturePass.map = glslProcRenderTarget_depth.texture;

                    }    

                }

            break;
            
            // case "exponent":
            //     console.log("exponent",  param.value);

            //     this.denoiseNew.uniforms.exponent.value = param.value;

            //     //OLD STYLE
            //  //   preprocessScene.children[0].material.uniforms.exponent.value = param.value;
      
            // break;

            // case "denoiseAmount":

            //     this.denoiseNew.uniforms.amount.value = param.value;
                
            //     //OLD STYLE
            //     //preprocessScene.children[0].material.uniforms.amount.value = param.value;

            // break;

            case "fxaaRadius":

                this.fxaa.uniforms.fxaaRadius.value = param.value;
                //this.fxaa2.uniforms.fxaaRadius.value = param.value;
                
                //OLD STYLE
                //preprocessScene.children[0].material.uniforms.amount.value = param.value;

            break;



            // case "sampleRadius":
       
            //     this.denoiseNew.uniforms.sampleRadius.value = param.value;    

            //     //OLD STYLE
            //   //  preprocessScene.children[0].material.uniforms.sampleRadius.value = param.value;
 
            // break;

            case "shaderProgram":

                initReaction_diffusion(param.value);
                break;

            case "RD_numIterations": 

                rdNumIterations = param.value;

                break;

            case "rd_stimulationStrength": 

                if (rdMaterial.uniforms["stimulationStrength"] !== undefined) {
                    rdMaterial.uniforms["stimulationStrength"].value = param.value;
                } 

                break;

            case "rd_stimulationStrengthHandPointers":  

                if (rdMaterial.uniforms["stimulationStrengthHandPointers"] !== undefined) {
                    rdMaterial.uniforms["stimulationStrengthHandPointers"].value = param.value;
                }

                break;

            case "RD_useDepth": 
                if (param.value === true) {
                    rdMaterial.uniforms["tDiffuse2"].value = glslProcRenderTarget_depth.texture;
                    
                    //switchKinectStreams(true, false, false, false);
                }
                else {
                    //rdMaterial.uniforms["tDiffuse2"].value = infrared_texture;
                    //switchKinectStreams(false, true, false, false);
                }
                break;

            case "RD_slow": 
                
                if (param.value)
                    rdMaterial.uniforms["slow"].value = 1.0;
                else
                    rdMaterial.uniforms["slow"].value = 0.0;
                
                break;

        
            case "maxBufferSize":

            

                echoPostFX_DEPTH.textureArrayMaxSize = param.value;
                echoPostFX_DEPTH.setSplits(echoPostFX_DEPTH.numSplits, echoPostFX_DEPTH.textureArrayMaxSize);
                echoPostFX_DEPTH.indexOfTextureToPutDataInto = 0;

                echoPostFX_IR.textureArrayMaxSize = param.value;
                echoPostFX_IR.setSplits(echoPostFX_IR.numSplits, echoPostFX_IR.textureArrayMaxSize);
                echoPostFX_IR.indexOfTextureToPutDataInto = 0;

                echoPostFX_COLOR.textureArrayMaxSize = param.value;
                echoPostFX_COLOR.setSplits(echoPostFX_COLOR.numSplits, echoPostFX_COLOR.textureArrayMaxSize);
                echoPostFX_COLOR.indexOfTextureToPutDataInto = 0;           
                
                echoPostFX_DEPTH.cleanTextureArray();
                echoPostFX_IR.cleanTextureArray();
                echoPostFX_COLOR.cleanTextureArray();       


                break;

            case "numSplits":

                echoPostFX_DEPTH.numSplits = param.value;
                echoPostFX_DEPTH.setSplits(param.value, echoPostFX_DEPTH.textureArrayMaxSize);
                echoPostFX_DEPTH.indexOfTextureToPutDataInto = 0;

                echoPostFX_IR.numSplits = param.value;
                echoPostFX_IR.setSplits(param.value, echoPostFX_IR.textureArrayMaxSize);
                echoPostFX_IR.indexOfTextureToPutDataInto = 0;

                echoPostFX_COLOR.numSplits = param.value;
                echoPostFX_COLOR.setSplits(param.value, echoPostFX_COLOR.textureArrayMaxSize);
                echoPostFX_COLOR.indexOfTextureToPutDataInto = 0;

                echoPostFX_DEPTH.cleanTextureArray();
                echoPostFX_IR.cleanTextureArray();
                echoPostFX_COLOR.cleanTextureArray();       

                break;


            default:

                if (paramKey.indexOf('rd') !== -1) {

                    if (rdMaterial.uniforms[paramKey.substring(3)] !== undefined) {
                        rdMaterial.uniforms[paramKey.substring(3)].value = param.value;
                    } else {
                        console.warn(paramKey, "uniform is undefined");
                    }
                    
                    
                }

        }

    }


    setBlurDepthForMaterials( param ) {

        blurDepth = param.value;

        if ( blurDepth ) {

            this.hblur.enabled = true;
            this.vblur.enabled = true;
            this.hblurTilt.enabled = true;
            this.vblurTilt.enabled = true;

        } else {

            this.hblur.enabled = false;
            this.vblur.enabled = false;
            this.hblurTilt.enabled = false;
            this.vblurTilt.enabled = false;

        }
    }


}




/// reaction-diffusion setup

    var rdScene, rdCamera, rdRenderTarget1, rdRenderTarget2;
    var rd2Scene, rd2Camera, rd2RenderTarget1, rd2RenderTarget2;
    var reactionDiffusionisOn = true;
    var rdBufferToggled = false;
    var rdMaterial, rd2Material;
    var preprocessDepthWithReactionDiffusion = false;
    var rdNumIterations = 2;

    function resetReactionDiffusion( width, height ) {

        var renderTargetParameters = {
            minFilter: THREE.LinearFilter,
            magFilter: THREE.LinearFilter,
            format: THREE.RGBAFormat,
            depthBuffer: false,
            stencilBuffer: false,
            type: THREE.FloatType
        };

        rdRenderTarget1 = new THREE.WebGLRenderTarget(width, height, renderTargetParameters);
        rdRenderTarget2 = new THREE.WebGLRenderTarget(width, height, renderTargetParameters);        

    }

    function setReactionDiffusionDimensions(w, h) {

        rdRenderTarget1.setSize(w, h);
        rdRenderTarget2.setSize(w, h);

        rdMaterial.uniforms["resolution"].value = new THREE.Vector2(w, h);

        rdCamera.left = w / -2;
        rdCamera.right = w / 2;
        rdCamera.top = h / 2;
        rdCamera.bottom = h / -2;

        rdCamera.updateProjectionMatrix();

        rdScene.children[0].geometry = new THREE.PlaneBufferGeometry(w, h);
    }
   
  
    function initReaction_diffusion(shaderProgram) {

        if (shaderProgram === undefined) shaderProgram = THREE.ReactionDiffusionShader_LiquidWall;
        else shaderProgram = eval(shaderProgram);

        rdScene = new THREE.Scene();

        var renderTargetParameters = {
            minFilter: THREE.LinearFilter,
            magFilter: THREE.LinearFilter,
            format: THREE.RGBAFormat,
            depthBuffer: true,
            stencilBuffer: true,
             type: THREE.FloatType,
             anisotropy: 1,
            //type: THREE.UnsignedInt248Type
        };

        var res = new THREE.Vector2();

        res.x = DEPTH_TO_COLOR_WIDTH; res.y = DEPTH_TO_COLOR_HEIGHT;

        rdRenderTarget1 = new THREE.WebGLRenderTarget(res.x, res.y, renderTargetParameters);

        rdRenderTarget2 = new THREE.WebGLRenderTarget(res.x, res.y, renderTargetParameters);

        rdMaterial = new THREE.ShaderMaterial({
            uniforms: shaderProgram.uniforms,
            vertexShader: shaderProgram.vertexShader,
            fragmentShader: shaderProgram.fragmentShader
        });

        rdMaterial.uniforms["resolution"].value = res;

        rdMaterial.uniforms["tDiffuse2"].value = glslProcRenderTarget_depth.texture;
 
        rdCamera = new THREE.OrthographicCamera(res.x / -2,
                res.x / 2,
                res.y / 2,
                res.y / -2, -1, 1000);

        var geometry = new THREE.PlaneBufferGeometry(res.x, res.y);
        var mesh = new THREE.Mesh(geometry, rdMaterial);
        mesh.position.z = -100;

        rdScene.add(mesh);

    }

