﻿/**
 * Created by mika on 15.05.2020.
 */

var my_uniforms = THREE.UniformsUtils.merge([
        THREE.UniformsUtils.clone(THREE.ShaderLib.physical.uniforms),
        {
            "resolution": { value: new THREE.Vector2(640,576) },
            "lookupRadius": { value: 1.0 },
            "normalsRatio": { value: 1.0 },
            "normalsRatioOld": { value: 1.0 },
            "normalZ": { value: -0.001 },
            "amplitude": { value: 10.0 },
            "rippleFrequency": { value: 5.0 },
            "rippleSpeed": { value: 10.0 },
            "time": { value: 0.0 },
            "rippleAmplitude": { value: 0.0 },
            "indexTexture": { value: null },
            "velocityScale": {value:1.0},
            "xySize":{value:1.0},
            "reflectivity":{value:0.0},
            "displacementScale":{value:1.0},
            "precomputedNormals":{value:null},
            "edgeMask":{value:null},
            "onlyDepthNormals":{value:0.0},
            "cutSidePixels":{value:false},
            "refine_cutSidePixels":{value:false},
            "edgeThreshold":{value:0.0},
            "edgeSoftness":{value:0.0},
            "edgeExtrude":{value:true},
            "oldNormals":{value:true},

            "farWall":{value:0.0},
            "reverseWall":{value:false},
            
            
            "displacementVectorMap":{value:null},
            //"diffuseColorOffset":{value: new THREE.Vector4(0,0,0,0)},
            "diffuseExposure":{value:1.0},
            "diffuseColorOffset":{value:0.0},

            //"isUsingColorMap": { value: 0 },
            //"colorCoordinateTransformMatrix": { value: new THREE.Matrix4() },
            //"lensCorrection": { value: 0 },

            "edgeWidth": { value: 1.0 },
            "edgeResolutionMultiplier": { value: 1.0 },
            "edgeAlphaMultiplier": { value: 1.0 },
            
            "useSingleChannelColorMap": { value: false },

            "showNormalMap": { value: false },

            "useCustomNormal": { value: true },

            "normalsFF" : { value: true },
            "oldEdge": { value: true },

            "frenelBias": { value: 0.0 },
            "frenelPower": { value: 1.0 },
            "frenelScale": { value: 0.2 },

            "deformNormalsFix": { value: true },
            "rippleNormalScale": { value: 0.1 },
            "normalBiasShading": { value: 0.0 },
            
            

            // "cutBackground":  { value: false },
            "cutDepthThreshold":  { value: 0.1 },
            "cutPosition_W":  { value: -1.0 },

            "envZoom": { value: 1.0 },
            "envRotation": { value: 1.0 },
            "envAutoRotation": { value: false },
            "envRotationSpeed": { value: 0.2 },

            "fresnelFixScaleX": { value: 0.5 },
            "fresnelFixScaleY": { value: 0.5 },
            "fresnelFixPosX": { value: 0.5 },
            "fresnelFixPosY": { value: 0.5 },
            "cutBigPolygons": { value: false},
            "largePolyThreshold": {value: 50.0}
        }
]
    );

var smoothLightsMaterialTemplate = new THREE.ShaderMaterial({

    uniforms: my_uniforms,

    vertexShader: [

        `#define STANDARD

        varying vec3 vViewPosition;

        #ifndef FLAT_SHADED

            varying vec3 vNormal;

            #ifdef USE_TANGENT

                varying vec3 vTangent;
                varying vec3 vBitangent;

            #endif

        #endif
        `,


        THREE.ShaderChunk["common"],
        /////THREE.ShaderChunk["uv_pars_vertex"],  chunk content:
        `
        #ifdef USE_UV

            #ifdef UVS_VERTEX_ONLY


                vec2 vUv;

            #else

                varying vec2 vUv;

            #endif

            uniform mat3 uvTransform;

        #endif
        `,
        THREE.ShaderChunk["uv2_pars_vertex"],
        THREE.ShaderChunk["displacementmap_pars_vertex"],
        THREE.ShaderChunk["color_pars_vertex"],
        THREE.ShaderChunk["morphtarget_pars_vertex"],

        // "#ifndef USE_MAP",

        // "   varying vec2 vUv;",

        // "#endif",

        "uniform vec2 resolution;",

        "uniform bool useCustomNormal;",
        "uniform bool normalsFF;",
        "uniform float lookupRadius;",
        "uniform float normalsRatio;",
        "uniform float normalsRatioOld;",
        "uniform float normalZ;",
        "uniform sampler2D displacementVectorMap;",

        "uniform float farWall;",
        "uniform bool reverseWall;",

        "uniform bool cutBigPolygons;",
        "uniform float largePolyThreshold;",

        "uniform sampler2D precomputedNormals;",
        "uniform sampler2D edgeMask;",
        "uniform float onlyDepthNormals;",
        "uniform bool cutSidePixels;",

        "uniform bool refine_cutSidePixels;",

        "uniform float amplitude;",
        "uniform bool oldEdge;",

        

        "uniform float time;",
        "uniform float rippleFrequency;",
        "uniform float rippleSpeed;",
        "uniform float rippleAmplitude;",
        "varying float vTime;",

        "uniform bool deformNormalsFix;",
        "uniform float rippleNormalScale;",
        "uniform float velocityScale;",
        "uniform float maxSpeed;",
        "uniform float xySize;",
        "uniform float displacementScale;",
        "uniform float edgeThreshold;",
        "uniform float edgeSoftness;",
        "uniform bool edgeExtrude;",
        "uniform bool oldNormals;",
        "uniform float normalBiasShading;",

        // "uniform bool cutBackground;",
        "uniform float cutDepthThreshold;",
        "uniform float cutPosition_W;",

        "attribute vec3 displacement;",

        `
        vec3 biasNormal( vec3 n, float bias ){
            if(bias > 0.0){
                n = normalize(vec3(n.x,n.y,n.z*(1.0-pow(bias,0.5))));
                // if (!oldNormals){
                    n = normalize(n);
                // }
            }else {
                bias = 1.0-pow(-bias, 0.5);
                n = normalize(vec3(n.x*bias, n.y*bias, n.z));
                // if (!oldNormals){
                    n = normalize(n);
                // }
            }
            return n;
        }
        `,


        // `vec3 extractNormal(sampler2D map, vec2 tc) {

        //     const int numSteps = 6;

        //     float aspect = resolution.x/resolution.y;
        //     float dx = lookupRadius / resolution.x;
        //     float dy = dx * aspect;

        //     vec4 dx1 = vec4(0.0);
        //     vec4 dx2 = vec4(0.0);
        //     vec4 dy1 = vec4(0.0);
        //     vec4 dy2 = vec4(0.0);

        //     //for (int i=1; i < numSteps; i++) {

        //         float dxS = float(1) * dx;
        //         float dyS = float(1) * dy;

        //         dx1 += texture2D(map, tc + vec2(dxS, 0.0));
        //         dx2 += texture2D(map, tc + vec2(-dxS, 0.0));

        //         dy1 += texture2D(map, tc + vec2(0.0, dyS));
        //         dy2 += texture2D(map, tc +vec2(0.0, -dyS));

        //         dx1 += texture2D(map, tc +vec2(dxS*1.5, 0.0));
        //         dx2 += texture2D(map, tc +vec2(-dxS*1.5, 0.0));

        //         dy1 += texture2D(map, tc +vec2(0.0, dyS*1.5));
        //         dy2 += texture2D(map, tc +vec2(0.0, -dyS*1.5));

        //     //}

        //     float difX = (dx2.r - dx1.r);
        //     float difY = (dy2.r - dy1.r);


        //     return vec3(difX, difY, normalZ);
        //     //return (vec3( difX, difY, normalZ )) * 0.5 + 0.5 ;

        // }`,

        
        //"uniform sampler2D indexTexture;",

        "void main() {",

            // THREE.ShaderChunk["uv_vertex"],   chunk content:
            // `#ifdef USE_UV

            `    vUv = ( uvTransform * vec3( uv, 1 ) ).xy;`,

            // #endif
            // `,

            THREE.ShaderChunk["uv2_vertex"],
            THREE.ShaderChunk["color_vertex"],
            //////THREE.ShaderChunk["beginnormal_vertex"],   chunk content:
            `
            vec3 objectNormal = vec3( normal );

            #ifdef USE_TANGENT

                vec3 objectTangent = vec3( tangent.xyz );

            #endif
            

            

            `,

           //THREE.ShaderChunk["morphnormal_vertex"],     
            
        //    `
        //    #ifdef USE_MORPHNORMALS

        //         objectNormal *= morphTargetBaseInfluence;
        //         objectNormal += morphNormal0 * morphTargetInfluences[ 0 ];
        //         objectNormal += morphNormal1 * morphTargetInfluences[ 1 ];
        //         objectNormal += morphNormal2 * morphTargetInfluences[ 2 ];
        //         objectNormal += morphNormal3 * morphTargetInfluences[ 3 ];

        //    #endif
        //    `,

           // THREE.ShaderChunk["defaultnormal_vertex"],  
           /*            defaultnormal_vertex chunk:

            vec3 transformedNormal = objectNormal;

            #ifdef USE_INSTANCING

                // this is in lieu of a per-instance normal-matrix
                // shear transforms in the instance matrix are not supported

                mat3 m = mat3( instanceMatrix );

                transformedNormal /= vec3( dot( m[ 0 ], m[ 0 ] ), dot( m[ 1 ], m[ 1 ] ), dot( m[ 2 ], m[ 2 ] ) );

                transformedNormal = m * transformedNormal;

            #endif

            transformedNormal = normalMatrix * transformedNormal;

             */

            `
            
            // vec3 deltaNormal = normalsRatio * normalize( extractNormal( displacementMap, uv ) );

            // vec3 rippleNormal = vec3(0.0);

            // if (deformNormalsFix) {  

            //     rippleNormal += deltaNormal * amplitude * rippleNormalScale;

            //     // vertical
            //     rippleNormal += deltaNormal * sin( uv[1] * rippleFrequency + rippleSpeed * time ) * rippleAmplitude * rippleNormalScale;

            //     // horizontal (added as option)
            //     //rippleNormal += deltaNormal * sin( uv[0] * rippleFrequency + rippleSpeed * time ) * rippleAmplitude * rippleNormalScale;

            //     // DIAGONAL
            //     rippleNormal += deltaNormal * ( sin (( uv[1] + uv[0] )  * rippleFrequency + rippleSpeed * time) * rippleAmplitude) * rippleNormalScale;

            // }


           
    
            vec3 transformedNormal = objectNormal;

            ////COMPUTE NORMAL CYBERLIGHT
           // if (normalsFF){
            if (onlyDepthNormals >= 1.0){
                transformedNormal = texture2D(precomputedNormals,uv).xyz;
            } else {
                float aspect = resolution.x/resolution.y;
                float dx = lookupRadius / resolution.x;
                float dy = dx * aspect;

                //center pixel
                vec3 d = texture2D( displacementVectorMap, uv).xyz;

                vec3 d1 = texture2D( displacementVectorMap, uv - vec2(dx, 0.0)).xyz;
                vec3 d2 = texture2D( displacementVectorMap, uv + vec2(dx, 0.0)).xyz;
                vec3 d3 = texture2D( displacementVectorMap, uv - vec2(0.0, dy)).xyz;
                vec3 d4 = texture2D( displacementVectorMap, uv + vec2(0.0, dy)).xyz;
                
                float sx = 1.0;
				float sy = 1.0;

                //if neighbours are 0.0 make it the same displacement as the center
                if (cutSidePixels){
                    if ( d1.z == 0.0 ){ d1 = d; sx = 2.0;}
				    if ( d2.z == 0.0 ){ d2 = d; sx = 2.0;}
				    if ( d3.z == 0.0 ){ d3 = d; sy = 2.0;}
				    if ( d4.z == 0.0 ){ d4 = d; sy = 2.0;}
                }
 
                d1 += vec3(uv.x - dx, uv.y, 0.0)/dx;
                d2 += vec3(uv.x + dx, uv.y, 0.0)/dx;
                d3 += vec3(uv.x , uv.y - dy, 0.0)/dy;
                d4 += vec3(uv.x, uv.y + dy, 0.0)/dy;

                
                vec3 t1 = d2-d1;
                vec3 t2 = d4-d3;

                if (refine_cutSidePixels){
                   t1.z *= sx;
                   t2.z *= sy;
                }
             
              
                float nr = normalsRatio *0.25;

                // SO THAT SHADING DOESN'T CHANGE WHEN YOU CHANGE LOOKUP RADIUS
                  nr/=lookupRadius;

                transformedNormal=cross(normalize(t1),normalize(t2));
                transformedNormal=normalize(transformedNormal);
                transformedNormal *= vec3(nr,nr,1.0);

                //CLAMPING NORMAL TO FORWARDFACING
                if(transformedNormal.z < 0.0){transformedNormal.z = 0.0;}
                
                if(onlyDepthNormals>0.0){
                    //BLEND INTO CUSTOM NORMALS (lerp between them)
                    transformedNormal = onlyDepthNormals*texture2D(precomputedNormals,uv).xyz 
                                        +(1.0-onlyDepthNormals)*transformedNormal;
                }
            }

            transformedNormal.z += normalZ;
            transformedNormal=normalize(transformedNormal);
            
            if (normalBiasShading != 0.0){
                transformedNormal = biasNormal(transformedNormal,normalBiasShading);
            }


            // if ( useCustomNormal && !normalsFF ) {

            //     // transformedNormal += deltaNormal;

            // }

            vec3 deltaNormal;


            deltaNormal = transformedNormal;

            if (oldNormals){
                  deltaNormal = (normalsRatio * 0.25) * transformedNormal;
             } 
            transformedNormal = normalMatrix * transformedNormal;


            #ifdef FLIP_SIDED

                transformedNormal = - transformedNormal;

            #endif

            `,

            `#ifdef USE_TANGENT

                vec3 transformedTangent = ( modelViewMatrix * vec4( objectTangent, 0.0 ) ).xyz;

                #ifdef FLIP_SIDED

                    transformedTangent = - transformedTangent;

                #endif

            #endif`,
                                    

            

            `
            #ifndef FLAT_SHADED // Normal computed with derivatives when FLAT_SHADED

                vNormal = normalize(transformedNormal);

                #ifdef USE_TANGENT

                    vTangent = normalize( transformedTangent );
                    vBitangent = normalize( cross( vNormal, vTangent ) * tangent.w );

                #endif

            #endif
            `,

            THREE.ShaderChunk["begin_vertex"],
            //THREE.ShaderChunk["morphtarget_vertex"],
            // THREE.ShaderChunk["displacementmap_vertex"],

            `
            #ifdef USE_DISPLACEMENTMAP

               // transformed += normalize( objectNormal ) * ( texture2D( displacementMap, vUv ).x * displacementScale + displacementBias );
           
            #endif
            `,

           

            //DISPLACEMENT VECToR CYBERLIGHT
            `       
             
            vec3 displacementVector =  texture2D( displacementVectorMap, uv ).xyz;

             //vec3 displacementVector =  vec3(0.0,0.0,0.0);

             ////Take it out of color space
             //displacementVector = (displacementVector-0.5)*2.0;

             transformed+=displacementVector*velocityScale;


            `,

            // FATTENING
            `
            // fattening
            transformed += deltaNormal * amplitude;

            //RIPPLE
            // vertical
            transformed += deltaNormal * ( sin ( uv[1] * rippleFrequency + rippleSpeed * time) * rippleAmplitude);

            // horizontal (added as option)
            //transformed += deltaNormal * ( sin ( uv[0] * rippleFrequency + rippleSpeed * time) * rippleAmplitude);

            //  DIAGONAL
            transformed += deltaNormal * ( sin (( uv[1] + uv[0] )  * rippleFrequency + rippleSpeed * time) * rippleAmplitude);
           
            

            if (farWall != 0.0){
                if( reverseWall ){
                    if(transformed.z > -farWall){
                        transformed.z = -farWall;
                    }
                } else if (transformed.z < farWall){
                    transformed.z = farWall;
                }
            }
            `,


            THREE.ShaderChunk["project_vertex"],

            "vViewPosition = - mvPosition.xyz;",

            

            THREE.ShaderChunk["worldpos_vertex"],

            "vTime = time;",

            // "#ifndef USE_MAP",


            // "   vUv = uv;",

            // "#endif",


            //cut off background plane
            //"float index_x = texture2D( indexTexture, uv ).x;",            

            `
            #ifdef CUT_BACKGROUND
                
                
               
                float d1 = texture2D( displacementVectorMap, uv ).z;

                //if ( texture2D( displacementMap, uv ).x < cutDepthThreshold) { 

                if ( d1 / displacementScale < cutDepthThreshold) { 
                    //cutting background
                    gl_Position.w = -1.0;
                    gl_Position.z = 65000.0;  // fix for orthographic camera
                }  else if (cutBigPolygons) {

                    //cutting large polygons 
                    float aspect = resolution.x/resolution.y;
                    float dx = lookupRadius / resolution.x;
                    float dy = dx * aspect;

                    float d2 = texture2D(displacementVectorMap, uv+vec2(dx,0.0)).z;

                    bool cut = false;

                    if (abs(d2-d1) > largePolyThreshold ){ 
                        cut = true;
                    } else {
                        d2 = texture2D(displacementVectorMap, uv+vec2(-dx,0.0)).z;
                        if (abs(d2-d1) > largePolyThreshold ){ 
                            cut = true;
                        } else {
                            d2 = texture2D(displacementVectorMap, uv+vec2(0.0,dy)).z;
                            if (abs(d2-d1) > largePolyThreshold ){ 
                                cut = true;
                            } else {
                                d2 = texture2D(displacementVectorMap, uv+vec2(0.0,-dy)).z;
                                if (abs(d2-d1) > largePolyThreshold ){ 
                                    cut = true;
                                }
                            }
                        }
                    }
                    if (cut){
                        gl_Position.w = -1.0;
                        gl_Position.z = 65000.0;  // fix for orthographic camera
                    }
                    
                }

            #endif
            `,

        "}"
    ].join("\n"),

    fragmentShader: [

        "#define STANDARD",

        `
        #ifdef PHYSICAL
            #define REFLECTIVITY
            #define CLEARCOAT
            #define TRANSPARENCY
        #endif
        `,

        "uniform vec3 diffuse;",
        "uniform vec3 emissive;",
        "uniform float roughness;",
        "uniform float metalness;",
        "uniform float opacity;",
        "uniform float edgeThreshold;",
        "uniform float edgeSoftness;",
        "uniform bool edgeExtrude;",
        "uniform sampler2D edgeMask;",

        "uniform float diffuseColorOffset;",
        "uniform float diffuseExposure;",

//======= <my
        //"uniform bool isUsingColorMap;",
        //"uniform mat4 colorCoordinateTransformMatrix;",
        //"uniform bool lensCorrection;",

        "uniform bool useSingleChannelColorMap;",

        "uniform sampler2D displacementMap;",

        "uniform vec2 resolution;",

        "uniform bool showNormalMap;",
        "uniform bool oldEdge;",

        "uniform float frenelBias;",
        "uniform float frenelPower;",
        "uniform float frenelScale;",
        "uniform float displacementScale;",

        "uniform float edgeWidth;",
        "uniform float edgeAlphaMultiplier;",
        "uniform float edgeResolutionMultiplier;",

        // "uniform bool cutBackground;",
        "uniform float envZoom;",
        "uniform float envRotation;",
        "uniform bool envAutoRotation;",
        "uniform float envRotationSpeed;",

        "varying float vTime;",

        "uniform float fresnelFixScaleX;",
        "uniform float fresnelFixScaleY;",
        "uniform float fresnelFixPosX;",
        "uniform float fresnelFixPosY;",
        "uniform sampler2D displacementVectorMap;",
     
//======= my>

        `
            mat3 G[2];

            const mat3 g0 = mat3( 1.0, 2.0, 1.0, 0.0, 0.0, 0.0, -1.0, -2.0, -1.0 );
            const mat3 g1 = mat3( 1.0, 0.0, -1.0, 2.0, 0.0, -2.0, 1.0, 0.0, -1.0 );

         

            float edge( in vec2 tc ) {

                float dfac = 1.0/displacementScale;

                mat3 I;
                float cnv[2];
                
                G[0] = g0;
                G[1] = g1;

                vec2 texel = vec2( edgeWidth * 1.0 / 640.0 * edgeResolutionMultiplier, edgeWidth * 1.0 / 360.0 *edgeResolutionMultiplier);

                // fetch the 3x3 neighbourhood and use the RGB vector's length as intensity value 
                for (float i=0.0; i<3.0; i++)
                    for (float j=0.0; j<3.0; j++) {
                        
                        float sample_pixel = texture2D( displacementVectorMap, tc + texel * vec2(i-1.0,j-1.0) ).z*dfac;
                        //float sample_pixel = texture2D( displacementMap, tc + texel * vec2(i-1.0,j-1.0) ).x;
                        I[int(i)][int(j)] = sample_pixel;
                    }

                // calculate the convolution values for all the masks
                for (int i=0; i<2; i++) {
                    float dp3 = dot(G[i][0], I[0]) + dot(G[i][1], I[1]) + dot(G[i][2], I[2]);
                    
                    cnv[i] = dp3 * dp3;
                    
                }

                return 0.5 * sqrt(cnv[0] * cnv[0] + cnv[1] * cnv[1]);

            }

            
        `,

        `
        #ifdef TRANSPARENCY
            uniform float transparency;
        #endif

        #ifdef REFLECTIVITY
            uniform float reflectivity;
        #endif

        #ifdef CLEARCOAT
            uniform float clearcoat;
            uniform float clearcoatRoughness;
        #endif

        #ifdef USE_SHEEN
            uniform vec3 sheen;
        #endif
        `,


        "varying vec3 vViewPosition;",

        "#ifndef FLAT_SHADED",

        "   varying vec3 vNormal;",
        `
        	#ifdef USE_TANGENT

                varying vec3 vTangent;
                varying vec3 vBitangent;

            #endif
        `,

        "#endif",

        THREE.ShaderChunk["common"],
        THREE.ShaderChunk["packing"],
        THREE.ShaderChunk["color_pars_fragment"],
        // THREE.ShaderChunk["uv_pars_fragment"],   chunk content:
        `
        #if ( defined( USE_UV ) && ! defined( UVS_VERTEX_ONLY ) )

            varying vec2 vUv;

        #endif
        `,
        THREE.ShaderChunk["uv2_pars_fragment"],
        THREE.ShaderChunk["map_pars_fragment"],
        

        THREE.ShaderChunk["lightmap_pars_fragment"],
        THREE.ShaderChunk["emissivemap_pars_fragment"],


        THREE.ShaderChunk["bsdfs"],
        THREE.ShaderChunk["cube_uv_reflection_fragment"],

        THREE.ShaderChunk["envmap_common_pars_fragment"],
        //THREE.ShaderChunk["envmap_physical_pars_fragment"],
        
        `
        #if defined( USE_ENVMAP ) 

            #ifdef ENVMAP_MODE_REFRACTION
                uniform float refractionRatio;
            #endif

            vec3 getLightProbeIndirectIrradiance( /*const in SpecularLightProbe specularLightProbe,*/ const in GeometricContext geometry, const in int maxMIPLevel ) {

                vec3 worldNormal = inverseTransformDirection( geometry.normal, viewMatrix );

                #ifdef ENVMAP_TYPE_CUBE

                    vec3 queryVec = vec3( flipEnvMap * worldNormal.x, worldNormal.yz );

                    // TODO: replace with properly filtered cubemaps and access the irradiance LOD level, be it the last LOD level
                    // of a specular cubemap, or just the default level of a specially created irradiance cubemap.

                    #ifdef TEXTURE_LOD_EXT

                        vec4 envMapColor = textureCubeLodEXT( envMap, queryVec, float( maxMIPLevel ) );

                    #else

                        // force the bias high to get the last LOD level as it is the most blurred.
                        vec4 envMapColor = textureCube( envMap, queryVec, float( maxMIPLevel ) );

                    #endif

                    envMapColor.rgb = envMapTexelToLinear( envMapColor ).rgb;

                #elif defined( ENVMAP_TYPE_CUBE_UV )

                    vec4 envMapColor = textureCubeUV( envMap, worldNormal, 1.0 );

                #else

                    vec4 envMapColor = vec4( 0.0 );

                #endif

                return PI * envMapColor.rgb * envMapIntensity;

            }

            // Trowbridge-Reitz distribution to Mip level, following the logic of http://casual-effects.blogspot.ca/2011/08/plausible-environment-lighting-in-two.html
            float getSpecularMIPLevel( const in float roughness, const in int maxMIPLevel ) {

                float maxMIPLevelScalar = float( maxMIPLevel );

                float sigma = PI * roughness * roughness / ( 1.0 + roughness );
                float desiredMIPLevel = maxMIPLevelScalar + log2( sigma );

                // clamp to allowable LOD ranges.
                return clamp( desiredMIPLevel, 0.0, maxMIPLevelScalar );

            }

            vec3 getLightProbeIndirectRadiance( /*const in SpecularLightProbe specularLightProbe,*/ const in vec3 viewDir, const in vec3 normal, const in float roughness, const in int maxMIPLevel ) {

                #ifdef ENVMAP_MODE_REFLECTION

                    vec3 reflectVec = reflect( -viewDir, normal );

                        // Cromatic abberations -- mika
                        vec3 reflectVec_r = reflect( -viewDir, normal + vec3(0.0, 0.0, 0.0) );
                        vec3 reflectVec_g = reflect( -viewDir, normal + vec3(-0.001 * frenelPower, -0.001 * frenelPower, 0.0) );
                        vec3 reflectVec_b = reflect( -viewDir, normal + vec3(0.001 * frenelPower, 0.001 * frenelPower, 0.0) );

                    // Mixing the reflection with the normal is more accurate and keeps rough objects from gathering light from behind their tangent plane.
                    reflectVec = normalize( mix( reflectVec, normal, roughness * roughness) );

                #else

                    vec3 reflectVec; // = refract( -viewDir, normal, refractionRatio );
                       

                        // Cromatic abberations -- mika

                        vec3 reflectVec_r, reflectVec_g, reflectVec_b;
                        vec3 IoR_Values;

                        #ifdef ENVMAP_FLAT

                            IoR_Values = vec3(1.0 - 20.0 * frenelPower * 0.02, 1.0 - 20.0 * frenelPower * 0.01 , 1.0);
                            IoR_Values *= refractionRatio;

                            reflectVec_r.xy = ((vUv - 0.5) * envZoom + 0.5) + pow(normal.xy, vec2(2.0)) * (1.0 - refractionRatio) * 1.0 * IoR_Values.x;
                            reflectVec_g.xy = ((vUv - 0.5) * envZoom + 0.5) + pow(normal.xy, vec2(2.0)) * (1.0 - refractionRatio) * 1.0 * IoR_Values.y;
                            reflectVec_b.xy = ((vUv - 0.5) * envZoom + 0.5) + pow(normal.xy, vec2(2.0)) * (1.0 - refractionRatio) * 1.0 * IoR_Values.z;

                        #else

                            IoR_Values = vec3(1.0 - frenelPower * 0.02, 1.0 - frenelPower * 0.01 , 1.0);
                            IoR_Values *= refractionRatio;

                            reflectVec_r = refract(-viewDir, normal, IoR_Values.x);
                            reflectVec_g = refract(-viewDir, normal, IoR_Values.y);
                            reflectVec_b = refract(-viewDir, normal, IoR_Values.z);

                        #endif
                        

                #endif

                reflectVec = inverseTransformDirection( reflectVec, viewMatrix );

                    #ifndef ENVMAP_FLAT

                        reflectVec_r = inverseTransformDirection( reflectVec_r, viewMatrix );
                        reflectVec_g = inverseTransformDirection( reflectVec_g, viewMatrix );
                        reflectVec_b = inverseTransformDirection( reflectVec_b, viewMatrix );

                    #endif

                float specularMIPLevel = getSpecularMIPLevel( roughness, maxMIPLevel );

                #ifdef ENVMAP_TYPE_CUBE
                    vec3 queryReflectVec = vec3( flipEnvMap * reflectVec.x, reflectVec.yz );
                    #ifdef TEXTURE_LOD_EXT
                        vec4 envMapColor = textureCubeLodEXT( envMap, queryReflectVec, specularMIPLevel );
                    #else
                        vec4 envMapColor = textureCube( envMap, queryReflectVec, specularMIPLevel );
                    #endif
                    envMapColor.rgb = envMapTexelToLinear( envMapColor ).rgb;
                #elif defined( ENVMAP_TYPE_CUBE_UV )
                    vec4 envMapColor = textureCubeUV( envMap, reflectVec, roughness );

                #elif defined( ENVMAP_TYPE_EQUIREC )

                    vec2 sampleUV;

                        vec2 sampleUV_r, sampleUV_g, sampleUV_b;
                    
                    float envRotationX = envRotation;
                    if (envAutoRotation == true) { envRotationX += vTime * 0.02 * envRotationSpeed; };

                    sampleUV.y = asin( clamp( reflectVec.y  * envZoom, - 1.0, 1.0 ) ) * RECIPROCAL_PI + 0.5;
			        sampleUV.x = ( atan( reflectVec.z, reflectVec.x * envZoom ) + envRotationX ) * RECIPROCAL_PI2 + 0.5;

                        #ifdef ENVMAP_FLAT
                            

                        #else

                            sampleUV_r.y = asin( clamp( reflectVec_r.y  * envZoom, - 1.0, 1.0 ) ) * RECIPROCAL_PI + 0.5;
                            sampleUV_r.x = ( atan( reflectVec_r.z, reflectVec_r.x * envZoom ) + envRotationX ) * RECIPROCAL_PI2 + 0.5;

                            sampleUV_g.y = asin( clamp( reflectVec_g.y  * envZoom, - 1.0, 1.0 ) ) * RECIPROCAL_PI + 0.5;
                            sampleUV_g.x = ( atan( reflectVec_g.z, reflectVec_g.x * envZoom ) + envRotationX ) * RECIPROCAL_PI2 + 0.5;

                            sampleUV_b.y = asin( clamp( reflectVec_b.y  * envZoom, - 1.0, 1.0 ) ) * RECIPROCAL_PI + 0.5;
                            sampleUV_b.x = ( atan( reflectVec_b.z, reflectVec_b.x * envZoom ) + envRotationX ) * RECIPROCAL_PI2 + 0.5;

                        #endif

                    #ifdef TEXTURE_LOD_EXT

                        vec4 envMapColor;   // = texture2DLodEXT( envMap, sampleUV, specularMIPLevel );

                            #ifdef ENVMAP_FLAT                            

                                envMapColor.r = texture2D( envMap, reflectVec_r.xy ).x;
                                envMapColor.g = texture2D( envMap, reflectVec_g.xy ).y;
                                envMapColor.b = texture2D( envMap, reflectVec_b.xy ).z;

                            #else                            

                                envMapColor.r = texture2DLodEXT( envMap, sampleUV_r, specularMIPLevel ).x;
                                envMapColor.g = texture2DLodEXT( envMap, sampleUV_g, specularMIPLevel ).y;
                                envMapColor.b = texture2DLodEXT( envMap, sampleUV_b, specularMIPLevel ).z;

                            #endif

                    #else

                        vec4 envMapColor = texture2D( envMap, sampleUV, specularMIPLevel );                        


                    #endif

                    envMapColor.rgb = envMapTexelToLinear( envMapColor ).rgb;


                #elif defined( ENVMAP_TYPE_SPHERE )
                    vec3 reflectView = normalize( ( viewMatrix * vec4( reflectVec, 0.0 ) ).xyz + vec3( 0.0,0.0,1.0 ) );
                    #ifdef TEXTURE_LOD_EXT
                        vec4 envMapColor = texture2DLodEXT( envMap, reflectView.xy * 0.5 + 0.5, specularMIPLevel );
                    #else
                        vec4 envMapColor = texture2D( envMap, reflectView.xy * 0.5 + 0.5, specularMIPLevel );
                    #endif
                    envMapColor.rgb = envMapTexelToLinear( envMapColor ).rgb;
                #endif

                return envMapColor.rgb * envMapIntensity;

            }

        #endif


        `,
        

        THREE.ShaderChunk["lights_pars_begin"],
        THREE.ShaderChunk["lights_physical_pars_fragment"],

        THREE.ShaderChunk["bumpmap_pars_fragment"],

        THREE.ShaderChunk["normalmap_pars_fragment" ],

        THREE.ShaderChunk["clearcoat_pars_fragment" ],

        THREE.ShaderChunk["roughnessmap_pars_fragment"],
        THREE.ShaderChunk["metalnessmap_pars_fragment"],

        // custom fresnel/ chromatic aberration --- mika            

        "#if defined( USE_ENVMAP ) && !defined( ENVMAP_MODE_REFLECTION )",

            "float fast_fresnel(vec3 I, vec3 N, vec3 fresnelValues)",
            "{",
            "    float bias = fresnelValues.x;",
            "    float power = fresnelValues.y;",
            "    float scale = 1.0 - bias;",
            "    return bias + pow(1.0 - dot(I, N), power) * scale;",
            "}",

            "vec3 getFresnel( sampler2D envMap, vec3 normal, vec3 eye ) {",

                //------ normalize incoming vectors

                "vec3 N = normalize(normal);",
                "vec3 I = normalize(eye);",

                //------ Find the reflection

                //"vec3 reflVec = normalize( reflect(-I, N) );",
                //"vec2 sampleUV;",
                //"sampleUV.y = saturate( reflVec.y * 0.5 + 0.5 );",
                //"sampleUV.x = atan( reflVec.z, reflVec.x ) * RECIPROCAL_PI2 + 0.5;",
                //"vec3 reflectColor = texture2D(envMap, sampleUV).xyz;",

                //------ Find the refraction

                "vec3 refractColor;",

                "vec3 IoR_Values = vec3(1.0 - frenelPower * 0.02, 1.0 - frenelPower * 0.01 , 1.0);",

                "IoR_Values *= refractionRatio;",

                "vec3 refractVec_r = refract(-I, N, IoR_Values.x);",
                "vec3 refractVec_g = refract(-I, N, IoR_Values.y);",
                "vec3 refractVec_b = refract(-I, N, IoR_Values.z);",

                "vec2 refractUV_r, refractUV_g, refractUV_b;",
                "refractUV_r.y = saturate( refractVec_r.y * 0.5 + 0.5 );",
                "refractUV_r.x = atan( refractVec_r.z, refractVec_r.x ) * RECIPROCAL_PI2 + 0.5;",

                "refractUV_g.y = saturate( refractVec_g.y * 0.5 + 0.5 );",
                "refractUV_g.x = atan( refractVec_g.z, refractVec_g.x ) * RECIPROCAL_PI2 + 0.5;",

                "refractUV_b.y = saturate( refractVec_b.y * 0.5 + 0.5 );",
                "refractUV_b.x = atan( refractVec_b.z, refractVec_b.x ) * RECIPROCAL_PI2 + 0.5;",

                "refractColor.x = texture2D(envMap, refractUV_r).x;",
                "refractColor.y = texture2D(envMap, refractUV_g).y;",
                "refractColor.z = texture2D(envMap, refractUV_b).z;",

                "return refractColor;",
            "}",

            "vec3 getFresnelFlat( sampler2D envMap, vec3 normal, vec3 eye ) {",

                //------ normalize incoming vectors

                "vec3 N = normalize(normal);",
                "vec3 I = normalize(eye);", 

                //------ Find the refraction

                "vec3 refractColor;",

                "vec3 IoR_Values = vec3(1.0 - 20.0 * frenelPower * 0.02, 1.0 - 20.0 * frenelPower * 0.01 , 1.0);",

                "IoR_Values *= refractionRatio;",

                "refractColor.x = texture2D( envMap, ((vUv - 0.5) * envZoom + 0.5) + pow(normal.xy, vec2(2.0)) * (1.0 - refractionRatio) * 1.0 * IoR_Values.x ).x;",
                "refractColor.y = texture2D( envMap, ((vUv - 0.5) * envZoom + 0.5) + pow(normal.xy, vec2(2.0)) * (1.0 - refractionRatio) * 1.0 * IoR_Values.y ).y;",
                "refractColor.z = texture2D( envMap, ((vUv - 0.5) * envZoom + 0.5) + pow(normal.xy, vec2(2.0)) * (1.0 - refractionRatio) * 1.0 * IoR_Values.z ).z;",

                "return refractColor;",
            "}",

        "#endif",

        `
        //OLD NORMALS CALCULATION
        // const float dx = 1.0 / 1280.0;
        // const float dy = 1.0 / 720.0;
        // const int numSteps = 6;

        // vec3 extractNormal(sampler2D map, vec2 tc) {
        
        //     vec4 dx1 = vec4(0.0);
        //     vec4 dx2 = vec4(0.0);
        //     vec4 dy1 = vec4(0.0);
        //     vec4 dy2 = vec4(0.0);

        //     for (int i=1; i < numSteps; i++) {

        //         float dxS = float(i) * dx;
        //         float dyS = float(i) * dy;

        //         dx1 += texture2D(map, tc +vec2(dxS, 0.0));
        //         dx2 += texture2D(map, tc +vec2(-dxS, 0.0));

        //         dy1 += texture2D(map, tc +vec2(0.0, dyS));
        //         dy2 += texture2D(map, tc +vec2(0.0, -dyS));

        //     }

        //     float difX = (dx2.x - dx1.x);
        //     float difY = (dy2.x - dy1.x);


        //     return vec3(difX, difY, 1.0);
        //     //return (vec3( difX, difY, 1.0 )) * 0.5 + 0.5;

        // }`,

        "void main() {",

            //THREE.ShaderChunk[ "clipping_planes_fragment" ],

            "vec4 diffuseColor = vec4( diffuse, opacity );",
            "ReflectedLight reflectedLight = ReflectedLight( vec3( 0.0 ), vec3( 0.0 ), vec3( 0.0 ), vec3( 0.0 ) );",
            "vec3 totalEmissiveRadiance = emissive;",

            /////THREE.ShaderChunk["map_fragment"], chunk content:
            "#ifdef USE_MAP",

            "    vec2 vUv_transformed = vUv;",

            //"    if (isUsingColorMap) {",
            //"       vUv_transformed = vec2( ((vUv.x - 0.5) * 0.75) + 0.5, ((vUv.y - 0.5) * 0.95) + 0.5 );",
            // "       if (lensCorrection) { ",
            // "          vec4 transf = vec4( vUv, 1.0, 1.0) * colorCoordinateTransformMatrix;",
            // "          vUv_transformed = transf.xy;",
            // "       };", 
            //"    } else { vUv_transformed = vUv; }",

            "    vec4 texelColor = texture2D( map, vUv_transformed ).xyzw;",
            // "    if ( useSingleChannelColorMap ) texelColor.rgb = texelColor.rrr;",
  
            "    texelColor = mapTexelToLinear( texelColor );",
            "    diffuseColor *= texelColor;",

            "    diffuseColor *= vec4(diffuseExposure,diffuseExposure,diffuseExposure,1.0);",
            "    diffuseColor += vec4(diffuseColorOffset,diffuseColorOffset,diffuseColorOffset,0.0);",

            

            "#endif",


            THREE.ShaderChunk["color_fragment"],
            THREE.ShaderChunk["alphatest_fragment"],

            THREE.ShaderChunk["roughnessmap_fragment"],
            THREE.ShaderChunk["metalnessmap_fragment"],

        
            THREE.ShaderChunk["normal_fragment_begin"],

            ////// THREE.ShaderChunk["normal_fragment_maps"], chunk content:
            `
            #ifdef OBJECTSPACE_NORMALMAP

                normal = texture2D( normalMap, vUv ).xyz * 2.0 - 1.0; // overrides both flatShading and attribute normals

                #ifdef FLIP_SIDED

                    normal = - normal;

                #endif

                #ifdef DOUBLE_SIDED

                    normal = normal * ( float( gl_FrontFacing ) * 2.0 - 1.0 );

                #endif

                normal = normalize( normalMatrix * normal );

            #elif defined( TANGENTSPACE_NORMALMAP )

                vec3 mapN = texture2D( normalMap, vUv ).xyz * 2.0 - 1.0;
                mapN.xy *= normalScale;

                #ifdef USE_TANGENT

                    normal = normalize( vTBN * mapN );

                #else

                    normal = perturbNormal2Arb( -vViewPosition, normal, mapN );

                #endif

            #elif defined( USE_BUMPMAP )

                normal = perturbNormalArb( -vViewPosition, normal, dHdxy_fwd() );

            #endif
            `,

            //"vec3 deltaNormal = extractNormal( displacementMap, vUv );",

            //"normal += normalize( deltaNormal );",

            THREE.ShaderChunk["clearcoat_normal_fragment_begin"],
            THREE.ShaderChunk["clearcoat_normal_fragment_maps"],


            THREE.ShaderChunk["emissivemap_fragment"],

            // accumulation
            THREE.ShaderChunk["lights_physical_fragment"],
            THREE.ShaderChunk["lights_fragment_begin"],
            THREE.ShaderChunk["lights_fragment_maps"],
            THREE.ShaderChunk["lights_fragment_end"],



            // //  custom fresnel/ chromatic aberration --- mika

            // "#if defined( USE_ENVMAP ) && !defined( ENVMAP_MODE_REFLECTION )",

            //     `#ifdef ENVMAP_FLAT

            //         radiance = getFresnelFlat( envMap, normal, vViewPosition );

            //     #else

            //        radiance = getFresnel( envMap, normal, vViewPosition );

            //     #endif`,
                
            //     "reflectedLight.indirectSpecular = mix(reflectedLight.indirectSpecular, radiance, frenelScale);",

            // "#endif",



            // modulation
            //THREE.ShaderChunk[ "aomap_fragment" ],

            "vec3 outgoingLight = reflectedLight.directDiffuse + reflectedLight.indirectDiffuse + reflectedLight.directSpecular + reflectedLight.indirectSpecular + totalEmissiveRadiance;",

            //THREE.ShaderChunk["envmap_fragment"],

            `
            // this is a stub for the transparency model
            #ifdef TRANSPARENCY
                diffuseColor.a *= saturate( 1. - transparency + linearToRelativeLuminance( reflectedLight.directSpecular + reflectedLight.indirectSpecular ) );
            #endif
            `,

            `            
            #ifdef CUT_BACKGROUND

                float edge_pixel = 0.0;

                if(edgeExtrude){

                    edge_pixel = texture2D( edgeMask, vUv ).x;

                    float maxThreshold = edgeThreshold + edgeSoftness;
                    if (edge_pixel < edgeThreshold){
                        edge_pixel = 1.0;
                    } else if (edge_pixel < maxThreshold){
                        edge_pixel = 1.0 - (edge_pixel - edgeThreshold)/edgeSoftness;
                    } else {
                        edge_pixel = 0.0;
                    }

                }
                
                if (oldEdge){
                    edge_pixel += edge( vUv );
                    if (edge_pixel>1.0){edge_pixel =1.0;}
                    edge_pixel *= edgeAlphaMultiplier;
                }

                gl_FragColor += vec4( outgoingLight, (1.0 - edge_pixel) * diffuseColor.a );
                


            #else

                gl_FragColor = vec4( outgoingLight, diffuseColor.a );

            #endif
            `,            
             
            THREE.ShaderChunk[ "tonemapping_fragment" ],
            THREE.ShaderChunk["encodings_fragment"],
            //THREE.ShaderChunk[ "fog_fragment" ],
            THREE.ShaderChunk["premultiplied_alpha_fragment"],

            `#ifdef PREMULTIPLIED_ALPHA
            gl_FragColor.rgb *= gl_FragColor.a;
            #endif`,
            "if ( gl_FragColor.a < 0.5 ) discard;",
            "if (showNormalMap) gl_FragColor = vec4( vNormal, gl_FragColor.a );",   //test normals

            

        "}"
    ].join("\n"),

    lights: true,
    transparent: true,

});

smoothLightsMaterialTemplate.extensions.derivatives = true;
smoothLightsMaterialTemplate.precision = "lowp";
