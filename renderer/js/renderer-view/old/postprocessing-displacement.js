var composerDisplacement;
var renderTargetDisplacement;
var texturePassDisplacement;
var vectorDisplacementMap, precomputeNormals;
var precomputedNormalsRenderTarget;
// var displacementActive = true;

function initDisplacement(renderTargetDynamics, processedDepth) {

  texturePassDisplacement = new THREE.TexturePass( processedDepth );
  var renderTargetParameters = {
      minFilter: TEXTURE_FILTER,
      magFilter: TEXTURE_FILTER,
      format: THREE.RGBAFormat,
      type: THREE.FloatType,
      // anisotropy:2,

  };

  renderTargetDisplacement = new THREE.WebGLRenderTarget(DEPTH_TO_COLOR_WIDTH, DEPTH_TO_COLOR_HEIGHT, renderTargetParameters);
 
  var copyPass = new THREE.ShaderPass(THREE.CopyShader);

  precomputedNormalsRenderTarget = new THREE.WebGLRenderTarget(DEPTH_TO_COLOR_WIDTH, DEPTH_TO_COLOR_HEIGHT, renderTargetParameters);
  savePrecomputeNormals = new THREE.SavePass(precomputedNormalsRenderTarget);
  precomputeNormals = new THREE.ShaderPass(THREE.precomputeNormalsShader, "processedDepth");   
  vectorDisplacementMap = new THREE.ShaderPass(THREE.vectorDisplacementMapShader, "precomputedNormals");   
  vectorDisplacementMap.uniforms['processedDepth'].value = processedDepth; 
  vectorDisplacementMap.uniforms['displacementVector'].value = renderTargetDynamics; 

  composerDisplacement = new THREE.EffectComposer(renderer, renderTargetDisplacement);

  composerDisplacement.renderToScreen = false;
  

  composerDisplacement.setSize( DEPTH_TO_COLOR_WIDTH, DEPTH_TO_COLOR_HEIGHT);
  
  composerDisplacement.addPass(texturePassDisplacement );

  //CREATE NORMAL MAP FOR VECTOR DISPLACEMENT MAP
  composerDisplacement.addPass(precomputeNormals);
  composerDisplacement.addPass(savePrecomputeNormals);
  composerDisplacement.addPass(vectorDisplacementMap);
  
  // for(var i = 0; i< 16; i++){
  //   composerDisplacement.addPass(fxaaDisplacement);
  // }




  composerDisplacement.addPass( copyPass );


}
