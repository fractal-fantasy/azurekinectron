
class EdgePostFX {

    width = DEPTH_TO_COLOR_WIDTH;
    height = DEPTH_TO_COLOR_HEIGHT;

    texturePass = null;
    fxComposer = null;

				
    copyPass = new THREE.ShaderPass(THREE.CopyShader);

    renderTargetParameters = {
        minFilter: TEXTURE_FILTER,
        magFilter: TEXTURE_FILTER,
        format: THREE.RGBAFormat,
        stencilBuffer: false,
        type: THREE.FloatType,
        generateMipmaps: false,
        anisotropy:1
    };
   

    renderTarget = new THREE.WebGLRenderTarget( this.width, this.height, this.renderTargetParameters );
    
    init( in_texture_depth, renderer, params ) {

        this.texturePass = new THREE.TexturePass( in_texture_depth );        

        this.fxComposer = new THREE.EffectComposer( renderer , this.renderTarget ); 
        
        this.fxComposer.renderToScreen = false;

        this.fxComposer.setSize( this.width, this.height);

    //     //1. where depth = 0 is white, else black
        this.edgeMask = new THREE.ShaderPass( edgeMaskShader ); //pass in raw depth
        
    //     //2. blur by 1 pixel
        this.edgeBlurH = new THREE.ShaderPass( THREE.HorizontalBlurShader );
        this.edgeBlurV = new THREE.ShaderPass( THREE.VerticalBlurShader );

    //    // 3.where depth = 0 set to white, else leave as the current color
        this.edgeMaskReset = new THREE.ShaderPass( edgeMaskResetShader ); //pass in edgeMask and depthMap 
        this.edgeMaskReset.uniforms.rawDepth.value=in_texture_depth;
        // iterate on last two steps

        // pass the result to extrude. Extrude simply multiplies the result from the passes by the 
        // extrudeDepth and subtracts from the depth... no need to check neighbors
  

       // motion blur depth
        this.edgeRenderTarget = new THREE.WebGLRenderTarget(this.width, this.height, this.renderTargetParameters);
        this.edgeExtrude = new THREE.ShaderPass( edgeExtrudeShader ); // extrude depth by edgemask
        this.edgeExtrude.uniforms.rawDepth.value = in_texture_depth;
        this.edgeBlurH.uniforms.h.value = params.depthPostprocessing.blurAmount.value / this.width;
        this.edgeBlurV.uniforms.v.value = params.depthPostprocessing.blurAmount.value / this.height;
        
        this.fxComposer.addPass( this.texturePass );

        //FILLET
        this.fxComposer.addPass( this.edgeMask );    
        for(var i = 0; i< 1; i++){
            this.fxComposer.addPass( this.edgeBlurH );
            this.fxComposer.addPass( this.edgeBlurV );
            this.fxComposer.addPass( this.edgeMaskReset );
        }
       this.fxComposer.addPass( this.edgeRenderTarget );
       this.fxComposer.addPass( this.edgeExtrude );
        //FILLET END


        this.fxComposer.addPass( this.copyPass );

        this.kinectTexture = this.renderTarget.texture;


    }

    setSize( width, height ) {
        
        this.width = width;
        this.height = height;

        this.edgeBlurH.uniforms[ "h" ].value.set( params.depthPostprocessing.blurAmount.value / this.width );
        this.edgeBlurV.uniforms[ "v" ].value.set( params.depthPostprocessing.blurAmount.value / this.height );

        this.renderTarget.setSize( width, height );
        this.fxComposer.setSize( width, height );

    }
}