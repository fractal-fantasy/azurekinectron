﻿/**
 * Created by mika on 22.07.2017.
 */

var my_uniforms = THREE.UniformsUtils.merge([
        THREE.UniformsUtils.clone(THREE.ShaderLib.physical.uniforms),
        {
            "resolution": { value: new THREE.Vector2(640,576) },
            "lookupRadius": { value: 1.0 },
            "normalsRatio": { value: 1.0 },
            "normalZ": { value: -0.001 },
            "amplitude": { value: 10.0 },
            "rippleFrequency": { value: 5.0 },
            "rippleSpeed": { value: 10.0 },
            "time": { value: 0.0 },
            "rippleAmplitude": { value: 0.0 },
            "indexTexture": { value: null },

            "isUsingColorMap": { value: 0 },
            "colorCoordinateTransformMatrix": { value: new THREE.Matrix4() },
            "lensCorrection": { value: 0 },
            
            "showNormalMap": { value: false }
        }
]
    );

var smoothLigthsMaterialTemplate = new THREE.ShaderMaterial({

    uniforms: my_uniforms,

    //defines: { PHYSICAL: "" },

    vertexShader: [

        "#define PHYSICAL",

        "varying vec3 vViewPosition;",

        "#ifndef FLAT_SHADED",
        "   varying vec3 vNormal;",
        "#endif",

        //"varying vec3 vReflect;",

        THREE.ShaderChunk["common"],
        THREE.ShaderChunk["uv_pars_vertex"],
        THREE.ShaderChunk["uv2_pars_vertex"],
        THREE.ShaderChunk["displacementmap_pars_vertex"],
        THREE.ShaderChunk["color_pars_vertex"],
        THREE.ShaderChunk["specularmap_pars_fragment"],

        "#ifndef USE_MAP",

        "   varying vec2 vUv;",

        "#endif",

        "varying float drawMode;",
        "uniform vec2 resolution;",
        "uniform float lookupRadius;",
        "uniform float normalsRatio;",
        "uniform float normalZ;",

        "uniform float amplitude;",

        "uniform float time;",
        "uniform float rippleFrequency;",
        "uniform float rippleSpeed;",
        "uniform float rippleAmplitude;",

        "attribute vec3 displacement;",


        `vec3 extractNormal(sampler2D map, vec2 tc) {

            const int numSteps = 6;

            float aspect = resolution.x/resolution.y;
            float dx = lookupRadius / resolution.x;
            float dy = dx * aspect;

            vec4 dx1 = vec4(0.0);
            vec4 dx2 = vec4(0.0);
            vec4 dy1 = vec4(0.0);
            vec4 dy2 = vec4(0.0);

            //for (int i=1; i < numSteps; i++) {

                float dxS = float(1) * dx;
                float dyS = float(1) * dy;

                dx1 += texture2D(map, tc + vec2(dxS, 0.0));
                dx2 += texture2D(map, tc + vec2(-dxS, 0.0));

                dy1 += texture2D(map, tc + vec2(0.0, dyS));
                dy2 += texture2D(map, tc +vec2(0.0, -dyS));

                dx1 += texture2D(map, tc +vec2(dxS*1.5, 0.0));
                dx2 += texture2D(map, tc +vec2(-dxS*1.5, 0.0));

                dy1 += texture2D(map, tc +vec2(0.0, dyS*1.5));
                dy2 += texture2D(map, tc +vec2(0.0, -dyS*1.5));

            //}

            float difX = (dx2.r - dx1.r);
            float difY = (dy2.r - dy1.r);


            return vec3(difX, difY, normalZ);
            //return (vec3( difX, difY, normalZ )) * 0.5 + 0.5 ;

        }`,

        
        //"uniform sampler2D indexTexture;",

        "void main() {",

            THREE.ShaderChunk["uv_vertex"],
            THREE.ShaderChunk["uv2_vertex"],
            THREE.ShaderChunk["color_vertex"],
            THREE.ShaderChunk["beginnormal_vertex"],
            THREE.ShaderChunk["morphnormal_vertex"],            
           // THREE.ShaderChunk["defaultnormal_vertex"],  ////vec3 transformedNormal = normalMatrix * objectNormal;
                                  

            `vec3 deltaNormal = normalsRatio * normalize( extractNormal( displacementMap, uv ) );

            vec3 transformedNormal = normalMatrix * objectNormal;            

            vNormal = normalize(transformedNormal) + deltaNormal;`,



            THREE.ShaderChunk["begin_vertex"],
            THREE.ShaderChunk["displacementmap_vertex"],

            // FATTENING            

              "transformed += deltaNormal * amplitude;",

              "transformed += deltaNormal * ( sin ( uv[1] * rippleFrequency + rippleSpeed * time) * rippleAmplitude);",

               //  DIAGONAL
              "transformed += deltaNormal * ( sin (( uv[1] + uv[0] )  * rippleFrequency + rippleSpeed * time) * rippleAmplitude);",


            THREE.ShaderChunk["project_vertex"],
            "vViewPosition = - mvPosition.xyz;",
            THREE.ShaderChunk["worldpos_vertex"],

            "#ifndef USE_MAP",

            "   vUv = uv;",

            "#endif",


            //cut off background plane
            //"float index_x = texture2D( indexTexture, uv ).x;",

            //"float depth_pixel_x = texture2D( displacementMap, uv ).x;",

            //"if ( depth_pixel_x < 0.1 ) drawMode = 0.0;",
            //"else drawMode = 1.0;",
            ////additional cut off
            //"if (dx1.x == 0.0 || dx2.x==0.0 || dy1.x==0.0 || dy2.x==0.0) drawMode = 0.0;",

        "}"
    ].join("\n"),

    fragmentShader: [

        "#define PHYSICAL",
        //"#define PHYSICALLY_CORRECT_LIGHTS",

        "uniform vec3 diffuse;",
        "uniform vec3 emissive;",
        "uniform float roughness;",
        "uniform float metalness;",
        "uniform float opacity;",

        "uniform bool isUsingColorMap;",
        "uniform mat4 colorCoordinateTransformMatrix;",
        "uniform bool lensCorrection;",
        "uniform sampler2D displacementMap;",

        "uniform vec2 resolution;",

        "uniform bool showNormalMap;",

        "#ifndef STANDARD",
        "   uniform float clearCoat;",
        "   uniform float clearCoatRoughness;",
        "#endif",

        //"uniform float envMapIntensity; // temporary",

        "varying vec3 vViewPosition;",

        "#ifndef FLAT_SHADED",

        "   varying vec3 vNormal;",       

        "#endif",

        "#ifndef USE_MAP",

        "   varying vec2 vUv;",

        "#endif",

        //"varying vec3 vReflect;",

        THREE.ShaderChunk["common"],
        THREE.ShaderChunk["packing"],
        THREE.ShaderChunk["color_pars_fragment"],
        THREE.ShaderChunk["uv_pars_fragment"],
        THREE.ShaderChunk["uv2_pars_fragment"],
        THREE.ShaderChunk["map_pars_fragment"],

        THREE.ShaderChunk["lightmap_pars_fragment"],
        THREE.ShaderChunk["emissivemap_pars_fragment"],

        THREE.ShaderChunk["envmap_pars_fragment"],

        THREE.ShaderChunk["bsdfs"],
        THREE.ShaderChunk["cube_uv_reflection_fragment"],
        THREE.ShaderChunk["lights_pars"],                           // ENV MAP here

        //`#ifndef ENVMAP_MODE_REFLECTION
        //    uniform sampler2D envMap;
        //    uniform float refractionRatio;
        //#endif
        //`,

        THREE.ShaderChunk["lights_physical_pars_fragment"],
        THREE.ShaderChunk["bumpmap_pars_fragment"],
        //THREE.ShaderChunk[ "normalmap_pars_fragment" ],
        THREE.ShaderChunk["roughnessmap_pars_fragment"],
        THREE.ShaderChunk["metalnessmap_pars_fragment"],

        `
        const float dx = 1.0 / 640.0;
        const float dy = 1.0 / 576.0;
        const int numSteps = 6;

        vec3 extractNormal(sampler2D map, vec2 tc) {
        
            vec4 dx1 = vec4(0.0);
            vec4 dx2 = vec4(0.0);
            vec4 dy1 = vec4(0.0);
            vec4 dy2 = vec4(0.0);

            for (int i=1; i < numSteps; i++) {

                float dxS = float(i) * dx;
                float dyS = float(i) * dy;

                dx1 += texture2D(map, tc +vec2(dxS, 0.0));
                dx2 += texture2D(map, tc +vec2(-dxS, 0.0));

                dy1 += texture2D(map, tc +vec2(0.0, dyS));
                dy2 += texture2D(map, tc +vec2(0.0, -dyS));

            }

            float difX = (dx2.x - dx1.x);
            float difY = (dy2.x - dy1.x);


            return vec3(difX, difY, 1.0);
            //return (vec3( difX, difY, 1.0 )) * 0.5 + 0.5;

        }`,

        "varying float drawMode;",

        "void main() {",

            //THREE.ShaderChunk[ "clipping_planes_fragment" ],

            "vec4 diffuseColor = vec4( diffuse, opacity );",
            "ReflectedLight reflectedLight = ReflectedLight( vec3( 0.0 ), vec3( 0.0 ), vec3( 0.0 ), vec3( 0.0 ) );",
            "vec3 totalEmissiveRadiance = emissive;",

            ///THREE.ShaderChunk["map_fragment"],
            "#ifdef USE_MAP",

            "    vec2 vUv_transformed = vUv;",

            "    if (isUsingColorMap) {",

            //"       vUv_transformed = vec2( ((vUv.x - 0.5) * 0.75) + 0.5, ((vUv.y - 0.5) * 0.95) + 0.5 );",

            "       if (lensCorrection) { ",

            "          vec4 transf = vec4( vUv, 1.0, 1.0) * colorCoordinateTransformMatrix;",

            "          vUv_transformed = transf.xy;",

            "       };", 

            "    } else { vUv_transformed = vUv; }",

            // here we change order of r/g/b channels because incoming diffuse texture is of BGRA format   -- mika
	        "    vec4 texelColor = texture2D( map, vUv_transformed ).zyxw;",

            "        texelColor = mapTexelToLinear( texelColor );",
            "        diffuseColor *= texelColor;",

            "#endif",


            THREE.ShaderChunk["color_fragment"],
            THREE.ShaderChunk["alphatest_fragment"],
            THREE.ShaderChunk["specularmap_fragment"],
            THREE.ShaderChunk["roughnessmap_fragment"],
            THREE.ShaderChunk["metalnessmap_fragment"],
            THREE.ShaderChunk["normal_flip"],


            ///THREE.ShaderChunk["normal_fragment"],

            //"vec3 deltaNormal = extractNormal( displacementMap, vUv );",

            "vec3 normal = normalize( vNormal );",

            //"vec3 normal = normalize( vNormal + deltaNormal ) * flipNormal;",

            //"normal = perturbNormalArb( -vViewPosition, normal, dHdxy_fwd() );",
        

            THREE.ShaderChunk["emissivemap_fragment"],

                        // accumulation
            THREE.ShaderChunk["lights_physical_fragment"],

            // Overrides the lights_physical_fragment to set specular color
           // "material.specularColor = vec3( 1, 0, 0 );",

            THREE.ShaderChunk["lights_template"],

                        // modulation
            //THREE.ShaderChunk[ "aomap_fragment" ],

            "vec3 outgoingLight = reflectedLight.directDiffuse + reflectedLight.indirectDiffuse + reflectedLight.directSpecular + reflectedLight.indirectSpecular + totalEmissiveRadiance;",

            //THREE.ShaderChunk["envmap_fragment"],


            "gl_FragColor = vec4( outgoingLight, diffuseColor.a );",

            THREE.ShaderChunk["premultiplied_alpha_fragment"],
            //THREE.ShaderChunk[ "tonemapping_fragment" ],
            THREE.ShaderChunk["encodings_fragment"],
            //THREE.ShaderChunk[ "fog_fragment" ],

            "float dx = 1.0 / resolution.x;",
            "float dy = 1.0 / resolution.y;",
            "vec4 depth_pixel = texture2D( displacementMap, vUv );",
            "vec4 dy1 = texture2D( displacementMap, vUv + vec2( 0.0, dy ) );",
            "vec4 dy2 = texture2D( displacementMap, vUv + vec2( 0.0, -dy ) );",
            "vec4 dx1 = texture2D( displacementMap, vUv + vec2( dx, 0.0 ) );",
            "vec4 dx2 = texture2D( displacementMap, vUv + vec2( -dx, 0.0 ) );",

            "bool cut = false;",

            "if ( depth_pixel.x < 0.1 ) cut = true;",            
            //additional cut off
            "if (dx1.x < 0.05 || dx2.x < 0.05 || dy1.x < 0.05 || dy2.x < 0.05) cut = true;",
            "if (cut) gl_FragColor.a = 0.0;",

            //"gl_FragColor = texture2D( map, vUv );",

            "if (showNormalMap) gl_FragColor = vec4( vNormal, diffuseColor.a );",   //test normals


        "}"
    ].join("\n"),

    //wireframe: false,
    lights: true,
    transparent: true,
    //depthWrite: false,
    //depthTest: false

});

smoothLigthsMaterialTemplate.extensions.derivatives = true;
