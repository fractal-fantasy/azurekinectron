var composerVelocity;
var renderTargetVelocity;
var texturePassVelocity;
var hblurVelocity,vblurVelocity;
var effectComputeLiquidVelocity, effectSaveLiquidVelocity;

function initVelocity() {

  texturePassVelocity = new THREE.TexturePass( liquidDisplacementRenderTarget.texture );
  var renderTargetParameters = {
      minFilter: THREE.LinearFilter,
      magFilter: THREE.LinearFilter,
      format: THREE.RGBAFormat,
      type: THREE.FloatType,
      // anisotropy:2,
  };

  renderTargetVelocity = new THREE.WebGLRenderTarget(DEPTH_TO_COLOR_WIDTH, DEPTH_TO_COLOR_HEIGHT, renderTargetParameters);

  hblurVelocity = new THREE.ShaderPass( THREE.HorizontalBlurShader );   
  vblurVelocity = new THREE.ShaderPass( THREE.VerticalBlurShader );  
  hblurVelocity.uniforms.h.value = 1 / DEPTH_TO_COLOR_HEIGHT;
  vblurVelocity.uniforms.v.value = 1 / DEPTH_TO_COLOR_WIDTH;

  var liquidVelocityRenderTarget = new THREE.WebGLRenderTarget(DEPTH_TO_COLOR_WIDTH, DEPTH_TO_COLOR_HEIGHT, renderTargetParameters);
  effectSaveLiquidVelocity = new THREE.SavePass(saveRenderTarget5);
  effectComputeLiquidVelocity = new THREE.ShaderPass(THREE.computeLiquidVelocityShader, "smoothDisplacement");      
  effectComputeLiquidVelocity.uniforms['velocity1'].value = liquidVelocityRenderTarget.texture;
  effectComputeLiquidVelocity.uniforms['displacement'].value = liquidDisplacementRenderTarget.texture;

  var copyPass = new THREE.ShaderPass(THREE.CopyShader);

  composerVelocity = new THREE.EffectComposer(renderer, renderTargetVelocity);
  composerVelocity.renderToScreen = false;
  composerVelocity.setSize( DEPTH_TO_COLOR_WIDTH, DEPTH_TO_COLOR_HEIGHT);

  
  composerVelocity.addPass( texturePassVelocity );

  composerVelocity.addPass( hblurVelocity );             
  composerVelocity.addPass( vblurVelocity );

  composerVelocity.addPass(effectComputeLiquidVelocity);
  composerVelocity.addPass(effectSaveLiquidVelocity);  
  

  composerVelocity.addPass( copyPass );

  effectComputeLiquidDisplacement.uniforms['velocity'].value = renderTargetVelocity.texture;


}
