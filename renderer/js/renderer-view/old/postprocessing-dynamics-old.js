var composerDynamics;
var renderTargetDynamics;
var texturePassDynamics;
var effectTemporalSmoothDynamic;
var effectSaveSmooth;
var hblurDynamic,vblurDynamic;
var hblurTilt,vblurTilt;
var effectSaveFlowDynamic, effectSceneFlowDynamic;
var effectSceneFlowDynamicCorrector;
var effectComputeLiquidDisplacement, effectSaveLiquidDisplacement;
var displacementBufferTexure;
var sceneFlowRenderTarget;
var vectorDisplacementMap, precomputeNormals;
var precomputedNormalsRenderTarget;

var liquidDisplacementRenderTarget;

var hblurVelocity,vblurVelocity;
var effectComputeLiquidVelocity, effectSaveLiquidVelocity;


function initDynamics(in_texture_depth, processedDepth) {

  texturePassDynamics = new THREE.TexturePass( in_texture_depth );
  var renderTargetParameters = {
      minFilter: TEXTURE_FILTER,
      magFilter: TEXTURE_FILTER,
      format: THREE.RGBAFormat,
      type: THREE.FloatType,
      // anisotropy:2,

  };

  var renderTargetParameters2 = {
      minFilter: TEXTURE_FILTER,
      magFilter: TEXTURE_FILTER,
      format: THREE.RGBAFormat,
      // depthBuffer: false,
      //  stencilBuffer: false,
      type: THREE.FloatType
  };


  renderTargetDynamics = new THREE.WebGLRenderTarget(DEPTH_TO_COLOR_WIDTH, DEPTH_TO_COLOR_HEIGHT, renderTargetParameters);

    hblurDynamic = new THREE.ShaderPass( THREE.HorizontalBlurShader );   
    vblurDynamic = new THREE.ShaderPass( THREE.VerticalBlurShader );  
    hblurDynamic.uniforms.h.value = 1 / DEPTH_TO_COLOR_HEIGHT;
    vblurDynamic.uniforms.v.value = 1 / DEPTH_TO_COLOR_WIDTH;

    hblurTilt = new THREE.ShaderPass( THREE.HorizontalTiltShiftShader );
    vblurTilt = new THREE.ShaderPass( THREE.VerticalTiltShiftShader );
    hblurTilt.uniforms.h.value = 1 / DEPTH_TO_COLOR_HEIGHT;
    vblurTilt.uniforms.v.value = 1 / DEPTH_TO_COLOR_WIDTH;
 
  
  var effectSaveRenderTarget = new THREE.WebGLRenderTarget(DEPTH_TO_COLOR_WIDTH, DEPTH_TO_COLOR_HEIGHT, renderTargetParameters);
  effectSaveSmooth = new THREE.SavePass(effectSaveRenderTarget);
  effectTemporalSmoothDynamic = new THREE.ShaderPass(THREE.temporalSmoothShader, "rawDepth");//current      
  effectTemporalSmoothDynamic.uniforms['rawDepthPrev'].value = effectSaveRenderTarget.texture;//previous
  effectTemporalSmoothDynamic.uniforms['temporalThreshold'].value = 0.02;
  effectTemporalSmoothDynamic.uniforms['temporalThresholdRange'].value = 4.0;


  var saveRenderTarget2 = new THREE.WebGLRenderTarget(DEPTH_TO_COLOR_WIDTH, DEPTH_TO_COLOR_HEIGHT, renderTargetParameters2);
  effectSaveFlowDynamic = new THREE.SavePass(saveRenderTarget2);
  effectSceneFlowDynamic = new THREE.ShaderPass(THREE.sceneFlowShader, "processedDepth");      
  effectSceneFlowDynamic.uniforms['processedDepthPrev'].value = saveRenderTarget2.texture;

  var saveRenderTarget3 = new THREE.WebGLRenderTarget(DEPTH_TO_COLOR_WIDTH, DEPTH_TO_COLOR_HEIGHT, renderTargetParameters2);
  effectSaveJiggle = new THREE.SavePass(saveRenderTarget3);
  effectJiggle = new THREE.ShaderPass(THREE.jiggleShader, "sourceVelocity");      
  effectJiggle.uniforms['jiggleVelocityPrev'].value = saveRenderTarget3.texture;

  liquidDisplacementRenderTarget = new THREE.WebGLRenderTarget(DEPTH_TO_COLOR_WIDTH, DEPTH_TO_COLOR_HEIGHT, renderTargetParameters2);
  effectSaveLiquidDisplacement = new THREE.SavePass(liquidDisplacementRenderTarget);
  
  effectComputeLiquidDisplacement = new THREE.ShaderPass(THREE.computeLiquidDisplacementShader, "emission");    
  effectComputeLiquidDisplacement.uniforms['displacement'].value = liquidDisplacementRenderTarget.texture;
  
  
  sceneFlowRenderTarget = new THREE.WebGLRenderTarget(DEPTH_TO_COLOR_WIDTH, DEPTH_TO_COLOR_HEIGHT, renderTargetParameters2);
  var effectSaveSceneFlow = new THREE.SavePass(sceneFlowRenderTarget);

    //VELOCITYSTART // INPUT liquidDisplacementRenderTarget.texture;
  hblurVelocity = new THREE.ShaderPass( THREE.HorizontalBlurShader );   
  vblurVelocity = new THREE.ShaderPass( THREE.VerticalBlurShader );  
  hblurVelocity.uniforms.h.value = 1 / DEPTH_TO_COLOR_HEIGHT;
  vblurVelocity.uniforms.v.value = 1 / DEPTH_TO_COLOR_WIDTH;

  var liquidVelocityRenderTarget = new THREE.WebGLRenderTarget(DEPTH_TO_COLOR_WIDTH, DEPTH_TO_COLOR_HEIGHT, renderTargetParameters);
  effectSaveLiquidVelocity = new THREE.SavePass(liquidVelocityRenderTarget);
  effectComputeLiquidVelocity = new THREE.ShaderPass(THREE.computeLiquidVelocityShader, "smoothDisplacement");      
  effectComputeLiquidVelocity.uniforms['velocity1'].value = liquidVelocityRenderTarget.texture;
  effectComputeLiquidVelocity.uniforms['displacement'].value = liquidDisplacementRenderTarget.texture;
  var renderTargetVelocity = new THREE.WebGLRenderTarget(DEPTH_TO_COLOR_WIDTH, DEPTH_TO_COLOR_HEIGHT, renderTargetParameters);
  effectSaveVelocity = new THREE.SavePass(renderTargetVelocity);
  effectComputeLiquidDisplacement.uniforms['velocity'].value = renderTargetVelocity.texture;
  //VELOCITYEND 

  
  //effectSceneFlowDynamic.uniforms['processedDepthPrev'].value = effectSaveRenderTarget.texture;
  //saveRenderTarget2.texture.needsUpdate = true;

  //DISPLACEMENT
  precomputedNormalsRenderTarget = new THREE.WebGLRenderTarget(DEPTH_TO_COLOR_WIDTH, DEPTH_TO_COLOR_HEIGHT, renderTargetParameters);
  savePrecomputeNormals = new THREE.SavePass(precomputedNormalsRenderTarget);
  precomputeNormals = new THREE.ShaderPass(THREE.precomputeNormalsShader);   
  precomputeNormals.uniforms['processedDepth'].value = processedDepth; 

  vectorDisplacementMap = new THREE.ShaderPass(THREE.vectorDisplacementMapShader, "precomputedNormals");   
  vectorDisplacementMap.uniforms['processedDepth'].value = processedDepth; 
  vectorDisplacementMap.uniforms['displacementVector'].value = sceneFlowRenderTarget.texture; 

  var copyPass = new THREE.ShaderPass(THREE.CopyShader);

  effectSceneFlowDynamicCorrector = new THREE.ShaderPass(THREE.sceneFlowShader2);    
  // vectorDisplacementMap = new THREE.ShaderPass(THREE.vectorDisplacementMapShader, "displacementVector");   
  // vectorDisplacementMap.uniforms['depth'].value = processedDepth; 

  composerDynamics = new THREE.EffectComposer(renderer, renderTargetDynamics);

  composerDynamics.renderToScreen = false;

  composerDynamics.setSize( DEPTH_TO_COLOR_WIDTH, DEPTH_TO_COLOR_HEIGHT);
  
  composerDynamics.addPass( texturePassDynamics );

  composerDynamics.addPass(effectTemporalSmoothDynamic);     
  composerDynamics.addPass(effectSaveSmooth);

  composerDynamics.addPass(effectSceneFlowDynamic);  
  composerDynamics.addPass(effectSaveFlowDynamic);
  composerDynamics.addPass(effectSceneFlowDynamicCorrector);

  composerDynamics.addPass( hblurDynamic );             
  composerDynamics.addPass( vblurDynamic );
  composerDynamics.addPass( hblurTilt );             
  composerDynamics.addPass( vblurTilt);

  composerDynamics.addPass(effectJiggle);  
  composerDynamics.addPass(effectSaveJiggle);

  composerDynamics.addPass(effectComputeLiquidDisplacement);  
  composerDynamics.addPass(effectSaveLiquidDisplacement);

  composerDynamics.addPass( hblurVelocity );             
  composerDynamics.addPass( vblurVelocity );

  composerDynamics.addPass(effectSaveSceneFlow);

  composerDynamics.addPass(effectComputeLiquidVelocity);
  composerDynamics.addPass(effectSaveLiquidVelocity);  
  composerDynamics.addPass(effectSaveVelocity);  

  composerDynamics.addPass(precomputeNormals);
  composerDynamics.addPass(savePrecomputeNormals);
  composerDynamics.addPass(vectorDisplacementMap);


  composerDynamics.addPass( copyPass );


}
