﻿/**
 * Created by mika on 15.05.2020.
 */

var my_uniforms = THREE.UniformsUtils.merge([
        THREE.UniformsUtils.clone(THREE.ShaderLib.physical.uniforms),
        {
            "resolution": { value: new THREE.Vector2(640,576) },
            "lookupRadius": { value: 1.0 },
            "normalsRatio": { value: 1.0 },
            "normalZ": { value: -0.001 },
            "amplitude": { value: 10.0 },
            "rippleFrequency": { value: 5.0 },
            "rippleSpeed": { value: 10.0 },
            "time": { value: 0.0 },
            "rippleAmplitude": { value: 0.0 },
            "indexTexture": { value: null },

            "isUsingColorMap": { value: 0 },
            "colorCoordinateTransformMatrix": { value: new THREE.Matrix4() },
            "lensCorrection": { value: 0 },

            "edgeWidth": { value: 1.0 },
            "edgeAlphaMultiplier": { value: 1.0 },
            
            "showNormalMap": { value: false },

            "useCustomNormal": { value: true },

            "frenelBias": { value: 0.0 },
            "frenelPower": { value: 1.0 },
            "frenelScale": { value: 0.2 },

            "deformNormalsFix": { value: true },
            "rippleNormalScale": { value: 0.1 },

            "cutBackground":  { value: true },
            "cutDepthThreshold":  { value: 0.1 },
            "cutPosition_W":  { value: -1.0 },

            "envZoom": { value: 1.0 },
            "fresnelFixScaleX": { value: 0.5 },
            "fresnelFixScaleY": { value: 0.5 },
            "fresnelFixPosX": { value: 0.5 },
            "fresnelFixPosY": { value: 0.5 }
        }
]
    );

var smoothLightsMaterialTemplate = new THREE.ShaderMaterial({

    uniforms: my_uniforms,

    vertexShader: [

        `#define STANDARD

        varying vec3 vViewPosition;

        #ifndef FLAT_SHADED

            varying vec3 vNormal;

            #ifdef USE_TANGENT

                varying vec3 vTangent;
                varying vec3 vBitangent;

            #endif

        #endif
        `,


        THREE.ShaderChunk["common"],
        /////THREE.ShaderChunk["uv_pars_vertex"],  chunk content:
        `
        #ifdef USE_UV

            #ifdef UVS_VERTEX_ONLY


                vec2 vUv;

            #else

                varying vec2 vUv;

            #endif

            uniform mat3 uvTransform;

        #endif
        `,
        THREE.ShaderChunk["uv2_pars_vertex"],
        THREE.ShaderChunk["displacementmap_pars_vertex"],
        THREE.ShaderChunk["color_pars_vertex"],
        THREE.ShaderChunk["morphtarget_pars_vertex"],

        // "#ifndef USE_MAP",

        // "   varying vec2 vUv;",

        // "#endif",

        "uniform vec2 resolution;",

        "uniform bool useCustomNormal;",
        "uniform float lookupRadius;",
        "uniform float normalsRatio;",
        "uniform float normalZ;",

        "uniform float amplitude;",

        "uniform float time;",
        "uniform float rippleFrequency;",
        "uniform float rippleSpeed;",
        "uniform float rippleAmplitude;",

        "uniform bool deformNormalsFix;",
        "uniform float rippleNormalScale;",

        "uniform bool cutBackground;",
        "uniform float cutDepthThreshold;",
        "uniform float cutPosition_W;",

        "attribute vec3 displacement;",


        `vec3 extractNormal(sampler2D map, vec2 tc) {

            const int numSteps = 6;

            float aspect = resolution.x/resolution.y;
            float dx = lookupRadius / resolution.x;
            float dy = dx * aspect;

            vec4 dx1 = vec4(0.0);
            vec4 dx2 = vec4(0.0);
            vec4 dy1 = vec4(0.0);
            vec4 dy2 = vec4(0.0);

            //for (int i=1; i < numSteps; i++) {

                float dxS = float(1) * dx;
                float dyS = float(1) * dy;

                dx1 += texture2D(map, tc + vec2(dxS, 0.0));
                dx2 += texture2D(map, tc + vec2(-dxS, 0.0));

                dy1 += texture2D(map, tc + vec2(0.0, dyS));
                dy2 += texture2D(map, tc +vec2(0.0, -dyS));

                dx1 += texture2D(map, tc +vec2(dxS*1.5, 0.0));
                dx2 += texture2D(map, tc +vec2(-dxS*1.5, 0.0));

                dy1 += texture2D(map, tc +vec2(0.0, dyS*1.5));
                dy2 += texture2D(map, tc +vec2(0.0, -dyS*1.5));

            //}

            float difX = (dx2.r - dx1.r);
            float difY = (dy2.r - dy1.r);


            return vec3(difX, difY, normalZ);
            //return (vec3( difX, difY, normalZ )) * 0.5 + 0.5 ;

        }`,

        
        //"uniform sampler2D indexTexture;",

        "void main() {",

            THREE.ShaderChunk["uv_vertex"],
            THREE.ShaderChunk["uv2_vertex"],
            THREE.ShaderChunk["color_vertex"],
            //////THREE.ShaderChunk["beginnormal_vertex"],   chunk content:
            `
            vec3 objectNormal = vec3( normal );

            #ifdef USE_TANGENT

                vec3 objectTangent = vec3( tangent.xyz );

            #endif
            `,



            THREE.ShaderChunk["morphnormal_vertex"],            

           // THREE.ShaderChunk["defaultnormal_vertex"],  
           /*            defaultnormal_vertex chunk:

            vec3 transformedNormal = objectNormal;

            #ifdef USE_INSTANCING

                // this is in lieu of a per-instance normal-matrix
                // shear transforms in the instance matrix are not supported

                mat3 m = mat3( instanceMatrix );

                transformedNormal /= vec3( dot( m[ 0 ], m[ 0 ] ), dot( m[ 1 ], m[ 1 ] ), dot( m[ 2 ], m[ 2 ] ) );

                transformedNormal = m * transformedNormal;

            #endif

            transformedNormal = normalMatrix * transformedNormal;

             */

            `vec3 deltaNormal = normalsRatio * normalize( extractNormal( displacementMap, uv ) );

            vec3 rippleNormal = vec3(0.0);

            if (deformNormalsFix) {  

                rippleNormal += deltaNormal * amplitude * rippleNormalScale;

                // vertical
                rippleNormal += deltaNormal * sin( uv[1] * rippleFrequency + rippleSpeed * time ) * rippleAmplitude * rippleNormalScale;

                // horizontal (added as option)
                //rippleNormal += deltaNormal * sin( uv[0] * rippleFrequency + rippleSpeed * time ) * rippleAmplitude * rippleNormalScale;

                // DIAGONAL
                rippleNormal += deltaNormal * ( sin (( uv[1] + uv[0] )  * rippleFrequency + rippleSpeed * time) * rippleAmplitude) * rippleNormalScale;

            }

            vec3 transformedNormal = objectNormal + rippleNormal;

            if ( useCustomNormal ) {

                transformedNormal += deltaNormal;

            }

            transformedNormal = normalMatrix * transformedNormal;


            #ifdef FLIP_SIDED

                transformedNormal = - transformedNormal;

            #endif

            `,

            `#ifdef USE_TANGENT

                vec3 transformedTangent = ( modelViewMatrix * vec4( objectTangent, 0.0 ) ).xyz;

                #ifdef FLIP_SIDED

                    transformedTangent = - transformedTangent;

                #endif

            #endif`,
                                    

            

            `
            #ifndef FLAT_SHADED // Normal computed with derivatives when FLAT_SHADED

                vNormal = normalize(transformedNormal);

                #ifdef USE_TANGENT

                    vTangent = normalize( transformedTangent );
                    vBitangent = normalize( cross( vNormal, vTangent ) * tangent.w );

                #endif

            #endif
            `,

            THREE.ShaderChunk["begin_vertex"],
            THREE.ShaderChunk["morphtarget_vertex"],
            THREE.ShaderChunk["displacementmap_vertex"],

            // FATTENING
            `
            transformed += deltaNormal * amplitude;

            // vertical
            transformed += deltaNormal * ( sin ( uv[1] * rippleFrequency + rippleSpeed * time) * rippleAmplitude);

            // horizontal (added as option)
            //transformed += deltaNormal * ( sin ( uv[0] * rippleFrequency + rippleSpeed * time) * rippleAmplitude);

            //  DIAGONAL
            transformed += deltaNormal * ( sin (( uv[1] + uv[0] )  * rippleFrequency + rippleSpeed * time) * rippleAmplitude);
           

            `,


            THREE.ShaderChunk["project_vertex"],

            "vViewPosition = - mvPosition.xyz;",

            THREE.ShaderChunk["worldpos_vertex"],

            // "#ifndef USE_MAP",

            // "   vUv = uv;",

            // "#endif",


            //cut off background plane
            //"float index_x = texture2D( indexTexture, uv ).x;",            

            `
            if (cutBackground) {

                if ( texture2D( displacementMap, uv ).x < cutDepthThreshold) { 
                    gl_Position.w = cutPosition_W;
                    gl_Position.z = 65000.0;  // fix for orthographic camera
                }

            }
            `,

        "}"
    ].join("\n"),

    fragmentShader: [

        "#define STANDARD",

        `
        #ifdef PHYSICAL
            #define REFLECTIVITY
            #define CLEARCOAT
            #define TRANSPARENCY
        #endif
        `,

        "uniform vec3 diffuse;",
        "uniform vec3 emissive;",
        "uniform float roughness;",
        "uniform float metalness;",
        "uniform float opacity;",

//======= <my
        "uniform bool isUsingColorMap;",
        "uniform mat4 colorCoordinateTransformMatrix;",
        "uniform bool lensCorrection;",
        "uniform sampler2D displacementMap;",

        "uniform vec2 resolution;",

        "uniform bool showNormalMap;",

        "uniform float frenelBias;",
        "uniform float frenelPower;",
        "uniform float frenelScale;",

        "uniform float edgeWidth;",
        "uniform float edgeAlphaMultiplier;",

        "uniform bool cutBackground;",
        "uniform float envZoom;",

        "uniform float fresnelFixScaleX;",
        "uniform float fresnelFixScaleY;",
        "uniform float fresnelFixPosX;",
        "uniform float fresnelFixPosY;",
//======= my>

        `
            mat3 G[2];

            const mat3 g0 = mat3( 1.0, 2.0, 1.0, 0.0, 0.0, 0.0, -1.0, -2.0, -1.0 );
            const mat3 g1 = mat3( 1.0, 0.0, -1.0, 2.0, 0.0, -2.0, 1.0, 0.0, -1.0 );

            float edge( in vec2 tc ) {

                mat3 I;
                float cnv[2];
                
                G[0] = g0;
                G[1] = g1;

                vec2 texel = vec2( edgeWidth * 1.0 / 640.0, edgeWidth * 1.0 / 360.0);

                // fetch the 3x3 neighbourhood and use the RGB vector's length as intensity value 
                for (float i=0.0; i<3.0; i++)
                    for (float j=0.0; j<3.0; j++) {
                        
                        float sample_pixel = texture2D( displacementMap, tc + texel * vec2(i-1.0,j-1.0) ).x;
                        I[int(i)][int(j)] = sample_pixel;
                    }

                // calculate the convolution values for all the masks
                for (int i=0; i<2; i++) {
                    float dp3 = dot(G[i][0], I[0]) + dot(G[i][1], I[1]) + dot(G[i][2], I[2]);
                    
                    cnv[i] = dp3 * dp3;
                    
                }

                return 0.5 * sqrt(cnv[0] * cnv[0] + cnv[1] * cnv[1]);

            }
        `,

        `
        #ifdef TRANSPARENCY
            uniform float transparency;
        #endif

        #ifdef REFLECTIVITY
            uniform float reflectivity;
        #endif

        #ifdef CLEARCOAT
            uniform float clearcoat;
            uniform float clearcoatRoughness;
        #endif

        #ifdef USE_SHEEN
            uniform vec3 sheen;
        #endif
        `,


        "varying vec3 vViewPosition;",

        "#ifndef FLAT_SHADED",

        "   varying vec3 vNormal;",
        `
        	#ifdef USE_TANGENT

                varying vec3 vTangent;
                varying vec3 vBitangent;

            #endif
        `,

        "#endif",

        // "#ifndef USE_MAP",

        // "   varying vec2 vUv;",

        // "#endif",

        THREE.ShaderChunk["common"],
        THREE.ShaderChunk["packing"],
        THREE.ShaderChunk["color_pars_fragment"],
        THREE.ShaderChunk["uv_pars_fragment"],
        THREE.ShaderChunk["uv2_pars_fragment"],
        THREE.ShaderChunk["map_pars_fragment"],

        THREE.ShaderChunk["lightmap_pars_fragment"],
        THREE.ShaderChunk["emissivemap_pars_fragment"],


        THREE.ShaderChunk["bsdfs"],
        THREE.ShaderChunk["cube_uv_reflection_fragment"],

        THREE.ShaderChunk["envmap_common_pars_fragment"],
        //THREE.ShaderChunk["envmap_physical_pars_fragment"],
        
        `
        #if defined( USE_ENVMAP )

            #ifdef ENVMAP_MODE_REFRACTION
                uniform float refractionRatio;
            #endif

            vec3 getLightProbeIndirectIrradiance( /*const in SpecularLightProbe specularLightProbe,*/ const in GeometricContext geometry, const in int maxMIPLevel ) {

                vec3 worldNormal = inverseTransformDirection( geometry.normal, viewMatrix );

                #ifdef ENVMAP_TYPE_CUBE

                    vec3 queryVec = vec3( flipEnvMap * worldNormal.x, worldNormal.yz );

                    // TODO: replace with properly filtered cubemaps and access the irradiance LOD level, be it the last LOD level
                    // of a specular cubemap, or just the default level of a specially created irradiance cubemap.

                    #ifdef TEXTURE_LOD_EXT

                        vec4 envMapColor = textureCubeLodEXT( envMap, queryVec, float( maxMIPLevel ) );

                    #else

                        // force the bias high to get the last LOD level as it is the most blurred.
                        vec4 envMapColor = textureCube( envMap, queryVec, float( maxMIPLevel ) );

                    #endif

                    envMapColor.rgb = envMapTexelToLinear( envMapColor ).rgb;

                #elif defined( ENVMAP_TYPE_CUBE_UV )

                    vec4 envMapColor = textureCubeUV( envMap, worldNormal, 1.0 );

                #else

                    vec4 envMapColor = vec4( 0.0 );

                #endif

                return PI * envMapColor.rgb * envMapIntensity;

            }

            // Trowbridge-Reitz distribution to Mip level, following the logic of http://casual-effects.blogspot.ca/2011/08/plausible-environment-lighting-in-two.html
            float getSpecularMIPLevel( const in float roughness, const in int maxMIPLevel ) {

                float maxMIPLevelScalar = float( maxMIPLevel );

                float sigma = PI * roughness * roughness / ( 1.0 + roughness );
                float desiredMIPLevel = maxMIPLevelScalar + log2( sigma );

                // clamp to allowable LOD ranges.
                return clamp( desiredMIPLevel, 0.0, maxMIPLevelScalar );

            }

            vec3 getLightProbeIndirectRadiance( /*const in SpecularLightProbe specularLightProbe,*/ const in vec3 viewDir, const in vec3 normal, const in float roughness, const in int maxMIPLevel ) {

                #ifdef ENVMAP_MODE_REFLECTION

                    vec3 reflectVec = reflect( -viewDir, normal );

                    // Mixing the reflection with the normal is more accurate and keeps rough objects from gathering light from behind their tangent plane.
                    reflectVec = normalize( mix( reflectVec, normal, roughness * roughness) );

                #else

                    vec3 reflectVec = refract( -viewDir, normal, refractionRatio );

                #endif

                reflectVec = inverseTransformDirection( reflectVec, viewMatrix );

                float specularMIPLevel = getSpecularMIPLevel( roughness, maxMIPLevel );

                #ifdef ENVMAP_TYPE_CUBE

                    vec3 queryReflectVec = vec3( flipEnvMap * reflectVec.x, reflectVec.yz );

                    #ifdef TEXTURE_LOD_EXT

                        vec4 envMapColor = textureCubeLodEXT( envMap, queryReflectVec, specularMIPLevel );

                    #else

                        vec4 envMapColor = textureCube( envMap, queryReflectVec, specularMIPLevel );

                    #endif

                    envMapColor.rgb = envMapTexelToLinear( envMapColor ).rgb;

                #elif defined( ENVMAP_TYPE_CUBE_UV )

                    vec4 envMapColor = textureCubeUV( envMap, reflectVec, roughness );

                #elif defined( ENVMAP_TYPE_EQUIREC )

                    vec2 sampleUV;
                    //reflectVec *= envZoom;
                    sampleUV.y = asin( clamp( reflectVec.y  * envZoom, - 1.0, 1.0 ) ) * RECIPROCAL_PI + 0.5;
			        sampleUV.x = atan( reflectVec.z, reflectVec.x  * envZoom ) * RECIPROCAL_PI2 + 0.5;

                    #ifdef TEXTURE_LOD_EXT

                        #ifdef ENVMAP_FLAT

                            vec4 envMapColor = texture2D( envMap, (vUv - 0.5) * envZoom + 0.5 + normal.xy * (1.0 - refractionRatio) * 0.5 );

                        #else

                            vec4 envMapColor = texture2DLodEXT( envMap, sampleUV, specularMIPLevel );

                        #endif

                    #else

                        vec4 envMapColor = texture2D( envMap, sampleUV, specularMIPLevel );                        


                    #endif

                    envMapColor.rgb = envMapTexelToLinear( envMapColor ).rgb;

                #elif defined( ENVMAP_TYPE_SPHERE )

                    vec3 reflectView = normalize( ( viewMatrix * vec4( reflectVec, 0.0 ) ).xyz + vec3( 0.0,0.0,1.0 ) );

                    #ifdef TEXTURE_LOD_EXT

                        vec4 envMapColor = texture2DLodEXT( envMap, reflectView.xy * 0.5 + 0.5, specularMIPLevel );

                    #else

                        vec4 envMapColor = texture2D( envMap, reflectView.xy * 0.5 + 0.5, specularMIPLevel );

                    #endif

                    envMapColor.rgb = envMapTexelToLinear( envMapColor ).rgb;

                #endif

                return envMapColor.rgb * envMapIntensity;

            }

        #endif


        `,
        

        THREE.ShaderChunk["lights_pars_begin"],
        THREE.ShaderChunk["lights_physical_pars_fragment"],

        THREE.ShaderChunk["bumpmap_pars_fragment"],

        THREE.ShaderChunk["normalmap_pars_fragment" ],

        THREE.ShaderChunk["clearcoat_pars_fragment" ],

        THREE.ShaderChunk["roughnessmap_pars_fragment"],
        THREE.ShaderChunk["metalnessmap_pars_fragment"],

        // custom fresnel/ chromatic aberration --- mika            

        "#if defined( USE_ENVMAP ) && !defined( ENVMAP_MODE_REFLECTION )",

            "float fast_fresnel(vec3 I, vec3 N, vec3 fresnelValues)",
            "{",
            "    float bias = fresnelValues.x;",
            "    float power = fresnelValues.y;",
            "    float scale = 1.0 - bias;",
            "    return bias + pow(1.0 - dot(I, N), power) * scale;",
            "}",

            "vec3 getFresnel( sampler2D envMap, vec3 normal, vec3 eye ) {",

                //------ normalize incoming vectors

                "vec3 N = normalize(normal);",
                "vec3 I = normalize(eye);",

                //------ Find the reflection

                //"vec3 reflVec = normalize( reflect(-I, N) );",
                //"vec2 sampleUV;",
                //"sampleUV.y = saturate( reflVec.y * 0.5 + 0.5 );",
                //"sampleUV.x = atan( reflVec.z, reflVec.x ) * RECIPROCAL_PI2 + 0.5;",
                //"vec3 reflectColor = texture2D(envMap, sampleUV).xyz;",

                //------ Find the refraction

                "vec3 refractColor;",

                "vec3 IoR_Values = vec3(1.0 - frenelPower * 0.02, 1.0 - frenelPower * 0.01 , 1.0);",

                "IoR_Values *= refractionRatio;",

                "vec3 refractVec_r = refract(-I, N, IoR_Values.x);",
                "vec3 refractVec_g = refract(-I, N, IoR_Values.y);",
                "vec3 refractVec_b = refract(-I, N, IoR_Values.z);",

                "vec2 refractUV_r, refractUV_g, refractUV_b;",
                "refractUV_r.y = saturate( refractVec_r.y * 0.5 + 0.5 );",
                "refractUV_r.x = atan( refractVec_r.z, refractVec_r.x ) * RECIPROCAL_PI2 + 0.5;",

                "refractUV_g.y = saturate( refractVec_g.y * 0.5 + 0.5 );",
                "refractUV_g.x = atan( refractVec_g.z, refractVec_g.x ) * RECIPROCAL_PI2 + 0.5;",

                "refractUV_b.y = saturate( refractVec_b.y * 0.5 + 0.5 );",
                "refractUV_b.x = atan( refractVec_b.z, refractVec_b.x ) * RECIPROCAL_PI2 + 0.5;",

                "refractColor.x = texture2D(envMap, refractUV_r).x;",
                "refractColor.y = texture2D(envMap, refractUV_g).y;",
                "refractColor.z = texture2D(envMap, refractUV_b).z;",

                "return refractColor;",
            "}",

            "vec3 getFresnelFlat( sampler2D envMap, vec3 normal, vec3 eye ) {",

                //------ normalize incoming vectors

                "vec3 N = normalize(normal);",
                "vec3 I = normalize(eye);", 

                //------ Find the refraction

                "vec3 refractColor;",

                "vec3 IoR_Values = vec3(1.0 - 20.0 * frenelPower * 0.02, 1.0 - 20.0 * frenelPower * 0.01 , 1.0);",

                "IoR_Values *= refractionRatio;",

                "refractColor.x = texture2D( envMap, ((vUv - 0.5) * envZoom + 0.5) + pow(normal.xy, vec2(2.0)) * (1.0 - refractionRatio) * 1.0 * IoR_Values.x ).x;",
                "refractColor.y = texture2D( envMap, ((vUv - 0.5) * envZoom + 0.5) + pow(normal.xy, vec2(2.0)) * (1.0 - refractionRatio) * 1.0 * IoR_Values.y ).y;",
                "refractColor.z = texture2D( envMap, ((vUv - 0.5) * envZoom + 0.5) + pow(normal.xy, vec2(2.0)) * (1.0 - refractionRatio) * 1.0 * IoR_Values.z ).z;",

                "return refractColor;",
            "}",

        "#endif",

        `
        const float dx = 1.0 / 1280.0;
        const float dy = 1.0 / 720.0;
        const int numSteps = 6;

        vec3 extractNormal(sampler2D map, vec2 tc) {
        
            vec4 dx1 = vec4(0.0);
            vec4 dx2 = vec4(0.0);
            vec4 dy1 = vec4(0.0);
            vec4 dy2 = vec4(0.0);

            for (int i=1; i < numSteps; i++) {

                float dxS = float(i) * dx;
                float dyS = float(i) * dy;

                dx1 += texture2D(map, tc +vec2(dxS, 0.0));
                dx2 += texture2D(map, tc +vec2(-dxS, 0.0));

                dy1 += texture2D(map, tc +vec2(0.0, dyS));
                dy2 += texture2D(map, tc +vec2(0.0, -dyS));

            }

            float difX = (dx2.x - dx1.x);
            float difY = (dy2.x - dy1.x);


            return vec3(difX, difY, 1.0);
            //return (vec3( difX, difY, 1.0 )) * 0.5 + 0.5;

        }`,

        "void main() {",

            //THREE.ShaderChunk[ "clipping_planes_fragment" ],

            "vec4 diffuseColor = vec4( diffuse, opacity );",
            "ReflectedLight reflectedLight = ReflectedLight( vec3( 0.0 ), vec3( 0.0 ), vec3( 0.0 ), vec3( 0.0 ) );",
            "vec3 totalEmissiveRadiance = emissive;",

            /////THREE.ShaderChunk["map_fragment"], chunk content:
            "#ifdef USE_MAP",

            "    vec2 vUv_transformed = vUv;",

            "    if (isUsingColorMap) {",

            //"       vUv_transformed = vec2( ((vUv.x - 0.5) * 0.75) + 0.5, ((vUv.y - 0.5) * 0.95) + 0.5 );",

            "       if (lensCorrection) { ",

            "          vec4 transf = vec4( vUv, 1.0, 1.0) * colorCoordinateTransformMatrix;",

            "          vUv_transformed = transf.xy;",

            "       };", 

            "    } else { vUv_transformed = vUv; }",

	        "    vec4 texelColor = texture2D( map, vUv_transformed ).xyzw;",

            "        texelColor = mapTexelToLinear( texelColor );",
            "        diffuseColor *= texelColor;",

            "#endif",


            THREE.ShaderChunk["color_fragment"],
            THREE.ShaderChunk["alphatest_fragment"],

            THREE.ShaderChunk["roughnessmap_fragment"],
            THREE.ShaderChunk["metalnessmap_fragment"],

        
            THREE.ShaderChunk["normal_fragment_begin"],

            ////// THREE.ShaderChunk["normal_fragment_maps"], chunk content:
            `
            #ifdef OBJECTSPACE_NORMALMAP

                normal = texture2D( normalMap, vUv ).xyz * 2.0 - 1.0; // overrides both flatShading and attribute normals

                #ifdef FLIP_SIDED

                    normal = - normal;

                #endif

                #ifdef DOUBLE_SIDED

                    normal = normal * ( float( gl_FrontFacing ) * 2.0 - 1.0 );

                #endif

                normal = normalize( normalMatrix * normal );

            #elif defined( TANGENTSPACE_NORMALMAP )

                vec3 mapN = texture2D( normalMap, vUv ).xyz * 2.0 - 1.0;
                mapN.xy *= normalScale;

                #ifdef USE_TANGENT

                    normal = normalize( vTBN * mapN );

                #else

                    normal = perturbNormal2Arb( -vViewPosition, normal, mapN );

                #endif

            #elif defined( USE_BUMPMAP )

                normal = perturbNormalArb( -vViewPosition, normal, dHdxy_fwd() );

            #endif
            `,

            //"vec3 deltaNormal = extractNormal( displacementMap, vUv );",

            //"normal += normalize( deltaNormal );",

            THREE.ShaderChunk["clearcoat_normal_fragment_begin"],
            THREE.ShaderChunk["clearcoat_normal_fragment_maps"],


            THREE.ShaderChunk["emissivemap_fragment"],

            // accumulation
            THREE.ShaderChunk["lights_physical_fragment"],
            THREE.ShaderChunk["lights_fragment_begin"],
            THREE.ShaderChunk["lights_fragment_maps"],
            THREE.ShaderChunk["lights_fragment_end"],

            //  custom fresnel/ chromatic aberration --- mika
            "#if defined( USE_ENVMAP ) && !defined( ENVMAP_MODE_REFLECTION )",

                `#ifdef ENVMAP_FLAT

                    radiance = getFresnelFlat( envMap, normal, vViewPosition );

                #else

                    radiance = getFresnel( envMap, normal, vViewPosition );

                #endif`,
                
                "reflectedLight.indirectSpecular = mix(reflectedLight.indirectSpecular, radiance, frenelScale);",

            "#endif",

            // modulation
            //THREE.ShaderChunk[ "aomap_fragment" ],

            "vec3 outgoingLight = reflectedLight.directDiffuse + reflectedLight.indirectDiffuse + reflectedLight.directSpecular + reflectedLight.indirectSpecular + totalEmissiveRadiance;",

            //THREE.ShaderChunk["envmap_fragment"],

            `
            // this is a stub for the transparency model
            #ifdef TRANSPARENCY
                diffuseColor.a *= saturate( 1. - transparency + linearToRelativeLuminance( reflectedLight.directSpecular + reflectedLight.indirectSpecular ) );
            #endif
            `,


            `
            if (cutBackground) {

                float edge_pixel = edge( vUv );
                gl_FragColor = vec4( outgoingLight, (1.0 - edge_pixel * edgeAlphaMultiplier) * diffuseColor.a );


            } else {

                gl_FragColor = vec4( outgoingLight, diffuseColor.a );

            }

            `,
             
            THREE.ShaderChunk[ "tonemapping_fragment" ],
            THREE.ShaderChunk["encodings_fragment"],
            //THREE.ShaderChunk[ "fog_fragment" ],
            THREE.ShaderChunk["premultiplied_alpha_fragment"],

            "if (showNormalMap) gl_FragColor = vec4( vNormal, diffuseColor.a );",   //test normals


        "}"
    ].join("\n"),

    lights: true,
    transparent: true,

});

smoothLightsMaterialTemplate.extensions.derivatives = true;
