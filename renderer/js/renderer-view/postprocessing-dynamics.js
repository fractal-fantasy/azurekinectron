
class DynamicsPostFX {

  width = DEPTH_TO_COLOR_WIDTH;
  height = DEPTH_TO_COLOR_HEIGHT;

  texturePass = null;
  fxComposer = null;

      
  copyPass = new THREE.ShaderPass(THREE.CopyShader);

  renderTargetParameters = {
      // minFilter: TEXTUREFILTER,
      // magFilter: TEXTUREFILTER,
      minFilter: THREE.LinearFilter,
      magFilter: THREE.NearestFilter,
      format: THREE.RGBAFormat,
      stencilBuffer: false,
      type: THREE.FloatType,
      generateMipmaps: false,
      anisotropy:ANISOTROPY
  };

  renderTargetParameters2 = {
    minFilter: THREE.LinearFilter,
    magFilter: THREE.LinearFilter,
    format: THREE.RGBAFormat,
    stencilBuffer: false,
    type: THREE.FloatType,
    generateMipmaps: false,
    anisotropy:ANISOTROPY
  };
 

  renderTarget = new THREE.WebGLRenderTarget( this.width, this.height, this.renderTargetParameters );
  
  init( in_texture_depth, processedDepth, renderer ) {

      this.texturePass = new THREE.TexturePass( in_texture_depth );        

      this.fxComposer = new THREE.EffectComposer( renderer , this.renderTarget ); 
      
      this.fxComposer.renderToScreen = false;

      this.fxComposer.setSize( this.width, this.height);

      //LIQUID BLUR
      this.hblurDynamic = new THREE.ShaderPass( THREE.HorizontalBlurShader );   
      this.vblurDynamic = new THREE.ShaderPass( THREE.VerticalBlurShader );  
      this.hblurDynamic.uniforms.h.value = 1 / this.height;
      this.vblurDynamic.uniforms.v.value = 1 / this.width;

      this.hblurTilt = new THREE.ShaderPass( THREE.HorizontalTiltShiftShader );
      this.vblurTilt = new THREE.ShaderPass( THREE.VerticalTiltShiftShader );
      this.hblurTilt.uniforms.h.value = 1 / this.height;
      this.vblurTilt.uniforms.v.value = 1 / this.width;
      this.hblurTilt.uniforms.r.value = this.vblurTilt.uniforms.r.value = 2;

      this.smoothRenderTarget = new THREE.WebGLRenderTarget(this.width, this.height, this.renderTargetParameters);
      this.effectSaveSmooth = new THREE.SavePass(this.smoothRenderTarget);
      this.effectTemporalSmoothDynamic = new THREE.ShaderPass(THREE.temporalSmoothShader, "rawDepth");//current      
      this.effectTemporalSmoothDynamic.uniforms['rawDepthPrev'].value = this.smoothRenderTarget.texture;//previous
      this.effectTemporalSmoothDynamic.uniforms['temporalThreshold'].value = 0.02;
      this.effectTemporalSmoothDynamic.uniforms['temporalThresholdRange'].value = 4.0;

      

      this.flowRenderTarget = new THREE.WebGLRenderTarget(this.width, this.height, this.renderTargetParameters);
      this.effectSaveFlowDynamic = new THREE.SavePass(this.flowRenderTarget);
      this.effectSceneFlowDynamic = new THREE.ShaderPass(THREE.sceneFlowShader, "processedDepth");      
      this.effectSceneFlowDynamic.uniforms['processedDepthPrev'].value = this.flowRenderTarget.texture;


      
      this.jiggleRenderTarget = new THREE.WebGLRenderTarget(this.width, this.height, this.renderTargetParameters);
      this.effectSaveJiggle = new THREE.SavePass(this.jiggleRenderTarget);
      this.effectJiggle = new THREE.ShaderPass(THREE.jiggleShader, "sourceVelocity");      
      this.effectJiggle.uniforms['jiggleVelocityPrev'].value = this.jiggleRenderTarget.texture;

      
      this.liquidDisplacementRenderTarget = new THREE.WebGLRenderTarget(this.width, this.height, this.renderTargetParameters);
      this.effectSaveLiquidDisplacement = new THREE.SavePass(this.liquidDisplacementRenderTarget);
  
      this.effectComputeLiquidDisplacement = new THREE.ShaderPass(THREE.computeLiquidDisplacementShader, "emission");    
      this.effectComputeLiquidDisplacement.uniforms['displacement'].value = this.liquidDisplacementRenderTarget.texture;
      
      this.sceneFlowRenderTarget = new THREE.WebGLRenderTarget(this.width, this.height, this.renderTargetParameters);
      this.effectSaveSceneFlow = new THREE.SavePass(this.sceneFlowRenderTarget);

      this.hblurVelocity = new THREE.ShaderPass( THREE.HorizontalBlurShader );   
      this.vblurVelocity = new THREE.ShaderPass( THREE.VerticalBlurShader );  
      this.hblurVelocity.uniforms.h.value = 1 / this.height;
      this.vblurVelocity.uniforms.v.value = 1 / this.width;

      

      this.liquidVelocityRenderTarget = new THREE.WebGLRenderTarget(this.width, this.height, this.renderTargetParameters);
      this.effectSaveLiquidVelocity = new THREE.SavePass(this.liquidVelocityRenderTarget);
      this.effectComputeLiquidVelocity = new THREE.ShaderPass(THREE.computeLiquidVelocityShader, "smoothDisplacement");      
      this.effectComputeLiquidVelocity.uniforms['velocity1'].value = this.liquidVelocityRenderTarget.texture;
      this.effectComputeLiquidVelocity.uniforms['displacement'].value = this.liquidDisplacementRenderTarget.texture;
      this.renderTargetVelocity = new THREE.WebGLRenderTarget(this.width, this.height, this.renderTargetParameters);
      this.effectSaveVelocity = new THREE.SavePass(this.renderTargetVelocity);
      this.effectComputeLiquidDisplacement.uniforms['velocity'].value = this.renderTargetVelocity.texture;
      //VELOCITYEND 

      //DISPLACEMENT
      this.precomputedNormalsRenderTarget = new THREE.WebGLRenderTarget(this.width, this.height, this.renderTargetParameters2);
      this.savePrecomputeNormals = new THREE.SavePass(this.precomputedNormalsRenderTarget);
      this.precomputeNormals = new THREE.ShaderPass(THREE.precomputeNormalsShader);   
      this.precomputeNormals.uniforms['processedDepth'].value = processedDepth; 

      this.vectorDisplacementMap = new THREE.ShaderPass(THREE.vectorDisplacementMapShader, "precomputedNormals");   
      this.vectorDisplacementMap.uniforms['processedDepth'].value = processedDepth; 
      this.vectorDisplacementMap.uniforms['displacementVector'].value = this.sceneFlowRenderTarget.texture; 


      this.effectSceneFlowDynamicCorrector = new THREE.ShaderPass(THREE.sceneFlowShader2);    

      this.fxComposer.addPass( this.texturePass );

      this.fxComposer.addPass( this.effectTemporalSmoothDynamic ); // check
      this.fxComposer.addPass( this.effectSaveSmooth ); // check

      this.fxComposer.addPass( this.effectSceneFlowDynamic);  // check
      this.fxComposer.addPass( this.effectSaveFlowDynamic ); // check
      this.fxComposer.addPass( this.effectSceneFlowDynamicCorrector ); // check

      this.fxComposer.addPass( this.hblurDynamic );   // check         
      this.fxComposer.addPass( this.vblurDynamic ); // check
      this.fxComposer.addPass( this.hblurTilt ); // check
      this.fxComposer.addPass( this.vblurTilt ); // check

      this.fxComposer.addPass( this.effectJiggle );   // check
      this.fxComposer.addPass( this.effectSaveJiggle ); //  check

      this.fxComposer.addPass( this.effectComputeLiquidDisplacement );  //check
      this.fxComposer.addPass( this.effectSaveLiquidDisplacement ); //check

      this.fxComposer.addPass( this.hblurVelocity ); // check         
      this.fxComposer.addPass( this.vblurVelocity ); // check

      this.fxComposer.addPass( this.effectSaveSceneFlow ); // check
      
      this.fxComposer.addPass( this.effectComputeLiquidVelocity ); // check
      this.fxComposer.addPass( this.effectSaveLiquidVelocity ); // check
      this.fxComposer.addPass( this.effectSaveVelocity ); // check

      this.fxComposer.addPass( this.precomputeNormals );  // 
      this.fxComposer.addPass( this.savePrecomputeNormals ); //
      this.fxComposer.addPass( this.vectorDisplacementMap );

      this.fxComposer.addPass( this.copyPass );

      this.kinectTexture = this.renderTarget.texture;


  }

  setSize( width, height ) {
      
      this.width = width;
      this.height = height;

      this.smoothRenderTarget.setSize( this.width, this.height );
      this.flowRenderTarget.setSize( this.width, this.height );
      this.jiggleRenderTarget.setSize( this.width, this.height );
      this.liquidDisplacementRenderTarget.setSize( this.width, this.height );
      this.sceneFlowRenderTarget.setSize( this.width, this.height );
      this.liquidVelocityRenderTarget.setSize( this.width, this.height );
      this.renderTargetVelocity.setSize( this.width, this.height );
      this.precomputedNormalsRenderTarget.setSize( this.width, this.height );

      this.precomputeNormals.uniforms[ "resolution" ].value.set( this.width, this.height );
      this.vectorDisplacementMap.uniforms[ "resolution" ].value.set( this.width, this.height );

      this.hblurDynamic.uniforms.h.value = 1.0 / this.height;
      this.vblurDynamic.uniforms.v.value = 1.0 / this.width;
      this.hblurVelocity.uniforms.h.value = 1.0 / this.height;
      this.vblurVelocity.uniforms.v.value = 1.0 / this.width;
      this.hblurTilt.uniforms.h.value = 1.0 / this.height;
      this.vblurTilt.uniforms.v.value = 1.0 / this.width;

      this.effectSceneFlowDynamic.uniforms[ "step" ].value.set( 1.0 / this.width, 1.0 / this.height );


      this.renderTarget.setSize( width, height );
      this.fxComposer.setSize( width, height );

  }
}