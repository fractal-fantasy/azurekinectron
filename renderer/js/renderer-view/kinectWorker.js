const KinectAzure = require('kinect-azure');
var kinect = new KinectAzure();
const path = require('path');

//var playbackMkvFilePath = "";
var playbackMkvFilePath = path.join(__dirname, "../../images/output.mkv");
var playbackStarted = false;

// var colorToDepth = true;

var depthType = {   
    WFOV: KinectAzure.K4A_DEPTH_MODE_WFOV_2X2BINNED,
    NFOV: KinectAzure.K4A_DEPTH_MODE_NFOV_UNBINNED,
    WFOV_UNBINNED: KinectAzure.K4A_DEPTH_MODE_WFOV_UNBINNED,
};

var fpsType = {
    
    WFOV: KinectAzure.K4A_FRAMES_PER_SECOND_30,
    NFOV: KinectAzure.K4A_FRAMES_PER_SECOND_30,
    WFOV_UNBINNED: KinectAzure.K4A_FRAMES_PER_SECOND_15
}

var transformationType = {
    depthToColor: 0,
    colorToDepth: 1
}

// var depthMode = depthType.WFOV;
var depthMode = depthType.NFOV;
var fpsMode = fpsType.NFOV;
var transformationMode = transformationType.depthToColor;

var isListening = false;
var enableBT = false;
var btTemporalSmoothing = 0.5;

var btOptions = {
     processing_mode: KinectAzure.K4ABT_TRACKER_PROCESSING_MODE_GPU_CUDA,
    //processing_mode: KinectAzure.K4ABT_TRACKER_PROCESSING_MODE_GPU_DIRECTML,
    // processing_mode: KinectAzure.K4ABT_TRACKER_PROCESSING_MODE_GPU_TENSORRT,
     model_path: "C:\\Program Files\\Azure Kinect Body Tracking SDK\\tools\\dnn_model_2_0_op11.onnx"
    //model_path: "C:\\Program Files\\Azure Kinect Body Tracking SDK\\tools\\dnn_model_2_0_lite_op11.onnx"
}

function startListening() {

    kinect.startListening( (data) => {
                        
        if ( transformationMode === transformationType.colorToDepth ){
            //// COLOR TO DEPTH
            postMessage( ["got depth frame", data.depthImageFrame.imageData ] );
            postMessage( ["got color frame", data.colorToDepthImageFrame.imageData ] );
            postMessage( ["got ir frame", data.irImageFrame.imageData ] );
        } else {
            //// DEPTH TO COLOR
            postMessage( ["got depth frame", data.depthToColorImageFrame.imageData ] );
            postMessage( ["got color frame", data.colorImageFrame.imageData ] );
            postMessage( ["got ir frame", data.irToColorImageFrame.imageData ] );
        }

        if ( data.bodyFrame.bodies.length > 0 )
            postMessage( ["got body frame", data.bodyFrame ] );                        

    });

}

function startCameras() {

    console.log('Starting kinect cameras..');

    kinect.startCameras(
        {
            depth_mode: depthMode,
            color_format: KinectAzure.K4A_IMAGE_FORMAT_COLOR_BGRA32,
            color_resolution: KinectAzure.K4A_COLOR_RESOLUTION_720P,
            camera_fps: fpsMode,

            // transform depth=>color; processes both ir and depth streams
            include_ir_to_color: transformationMode === transformationType.depthToColor ? true : false,
            
            // transform color=>depth
            include_color_to_depth: transformationMode === transformationType.colorToDepth ? true : false,

        }
    );

    if (enableBT) { 
        kinect.createTracker({
            processing_mode: btOptions.processing_mode,
            model_path: btOptions.model_path
        });

        kinect.setTemporalSmooting( btTemporalSmoothing );

    }           

    startListening();

    postMessage( [ "msg", "kinect started" ] );

    isListening = true;
}

function startKinect() {

    if ( kinect.open() ) {

        startCameras();
        
    }
}

function startPlayback() {

    //var depthModeParam = depthModeStr === "WFOV" ? depthMode.WFOV : depthMode.NFOV;

    if ( kinect.openPlayback( playbackMkvFilePath ) ) {
        playbackStarted = kinect.startPlayback(
            {
                depth_mode: depthMode,
                color_format: KinectAzure.K4A_IMAGE_FORMAT_COLOR_BGRA32,
                color_resolution: KinectAzure.K4A_COLOR_RESOLUTION_720P,
                camera_fps: fpsMode,


                // transform depth=>color; processes both ir and depth streams
                include_ir_to_color: transformationMode === transformationType.depthToColor ? true : false,
                
                // transform color=>depth
                include_color_to_depth: transformationMode === transformationType.colorToDepth ? true : false,

            }
        );
        
        postMessage( [ "msg", "playback started" ] );

        if (enableBT) { 
            kinect.createTracker({
                processing_mode: btOptions.processing_mode,
                model_path: btOptions.model_path
            });

            kinect.setTemporalSmooting( btTemporalSmoothing );

        } 

        startListening();

        isListening = true;
    } else {
        console.log("sorry that didnt work lol");
    }
}

function stopPlayback() {

   // if (playbackStarted) kinect.stopPlayback();

   if ( isListening === false ) return;

    kinect.stopListening().then(() => {
          console.log('kinect stopped listening');
          isListening = false;
       }).catch(e => {
           console.error(e);
       }).then(() => {
            kinect.destroyTracker();
            kinect.stopPlayback();
            kinect.stopCameras();
            kinect.close();

            isListening = false;


            setTimeout( function() {

                kinect = new KinectAzure();

                startKinect();

            }, 1000);

    });
}

function switchToPlayback() {

    if (kinect) {

        //properly close the kinect and send a message back to the renderer
        console.log('close the kinect');

        if ( isListening ) {

            kinect.stopListening().then(() => {
                isListening = false;
                console.log('kinect stopped listening');
             }).catch(e => {
                console.error(e);
             }).then(() => {
                 kinect.destroyTracker();
                 kinect.stopCameras();
                 kinect.close();
     
                 startPlayback();
     
             });

        } else {

            kinect.destroyTracker();
            kinect.stopCameras();
            kinect.close();

            startPlayback();

        }

    }

}

function restartPlayback() {

    if ( isListening === false ) return;

    if (kinect) {

        // properly close the kinect and send a message back to the renderer
        //console.log('close the kinect');
        kinect.stopListening().then(() => {
           console.log('kinect stopped listening');
        }).catch(e => {
           console.error(e);
        }).then(() => {
            kinect.destroyTracker();
            kinect.stopPlayback();
            kinect.close();

            startPlayback();

        });
    }

}


onmessage = function( e ) {

  //  console.log( e.data );


    if (e.data === "start kinect") {

        startKinect();

    }
    
    if (e.data === "close kinect") {

        closeKinect();

    }

    if (e.data === "restart kinect") {

        restartKinect();

    }

    if (e.data === "start playback") {

        switchToPlayback();

    }

    if (e.data === "stop playback") {

        stopPlayback();

    }

    if (e.data === "restart playback") {

        restartPlayback();

    }

    if (e.data === "pause kinect") {

        kinect.pause();

    }

    if (e.data === "resume kinect") {

        kinect.resume();

    }

    if ( e.data.length !== undefined ) {

        if ( e.data[0] === "set depth mode" ) {

            if ( isListening === false ) return;

            isListening = false;

            console.log("Depth mode changed:", e.data[1]);

            switch (e.data[1]) {                

                case "0":
                    depthMode = depthType.WFOV;
                    fpsMode = fpsType.WFOV;
                    transformationMode = transformationType.depthToColor;
                    break;
                case "1":
                    depthMode = depthType.NFOV;
                    fpsMode = fpsType.NFOV;
                    transformationMode = transformationType.depthToColor;
                    break;
                case "2":
                    depthMode = depthType.NFOV;
                    fpsMode = fpsType.NFOV;
                    transformationMode = transformationType.colorToDepth;
                    break;
                // case "3":
                //     depthMode = depthType.WFOV;
                //     fpsMode = fpsType.WFOV;
                //     transformationMode = transformationType.colorToDepth;
                //     break;
                case "3":
                    depthMode = depthType.WFOV_UNBINNED;
                    fpsMode = fpsType.WFOV_UNBINNED;
                    transformationMode = transformationType.depthToColor;
                    break;

            }

            kinect.stopCameras();

            kinect.stopListening().then(() => {
                //console.log('kinect stopped listening');
                postMessage( [ "msg", "kinect stopped listening" ] );
                
            }).catch(e => {
                console.error(e);
            }).then(() => {

                startCameras();

            });

        }

        if (e.data[0] === "setPlaybackMkvFilePath") {

            playbackMkvFilePath = e.data[1];
        }

        if (e.data[0] === "setTemporalSmoothing") {

            btTemporalSmoothing = e.data[1];

            if (enableBT) {
                kinect.setTemporalSmooting( e.data[1] );
            }
        }

        if (e.data[0] === "startBodyTracking") {

            console.log("starting body tracking",e.data);
            if (enableBT) return;

            if ( isListening === false ) return; 

            isListening = false;

            kinect.stopListening().then(() => {
                // console.log('kinect stopped listening');
                postMessage( [ "msg", "kinect stopped listening" ] );
                
            }).catch(e => {
                console.error(e);
            }).then(() => {

                    kinect.destroyTracker();

                    kinect.createTracker({
                        processing_mode: btOptions.processing_mode,
                        model_path: btOptions.model_path
                    });

                    startListening();

                    isListening = true;

                    enableBT = true;

                    console.log('Body tracking enabled.');        
                    postMessage( [ "msg", "kinect started listening" ] );        

            });
            

        }

        if (e.data[0] === "stopBodyTracking") {

            if (!enableBT) return;

            if ( isListening === false ) return; 

            isListening = false;

            kinect.stopListening().then(() => {
                // console.log('kinect stopped listening');
                postMessage( [ "msg", "kinect stopped listening" ] );
                
            }).catch(e => {
                console.error(e);
            }).then(() => {

                kinect.destroyTracker();

                startListening();

                isListening = true;

                enableBT = false;

                console.log('Body tracking disabled.');
                postMessage( [ "msg", "kinect started listening" ] );   

            });
        }


        if (e.data[0] === "setBTProcessingMode") {

            console.log("set body tracking", e.data);

            var mode;
            switch( e.data[1] ) {
                case 0:
                    mode = KinectAzure.K4ABT_TRACKER_PROCESSING_MODE_GPU_CUDA;
                    break;
                case 1:
                    mode = KinectAzure.K4ABT_TRACKER_PROCESSING_MODE_GPU_DIRECTML;
                    break;
                case 2:
                    mode = KinectAzure.K4ABT_TRACKER_PROCESSING_MODE_GPU_TENSORRT
                    break;
            }

            btOptions.processing_mode = mode;

            restartKinect();

            console.log("bt tracker mode changed:", mode);
        }

        if (e.data[0] === "setBTModelPath") {

            var path;

            switch( e.data[1] ) {
                case 0:
                    path = "C:\\Program Files\\Azure Kinect Body Tracking SDK\\tools\\dnn_model_2_0_op11.onnx";
                    break;
                case 1:
                    path = "C:\\Program Files\\Azure Kinect Body Tracking SDK\\tools\\dnn_model_2_0_lite_op11.onnx";
                    break;
            }

            btOptions.model_path = path;

            restartKinect();

            console.log("bt DNN model path changed:", path);
        }
    }

}

function closeKinect() {

    if (kinect) {

        // properly close the kinect and send a message back to the renderer

        isListening = false;

        console.log('closing kinect...');

        kinect.stopListening().then(() => {
            console.log('kinect closed');
        }).catch(e => {
            console.error(e);
        }).then(() => {
            kinect.destroyTracker();
            kinect.stopCameras();
            kinect.close();

            postMessage( [ "msg", "kinect closed" ] );

        });
    } else {

        postMessage( [ "msg", "kinect is undefined" ] );

    }

}

function restartKinect() {

    if (kinect) {

        // properly close the kinect and send a message back to the renderer

        // if ( isListening === true ) {

            isListening = false;

            console.log('restarting kinect...');

            kinect.stopListening().then(() => {
                //console.log('kinect stopped listening');
                postMessage( [ "msg", "kinect stopped listening" ] );
                
            }).catch(e => {
                console.error(e);
            }).then(() => {
                kinect.destroyTracker();
                kinect.stopCameras();
                kinect.close();

                // postMessage( [ "msg", "kinect closed" ] );

                startKinect();

            });
        // } else {

        //     kinect.destroyTracker();
        //     kinect.stopCameras();
        //     kinect.close();

        //     postMessage( [ "msg", "kinect closed" ] );


        // }

        
    }

}