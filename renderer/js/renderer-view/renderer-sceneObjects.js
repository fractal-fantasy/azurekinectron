var rawDepthProcShader = {

        uniforms: {
                "in_texture": { value: null },
                "minDepth": { value: 100 },
                "maxDepth": { value: 2000 },
                "depthProc_gradientEnable": { value: false },
                "depthProc_uvScale": { value: 1.0 },
                "depthProc_gradThreshold": { value: 0.1 }
        },


        vertexShader: 
        `   #version 300 es

            #define attribute in
			#define varying out

 			uniform mat4 modelViewMatrix;
			uniform mat4 projectionMatrix;

            attribute vec3 position;
			attribute vec2 uv;
        
            varying vec2 vUv;

            void main() {

                vUv = uv;

                gl_Position = projectionMatrix * modelViewMatrix * vec4( position, 1.0 );

            }
        
        `,

        fragmentShader: 
        `   #version 300 es

            #define varying in
            #define texture2D texture

            precision highp float;
            precision highp int;
            precision highp usampler2D;

            varying vec2 vUv;
            uniform usampler2D in_texture;
            uniform float minDepth;
            uniform float maxDepth;
            uniform bool depthProc_gradientEnable;
            uniform float depthProc_uvScale;
            uniform float depthProc_gradThreshold;

            out vec4 out_FragColor;

            vec2 resolution = vec2(1280.0, 720.0);

            float map( in float value, in float inputMin, in float inputMax, in float outputMin, in float outputMax ){
                return (value - inputMin) * (outputMax - outputMin) / (inputMax - inputMin) + outputMin;
            }
            
            //COMPUTES GRADIENT
            vec2 dHdxy_fwd() {

                float bumpScale = 1.0;

                vec2 dSTdx = dFdx( vUv * depthProc_uvScale );
                vec2 dSTdy = dFdy( vUv * depthProc_uvScale );

                //CENTER SAMPLE
                float Hll = bumpScale * float( ( texture2D( in_texture, vUv ).y << 8u ) + texture2D( in_texture, vUv ).x ) / 1000.0;

                //CALCULATE SLOPES FROM OFFSET SAMPLES dbx, dby
                float dBx = bumpScale * float( ( texture2D( in_texture, vUv + dSTdx ).y << 8u ) + texture2D( in_texture, vUv + dSTdx ).x ) / 1000.0 - Hll;
                float dBy = bumpScale * float( ( texture2D( in_texture, vUv + dSTdy ).y << 8u ) + texture2D( in_texture, vUv + dSTdy ).x ) / 1000.0 - Hll;

                return vec2( dBx, dBy );

            }

            void main() {

                uint byteX = texture2D( in_texture, vUv ).x;
                uint byteY = texture2D( in_texture, vUv ).y;
                uint depthu = ( byteY << 8u ) + byteX;

                float depthf =  float(depthu);

                float mapped = depthf >= minDepth && depthf <= maxDepth ? 1.0 - map( depthf, minDepth, maxDepth, 0.0, 1.0 ) : 0.0;
                
          
                if (depthProc_gradientEnable) {
                    vec2 gr = dHdxy_fwd();
                    float gradient = abs( gr.x + gr.y );    
                    if ( gradient > depthProc_gradThreshold ) mapped = 0.0;
                }

                out_FragColor = vec4( vec3( mapped ), 1.0 );

                // recover true zero => undefined pixels <= from the original raw data
                if ( depthf <= 0.0 ) out_FragColor.rgb = vec3( 0.0 );


            }
        `
}


var rawIRProcShader = {

        uniforms: {
            "in_texture": { value: null },
            "minIR": { value: 100 },
            "maxIR": { value: 2000 },

            "barrelAlpha": { value: 0.0 },
            "barrelScale": { value: 1.0 }
        },

        vertexShader: 
        `   #version 300 es

            #define attribute in
			#define varying out

 			uniform mat4 modelViewMatrix;
			uniform mat4 projectionMatrix;

            attribute vec3 position;
			attribute vec2 uv;

            varying vec2 vUv;

            void main() {
                vUv = uv;
                gl_Position = projectionMatrix * modelViewMatrix * vec4( position, 1.0 );
            }        
        `,
        fragmentShader: 
        `   #version 300 es

            #define varying in
            #define texture2D texture

            precision mediump float;
            precision mediump int;
            precision mediump usampler2D;

            varying vec2 vUv;
            uniform usampler2D in_texture;
            uniform float minIR;
            uniform float maxIR;

            uniform float barrelAlpha;
            uniform float barrelScale;

            out vec4 out_FragColor;

            void main() {

                // barrel distortion correction

                float irf = 0.0;

                vec2 p1 = vec2( 2.0 * vUv - 1.0 );
                p1 *= barrelScale;
                vec2 p2 = p1 / ( 1.0 - barrelAlpha * length( p1 ) );
                p2 = ( p2 + 1.0 ) * 0.5;

                if ( all( greaterThanEqual( p2, vec2( 0.0 ) ) ) && all( lessThanEqual( p2, vec2( 1.0 ) ) ) ) {

                    uint byteX = texture2D( in_texture, p2 ).x;
                    uint byteY = texture2D( in_texture, p2 ).y;
                    
                    uint iru = ( byteY << 8u ) + byteX;
                    irf = float(iru) / maxIR;
    
                }

                out_FragColor = vec4( vec3(irf), 1.0 );

                //

                /*
                uint byteX = texture2D( in_texture, vUv ).x;
                uint byteY = texture2D( in_texture, vUv ).y;

                uint iru = ( byteY << 8u ) + byteX;
                float irf = float(iru) / maxIR;
                
                out_FragColor = vec4( vec3(irf), 1.0 );
                */

            }        
        `
    }


var rawColorProcShader = {

        uniforms: {
                "in_texture": { value: null },
                "barrelAlpha": { value: 0.0 },
                "barrelScale": { value: 1.0 }
        },

        vertexShader:
         `   #version 300 es

            #define attribute in
			#define varying out

 			uniform mat4 modelViewMatrix;
			uniform mat4 projectionMatrix;

            attribute vec3 position;
			attribute vec2 uv;
     
            varying vec2 vUv;

            void main() {
                vUv = uv;
                gl_Position = projectionMatrix * modelViewMatrix * vec4( position, 1.0 );
            }        
        `,
        fragmentShader:
        `   #version 300 es

            #define varying in
            #define texture2D texture

            precision lowp float;
            precision lowp int;
            precision lowp usampler2D;

            varying vec2 vUv;
            uniform usampler2D in_texture;

            uniform float barrelAlpha;
            uniform float barrelScale;

            out vec4 out_FragColor;

            void main() {

                // barrel distortion correction

                uvec3 color = uvec3( 0.0 );

                vec2 p1 = vec2( 2.0 * vUv - 1.0 );
                p1 *= barrelScale;
                vec2 p2 = p1 / ( 1.0 - barrelAlpha * length( p1 ) );
                p2 = ( p2 + 1.0 ) * 0.5;

                if ( all( greaterThanEqual( p2, vec2( 0.0 ) ) ) && all( lessThanEqual( p2, vec2( 1.0 ) ) ) ) {

                    color = texture2D( in_texture, p2 ).zyx;   // Reverse BGR order to RGB
    
                }

                //uvec3 color = texture2D( in_texture, vUv ).zyx;   // Reverse BGR order to RGB

                out_FragColor = vec4( float(color.x) / 255., float(color.y) / 255., float(color.z) / 255., 1.0 );

            }        
        `
    }

    

// var rawColorrampProcShader = {

//     uniforms: {
//         "in_texture": { value: null },
//         "resolution": { value: new THREE.Vector2(1280,720) },
//         "time": { value: null }
        
//     },

//     vertexShader:
//     `   #version 300 es

//         #define attribute in
//         #define varying out

//         uniform mat4 modelViewMatrix;
//         uniform mat4 projectionMatrix;

//         attribute vec3 position;
//         attribute vec2 uv;
        
//         varying vec2 vUv;

//         void main() {
//             vUv = uv;
//             gl_Position = projectionMatrix * modelViewMatrix * vec4( position, 1.0 );
//         }        
//     `,
//     fragmentShader:
//     `   #version 300 es
        
//         #define varying in
//         #define texture2D texture

//         precision highp float;
//         precision lowp usampler2D;

//         //precision highp vec2;
//         varying vec2 vUv;

//         out vec4 out_FragColor;


//         uniform vec2 resolution;
//         uniform float time;

//         #define iterations 17
//         #define formuparam 0.53

//         #define volsteps 100
//         #define stepsize 0.050

//         #define zoom   0.800
//         #define tile   0.850
//         #define speed  0.10 

//         #define brightness 0.0015
//         #define darkmatter 0.300
//         #define distfading 0.760
//         #define saturation 0.800

//         uniform usampler2D in_texture;
        

//         void main(){
//             vec2 mouseXY = vec2(200.0,200.0);

//             //get coords and direction
//             vec2 textureUv=vUv.xy/resolution.xy-.5;
//             textureUv.y*=resolution.y/resolution.x;
//             vec3 dir=vec3(textureUv*zoom,1.);
//             //float myTime=(time-3311.)*speed;
//             float myTime=100.0;
            
//             vec3 from=vec3(1.,.5,0.5);
            
            
//             vec3 forward = vec3(0.,0.,1.);
            
//             //mouse rotation
//             float a1 = 0.3;//3.1415926 * (mouseXY.x/resolution.x-.5);
//             mat2 rot1 = mat2(cos(a1),sin(a1),-sin(a1),cos(a1));
//             float a2 = .6;//3.1415926 * (mouseXY.y/resolution.y-.5);
//             mat2 rot2 = mat2(cos(a2),sin(a2),-sin(a2),cos(a2));
//             dir.xz*=rot1;
//             forward.xz *= rot1;
//             dir.yz*=rot1;
//             forward.yz *= rot1;

//             // pan (dodgy)
//             from += (mouseXY.x/resolution.x-.5)*vec3(-forward.z,0.,forward.x);
            
//             //zoom
//             float zooom = myTime;
//             from += forward* zooom;
//             float sampleShift = mod( zooom, stepsize );
//             float zoffset = -sampleShift;
//             sampleShift /= stepsize; // make from 0 to 1
            
//             //volumetric rendering
//             float s=0.1;
//             vec3 v=vec3(0.);
//             for (int r=0; r<volsteps; r++) {
//                 vec3 p=from+(s+zoffset)*dir;// + vec3(0.,0.,zoffset);
//                 p = abs(vec3(tile)-mod(p,vec3(tile*2.))); // tiling fold
//                 float pa,a=pa=0.;
//                 for (int i=0; i<iterations; i++) { 
//                     p=abs(p)/dot(p,p)-formuparam; // the magic formula
//                     //p=abs(p)/max(dot(p,p),0.005)-formuparam; // another interesting way to reduce noise
//                     float D = abs(length(p)-pa); // absolute sum of average change
//                     a += i > 7 ? min( 12., D) : D;
//                     pa=length(p);
//                 }
//                 //float dm=max(0.,darkmatter-a*a*.001); //dark matter
//                 a*=a*a; // add contrast
//                 //if (r>3) fade*=1.-dm; // dark matter, don't render near
//                 // brightens stuff up a bit
//                 float s1 = s+zoffset;
//                 // need closed form expression for this, now that we shift samples
//                 float fade = pow(distfading,max(0.,float(r)-sampleShift));
//                 v+=fade;
                
//                 // fade out samples as they approach the camera
//                 if( r == 0 )
//                     fade *= 1. - sampleShift;
//                 // fade in samples as they approach from the distance
//                 if( r == volsteps-1 )
//                     fade *= sampleShift;
//                 v+=vec3(2.*s1,4.*s1*s1,16.*s1*s1*s1*s1)*a*brightness*fade; // coloring based on distance
//                 s+=stepsize;
//             }
//             v=mix(vec3(length(v)),v,saturation); //color adjust
//             //out_FragColor = vec4(v*0.01,1.0);	
//             //out_FragColor = vec4(v*2.0,1.0);	


         
//              //uvec3 color = texture2D( in_texture, vUv ).xyz;

//             out_FragColor = vec4(time,time,time,1.0);	
//             //out_FragColor = vec4( ramp, 1.0 );
//         } 
//     `
// }

var rawColorrampProcShader = {

    uniforms: {
        "in_texture": { value: null },
        "minDepth": { value: 100 },
        "maxDepth": { value: 2000 },
        "colorStart": { value: 0.9 },
        "colorEnd": { value: 1.0 }
        
    },

    vertexShader:
    `   #version 300 es

        #define attribute in
        #define varying out

        uniform mat4 modelViewMatrix;
        uniform mat4 projectionMatrix;

        attribute vec3 position;
        attribute vec2 uv;
        
        varying vec2 vUv;

        void main() {
            vUv = uv;
            gl_Position = projectionMatrix * modelViewMatrix * vec4( position, 1.0 );
        }        
    `,
    fragmentShader:
    `   #version 300 es

        #define varying in
        #define texture2D texture

        precision lowp float;
        precision lowp int;
        precision lowp usampler2D;

        varying vec2 vUv;
        uniform usampler2D in_texture;
        uniform float minDepth;
        uniform float maxDepth;
        uniform float colorStart;
        uniform float colorEnd;

        out vec4 out_FragColor;

        float map( in float value, in float inputMin, in float inputMax, in float outputMin, in float outputMax ){
            return (value - inputMin) * (outputMax - outputMin) / (inputMax - inputMin) + outputMin;
        }

        
        vec3 hsv2rgb(in vec3 c) {
            vec4 K = vec4(1.0, 2.0 / 3.0, 1.0 / 3.0, 3.0);
            vec3 p = abs(fract(c.xxx + K.xyz) * 6.0 - K.www);
            return c.z * mix(K.xxx, clamp(p - K.xxx, 0.0, 1.0), c.y);
        }

        void main() {

            //vec2 pos = vec2(vUv) * 0.5 + 0.5;

            uint byteX = texture2D( in_texture, vUv ).x;
            uint byteY = texture2D( in_texture, vUv ).y;

            uint depthu = ( byteY << 8u ) + byteX;

            float depthf = float(depthu);

            float mapped = depthf >= minDepth && depthf <= maxDepth ? 1.0 - map( depthf, minDepth, maxDepth, colorStart, colorEnd ) : 0.0;

            float vel = mapped > 0.0 ? 1.0 : 0.0;
            vec3 ramp = hsv2rgb( vec3(mapped, 1.0, vel) );

            out_FragColor = vec4( ramp, 1.0 );

        }        
    `
}
var rawNormalProcShader = {

        uniforms: {
                "bumpMap": { value: undefined },
                "bumpScale": { value: 1 },
                "uvScale": { value: 2 }
        },

        vertexShader: `

            varying vec2 vUv;
            varying vec3 vViewPosition;
            varying vec3 vNormal;

            void main() {

                vUv = uv;

                vec3 objectNormal = vec3( normal );

                vec3 transformedNormal = objectNormal;

                vNormal = normalize( transformedNormal );


                vec3 transformed = vec3( position );

                vec4 mvPosition = vec4( transformed, 1.0 );

                mvPosition = modelViewMatrix * mvPosition;

                vViewPosition = - mvPosition.xyz;


                gl_Position = projectionMatrix * modelViewMatrix * vec4( position, 1.0 );
            }        
        `,
        fragmentShader: `

        
          


            varying vec2 vUv;

            varying vec3 vViewPosition;
            varying vec3 vNormal;

            uniform sampler2D bumpMap;
            uniform float bumpScale;
            uniform float uvScale;

            // Bump Mapping Unparametrized Surfaces on the GPU by Morten S. Mikkelsen
            // http://api.unrealengine.com/attachments/Engine/Rendering/LightingAndShadows/BumpMappingWithoutTangentSpace/mm_sfgrad_bump.pdf

            // Evaluate the derivative of the height w.r.t. screen-space using forward differencing (listing 2)

            vec2 dHdxy_fwd() {

                vec2 dSTdx = dFdx( vUv * uvScale );
                vec2 dSTdy = dFdy( vUv * uvScale );

                float Hll = bumpScale * texture2D( bumpMap, vUv ).x;
                float dBx = bumpScale * texture2D( bumpMap, vUv + dSTdx ).x - Hll;
                float dBy = bumpScale * texture2D( bumpMap, vUv + dSTdy ).x - Hll;

                return vec2( dBx, dBy );

            }

            vec3 perturbNormalArb( vec3 surf_pos, vec3 surf_norm, vec2 dHdxy ) {

                // Workaround for Adreno 3XX dFd*( vec3 ) bug. See #9988

                vec3 vSigmaX = vec3( dFdx( surf_pos.x ), dFdx( surf_pos.y ), dFdx( surf_pos.z ) );
                vec3 vSigmaY = vec3( dFdy( surf_pos.x ), dFdy( surf_pos.y ), dFdy( surf_pos.z ) );
                vec3 vN = surf_norm;		// normalized

                vec3 R1 = cross( vSigmaY, vN );
                vec3 R2 = cross( vN, vSigmaX );

                float fDet = dot( vSigmaX, R1 );

                fDet *= ( float( gl_FrontFacing ) * 2.0 - 1.0 );

                vec3 vGrad = sign( fDet ) * ( dHdxy.x * R1 + dHdxy.y * R2 );
                return normalize( abs( fDet ) * surf_norm - vGrad );

            }

            vec3 packNormalToRGB( const in vec3 normal ) {
                return normalize( normal ) * 0.5 + 0.5;
            }

            void main() {

                vec3 normal = perturbNormalArb( -vViewPosition, vNormal, dHdxy_fwd() );

                vec3 packed = packNormalToRGB( normal );

                gl_FragColor = vec4( packed, 1.0 );

            }
        `
    }


//     var rawColorrampProcShader2 = {

//     uniforms: {
 
//         "resolution": { value: new THREE.Vector2(1280,720) },
//         "time": { value: 1.0 },
        
//     },

//     vertexShader: `
     
//         varying vec2 vUv;

//         void main() {
//             vUv = uv;

//             gl_Position =   projectionMatrix * 
//                             modelViewMatrix * 
//                             vec4(position,1.0);
//         }

//     `,
//     fragmentShader: `

//         varying vec2 vUv;

//         uniform vec2 resolution;
//         varying float time;

//         #define iterations 17
//         #define formuparam 0.53

//         #define volsteps 100
//         #define stepsize 0.050

//         #define zoom   0.800
//         #define tile   0.850
//         #define speed  0.10 

//         #define brightness 0.0015
//         #define darkmatter 0.300
//         #define distfading 0.760
//         #define saturation 0.800

        
        

//         void main(){
//             vec2 mouseXY = vec2(0.5,0.5);

//             //get coords and direction
//             vec2 uv=vUv.xy/resolution.xy-.5;
//             uv.y*=resolution.y/resolution.x;
//             vec3 dir=vec3(uv*zoom,1.);
//             float myTime=(time-3311.)*speed;

            
//             vec3 from=vec3(1.,.5,0.5);
            
            
//             vec3 forward = vec3(0.,0.,1.);
            
//             //mouse rotation
//             float a1 = 0.3;//3.1415926 * (mouseXY.x/resolution.x-.5);
//             mat2 rot1 = mat2(cos(a1),sin(a1),-sin(a1),cos(a1));
//             float a2 = .6;//3.1415926 * (mouseXY.y/resolution.y-.5);
//             mat2 rot2 = mat2(cos(a2),sin(a2),-sin(a2),cos(a2));
//             dir.xz*=rot1;
//             forward.xz *= rot1;
//             dir.yz*=rot1;
//             forward.yz *= rot1;

//             // pan (dodgy)
//             from += (mouseXY.x/resolution.x-.5)*vec3(-forward.z,0.,forward.x);
            
//             //zoom
//             float zooom = myTime;
//             from += forward* zooom;
//             float sampleShift = mod( zooom, stepsize );
//             float zoffset = -sampleShift;
//             sampleShift /= stepsize; // make from 0 to 1
            
//             //volumetric rendering
//             float s=0.1;
//             vec3 v=vec3(0.);
//             for (int r=0; r<volsteps; r++) {
//                 vec3 p=from+(s+zoffset)*dir;// + vec3(0.,0.,zoffset);
//                 p = abs(vec3(tile)-mod(p,vec3(tile*2.))); // tiling fold
//                 float pa,a=pa=0.;
//                 for (int i=0; i<iterations; i++) { 
//                     p=abs(p)/dot(p,p)-formuparam; // the magic formula
//                     //p=abs(p)/max(dot(p,p),0.005)-formuparam; // another interesting way to reduce noise
//                     float D = abs(length(p)-pa); // absolute sum of average change
//                     a += i > 7 ? min( 12., D) : D;
//                     pa=length(p);
//                 }
//                 //float dm=max(0.,darkmatter-a*a*.001); //dark matter
//                 a*=a*a; // add contrast
//                 //if (r>3) fade*=1.-dm; // dark matter, don't render near
//                 // brightens stuff up a bit
//                 float s1 = s+zoffset;
//                 // need closed form expression for this, now that we shift samples
//                 float fade = pow(distfading,max(0.,float(r)-sampleShift));
//                 v+=fade;
                
//                 // fade out samples as they approach the camera
//                 if( r == 0 )
//                     fade *= 1. - sampleShift;
//                 // fade in samples as they approach from the distance
//                 if( r == volsteps-1 )
//                     fade *= sampleShift;
//                 v+=vec3(2.*s1,4.*s1*s1,16.*s1*s1*s1*s1)*a*brightness*fade; // coloring based on distance
//                 s+=stepsize;
//             }
//             v=mix(vec3(length(v)),v,saturation); //color adjust
//             fragColor = vec4(v*.01,1.);	
//             //out_FragColor = vec4( ramp, 1.0 );
//         }
//     `,
// }
    

// function starClouds() {

//     var geometry = new THREE.PlaneBufferGeometry(COLOR_WIDTH, COLOR_HEIGHT, 2, 2);
//     var material = new THREE.MeshBasicMaterial({ map: depth_colorramp_texture });
//     var mesh = new THREE.Mesh( geometry, material );
//     return mesh;
// }

// function creatDepthColorRamp() {

//     var geometry = new THREE.PlaneBufferGeometry(COLOR_WIDTH, COLOR_HEIGHT, 2, 2);
//     var material = new THREE.MeshBasicMaterial({ map: depth_colorramp_texture });
//     var mesh = new THREE.Mesh( geometry, material );
//     return mesh;
// }


// function creatIRProc() {

//     var geometry = new THREE.PlaneBufferGeometry(COLOR_WIDTH, COLOR_HEIGHT, 2, 2);
//     var material = new THREE.MeshBasicMaterial({ map: glslProcRenderTarget_ir.texture });
//     var mesh = new THREE.Mesh( geometry, material );
//     return mesh;
// }


// function creatColorProc() {

//     var geometry = new THREE.PlaneBufferGeometry(COLOR_WIDTH, COLOR_HEIGHT, 2, 2);
//     var material = new THREE.MeshBasicMaterial({ map: color_texture });
//     var mesh = new THREE.Mesh( geometry, material );
//     return mesh;
// }


// function creatNormalMap() {

//     var geometry = new THREE.PlaneBufferGeometry(COLOR_WIDTH, COLOR_HEIGHT, 2, 2);
//     var material = new THREE.MeshBasicMaterial({ map: normal_texture });
//     var mesh = new THREE.Mesh( geometry, material );
//     return mesh;
// }

var clippingPlane;
function createPhysicalMaterialMesh(paramsProgram) {

    clippingPlane = new THREE.Plane( new THREE.Vector3( 0, 0, 1 ), -1 );
    renderer.localClippingEnabled = true;
    clippingPlane.constant = -12.0;

    var geometry = new THREE.PlaneBufferGeometry(DEPTH_TO_COLOR_WIDTH, DEPTH_TO_COLOR_HEIGHT, DEPTH_TO_COLOR_WIDTH, DEPTH_TO_COLOR_HEIGHT);

    //THREE.BufferGeometryUtils.computeTangents( geometry );

    //var material = new THREE.MeshPhongMaterial({
    var material = new THREE.MeshStandardMaterial({

        color: new THREE.Color( "#" + paramsProgram.material.diffuse.value ),
        displacementMap: depth_texture,
        displacementScale: paramsProgram.material.displacementScale.value,
        bumpMap: depth_texture,
        bumpScale: paramsProgram.material.bumpScale.value,
        roughness: paramsProgram.material.roughness.value,
        metalness: paramsProgram.material.metalness.value,
        clippingPlanes: [ clippingPlane ],
        envMapIntensity: 1.0,
        
        flatShading: true,
        transparent: true,
        blending: 1


    })
    var mesh = new THREE.Mesh(geometry, material);
  
    return mesh;

}

function createPhongMaterialMesh(paramsProgram) {

    clippingPlane = new THREE.Plane( new THREE.Vector3( 0, 0, 1 ), -1 );
    renderer.localClippingEnabled = true;
    clippingPlane.constant = -12.0;

    var geometry = new THREE.PlaneBufferGeometry(COLOR_WIDTH, COLOR_HEIGHT, COLOR_WIDTH, COLOR_HEIGHT);

    var material = new THREE.MeshPhongMaterial({

        color: new THREE.Color( "#" + paramsProgram.material.diffuse.value ),
        displacementMap: depth_texture,
        displacementScale: paramsProgram.material.displacementScale.value,
        bumpMap: depth_texture,
        bumpScale: paramsProgram.material.bumpScale.value,
        shininess: paramsProgram.material.shininess.value,
        reflectivity: paramsProgram.material.reflectivity.value,
        clippingPlanes: [ clippingPlane ],
        
        flatShading: false,
        transparent: true,
        blending: 1


    })
    var mesh = new THREE.Mesh(geometry, material);
  
    return mesh;

}

var smoothLightsMaterial;
var smoothLightsMaterial2;


function createDepthMesh( paramsProgram, width, height ) {

    
   
    smoothLightsMaterial = smoothLightsMaterialTemplate.clone();
    smoothLightsMaterial.uniforms['diffuse'].value = new THREE.Color(0xffffff);
    //smoothLightsMaterial.uniforms['precomputedNormals'].value = dynamicsPostFX.precomputedNormalsRenderTarget.texture;
    smoothLightsMaterial.uniforms['precomputedNormals'].value = dynamicsPostFX.precomputedNormalsRenderTarget.texture;
    
    smoothLightsMaterial.uniforms['edgeMask'].value = depthPostFX.edgeRenderTarget.texture;
    //smoothLightsMaterial.displacementMap = depth_texture;
   // smoothLightsMaterial.uniforms['displacementScale'].value = paramsProgram.material.displacementScale.value;
    smoothLightsMaterial.uniforms['displacementVectorMap'].value = dynamicsPostFX.renderTarget.texture;
    smoothLightsMaterial.uniforms['opacity'].value = paramsProgram.material.opacity.value;
    smoothLightsMaterial.uniforms['roughness'].value = paramsProgram.material.roughness.value;
    smoothLightsMaterial.uniforms['metalness'].value = paramsProgram.material.metalness.value;
    smoothLightsMaterial.uniforms['resolution'].value = new THREE.Vector2( width, height );
    smoothLightsMaterial.uniforms['normalsRatio'].value = params.depthPostprocessing.normalsRatio.value;
    smoothLightsMaterial.uniforms['lookupRadius'].value = params.depthPostprocessing.lookupRadius.value;
    //smoothLightsMaterial.uniforms['normalMap'].value = true;
    // smoothLightsMaterial.uniforms['amplitude'].value = paramsProgram.properties.amplitude.value;
    // smoothLightsMaterial.uniforms['rippleSpeed'].value = paramsProgram.properties.rippleSpeed.value;
    // smoothLightsMaterial.uniforms['rippleFrequency'].value = paramsProgram.properties.rippleFrequency.value;
    // smoothLightsMaterial.uniforms['rippleAmplitude'].value = paramsProgram.properties.rippleAmplitude.value;

    // smoothLightsMaterial.uniforms['normalZ'].value = paramsProgram.material.normalZ.value;
    // smoothLightsMaterial.vertexTangents = true;
    // smoothLightsMaterial.alphaTest = 0.5;
    // smoothLightsMaterial.depthWrite = true;

    // smoothLightsMaterial.depthTest = false;
    smoothLightsMaterial.uniforms['normalScale'].value = new THREE.Vector2( 1, 1 );
    smoothLightsMaterial.uniforms['showNormalMap'].value = false;
    smoothLightsMaterial.transparent = true;
    smoothLightsMaterial.needsUpdate = true;



    // smoothLightsMaterial2 = smoothLightsMaterialTemplate2.clone();
    // smoothLightsMaterial2.name = "KINECT PHYSICAL MATERIAL MIKA";
    // smoothLightsMaterial2.uniforms['diffuse'].value = new THREE.Color(0xffffff);
    // smoothLightsMaterial2.displacementMap = depth_texture;
    // smoothLightsMaterial2.uniforms['displacementMap'].value =depth_texture;
    // smoothLightsMaterial2.uniforms['displacementScale'].value = paramsProgram.material.displacementScale.value;
    // //smoothLightsMaterial2.uniforms['displacementVectorMap'].value = renderTargetDisplacement.texture;
    // smoothLightsMaterial2.uniforms['opacity'].value = paramsProgram.material.opacity.value;
    // smoothLightsMaterial2.uniforms['roughness'].value = paramsProgram.material.roughness.value;
    // smoothLightsMaterial2.uniforms['metalness'].value = paramsProgram.material.metalness.value;
    // smoothLightsMaterial2.uniforms['map'].value = paramsProgram.material.map.value;
    // smoothLightsMaterial2.uniforms['resolution'].value = new THREE.Vector2( width, height );
    // smoothLightsMaterial2.uniforms['normalsRatio'].value = params.depthPostprocessing.normalsRatio.value;
    // smoothLightsMaterial2.uniforms['lookupRadius'].value = params.depthPostprocessing.lookupRadius.value;
    // smoothLightsMaterial2.uniforms['amplitude'].value = paramsProgram.properties.amplitude.value;
    // smoothLightsMaterial2.uniforms['rippleSpeed'].value = paramsProgram.properties.rippleSpeed.value;
    // smoothLightsMaterial2.uniforms['rippleFrequency'].value = paramsProgram.properties.rippleFrequency.value;
    // smoothLightsMaterial2.uniforms['rippleAmplitude'].value = paramsProgram.properties.rippleAmplitude.value;
    // smoothLightsMaterial2.uniforms['normalZ'].value = paramsProgram.material.normalZ.value;
    //  smoothLightsMaterial2.vertexTangents = true;
    //  smoothLightsMaterial2.flatShading = false;
    // smoothLightsMaterial2.uniforms['normalScale'].value = new THREE.Vector2( 1, 1 );
    // smoothLightsMaterial2.uniforms['showNormalMap'].value = false;
    // smoothLightsMaterial2.transparent = true;
    // smoothLightsMaterial2.needsUpdate = true;

    if (paramsProgram.material.doubleSide.value){
        smoothLightsMaterial.side = THREE.DoubleSide;
    }else {
        smoothLightsMaterial.side = THREE.FrontSide;
    }
    

    var geometry = new THREE.PlaneBufferGeometry( width, height, width, height );
    var mesh = new THREE.Mesh(geometry, smoothLightsMaterial);
  
    //OLD MATERIAL TEST
    mesh.material= smoothLightsMaterial;

   // mesh.renderOrder = 10;  // fix for opacity issue on background plane
    mesh.isUsingEcho = true;
    mesh.frustumCulled = false;

    return mesh;

}



function createPointcloud( paramsProgram, width, height ) {

    var geometry = new THREE.PlaneBufferGeometry( width, height, width / 4, height / 4);

    var material = new THREE.ShaderMaterial({

        uniforms: {

            //"displacementMap": { value: glslProcRenderTarget_depth.texture },
            "displacementMap": { value: depth_texture },
            "displacementScale": { value: paramsProgram.material.displacementScale.value },
            "map": { value: glslProcRenderTarget_ir.texture },
            "pointSize": { value: 1.0 },
        
            "depthCutLevel": { value: 0.1 },
            "useColorRamp": { value: false },
            "colorShift": { value: paramsProgram.material.colorShift.value },
            "colorWidth": { value: paramsProgram.material.colorWidth.value },
            "brightness": { value: paramsProgram.material.brightness.value },
            "saturation": { value: paramsProgram.material.saturation.value },

            minDepth: { value: params.depth.minDepth.value },
            maxDepth: { value: params.depth.maxDepth.value },

            "useSingleChannelColorMap": { value: false }
        },
        vertexShader: shader_pointcloud.vertex,
        fragmentShader: shader_pointcloud.fragment

    })

    var mesh = new THREE.Points(geometry, material);

    mesh.frustumCulled = false;

    return mesh;

}

    function setPointcloudGeometryDensity( division ) {

        sceneObjects.pointcloud.children[0].geometry = new THREE.PlaneBufferGeometry(DEPTH_TO_COLOR_WIDTH, DEPTH_TO_COLOR_HEIGHT, 
                                                    DEPTH_TO_COLOR_WIDTH / division, DEPTH_TO_COLOR_HEIGHT / division);
        sceneObjects.pointcloud.children[1].geometry = new THREE.PlaneBufferGeometry(DEPTH_WIDTH, DEPTH_HEIGHT, 
                                                    DEPTH_WIDTH / division, DEPTH_HEIGHT / division);


    }

    function createInstancedSample( paramsProgram, width, height ) {

        var divider = 8;

        var instances_X = width / divider;
        var instances_Y = height / divider;

        var instances = instances_X * instances_Y;

        var instancesFull = instances_X * instances_Y;

        //var primitive = new THREE.OctahedronBufferGeometry(5, 0);
        //var primitive = new THREE.CylinderBufferGeometry(2, 2, 20);
        primitive = new THREE.IcosahedronBufferGeometry(1.5, 2);

        var geometry = new THREE.InstancedBufferGeometry().copy(primitive);

        var offsetsFull = new THREE.InstancedBufferAttribute(new Float32Array(instancesFull * 3), 3, false);

        for (var i = 0, l = instances; i < l; i++) {

            var position = new THREE.Vector3();
            position.x = (i % instances_X) - instances_X / 2;
            position.y = Math.floor(i / instances_X) - instances_Y / 2;

            offsetsFull.setXYZ(i, position.x * divider, position.y * divider, position.z);

        }

        for (var i = instances; i < offsetsFull.count; i++) {

            offsetsFull.setXYZ(i, 0, 0, 2000);

        }

        geometry.setAttribute('offset', offsetsFull);

        var smoothLightsMaterial = smoothLightsMaterialInstancedTemplate.clone();
        smoothLightsMaterial.uniforms['diffuse'].value = new THREE.Color(0xffffff);
        smoothLightsMaterial.uniforms['displacementMap'].value = depth_texture;
        smoothLightsMaterial.displacementMap = depth_texture;
        smoothLightsMaterial.uniforms['displacementScale'].value = paramsProgram.material.displacementScale.value;
        smoothLightsMaterial.uniforms['opacity'].value = 1;
        smoothLightsMaterial.uniforms['roughness'].value = paramsProgram.material.roughness.value;
        smoothLightsMaterial.uniforms['metalness'].value = paramsProgram.material.metalness.value;

        smoothLightsMaterial.uniforms['ballRadius'].value = paramsProgram.material.ballRadius.value;

        smoothLightsMaterial.uniforms['resolution'].value = new THREE.Vector2( width, height );

        smoothLightsMaterial.uniforms['refractionRatio'].value = paramsProgram.material.refractionRatio.value;

        smoothLightsMaterial.flatShading = false;
        smoothLightsMaterial.transparent = true;

        //smoothLightsMaterial.side = THREE.DoubleSide;
        smoothLightsMaterial.side = THREE.FrontSide;
        smoothLightsMaterial.vertexColors = THREE.VertexColors;

        var mesh = new THREE.Mesh(geometry, smoothLightsMaterial);
        mesh.frustumCulled = false;
        return mesh;



    }

    function setInstancedOffsetsAttribute(object, newDivider) {

        if (params.program.adaptiveInstances && params.program.adaptiveInstances.value === false) return;

        var divider = 8 + newDivider;

        var instances_X = Math.floor(DEPTH_TO_COLOR_WIDTH / divider);
        var instances_Y = Math.floor(DEPTH_TO_COLOR_HEIGHT / divider);

        var instances = instances_X * instances_Y;

        // check if geometry has offset attribute
        if (object.geometry.attributes == undefined || object.geometry.attributes.offset == undefined) return;

        var offsetsFull = object.geometry.attributes.offset;
        

        for (var i = 0, l = instances; i < l; i++) {

            var position = new THREE.Vector3();
            position.x = (i % instances_X) - instances_X / 2;
            position.y = Math.floor(i / instances_X) - instances_Y / 2;

            offsetsFull.setXYZ(i, position.x * divider, position.y * divider, position.z);

        }

        for (var i = instances; i < offsetsFull.count; i++) {

            offsetsFull.setXYZ(i, 0, -10000, 20000);

        }

        offsetsFull.needsUpdate = true;


    }




    function createRefractionTest(paramsProgram) {

        var geometry = new THREE.SphereBufferGeometry(100, 20, 20);

        var material = new THREE.MeshStandardMaterial({
            envMap: currentPanoramaTexture,
            roughness: 0.2,
            metalness: 0.9
        })

        var mesh = new THREE.Mesh(geometry, material);

        return mesh;

    }

    function setScreenMaterialLightness(newValue) {

        if (activeSceneObject.material.uniforms && activeSceneObject.material.uniforms["diffuse"]) {

            var hsl = activeSceneObject.material.uniforms["diffuse"].value.getHSL();
            activeSceneObject.material.uniforms["diffuse"].value.setHSL(hsl.h, hsl.s, newValue);

        }

    }


    function createBlackout() {

        blackout = document.createElement('img');
        blackout.src = "images/blackout.jpg";

        document.body.appendChild(blackout);
        blackout.style.width = "100%";
        blackout.style.height = "100%";
        blackout.style.position = "absolute";
        blackout.style.top = "0px";
        blackout.style.visibility = "hidden";
        blackout.style.zindex = "-1000";

    }

    function showBlackout() {
        blackout.style.visibility = "visible";
        blackout.style.zindex = "-1000";
    }

    function hideBlackout() {
        blackout.style.visibility = "hidden";
        blackout.style.zindex = "-1000";
    }

    function createCalibrationGrid() {

        calibrationGrid = document.createElement('img');
        calibrationGrid.src = "images/grid.png";

        document.body.appendChild(calibrationGrid);
        calibrationGrid.style.width = "100%";
        calibrationGrid.style.height = "100%";
        calibrationGrid.style.position = "absolute";
        calibrationGrid.style.top = "0px";
        calibrationGrid.style.visibility = "hidden";
        calibrationGrid.style.zindex = "1000";
    }

    function showCalibrationGrid() {
        calibrationGrid.style.visibility = "visible";
        calibrationGrid.style.zindex = "1000";
    }

    function hideCalibrationGrid() {
        calibrationGrid.style.visibility = "hidden";
        calibrationGrid.style.zindex = "1000";
    }








//=================================

function createBackgroundPlane( params, width, height ) {


    var planeMaterial = new THREE.MeshStandardMaterial( {
        color: 0xffffff,
        //map: renderTarget.texture
        // displacementMap: glslProcRenderTarget_depth.texture,
        // displacementScale: 512,
        map: depth_texture,
        transparent: true,
        // depthTest: false
    } );
    planeMaterial.alphaTest = 0.5
    
    planeMaterial.onBeforeCompile = function ( shader ) {
        
        // we need to initialize the uniform value from material.userData 
        // (set in UI handler "setMaterialMapProperty" in ff-electron-adapter-renderer.js)
        // as onBeforeCompile is called later on, upon the first render
        // (after runtime value could have changed, e.g. when changing
        //  the texture of the other background plane)
        
        shader.uniforms.useSingleChannelColorMap = { value: false };

        shader.fragmentShader = 'uniform bool useSingleChannelColorMap;\n' + shader.fragmentShader;
        shader.fragmentShader = shader.fragmentShader.replace(
            '#include <map_fragment>',
            [
                '#ifdef USE_MAP',
                'vec4 texelColor = texture2D( map, vUv );',
                'if ( useSingleChannelColorMap ) texelColor.rgb = texelColor.rrr;',
               
                //red for invalid values -- uncomment for debugging
                // 'if ( texelColor.r < 0.0 ) texelColor.rgb = vec3( 0.0, 0.0, 1.0 );',
                // 'else if ( texelColor.r == 0.0 ) texelColor.rgb = vec3( 1.0, 0.0, 0.0 );',

               
               
               
                'texelColor = mapTexelToLinear( texelColor );',
                'diffuseColor *= texelColor;',
                '#endif'
            ].join( '\n' )
        );

        planeMaterial.userData.shader = shader;

    };
    
    var planeMesh = new THREE.Mesh( new THREE.PlaneBufferGeometry( width, height, 2, 2 ), planeMaterial );

    return planeMesh;

}

function createBackgroundSphere( params ) {

    var geometry = new THREE.SphereBufferGeometry(2000, 100, 100);

    // invert the geometry on the x-axis so that all of the faces point inward
	geometry.scale( - 1, 1, 1 );

    var pano = new THREE.Mesh(geometry, new THREE.MeshBasicMaterial());

    pano.rotation.y = - Math.PI;

    pano.userData.rotationYorigin = - Math.PI;

    pano.material.transparent = true;

    window.bg = pano;

    return pano;

}

function createTopSphere( envTexture ) {

    var geometry = new THREE.SphereBufferGeometry(200, 100, 100);

    
	// geometry.scale(  1, 1, 1 );

    var topSphere = new THREE.Mesh(geometry, new THREE.MeshStandardMaterial());

   
    topSphere.material.metalness = 1.0;
    topSphere.material.roughness = 0.0;
    topSphere.material.color = new THREE.Color(1,1,1);
    topSphere.material.needsUpdate = true;
    topSphere.position.y = 1500;
    topSphere.scale.x = 2;
    topSphere.scale.z = 2;
    topSphere.scale.y = 8;
    topSphere.material.transparent = true;



    return topSphere;

}

function createHandGeometry( params ) {

    var hm = new THREE.Object3D();

    hm.add( markerLeft );
    hm.add( markerRight )

    markerLeft.visible = true;
    markerRight.visible = true;
    
    return hm;

}