

function makeAutoswitchControls() {

    var autoswitcherCheckbox = document.getElementById("autoswitcherCheckbox");

    autoswitcherCheckbox.onchange = function (event) {

        var state = autoswitcherCheckbox.checked;

        setPresetAutoswitcherState(state);

        //renderview.postMessage(JSON.stringify({ type: "paramChange", message: { param: { value: state }, paramKey: "presetAutoswitcherState" } }));

    }

    
    var autoswitcherIntervalSlider = document.getElementById("autoswitcherIntervalSlider");

    autoswitcherIntervalSlider.oninput = function (e) {

        autoswitcherInterval = this.valueAsNumber * 1000;

    }

    var presetSwitchSlider = document.getElementById("presetSwitchSlider");
    presetSwitchSlider.oninput = function (e) {

        switchProgramByPresetButton( this.valueAsNumber );

    }

}


function setPresetAutoswitcherState(value) {

    function getPresetButton(presetNum) {
        for (var i = 0; i < presetButtonsBlock.children.length; i++) {
            var id = presetButtonsBlock.children[i].id;

            if (id === presetNum.toString()) return presetButtonsBlock.children[i];

        }
        return null;
    }

    value === true ? startAutoswitcher(autoswitcherInterval) : stopAutoswitcher();
    //value === true ? startAutoswitcher(5*1000) : stopAutoswitcher(); //debug

    function switchPresetByTimer() {

        //if (currentPresetButtonIndex === presetButtonsBlock.children.length - 1) currentPresetButtonIndex = -1;
        /////var presetButton = getPresetButton(currentProgram + 1);
        //var presetButton = presetButtonsBlock.children[currentPresetButtonIndex + 1];

        var nextPreset = currentProgram + 1;
        if ( nextPreset > presetButtonsCollection.length ) nextPreset = 1;
        var presetButton = presetButtonsCollection[ nextPreset - 1 ];

        if (presetButton !== null && presetButton !== undefined) {
            presetButton.click();
        }

        autoswitcherTimer = setTimeout(switchPresetByTimer, autoswitcherInterval);

    }

    function startAutoswitcher(freq) {

        autoswitcherTimer = setTimeout(switchPresetByTimer, freq);
    }


    function stopAutoswitcher() {

        clearTimeout(autoswitcherTimer);

    }

}