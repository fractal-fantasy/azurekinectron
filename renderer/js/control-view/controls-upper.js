function makeAdditionalControls() {

    // Midi setitngs button

    var MIDISettingsButton = document.getElementById("MIDISettingsButton");
    MIDISettingsButton.setActive = function () {
        this.style.backgroundColor = '#000000';
        this.style.borderColor = 'darkturquoise';
        this.style.color = 'darkturquoise';
        // this.value = "Close";
        this.pressed = 1;
    }
    MIDISettingsButton.setActive.bind(MIDISettingsButton);

    MIDISettingsButton.setInactive = function () {
        this.style.backgroundColor = '#000000';
        this.style.borderColor = 'lightblue';
        this.style.color = 'lightblue';
        // this.value = "Open";
        this.pressed = 0;
    }
    MIDISettingsButton.setInactive.bind(MIDISettingsButton);

    MIDISettingsButton.setInactive();

    var midiSettingsPopup = document.getElementById("midiSettings");

    MIDISettingsButton.onclick = function (event) {

        MIDISettingsPopupIsActive = !MIDISettingsPopupIsActive;

        if (MIDISettingsPopupIsActive) {
            midiSettingsPopup.style.visibility = "visible";

            renderview.postMessage({ type: "enableSendHandCoordinates", message: true });

            MIDISettingsButton.setActive();

            if (midiMappingModeOn === true) MIDIMapButton.click();
            if (oscMappingModeOn === true) OSCMapButton.click();

        } else {
            midiSettingsPopup.style.visibility = "hidden";
            MIDISettingsButton.setInactive();

            renderview.postMessage({ type: "enableSendHandCoordinates", message: false });

        }

    }

    var midiSettingsPopupOkButton = document.getElementById("midiSettingsOkButton");
    midiSettingsPopupOkButton.onclick = function () {

        MIDISettingsButton.onclick();
    }


    //Midi and Osc mapping button
    var MIDIMapButton = document.getElementById("MIDIMapButton");
    var OSCMapButton = document.getElementById("OSCMapButton");


    MIDIMapButton.setActive = function () {
        this.style.backgroundColor = '#000000';
        this.style.borderColor = 'darkturquoise';
        this.style.color = 'darkturquoise';
        // this.value = "Close";
        this.pressed = 1;
    }
    MIDIMapButton.setActive.bind(MIDIMapButton);

    MIDIMapButton.setInactive = function () {
        this.style.backgroundColor = '#000000';
        this.style.borderColor = 'lightblue';
        this.style.color = 'lightblue';
        // this.value = "Open";
        this.pressed = 0;
    }
    MIDIMapButton.setInactive.bind(MIDIMapButton);

    MIDIMapButton.setInactive();

    MIDIMapButton.onclick = function (event) {        

        midiMappingModeOn = !midiMappingModeOn;        

        if (midiMappingModeOn) {

            if (oscMappingModeOn) OSCMapButton.click();

            enumerateMappableControls();

            enterMidiMappingMode();
            MIDIMapButton.setActive();

            if (MIDISettingsPopupIsActive === true) MIDISettingsButton.click();

        } else {
            quitMidiMappingMode();
            MIDIMapButton.setInactive();
        }

    }


    //OSC mapping button    
    OSCMapButton.setActive = function () {
        this.style.backgroundColor = '#000000';
        this.style.borderColor = 'lightsalmon';
        this.style.color = 'lightsalmon';
        this.pressed = 1;
    }
    OSCMapButton.setActive.bind(OSCMapButton);

    OSCMapButton.setInactive = function () {
        this.style.backgroundColor = '#000000';
        this.style.borderColor = 'lightblue';
        this.style.color = 'lightblue';
        this.pressed = 0;
    }
    OSCMapButton.setInactive.bind(OSCMapButton);

    OSCMapButton.setInactive();

    OSCMapButton.onclick = function (event) {        

        oscMappingModeOn = !oscMappingModeOn;      

        if (oscMappingModeOn) {

            if (midiMappingModeOn) MIDIMapButton.click();

            enumerateMappableControls();

            enterOscMappingMode();
            OSCMapButton.setActive();

            if (MIDISettingsPopupIsActive === true) MIDISettingsButton.click();

        } else {
            quitOscMappingMode();
            OSCMapButton.setInactive();
        }

    }



    var KinectReinitButton = document.getElementById("KinectReinitButton");

    KinectReinitButton.midimapLever = {
        func: "reinitKinect",
        paramKey: "",
        type: "button"
    }

    KinectReinitButton.onclick = function (event) {

        renderview.postMessage({ type: "action", message: "reinitKinect" });

    }

    var StartProjectionButton = document.getElementById("StartProjectionButton");
    StartProjectionButton.onclick = function (event) {

        ipcRenderer.send('start-projection');

    }

}
