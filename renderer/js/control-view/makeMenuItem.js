/**
 * @author mikkamikka / https://mikkamikka.com
 */

var midiActivated = false;

function makeMenuItem(param, paramKey, parentBlockId, objectKey) {

    if ( !param.type ) return;

    var itemHolder = document.createElement("div");
    itemHolder.style.position = "relative";
    itemHolder.style.zIndex = "0";
    var labelValue;

    if ( parentBlockId === "global" ) {

            itemHolder.className = "checkboxInputBlock";

            var label = document.createElement("label");
            label.className = "checkboxInputLabel";
            label.innerHTML = param.name;

            var input = document.createElement("input");
            input.className = "checkboxInputBox";

            input.type = param.type;
            input.id = paramKey;            
            input.checked = param.value;

            input.parentBlockId = parentBlockId;

            input.midimapLever = {
                func: "setParameter",
                paramKey: paramKey,
                type: "checkbox"
            }

            input.onchange = function (event) {

                param.value = this.checked;

                switch (paramKey) {

                    case "camera":
                        keepGlobal.camera = this.checked;
                        break;
                    case "depth":
                        keepGlobal.depth = this.checked;
                        break;
                    case "displacement":
                        keepGlobal.displacement = this.checked;
                        break;
                    case "flipX":

                        keepGlobal.flipX = this.checked;
                        
                        var message = { type: "paramChange", message: { param: param, paramKey: paramKey, needUpdateParams: undefined } };

                        renderview.postMessage(message);

                        break;
                    case "midiActive":

                        keepGlobal.midiActive = this.checked;

                        if (this.checked && !midiActivated){
                            initMidiInterface(); 
                            var message = { type: "paramChange", message: { param: param, paramKey: paramKey, needUpdateParams: undefined } };
                            renderview.postMessage(message);
                            midiActivated = true;
                        }

                        break;
                }
            
            }

        itemHolder.appendChild(label);
        if (labelValue !== undefined) itemHolder.appendChild(labelValue);
        itemHolder.appendChild(input);

        return itemHolder;

    }

    switch (param.type) {

        case "color": 

            var colorBlockWrapper = document.createElement("div");  

            colorBlockWrapper.className = "colorBlockWrapper";

            // color picker

            itemHolder.className = "colorInputBlock";

            var label = document.createElement("label");
            if (param.name === undefined || param.name === "") param.name = paramKey;
            label.innerHTML = param.name;
            label.classList.add( "colorInputLabel" );
                        
            var colorPicker = document.createElement("input");

            colorPicker.className = "colorPickerInput";
            colorPicker.id = paramKey;
            colorPicker.parentBlockId = parentBlockId;

            colorPicker.objectKey = objectKey;

            var opts = {backgroundColor:'#333'}
            var picker = new jscolor(colorPicker, opts) // 'JSColor' is an alias to 'jscolor'
            picker.fromString( param.value );
            colorPicker.picker = picker;
            colorPicker.isColor = true;
            // picker.onFineChange = function(event) {
            //     console.log(event);
            // }
            // colorPicker.midimapLever = {
            //     func: "setParameter",
            //     paramKey: paramKey,
            //     type: "color"
            // }
            colorPicker.onchange = function(event) {
            //picker.onFineChange = function(event) {
                param.value = colorPicker.value;
                var needUpdateParamsRenderview = { value: false, parentBlockId: colorPicker.parentBlockId };

                // update preset's super config on manual input

                //if (event !== undefined) {

                    needUpdateParamsRenderview.value = true;   
                                     
                    if (colorPicker.parentBlockId !== undefined) {  //if manual input on control && if global param
                        if (params.program[currentProgram ][colorPicker.parentBlockId] === undefined) {
                            params.program[currentProgram ][colorPicker.parentBlockId] = {};
                        }

                        //params.program[currentProgram ][colorPicker.parentBlockId][paramKey] = param.value;   

                        //if (this.objectKey !== undefined) {

                            if (this.parentBlockId === "objects") {

                                params.program[currentProgram][this.parentBlockId][this.objectKey].properties[paramKey].value = this.value;

                            } else if (this.parentBlockId === "material") {

                                params.program[currentProgram]["objects"][param.object].material[paramKey].value = this.value;

                            }
                        //}

                    }
                //}
                var message = { type: "paramChange", message: { param: param, paramKey: paramKey, needUpdateParams: needUpdateParamsRenderview } };
                renderview.postMessage( message );

                colorPicker.picker.fromString(this.value);

                input_H.value = colorPicker.picker.hsv[0];
                input_S.value = colorPicker.picker.hsv[1];
                input_V.value = colorPicker.picker.hsv[2];

            }

            itemHolder.appendChild(label);
            if (labelValue !== undefined) itemHolder.appendChild(labelValue);
            itemHolder.appendChild(colorPicker);

            // H S V number input boxes

            var subItemHolder = document.createElement("div");
            subItemHolder.className = "hsvInputBlock";

            var numberInputBlock_H = document.createElement("div");
            var numberInputBlock_S = document.createElement("div");
            var numberInputBlock_V = document.createElement("div");
            numberInputBlock_H.className = "subHSVInputBlock";
            numberInputBlock_S.className = "subHSVInputBlock";
            numberInputBlock_V.className = "subHSVInputBlock";

            var label_H = document.createElement("label");
            label_H.innerHTML = " HSV ";
            label_H.classList.add( "hsvInputLabel" );

            var label_S = document.createElement("label");
            label_S.innerHTML = " S ";
            label_S.classList.add( "hsvInputLabel" );

            var label_V = document.createElement("label");
            label_V.innerHTML = " V ";
            label_V.classList.add( "hsvInputLabel" );
                        
            var input_H = document.createElement("input");
            input_H.className = "hsvInputBox";            
            input_H.id = paramKey + "H";
            input_H.type = "number";
            input_H.min = 0;
            input_H.max = 360;
            input_H.value = colorPicker.picker.hsv[0];
            input_H.midimapLever = {
                func: "setParameter",
                paramKey: paramKey + "H",
                type: "number",
                min: input_H.min,
                max: input_H.max
            }
            input_H.onchange = function(event){
                var new_H = Number(input_H.value);
                var hsv = colorPicker.picker.hsv;
                colorPicker.picker.fromHSV( new_H, hsv[1], hsv[2] );
                colorPicker.onchange();
            }

            var input_S = document.createElement("input");
            input_S.className = "hsvInputBox";            
            input_S.id = paramKey + "S";
            input_S.type = "number";
            input_S.min = 0;
            input_S.max = 100;
            input_S.value = colorPicker.picker.hsv[1];
            input_S.midimapLever = {
                func: "setParameter",
                paramKey: paramKey + "S",
                type: "number",
                min: input_S.min,
                max: input_S.max
            }
            input_S.onchange = function(event){
                var new_S = Number(input_S.value);
                var hsv = colorPicker.picker.hsv;
                colorPicker.picker.fromHSV( hsv[0], new_S, hsv[2] );
                colorPicker.onchange();
            }

            var input_V = document.createElement("input");
            input_V.className = "hsvInputBox";            
            input_V.id = paramKey + "V";
            input_V.type = "number";
            input_V.min = 0;
            input_V.max = 100;
            input_V.value = colorPicker.picker.hsv[2];
            input_V.midimapLever = {
                func: "setParameter",
                paramKey: paramKey + "V",
                type: "number",
                min: input_V.min,
                max: input_V.max
            }
            input_V.onchange = function(event){
                var new_V = Number(input_V.value);
                var hsv = colorPicker.picker.hsv;
                colorPicker.picker.fromHSV( hsv[0], hsv[1], new_V );
                colorPicker.onchange();
            }

            numberInputBlock_H.appendChild(label_H);
            numberInputBlock_H.appendChild(input_H);
            // numberInputBlock_S.appendChild(label_S);
            numberInputBlock_S.appendChild(input_S);
            // numberInputBlock_V.appendChild(label_V);
            numberInputBlock_V.appendChild(input_V);

            subItemHolder.appendChild(numberInputBlock_H);
            subItemHolder.appendChild(numberInputBlock_S);
            subItemHolder.appendChild(numberInputBlock_V);

            colorBlockWrapper.appendChild(itemHolder);
            colorBlockWrapper.appendChild(subItemHolder);

            return colorBlockWrapper;

            break;

        case "file": 

            itemHolder.classList.add( "fileLoaderContainer" );

            itemHolder.id = param.name;

            var fileLoaderlabel = document.createElement("label");
            fileLoaderlabel.classList.add( "fileLoaderlabel" );
            if (param.name === undefined || param.name === "") param.name = paramKey;
            fileLoaderlabel.innerHTML = param.name;  

            var upfile_overlay = document.createElement('img');
            upfile_overlay.src = "images/folder_icon.png";
            upfile_overlay.classList.add( "fileLoaderInput" );
            upfile_overlay.onclick = function() {
                input.click();
            }
    
            var input = document.createElement("input");
            input.style.display = "none";
            input.type = param.type;
            input.id = paramKey;
            input.setAttribute( 'accept', ".jpg, .jpeg, .png, .mp4" );
            
            itemHolder.appendChild( fileLoaderlabel );
            itemHolder.appendChild( upfile_overlay );
            itemHolder.appendChild( input );

            input.addEventListener( 'change', processLoadedImage );

            function processLoadedImage( event ) {
                console.log("processing loaded image");
                if (this.files.length === 0) return;

                var file = this.files[0];

                var filename = file.name;
                var filepath = file.path;                

                param.userLoadedEnvMapNameList.push( { name: filename, path: filepath } );

                // add image asset path into params cache
              
                console.log(filepath);
                 params.envMapTexturePathList[ filename ] = filepath;
               // params.envMapTexturePathList[ filename ] = filename;    // - use filename instead of full file path
                console.log( params.envMapTexturePathList);
                var objectMaterialPane = materialSettingsBlock.querySelector( "#" + param.object );

                var envMapSelectElem = objectMaterialPane.querySelector("#envMap");

                var newOption = document.createElement("option");
                newOption.text = filename;
                newOption.value = envMapSelectElem.options.length;
                envMapSelectElem.add( newOption );

                envMapSelectElem.options[envMapSelectElem.options.length-1].selected = 'selected';

                // update envMap property in material category

                var envMapProp = params.program[ currentProgram ].objects[ param.object ].material.envMap;

                if ( envMapProp ) {
                    envMapProp.optionsList.push( filename );
                    envMapProp.value = newOption.value;
                }

                var message = { type: "loadNewTexture", message: { param: param, paramKey: paramKey } };

                renderview.postMessage( message );

            }

            return itemHolder;

        case "label":

            var label = document.createElement("div");
            label.innerHTML = param.value;
            label.className = "settings_title_2";
            return label;
        
        case "seperator":

            var label = document.createElement("div");
            label.innerHTML = param.value;
            label.className = "seperator";
            return label;
            
        case "text":

            var text = document.createElement("div");
            text.innerHTML = param.value;
            text.className = "simple-text-label";
            return text;

        case "range":

            if (paramKey === "pointLight1Position_X" || paramKey === "pointLight1Position_Y" || paramKey === "pointLight1Position_Z"
                || paramKey === "pointLight2Position_X" || paramKey === "pointLight2Position_Y" || paramKey === "pointLight2Position_Z") {

                itemHolder.className = "sliderInputBoxInline";

            } else {
                itemHolder.className = "sliderInputBox";
            }

            var label = document.createElement("label");
            if (param.name === undefined || param.name === "") param.name = paramKey;
            label.innerHTML = param.name;
            label.className = "sliderInputLabel";

            var input = document.createElement("input");            
            input.type = param.type;
            input.id = paramKey;

                        // Add dblclick event listener
           
            labelValue = document.createElement("label");
            labelValue.className = "guiLabelValue";
            labelValue.innerHTML = param.value;

            if (param.min) input.min = param.min;
            if (param.max) input.max = param.max;
            if (param.step) input.step = param.step;

            input.value = param.value;
            input.labelValue = labelValue;
            input.parentBlockId = parentBlockId;

            input.objectKey = objectKey;

            input.midimapLever = {
                func: "setParameter",
                paramKey: paramKey,
                type: "range",
                min: param.min,
                max: param.max
            }

            input.addEventListener('dblclick', function() {
                this.value = 0.0;
                // Trigger the oninput event manually
                param.value = 0.0;

                input.labelValue.innerHTML = 0.0;                    
                    
                    if (this.parentBlockId !== undefined) {  //if manual input on control && if global param

                        if (params.program[currentProgram ][this.parentBlockId] === undefined) {

                            params.program[currentProgram ][this.parentBlockId] = {};                        

                        }

                        if (this.objectKey !== undefined) {

                            params.program[currentProgram][this.parentBlockId][this.objectKey].properties[paramKey].value = 0.0;

                        } else {

                            params.program[currentProgram][this.parentBlockId][paramKey] = 0.0;

                        }

                        // params.program[currentProgram ][this.parentBlockId][paramKey] = Number(this.value);
                     
                    }
                

                renderview.postMessage( { type: "paramChange", message: { param: param, paramKey: paramKey } } );
                
                this.oninput();
            });


            input.oninputSilent = function (newValue) {
                
                param.value = newValue;

                renderview.postMessage( { type: "paramChange", message: { param: param, paramKey: paramKey } } );
            }

            input.oninput = function (event) {

                var needUpdateParamsRenderview = { value: false, parentBlockId: this.parentBlockId };

                param.value = Number(this.value);

                input.labelValue.innerHTML = this.value;                                

                // update preset's super config on manual input

                if (event !== undefined) {

                    needUpdateParamsRenderview.value = true;
                    
                    
                    if (this.parentBlockId !== undefined) {  //if manual input on control && if global param

                        if (params.program[currentProgram ][this.parentBlockId] === undefined) {

                            params.program[currentProgram ][this.parentBlockId] = {};                        

                        }

                        if (this.objectKey !== undefined) {

                            params.program[currentProgram][this.parentBlockId][this.objectKey].properties[paramKey].value = Number(this.value);

                        } else {

                            params.program[currentProgram][this.parentBlockId][paramKey] = Number(this.value);

                        }

                        // params.program[currentProgram ][this.parentBlockId][paramKey] = Number(this.value);
                     
                    }
                }

                var message = { type: "paramChange", message: { param: param, paramKey: paramKey, needUpdateParams: needUpdateParamsRenderview } };

                renderview.postMessage( message );

            }
            break;

        case "checkbox":

            if (paramKey === "preprocessDepthWithEcho" || paramKey === "preprocess_IR_WithEcho" || paramKey === "preprocess_Color_WithEcho") {
                itemHolder.className = "checkboxInputBlockInlineFlex";
            } else {
                itemHolder.className = "checkboxInputBlock";
            }            

            var label = document.createElement("label");
            label.className = "checkboxInputLabel";
            label.innerHTML = param.name;

            var input = document.createElement("input");
            input.className = "checkboxInputBox";

            input.type = param.type;
            input.id = paramKey;            
            input.checked = param.value;

            input.parentBlockId = parentBlockId;

            input.objectKey = objectKey;

            input.midimapLever = {
                func: "setParameter",
                paramKey: paramKey,
                type: "checkbox"
            }

            input.onchange = function (event) {

                var needUpdateParamsRenderview = { value: false, parentBlockId: this.parentBlockId };

                param.value = this.checked;

                if (event !== undefined) {

                    needUpdateParamsRenderview.value = true;

                    if (this.parentBlockId !== undefined) {  //if manual input on control

                        if (params.program[ currentProgram ][this.parentBlockId] === undefined) {

                            params.program[ currentProgram ][this.parentBlockId] = {};

                        }

                        // params.program[ currentProgram ][this.parentBlockId][paramKey] = this.checked;

                        if (this.objectKey !== undefined) {

                            params.program[currentProgram][this.parentBlockId][this.objectKey].properties[paramKey].value = this.checked;

                        } else {

                            params.program[currentProgram][this.parentBlockId][paramKey] = this.checked;

                        }

                    }
                }

                var message = { type: "paramChange", message: { param: param, paramKey: paramKey, needUpdateParams: needUpdateParamsRenderview } };

                renderview.postMessage(message);

            }
            break;

        case "button":

            itemHolder.className = "buttonInputBlock";

            var label = document.createElement("label");
            label.className = "buttonInputLabel";
            label.innerHTML = param.name;

            var input = document.createElement("input");
            input.className = "buttonInputBox";
            input.type = param.type;
            input.id = paramKey;            
            input.value = param.value;

            input.onclickSilent = function () {

                eval(param.actionFunction + ".call()");                
            }

            input.onclick = function (event) {

                eval(param.actionFunction + ".call()");

            }
            break;

        case "dropdown":

            itemHolder.className = "dropdownInputBlock";

            var label = document.createElement("label");
            label.className = "dropdownInputLabel";
            label.innerHTML = param.name;

            var input = document.createElement("select");
            input.className = "dropdownInputBox";

            input.type = param.type;
            input.id = paramKey;            

            input.parentBlockId = parentBlockId;

            input.midimapLever = {
                func: "setParameter",
                paramKey: paramKey,
                type: "dropdown"
            }

            for ( var i = 0; i < param.optionsList.length; i++ ) {
            
                // create new option element
                var option = document.createElement('option');

                // create text node to add to option element (opt)
                option.appendChild( document.createTextNode( param.optionsList[ i ] ) );

                // set value property of opt
                option.value = i;

                // add opt to end of select box (input)
                input.appendChild( option ); 

            }

            input.value = param.value;

            input.onchange = function (event) {

                var needUpdateParamsRenderview = { value: false, parentBlockId: this.parentBlockId };

                // update preset's super config

                if (event !== undefined) {

                    needUpdateParamsRenderview.value = true;

                    if (this.parentBlockId !== undefined) {  //if manual input on control

                        if (params.program[ currentProgram ][this.parentBlockId] === undefined) {

                            params.program[ currentProgram ][this.parentBlockId] = {};

                        }

                        params.program[ currentProgram ][this.parentBlockId][paramKey] = this.value;

                    }
                }

                param.value = this.value;

                var message = { type: "paramChange", message: { param: param, paramKey: paramKey, needUpdateParams: needUpdateParamsRenderview } };

                renderview.postMessage(message);

            }
            break;

        case "dropdownColorMap":

            itemHolder.className = "dropdownInputBlock";

            var label = document.createElement("label");
            label.className = "dropdownInputLabel";
            label.innerHTML = param.name;

            var input = document.createElement("select");
            input.className = "dropdownInputBox";

            input.type = param.type;
            input.id = paramKey;            

            input.parentBlockId = parentBlockId;

            input.midimapLever = {
                func: "setParameter",
                paramKey: paramKey,
                type: "dropdown"
            }

            for ( var i = 0; i < param.optionsList.length; i++ ) {
            
                // create new option element
                var option = document.createElement('option');

                // create text node to add to option element (opt)
                option.appendChild( document.createTextNode( param.optionsList[ i ] ) );

                // set value property of opt
                option.value = i;

                // add opt to end of select box (input)
                input.appendChild( option ); 

            }

            input.value = param.value;

            input.onchange = function (event) {

                var needUpdateParamsRenderview = { value: false, parentBlockId: this.parentBlockId };

                // update preset's super config

                if (event !== undefined) {

                    needUpdateParamsRenderview.value = true;

                    if (this.parentBlockId !== undefined) {  //if manual input on control

                        if (params.program[ currentProgram ][this.parentBlockId] === undefined) {

                            params.program[ currentProgram ][this.parentBlockId] = {};

                        }

                        params.program[ currentProgram ][this.parentBlockId][paramKey] = this.value;

                    }
                }

                param.value = this.value;

                var message = { type: "paramChange", message: { param: param, paramKey: paramKey, needUpdateParams: needUpdateParamsRenderview } };

                renderview.postMessage(message);

            }
            break;

        case "dropdownEnvMap":

            itemHolder.className = "dropdownInputBlock";

            var label = document.createElement("label");
            label.className = "dropdownInputLabel";
            label.innerHTML = param.name;

            var input = document.createElement("select");
            input.className = "dropdownInputBox";

            input.type = param.type;
            input.id = paramKey;            

            input.parentBlockId = parentBlockId;

            input.midimapLever = {
                func: "setParameter",
                paramKey: paramKey,
                type: "dropdown"
            }

            param.optionsList = [ "Null" ];

            for ( var fileName in params.envMapTexturePathList ) {            

                param.optionsList.push( fileName );

            }

            for ( var i = 0; i < param.optionsList.length; i++ ) {
            
                // create new option element
                var option = document.createElement('option');

                // create text node to add to option element (opt)
                option.appendChild( document.createTextNode( param.optionsList[ i ] ) );

                // set value property of opt
                option.value = i;

                // add opt to end of select box (input)
                input.appendChild( option ); 

            }

            input.value = param.value;

            input.onchange = function (event) {

                var needUpdateParamsRenderview = { value: false, parentBlockId: this.parentBlockId };

                // update preset's super config

                if (event !== undefined) {

                    needUpdateParamsRenderview.value = true;

                    if (this.parentBlockId !== undefined) {  //if manual input on control

                        if (params.program[ currentProgram ][this.parentBlockId] === undefined) {

                            params.program[ currentProgram ][this.parentBlockId] = {};

                        }

                        params.program[ currentProgram ][this.parentBlockId][paramKey] = this.value;

                    }
                }

                param.value = this.value;

                var message = { type: "paramChange", message: { param: param, paramKey: paramKey, needUpdateParams: needUpdateParamsRenderview } };

                renderview.postMessage(message);

            }

            lastAddedMenuItem.appendChild(input);
            
            return null;

            break;
    }    

    itemHolder.appendChild(label);
    if (labelValue !== undefined) itemHolder.appendChild(labelValue);
    itemHolder.appendChild(input);

    return itemHolder;
}