﻿/**
 * @author mikkamikka / https://mikkamikka.com
 */


const { remote, ipcRenderer } = require('electron');

var kinectWindow = remote.getGlobal('kinectWindow');

const {Menu, MenuItem} = remote;

window.kinectWindow = kinectWindow;

var nextPresetId = 0;
var kinectIsListening = false;
var switchProgramCompleted = false;
var initialParamsSent = false;

class Renderview {

    postMessage( arg1 ) {

        if (kinectWindow) kinectWindow.webContents.send('data', arg1 );

    }

}

const renderview = new Renderview();


ipcRenderer.on('message', (event, message) => { 

    handleMessage( message );

    if ( message.type !== undefined ) {

        // console.log(message.type); 

        switch( message.type ) {

            case "reinitKinectWindow":

                kinectWindow = remote.getGlobal ('kinectWindow');
                break;

            case "viewerDOMContentLoaded":

                //console.log("sendAllParamsToRenderview");
                
                if(!initialParamsSent){
                    kinectWindow = remote.getGlobal ('kinectWindow');
                    sendAllParamsToRenderview();
                    initialParamsSent = true;
                }
                break;

            case "gotAllParams":

                renderview.postMessage( { type: "action", message: "init" } );
                break;

            case "viewerInitDone":

                initWithParams(params);

                initParamsAndStart();

                renderview.postMessage( { type: "action", message: "startKinect" } );

                renderview.postMessage( { type: "action", message: "startAnimation" } );

               ipcRenderer.sendSync('get-file-data');
                //console.log("viewerInitDone"); 
                
                break;

            case "kinectStartedListening":
                kinectIsListening = true;
                programChangeCallback();
                console.log("kinectStartedListening");
                break;

            case "kinectStoppedListening":
                kinectIsListening = false;
                console.log("kinectStoppedListening");
                break;

        }   
    }
});

function programChangeCallback(){
    //setting params again after DepthMode change
    console.log("setting params again after depth mode change");
    setPresetParams( params.program[ currentProgram ] );
};

document.addEventListener("keydown", function (e) {
     if (e.code === 'Backslash') {
       
        kinectWindow.webContents.send( 'dev' );
        remote.getCurrentWindow().toggleDevTools();
        
    }
});

function closeKinect() {

    if (kinectWindow) kinectWindow.webContents.send( 'close-kinect' );

}

// function initMidi() {

//     if (!midiActivated){
//         initMidiInterface(); 
//         if (kinectWindow) kinectWindow.webContents.send( 'initMidi' );
//         midiActivated = true;
//     }


// }



//----------------++++++++++@@@@@@@@@@@@@@+++++++++++-------------------//



document.addEventListener("DOMContentLoaded", function () {

    window.addEventListener('keydown', onDocumentKeyDown, false);
    window.addEventListener('keyup', onDocumentKeyUp, false);

}, false);



function handleMessage(msg) {

    switch (msg.type) {
        
        case "OrbitControlsState":

            var orbitControlsState = msg.message;

            if (params.program[ currentProgram ]) {
                params.program[ currentProgram ].orbitControlsState = JSON.parse( orbitControlsState );
            }

            //console.log( "orbit controls state:", params.program[ currentProgram ].orbitControlsState );

            switchProgram__Finish( nextPresetId );

            break;

        case "screenshotSaved":
            processLoadedImage(msg.message);
            break;

        case "handPositionLeft":
            updateUIMidiHandPositionsLeft(msg.message);
            break;

        case "handPositionRight":
            updateUIMidiHandPositionsRight(msg.message);
            break;

        case "logMidiPortEvent":
            logMidiPortEvent(msg.message);
            break;


        case "keyDown":
            var keyCode = msg.data.message;
            // fake event
            var event = { keyCode: keyCode, preventDefault: function () { } };
            onDocumentKeyDown(event);
            break;
        case "keyUp":
            var keyCode = msg.data.message;
            // fake event
            var event = { keyCode: keyCode, preventDefault: function () { } };
            onDocumentKeyUp(event);
            break;
        case "updateMidiSettings_PortID":
            updateMidiSettings_PortID(msg.data.message);
            break;
        case "updateMidiSettings_InputChannel":
            updateMidiSettings_InputChannel(msg.data.message);
            break;
        case "updateMidiSettings_OutputChannel":
            updateMidiSettings_OutputChannel(msg.data.message);
            break;
        case "switchProgram":
            switchProgramWithoutSendingParams(msg.data.message);
            break;
        case "resetParams":

            if (msg.data.message === 1) resetButton.onclick();

            break;

    }
}


function processProjectSettingsLoad(paramsCombined) {   // called from file-system.js

    console.log("processProjectSettingsLoad", paramsCombined)

    params = paramsCombined.params;
   

    paramsBackup = JSON.parse(JSON.stringify(params));

    defaultParams = JSON.parse(JSON.stringify(params));

    currentProgram = params.global.currentProgram;

    renderview.postMessage( { type: "loadAssets", message: params.envMapTexturePathList } );


    // resetAndRestartAllSettings();


    cleanMaterialSettingsMenu();

    // resetCamera();

    resetDepthParams(params);

    resetPostprocessingParams(params.postprocessing);


    //renderview.postMessage({ type: "setOrbitControlsState", message: params.program[ currentProgram ].orbitControlsState });
    
    cleanPresetsMenu();

    makePresetsMenu( params.program );

    makeAllObjectsMaterialPanes( params );

    

    setGlobalParams( params.global );
    //renderview.postMessage({ type: "action", message: "resetOrbitControls" });
    renderview.postMessage({ type: "action", message: "resetOrbitControls", camY: params.program[ currentProgram ].camera.camY, camDistance:  params.program[ currentProgram ].camera.camDistance, camFOV:  params.program[ currentProgram ].camera.camFOV  });
    
  //  setPresetParams( params.program[ currentProgram ] );
                



    midiMap = paramsCombined.midiMap;    
    portSettingsCache = paramsCombined.portSettingsCache;

    oscMap = paramsCombined.oscMap;
    
    logoFilePath = paramsCombined.logoPath;
    renderview.postMessage( { type: "setLogoFilePath", message: logoFilePath } );   

    
    if (paramsCombined.oscParams !== undefined) {        
        oscParams = paramsCombined.oscParams;        
        document.getElementById("oscLocalAddress").value = oscParams.localAddress;
        document.getElementById("oscLocalPort").value = oscParams.localPort;
        document.getElementById("oscRemoteAddress").value = oscParams.remoteAddress;
        document.getElementById("oscRemotePort").value = oscParams.remotePort;
        updateUdpPortOptions();
    }


    const delayLoadingForMidi = setTimeout(function(){
        if(midiActivated){
            initMidiPortsFromFileSettings();
            resetAllMidiMappings();
            restoreMidiMappingsOnLoad();
            updateRendererMidiMap();
        }
        
        enumerateMappableControls();
        
        resetAllOSCMappings();
        
        restoreOSCMappingsOnLoad();
        
        restoreSkeletonBodyTrackingMapFromFileSettings(paramsCombined.skeletonBodyTrackingMap);
        switchProgram( currentProgram );
    
    }, 500);
    

}

function processOSCSettingsLoad(paramsOSCCombined) {   // called from file-system.js

    // console.log(oscMap);
    
    paramsOSC = paramsOSCCombined.params;

    // paramsOSCBackup = JSON.parse(JSON.stringify(paramsOSC));

    // defaultOSCParams = JSON.parse(JSON.stringify(paramsOSC));

    console.log(paramsOSCCombined.oscMap);

    oscMap =  paramsOSCCombined.oscMap;
    
    if (paramsOSCCombined.oscParams !== undefined) {        
        oscParams = paramsOSCCombined.oscParams;        
        document.getElementById("oscLocalAddress").value = oscParams.localAddress;
        document.getElementById("oscLocalPort").value = oscParams.localPort;
        document.getElementById("oscRemoteAddress").value = oscParams.remoteAddress;
        document.getElementById("oscRemotePort").value = oscParams.remotePort;
        updateUdpPortOptions();
    }


    // const delayLoadingForMidi = setTimeout(function(){
    //     if(midiActivated){
    //         initMidiPortsFromFileSettings();
    //         resetAllMidiMappings();
    //         restoreMidiMappingsOnLoad();
    //         updateRendererMidiMap();
    //     }
    //     enumerateMappableControls();
    //     resetAllOSCMappings();
    //     restoreOSCMappingsOnLoad();
    //     restoreSkeletonBodyTrackingMapFromFileSettings(paramsOSCCombined.skeletonBodyTrackingMap);
    
    // }, 500);
    restoreOSCMappingsOnLoad();

}


function initWithParams(initParams) {

    params = initParams;

    // backup params

    paramsBackup = JSON.parse(JSON.stringify(params));

    defaultParams = JSON.parse(JSON.stringify(params));

    startParams = JSON.parse(JSON.stringify(params));


    makeMenus();    

    setGlobalParams( params.global );

    // preload included set of envMap textures
    renderview.postMessage( { type: "loadAssets", message: params.envMapTexturePathList } );

}
// var midiActive = false, midiActivated = false;
// function setupMidiCheckbox(){
    
//     var midiActiveCheckboxControl = document.getElementById('midiActiveCheckboxControl');
//     console.log(midiActiveCheckboxControl);
//     midiActiveCheckboxControl.onchange = function (args) {


//             if (this.checked === true) {

//             midiActive = true;
//             //if (midiInPort) midiInPort.onmidimessage = null;
//                 if(!midiActivated){
//                     initMidi();
//                 }

//             } else {
//                 midiActive = false;
//                 //if (midiInPort) midiInPort.onmidimessage = MidiInPort_MessageReceived;

//             }

//             updateMidiSettings_MuteState( { owner: "control", key: "midiInPort_isMuted" }, this.checked );

//         }

//     // unmute control's midi in port by default
//     midiActiveCheckboxControl.checked = false;
//     midiActiveCheckboxControl.onchange();
// }

function initParamsAndStart() {

    /// MIDI

    // initMidiInterface();
    
    // setupMidiCheckbox();

    initSkeletonMidiMappingInterface();    

    document.getElementById("splashExt").style.display = "none";

    currentProgram = 1;

    addPreset();

    switchProgram(currentProgram);
    
}


function switchProgram(id) {

    switchProgramCompleted = false;

    nextPresetId = id;

    // make renderview aware of switching preset
    // renderview.postMessage({ type: "programChange__Prepare", message: id });


    // renderview.postMessage({ type: "action", message: "resetSceneAutoRotation" });

    // var autoRotationBackgroundSphereCheckbox = document.getElementById("autoRotation");
    // autoRotationBackgroundSphereCheckbox.onchange();

    // get orbit controls state
    renderview.postMessage({ type: "getOrbitControlsState", message: "" });
    

}

function switchProgram__Finish(id) {

    currentProgram = id;

    var programParams;

    if (params.program[ id ] === undefined) {
        programParams = defaultParams.program[ id ];
    } else {
        programParams = params.program[ id ];
      //  console.log(params.program[ id ]);
    }

    //cleanAddableSceneObjectsMenu();   

    //makeAddableSceneObjectsMenu();

    cleanMaterialSettingsMenu();

    makeAllObjectsMaterialPanes(programParams);

    // enumerateMappableControls();

    refreshMidiMappingOnProgramSettingsLoad();
    refreshOSCMappingOnProgramSettingsLoad();

   // renderview.postMessage({ type: "setOrbitControlsState", message: params.program[ currentProgram ].orbitControlsState });
   
    // Set auto-detected IP addresses in osc params and update render view's osc params

    document.getElementById("osc-local-wifi-detected-address").innerHTML = "Your device's Wi-Fi address:  " + localWiFiAddress;
    document.getElementById("oscLocalAddress").value = oscParams.localAddress;
    document.getElementById("oscRemoteAddress").value = oscParams.remoteAddress;
    renderview.postMessage( { type: "updateOSCParams", message: oscParams } )

    
    setPresetParams(programParams);  // update gui and send preset params to render view

    console.log(params.program[ id ].camera.camY, params.program[ id ].camera.camDistance,  params.program[ id ].camera.camFOV  )

    renderview.postMessage({ type: "action", message: "resetOrbitControls", camY: params.program[ id ].camera.camY, camDistance: params.program[ id ].camera.camDistance, camFOV: params.program[ id ].camera.camFOV  });

    // open material tab
    // var addableObjectBlock = document.getElementById(presetButtonsCollection[currentProgram - 1].lastSelectedAddableObjectBlock);
    var addableObjectLabel = document.getElementById(presetButtonsCollection[currentProgram - 1].lastSelectedAddableObjectBlock + "Label");
    addableObjectLabel.click();
    
    // resetCamera();  // controls-camera.js

    

    console.log( "switch program done:", currentProgram );

    switchProgramCompleted = true;

}

function makeAllObjectsMaterialPanes(programParams) {

    for ( var object in programParams.objects ) {

        var objectMaterialPane = makeMaterialSettingsMenu( programParams.objects[ object ].material, object );

        objectMaterialPane.id = object;

        materialSettingsBlock.appendChild( objectMaterialPane );

        objectMaterialPane.style.display = "none";

        // console.log(objectMaterialPane);

    }

}


// called by midi "Program Change" message -- midi.js -- handlePCMessage(program)
function switchProgramByPresetButton( id ) {

    var presetButton = presetButtonsCollection[ id - 1 ];

    if (presetButton !== null && presetButton !== undefined) {
        presetButton.click();
    }

}

function switchProgramWithoutSendingParams(id) {

    if (currentProgram === id) return;

    currentProgram = id;


    var programParams;

    if (params.program[id - 1] === undefined) {
        programParams = defaultParams.program[id - 1];
    } else {
        programParams = params.program[id - 1];
    }

    cleanMaterialSettingsMenu();

    makeMaterialSettingsMenu(programParams);

    refreshMidiMappingOnProgramSettingsLoad();
    refreshOSCMappingOnProgramSettingsLoad();

    for (var i = 0; i < presetButtonsCollection.length; i++) {

        if (Number(presetButtonsCollection[i].id) == id) {
            
            currentPresetButtonIndex = presetButtonsCollection[i].currentPresetButtonIndex;

            presetButtonsCollection[i].setActive();

            if (presetButtonsBlock.lastActiveButton !== null && presetButtonsBlock.lastActiveButton !== presetButtonsCollection[i]) {
                presetButtonsBlock.lastActiveButton.setInactive();
            }

            presetButtonsBlock.lastActiveButton = presetButtonsCollection[i];

        }
    }
}



function makeMenus() {

    addableSceneObjectsBlock = document.createElement("div");

    scene_objects_panel = document.getElementById("scene_objects_panel");
    scene_objects_panel.appendChild( addableSceneObjectsBlock );
        
    permanent_settings_left_panel = document.getElementById("permanent_settings_left_panel");

    var cameraContainer = document.getElementById("camera_settings_panel");
    cameraSettingsBlock = document.createElement("div");
    cameraContainer.appendChild(cameraSettingsBlock);
    makeCameraSettingsMenu(params.camera);
    
    depth_settings_panel = document.getElementById("depth_settings_panel");
    depthSettingsBlock = document.createElement("div");
    depth_settings_panel.appendChild(depthSettingsBlock);
    makeDepthSettingMenu(params.depth);

    global_settings_panel = document.getElementById("global_settings_panel");
    globalSettingsBlock = document.createElement("div");
    global_settings_panel.appendChild(globalSettingsBlock);
    makeGlobalSettingsMenu(params.global);

    makeVideoModeControls();    

    makeAddableSceneObjectsMenu();

    materialSettingsBlock = document.createElement("div");  
    var column4Container = document.getElementById("material_sidebar_block");
    column4Container.appendChild(materialSettingsBlock);  

    makePresetsMenu(params.program, currentProgram);

    makeLOGOandSTROBEButtons();

    makePostprocessingMenu(params.postprocessing);

    makeDepthPostprocessingMenu(params.depthPostprocessing);
    makeDepthFXMenu(params.depthFX);

    makeAdditionalControls();

    makeAutoswitchControls();

}

function makeAddableSceneObjectsMenu() {

    var scene_objects_container = document.getElementById("scene_objects_container");
    var scene_lights_container = document.getElementById("scene_lights_container");

    // unified blocks
    for ( var objectKey in params.program[ currentProgram ].objects ) {

        if ( objectKey === "lights" ) continue;

        var object = params.program[ currentProgram ].objects[ objectKey ];

        var objectBlock = document.createElement("div");
        objectBlock.id = objectKey + "Block";

        //if ( objectKey !== "lights" ) {
            objectBlock.classList.add("addable_object_block");
        // } else {
        //     objectBlock.classList.add("addable_object_block_lights");
        // }
        scene_objects_container.appendChild( objectBlock );

        var objectVisibilityProperty = object.visibility; 

        objectVisibilityProperty.object = objectKey;

        //// function makeObjectControlHeader(param, paramKey, parentBlockId, objectKey) {
        var objectControlHeader = makeObjectControlHeader( objectVisibilityProperty, "visibility", "objects", objectKey );

        objectBlock.appendChild( objectControlHeader );

        if ( object.properties != undefined && Object.keys( object.properties ).length > 0 ) {

            var propertiesSettingsBlock = document.createElement("div");
            propertiesSettingsBlock.classList.add( "addable_object_inner_block" );
            objectBlock.appendChild( propertiesSettingsBlock );
    
            for (var paramKey in object.properties) {
    
                var param = object.properties[ paramKey ];
    
                param.object = objectKey;
    
                var menuItem = makeMenuItem( param, paramKey, "objects", objectKey );
                propertiesSettingsBlock.appendChild(menuItem);
    
            }
        }

    }


        var lights = params.program[ currentProgram ].objects[ "lights" ];

        var lightsBlock = document.createElement("div");
        lightsBlock.id = "lightsBlock";

        lightsBlock.classList.add("addable_object_block_lights");

        scene_lights_container.appendChild( lightsBlock );

        var objectVisibilityProperty = lights.visibility; 

        objectVisibilityProperty.object = "lights";

        //var lightsControlHeader = makeObjectControlHeader(objectVisibilityProperty, "visibility");

        //lightsBlock.appendChild( lightsControlHeader );

        if ( lights.properties != undefined && Object.keys( lights.properties ).length > 0 ) {

            var propertiesSettingsBlock = document.createElement("div");
            propertiesSettingsBlock.classList.add( "addable_object_inner_block" );
            lightsBlock.appendChild( propertiesSettingsBlock );
    
            for (var paramKey in lights.properties) {
    
                var param = lights.properties[ paramKey ];
    
                param.object = "lights";
    
                var menuItem = makeMenuItem( param, paramKey,  "objects", "lights"  );
                propertiesSettingsBlock.appendChild(menuItem);
    
            }
        }

}

function makeDepthSettingMenu(paramsList) {


    for (var paramKey in paramsList) {

        if (paramsList[paramKey].type === undefined) continue;
        if (paramsList[paramKey].hidden === true) continue;

        var menuItem = makeMenuItem(paramsList[paramKey], paramKey, "depth");

        depthSettingsBlock.appendChild(menuItem);
    }    
}

function makeMaterialSettingsMenu( paramsList, object ) {

    var pane = document.createElement("div");

    for (var paramKey in paramsList) {

        if (paramKey !== "postprocessing" && paramKey !== "depth" && 
            paramKey !== "lights" && 
            paramKey !== "alias" && paramKey !== "programIndex") {   // fill the rest of params in menu
        
            if (paramsList[paramKey].type === undefined) continue;
            if (paramsList[paramKey].hidden === true) continue;

            paramsList[paramKey].object = object;

            var menuItem = makeMenuItem(paramsList[paramKey], paramKey, "material");

            lastAddedMenuItem = menuItem;

            if (menuItem) pane.appendChild(menuItem);

        }              
    }

    return pane;

}

function makeObjectControlHeader(param, paramKey, parentBlockId, objectKey) {


    var itemHolder = document.createElement("div");
    itemHolder.style.position = "relative";
    itemHolder.style.zIndex = "0";
    var labelValue;

    itemHolder.className = "checkboxInputBlock";

    var label = document.createElement("label");
    label.className = "checkboxInputLabel_ObjectControl";
    label.innerHTML = param.name;

    label.id = objectKey + "BlockLabel";

    label.onclick = function( event ) {

        if ( this !== addableSceneObjectsBlock.lastSlectedObjectLabel ) {
            this.classList.add( "checkboxInputLabel_ObjectControl_selected" );

            if ( addableSceneObjectsBlock.lastSlectedObjectLabel !== undefined ) {
                addableSceneObjectsBlock.lastSlectedObjectLabel.classList.remove( "checkboxInputLabel_ObjectControl_selected" );
            }
        }

        addableSceneObjectsBlock.lastSlectedObjectLabel = this;

        for (var i = 0; i < materialSettingsBlock.children.length; i++) {
            materialSettingsBlock.children[ i ].style.display = "none";
        }

        var objectMaterialPane = materialSettingsBlock.querySelector( "#" + param.object );

        objectMaterialPane.style.display = "block";

        presetButtonsCollection[ currentProgram - 1 ].lastSelectedAddableObjectBlock = objectKey + "Block";

    }


    var input = document.createElement("input");
    input.className = "checkboxInputBox";

    input.type = param.type;
    input.id = objectKey + paramKey;            
    input.checked = param.value;

    input.parentBlockId = parentBlockId;
    input.objectKey = objectKey;

    input.midimapLever = {
        func: "setParameter",
        paramKey: paramKey,
        type: "checkbox"
    }

    input.onchange = function (event) {

        var needUpdateParamsRenderview = undefined;

        //param.value = this.checked;

        var param = params.program[currentProgram].objects[this.objectKey][paramKey];
        param.value = this.checked;

        param.object = objectKey;
 
        var message = { type: "paramChange", message: { param: param, paramKey: paramKey, needUpdateParams: needUpdateParamsRenderview } };

        renderview.postMessage(message);
    }

    itemHolder.appendChild(label);
    if (labelValue !== undefined) itemHolder.appendChild(labelValue);
    itemHolder.appendChild(input);

    return itemHolder;

}

function makeGlobalSettingsMenu(paramsList) {

    for (var paramKey in paramsList) {

        // skip item if its type is not defined
        if ( !paramsList[paramKey].type ) continue;

        var menuItem = makeMenuItem(paramsList[paramKey], paramKey, "global");

        globalSettingsBlock.appendChild(menuItem);
    }

}


function resetProgramParams() {

    params.program[ currentProgram ] = JSON.parse( JSON.stringify( defaultParams.program[ currentProgram ] ) );

    

    // cleanAddableSceneObjectsMenu();

    // makeAddableSceneObjectsMenu();

    cleanMaterialSettingsMenu();

    makeAllObjectsMaterialPanes( params.program[ currentProgram ] );    

    refreshMidiMappingOnProgramSettingsLoad();
    refreshOSCMappingOnProgramSettingsLoad();

    setPresetParams( params.program[ currentProgram ] );

    // also reset reaction-diffusion render targets
    renderview.postMessage({
        type: "action",
        message: "resetReactionDiffusion"
    });

}

function bgSpherePlay() {

  
    ipcRenderer.send( 'bgSpherePlay' );

}


function LoadLatestProject() {

    ipcRenderer.send( 'load-latest-project' );

}

function resetPostprocessingParams(paramsList) {

    for (var effectKey in paramsList) {

        if (paramsList[effectKey].active !== undefined) {

            var isChecked = paramsList[effectKey].active;

            var checkboxControl = document.getElementById(effectKey);

            if (checkboxControl === null) continue;

            if (checkboxControl.type === "checkbox") {

                checkboxControl.checked = isChecked;

                checkboxControl.onchange();

            }

            for (var paramKey in paramsList[effectKey].params) {

                var value = paramsList[effectKey].params[paramKey].value;

                var sliderControl = document.getElementById(paramKey);

                if (sliderControl === null) continue;

                if (sliderControl.type === "range") {

                    sliderControl.value = value;

                    sliderControl.oninput();

                }
            }
        }
    }
}
// function cleanBuffer() {

//     echoPostFX_DEPTH.cleanTextureArray();
//     echoPostFX_IR.cleanTextureArray();
//     echoPostFX_COLOR.cleanTextureArray();   
// }
function resetRDparams() {

    for (var paramKey in params.depthFX) {        

        if ( paramKey.indexOf('rd') !== -1 || paramKey.indexOf('RD') !== -1 ) {

            var control = document.getElementById(paramKey);

            if (!control) continue;

            console.log(paramKey);

            var paramValue = defaultParams.program[ currentProgram ][ "depthPostprocessing" ][ paramKey ];

            params.depthPostprocessing[ paramKey ].value = paramValue;

            params.program[ currentProgram ][ "depthPostprocessing" ][ paramKey ] = paramValue;

            if (control.type === "range") {

                control.value = paramValue;
                control.oninput();

            } else
                if (control.type === "checkbox") {

                    control.checked = paramValue;
                    control.onchange();
                }
        }
    }

}

function sendPostprocessingParamsToRenderview(effectsList) {

    for (var effectKey in effectsList) {

        var paramsList = effectsList[effectKey].params;

        for (var paramKey in paramsList) {

            renderview.postMessage( {
                type: "postprocessingParamChange",
                message: { effectKey: effectKey, paramKey: paramKey, param: paramsList[paramKey] }
            });
            
        }
    }
}


function makeDepthPostprocessingMenu( paramsList ) {


    var depth_postprocessing_panel = document.getElementById("depth_postprocessing_panel");
    var depthPostprocessingBlock = document.createElement("div");
    depth_postprocessing_panel.appendChild(depthPostprocessingBlock);

    for (var paramKey in paramsList) {

        if (paramsList[paramKey].type === undefined) continue;
        if (paramsList[paramKey].hidden === true) continue;

        paramsList[paramKey].object = "depthPostprocessing";

        var menuItem = makeMenuItem(paramsList[paramKey], paramKey, "depthPostprocessing");

        depthPostprocessingBlock.appendChild(menuItem);
    }   

}
function makeDepthFXMenu( paramsList ) {


    var depth_fx_panel = document.getElementById("depth_fx_panel");
    var depthFXBlock = document.createElement("div");
    depth_fx_panel.appendChild(depthFXBlock);

    for (var paramKey in paramsList) {

        if (paramsList[paramKey].type === undefined) continue;
        if (paramsList[paramKey].hidden === true) continue;

        paramsList[paramKey].object = "depthFX";

        var menuItem = makeMenuItem(paramsList[paramKey], paramKey, "depthFX");

        depthFXBlock.appendChild(menuItem);
    }   

}

function sendAllParamsToRenderview() {

    var paramsToSend = JSON.parse( JSON.stringify( params ) );

    paramsToSend.program = params.program[ currentProgram ];

    renderview.postMessage( { type: "setAllParams", message: paramsToSend } );

}

function sendParamsToRenderview(paramsList) {

    for (var paramKey in paramsList) {

        if (paramsList[paramKey].type !== "button") {

            renderview.postMessage({
                type: "paramChange",
                message: { paramKey: paramKey, param: paramsList[paramKey] }
            });

        }        

    }
}

function setGlobalParams(params_global) {
    
    if (globalSettingsBlock === undefined) return;

    for (var paramKey in params_global) {
        

        var control = globalSettingsBlock.querySelector("#" + paramKey);

        var paramValue;

        if (control === null || control === undefined) {

            continue;

        } else {

            paramValue = params_global[paramKey].value;
        }

        if (control.type === "range") {

            control.value = paramValue;
            control.oninput();

        } else
            if (control.type === "checkbox") {

                control.checked = paramValue;
                control.onchange();
            }

    }

}

function resetDepthParams(paramsList) {
    if (depthSettingsBlock === undefined) return;
    for (var paramKey in paramsList["depth"]) {

        var control = depthSettingsBlock.querySelector("#" + paramKey);

        var paramValue;

        if (control === null || control === undefined) {

            //paramValue = defaultParams["depth"][paramKey].value;
            //console.log(paramKey, "in depth set from default params");

            continue;

        } else {

            paramValue = paramsList["depth"][paramKey].value;
        }

        if (control.type === "range") {

            control.value = paramValue;
            control.oninput();

        } else
            if (control.type === "checkbox") {

                control.checked = paramValue;
                control.onchange();
            }

    }

}

function sendDepthParams(paramsList) {

    for (var paramKey in paramsList) {

        //if (paramKey !== "postprocessing" && paramKey !== "depth" && paramKey !== "lights" && paramKey !== "color") {

            renderview.postMessage({
                type: "paramChange",
                message: { paramKey: paramKey, param: paramsList[paramKey] }
            });
       // }
    }

}


function setDepth(paramsList) {
    return new Promise((resolve, reject) => {
       /// console.log('Executing function one...', paramsList);
        if (keepGlobal.depth === false) {

            for (var paramKey in paramsList["depth"]) {

                var control = depthSettingsBlock.querySelector("#" + paramKey);

                switch (control.type) {

                    case "range":
                        control.value = paramsList["depth"][paramKey];
                        control.oninput();
                        break;
                    case "checkbox":
                        control.checked = paramsList["depth"][paramKey];
                        control.onchange();
                        break;
                    case "select-one":
                        control.value = paramsList["depth"][paramKey];
                     //   console.log(control);
                        control.onchange();
                        break;
                }
            }
        }
        resolve(); // Resolve the promise
    });
}

function setPresetParams(paramsList) {

    
     // set depth params for the program
     

    // paramsList - current program params {}

    // set objects variables, lights
    for ( var objectKey in paramsList.objects ) {

        var object = paramsList.objects[ objectKey ];

        for ( var paramKey in object.properties ) {

            var control = document.getElementById(paramKey);

            // console.log( control );

            if (control === null) continue;

            var value = object.properties[ paramKey ].value;

            if (control.type === "checkbox") {

                control.checked = value;

                control.onchange();

            }
            else if (control.type === "range") {

                control.value = value;

                control.oninput();

            }
            else if (control.type === "text") {   //// color picker

                control.value = value;

                control.onchange();

            }

        }
    

        // set objects visivility

        // for ( var paramsCategoryKey in object ) {

            // if (paramsCategoryKey === "properties") continue;

            // var paramsCategory =  object.visibility;

            // if ( paramsCategory.hasOwnProperty( "value" ) ) {
            //for ( var paramKey in object.visibility ) {

                var visibiltyCheckbox = document.getElementById(objectKey + "visibility");

                //var param = object.visibility;
                //param.object = objectKey;

                if (visibiltyCheckbox) {

                    visibiltyCheckbox.checked = object.visibility.value;
                    visibiltyCheckbox.onchange();
                }

                // var needUpdateParamsRenderview = undefined;    
                // var message = { type: "paramChange", message: { param: param, paramKey: "visibility", needUpdateParams: needUpdateParamsRenderview } };
                // renderview.postMessage(message);

            //}
            // } else {

            //set object's material variables

            for ( var paramKey in object.material ) {

                if ( keepGlobal.displacement === true && paramKey === "displacementScale") continue;

                var param = object.material[ paramKey ];

                param.object = objectKey;                

                var needUpdateParamsRenderview = undefined;    
                var message = { type: "paramChange", message: { param: param, paramKey: paramKey, needUpdateParams: needUpdateParamsRenderview } };
                renderview.postMessage(message);
            }
            setDepth(paramsList);
            
        //
    }



    for (var paramKey in paramsList) {

        if (paramKey !== "postprocessing" && paramKey !== "depth" && paramKey !== "camera" &&
            paramKey !== "lights" && paramKey !== "color" && paramKey !== "meshDeformers" &&
            paramKey !== "alias") {

            renderview.postMessage({
                type: "paramChange",
                message: { paramKey: paramKey, param: paramsList[paramKey] }
            });
        }
    }


    for (var effectKey in paramsList["postprocessing"]) {

        var value = false;

        value = paramsList["postprocessing"][effectKey];

        // renderview.postMessage( {
        //     type: "postprocessingParamToggle",
        //     message: { effectKey: effectKey, active: value }
        // });

        var control = document.getElementById(effectKey);

        //var type = control.type;

        if (control.type === "checkbox") {

            control.checked = value;

            control.onchange();

        }
        else 
            if (control.type === "range") {

                control.value = value;

                control.oninput();

            }
    }

    for (var paramKey in paramsList["depthPostprocessing"]) {

        var value = false;

        value = paramsList["depthPostprocessing"][paramKey];

        // console.log(paramsList["depthPostprocessing"]);

        var param = params.depthPostprocessing[paramKey];


        if (param != undefined){

            param.value = value;

            var needUpdateParamsRenderview = undefined;    
            var message = { type: "paramChange", message: { param: param, paramKey: paramKey, needUpdateParams: needUpdateParamsRenderview } };
            
          
            renderview.postMessage( message );

        }

        var control = document.getElementById(paramKey);

        if (control.type === "checkbox") {

            control.checked = value;

            control.onchange();

        }
        else 
            if (control.type === "range") {

                control.value = value;

                control.oninput();

            }
    }

    for (var paramKey in paramsList["depthFX"]) {

        var value = false;
        
        value = paramsList["depthFX"][paramKey];
        var param = params.depthFX[paramKey];

         if (param != undefined){
            param.value = value;

            var needUpdateParamsRenderview = undefined;    
            var message = { type: "paramChange", message: { param: param, paramKey: paramKey ,needUpdateParams: needUpdateParamsRenderview } };
            renderview.postMessage( message );

         }
            var control = document.getElementById(paramKey);

            // console.log(paramKey, control)
            if (control === null) return;
            if (control.type === "checkbox") {

                control.checked = value;

                control.onchange();

            }
            else if (control.type === "range") {

                    control.value = value;

                    control.oninput();

            }   
    }


   

    // set camera params for the program
    //if ( keepGlobal.camera === false ) {

        for (var paramKey in paramsList["camera"]) {

            // if ( keepGlobal.camera === true && ( paramKey === "flipX") {
            //     continue;
            // }

            if ( keepGlobal.camera === true && ( paramKey === "camDistance" || paramKey === "camFOV" ) ) {
                continue;
            }

            var control = cameraSettingsBlock.querySelector("#" + paramKey);

            var paramValue;

            // if (control === null || control === undefined) {

            //     paramValue = defaultParams["camera"][paramKey];
            //     console.log(paramKey, "in camera set from default params");

            // } else {

            paramValue = paramsList["camera"][paramKey];
            // }

            if (control.type === "range") {

                control.value = paramValue;
                control.oninput();

            } else
                if (control.type === "checkbox") {

                    control.checked = paramValue;
                    control.onchange();
                }
        }

    //}

    // if ( keepGlobal.camera === true ) {
    
    //     var control = cameraSettingsBlock.querySelector("#camDistance");

    
    // }
       
    
}



function cleanAddableSceneObjectsMenu() {

    var scene_objects_container = document.getElementById("scene_objects_container");

    while (scene_objects_container.firstChild) {
        scene_objects_container.removeChild(scene_objects_container.firstChild);
    }


    var scene_lights_container = document.getElementById("scene_lights_container");

    while (scene_lights_container.firstChild) {
        scene_lights_container.removeChild(scene_lights_container.firstChild);
    }

}

function cleanMaterialSettingsMenu() {

    if (materialSettingsBlock === undefined) return;
    while (materialSettingsBlock.firstChild) {
        materialSettingsBlock.removeChild(materialSettingsBlock.firstChild);
    }


}

function cleanDepthSettingsMenu() {

    if (depthSettingsBlock === undefined) return;
    while (depthSettingsBlock.firstChild) {
        depthSettingsBlock.removeChild(depthSettingsBlock.firstChild);
    }
}

function cleanPostprocessingSettingsMenu() {
    if (postprocessingMenuBlock === undefined) return;
    while (postprocessingMenuBlock.firstChild) {
        postprocessingMenuBlock.removeChild(postprocessingMenuBlock.firstChild);
    }
}

function resetAndRestartAllSettings() {
    
    params = JSON.parse(JSON.stringify(startParams));  


    // console.log(params);

    setPresetParams( params.program[ currentProgram ] );

    cleanMaterialSettingsMenu();

    resetDepthParams(params);

    resetPostprocessingParams(params.postprocessing);


    

    // var programParams;

    // if (params.program[ currentProgram ] === undefined) {
    //     programParams = defaultParams.program[ currentProgram ];
    // } else {
    //     programParams = params.program[ currentProgram ];
    // }

    cleanPresetsMenu();

    makePresetsMenu( params.program );

    addPreset();
    
    //renderview.postMessage({ type: "setOrbitControlsState", message: params.program[ currentProgram ].orbitControlsState });
    
    makeAllObjectsMaterialPanes( params );

    //renderview.postMessage({ type: "action", message: "resetOrbitControls" });
    renderview.postMessage({ type: "action", message: "resetOrbitControls", camY: params.program[ currentProgram ].camera.camY, camDistance:  params.program[ currentProgram ].camera.camDistance, camFOV:  params.program[ currentProgram ].camera.camFOV  });
    

    //setPresetParams( params.program[ currentProgram ] );

    

    setGlobalParams( params.global );

    

    // resetCamera();   // controls-camera.js


   

}




function onDocumentKeyDown(event) {

    // prevent processing if inputting text to input element text area (preset buttons)
    if (event.target.type === "text") return;

    switch (event.keyCode) {


        case 123: // F12 - devTools
            remote.getCurrentWindow().toggleDevTools();
            kinectWindow.webContents.send( 'dev' );
            break;

        case 75: // ripple k
            var control = meshDeformersSettingsBlock.querySelector("#rippleAmplitude");
            control.value = Number(control.value) - .9;
            control.oninput();
            break;


        case 76: // ripple l
            var control = meshDeformersSettingsBlock.querySelector("#rippleAmplitude");
            control.value = Number(control.value) + .9;
            control.oninput();
            break;

        case 186: // fatness ;
            var control = meshDeformersSettingsBlock.querySelector("#amplitude");
            control.value = Number(control.value) - .9;
            control.oninput();
            break;


        case 222: // fatness '
            var control = meshDeformersSettingsBlock.querySelector("#amplitude");
            control.value = Number(control.value) + .9;
            control.oninput();
            break;


        case 188:   // bloom ,
            var control = postprocessingMenuBlock.querySelector("#threshold");
            control.value = Number(control.value) - 0.1;
            control.oninput();
            break;


        case 190:  // bloom .
            var control = postprocessingMenuBlock.querySelector("#threshold");
            control.value = Number(control.value) + 0.1;
            control.oninput();
            break;


        case "W".charCodeAt(0): // Y up
            var control = cameraSettingsBlock.querySelector("#camY");
            
            control.value = Number(control.value) - 10;
            control.oninput();
            break;


        case "S".charCodeAt(0): // Y down
            // var control = cameraSettingsBlock.querySelector("#camY");
            // control.value = Number(control.value) + 10;
            // control.oninput();
            break;
        
        case "A".charCodeAt(0): // Y up
            // var control = cameraSettingsBlock.querySelector("#camDistance");

            // control.value = Number(control.value) + 10;
            // control.oninput();
            break;


        case "D".charCodeAt(0): // Y down
            var control = cameraSettingsBlock.querySelector("#camDistance");
            control.value = Number(control.value) - 10;
            control.oninput();
            break;

        case 219: // keystroke "[" depth focus down
            var control = depthSettingsBlock.querySelector("#depthLimit");
            control.value = Number(control.value) - 30;
            control.oninput();
            console.log("Depth DWN");
            break;


        case 221: // keystroke "]" depth focus up
            var control = depthSettingsBlock.querySelector("#depthLimit");
            control.value = Number(control.value) + 30;
            control.oninput();
            console.log("Depth UP");
            break;


        case " ".charCodeAt(0):     // turn on strobe on 'space' press 
            if (buttonStrobe.pressed === 0) buttonStrobe.onmousedown();
            event.preventDefault();
            break;
        case "Z".charCodeAt(0):     // turn on black on 'z' press 
            if (buttonBlack.pressed === 0) buttonBlack.onmousedown();
            event.preventDefault();
            break;

        case "M".charCodeAt(0):     // turn on LOGO on 'm' press 
          
            if (buttonLOGO.pressed === 0) buttonLOGO.onmousedown();
            event.preventDefault();
            break;

        case "B".charCodeAt(0):     // blackout on 'b' press
            toggleBlack.click();

            event.preventDefault();
            break;

        case "H".charCodeAt(0):     // show/hide hand markers
            toggleMIDISettingsPopup();
  
            event.preventDefault();
            break;

        case "L".charCodeAt(0):
  
            //setMidiOutputDefaultPort();
            //setMidiInDefault();
            event.preventDefault();
            break;


        case "J".charCodeAt(0):     // show/hide hand markers
            toggleHandMarkersVisibility();
            event.preventDefault();
            break;

        case 37: // arrow left
            renderview.postMessage({ type: "action", message: "setMarkersScale_L" });
            event.preventDefault();
            break;
        case 39: // arrow right
            renderview.postMessage({ type: "action", message: "setMarkersScale_R" });
            event.preventDefault();
            break;
        case 38: // arrow up
            // renderview.postMessage({ type: "action", message: "setMarkersScale_U" });
            event.preventDefault();

            var control = cameraSettingsBlock.querySelector("#camY");
            control.value = Number(control.value) - 10;
            control.oninput();
            break;
        case 40: // arrow down
            // renderview.postMessage({ type: "action", message: "setMarkersScale_D" });
            event.preventDefault();

             var control = cameraSettingsBlock.querySelector("#camY");
            control.value = Number(control.value) + 10;
            control.oninput();
            break;
        case 187: // plus key
            renderview.postMessage({ type: "action", message: "setMarkersOffset_Plus" });
            event.preventDefault();
            break;
        case 189: // minus key
            renderview.postMessage({ type: "action", message: "setMarkersOffset_Minus" });
            event.preventDefault();
            break;


        default:

            //midi send test
            //sendMidiMessage("NoteOn", event.keyCode, 111);
            
    }    

}

function onDocumentKeyUp(event) {

    // prevent processing if inputting text to input element text area (preset buttons)
    if (event.target.type === "text") return;

    switch (event.keyCode) {

        case " ".charCodeAt(0):     // turn off strobe on 'space' key up 
            if (buttonStrobe.pressed === 1) buttonStrobe.onmouseup();
            event.preventDefault();
            break;
        case "Z".charCodeAt(0):     // turn off black on 'z' key up 
            if (buttonBlack.pressed === 1) buttonBlack.onmouseup();
            event.preventDefault();
            break;
        case "M".charCodeAt(0):     // turn off strobe on 'space' key up 
            if (buttonLOGO.pressed === 1) buttonLOGO.onmouseup();
            event.preventDefault();
            break;


            default:

                //midi send test
              //  sendMidiMessage("NoteOff", event.keyCode, 64);
    }

}

