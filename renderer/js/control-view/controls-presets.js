var presetButtonsBlock;
var presetButtonContextMenu;

function makePresetsMenu(programList, programNumberToSwitchTo) {

    presetButtonsBlock = document.getElementById("preset_buttons_existing");

    presetButtonsBlock.lastActiveButton = null;


    presetButtonsCollection = [];


    // Sort existing programs ======================================

    var programList_copy = programList.slice();

    programList_copy.forEach(function (currentValue, index, array) {
        currentValue.programIndex = index;

    });

    // programList_copy.forEach( function(currentValue, index, array) {
    //     var isNotIndexed = isNaN(parseInt(currentValue.alias));

    //     if (isNotIndexed) {
    //         array.splice(index, 1);
    //     }

    // });

    // programList_copy.sort(function (a, b) {
    //     if ( isNaN( parseInt(b.alias) ) ) return 0;
    //     else
    //         return parseInt(a.alias) - parseInt(b.alias);
    // });

    // end of Sort existing programs ======================================



    // Skip preset 0 - template for a new preset
    for (var i = 1, il = programList_copy.length; i < il; i++) {

        makePresetButton( programList_copy[ i ], i );

    }


    // presetButtonsCollection[ currentProgram - 1 ].onclick();


}

function makePresetButton( program, i ) {

    var divWrapper = document.createElement("div");
    divWrapper.style.position = "relative";
    //divWrapper.style.display = "inline-block";

    var button = document.createElement("input");
    button.type = "text";
    button.readOnly = true;

    //button.value = "Preset " + i;
    button.value = program.alias;

    //button.id = program.programIndex;

    button.id = i;

    button.currentPresetButtonIndex = i;

    //if (i === 1) {
    //    currentProgram = program.programIndex;
    //}                 

    button.classList.add( "presetButtonDefault" );
    button.classList.add( 'cursorPointer' );

    button.setActive = function () {
        this.style.backgroundColor = '#201010';
        this.style.borderColor = '#ff0000';
        this.style.color = '#ff0000';
        //this.disabled = true;
    }
    button.setActive.bind(button);

    button.setInactive = function () {
        this.style.backgroundColor = '#000000';
        this.style.borderColor = 'grey';
        this.style.color = 'grey';
        //this.disabled = false;
    }
    button.setInactive.bind(button);
    

    if ( Number(button.id) === currentProgram ) {
        button.setActive();
        presetButtonsBlock.lastActiveButton = button;
    } 
    else {
        button.setInactive();
    }


    button.presetButtonContextMenu = new Menu();
    button.presetButtonContextMenu.append(new MenuItem(
        {   label: 'Duplicate', 
            click (e) 
            { 
                duplicatePreset( button );
            } 
        }));
    button.presetButtonContextMenu.append(new MenuItem(
        {   label: 'Rename', 
            click (e) 
            { 
                renamePreset( button );
            } 
        }));
    button.presetButtonContextMenu.append(new MenuItem(
        {   label: 'Delete', 
            click (e) 
            { 
                deletePreset( button );
            } 
        }));


    button.onmousedown = null;
    button.onmouseup = null;

    button.onclick = function (event) {

        if ( Number(this.id) === currentProgram ) {
            //console.log("preset is already active");
            return;            
        }

        // prevent  program switch if kinect is not listening (in mode change operation)
        // or if previouse preset switch call has not completed
        if ( kinectIsListening === false || switchProgramCompleted === false ) return;

        currentPresetButtonIndex = this.currentPresetButtonIndex;


        switchProgram(Number(this.id));

        this.setActive();

        if (presetButtonsBlock.lastActiveButton !== null) {

            presetButtonsBlock.lastActiveButton.setInactive();

        }
        presetButtonsBlock.lastActiveButton = this;

    }

    button.ondblclick = function(e) {

        renamePreset( this );

    }


    button.oncontextmenu = function(e) {


        e.preventDefault();

        //this.presetButtonContextMenu.presetID = this.id;
        this.presetButtonContextMenu.popup(remote.getCurrentWindow());


    }

    button.onkeyup = function(e) {

        e.preventDefault();
        //e.stopPropagation();

        if (e.keyCode == 13) {

            this.blur();
            
            setPresetName( this.value, this.id );
            this.readOnly = true;
            button.classList.add( 'cursorPointer' );
            
        }
    }
    button.onblur = function(e) {

        if (this.readOnly === true) return;

        setPresetName( this.value, this.id );
        this.readOnly = true;
        button.classList.add( 'cursorPointer' );
        
    }

    // set default addable object
    button.lastSelectedAddableObjectBlock = "depthMeshBlock";

    presetButtonsCollection.push(button);

    divWrapper.appendChild(button);
    presetButtonsBlock.appendChild(divWrapper);

}

function addPreset() {


    // var newProgramParams = JSON.parse( JSON.stringify( defaultParams.program[ 0 ] ) );
    // console.log(newProgramParams);

    var defaultProgramParams = JSON.parse( JSON.stringify( paramsDefaultApp.program[ 0 ] ) );
  //  console.log(defaultProgramParams);
    
    defaultProgramParams.alias = "Untitled";


    params.program.push( defaultProgramParams );


    defaultParams.program.push( JSON.parse( JSON.stringify( defaultProgramParams ) ) );


    makePresetButton( defaultProgramParams, params.program.length - 1 );    

    presetButtonsCollection[ presetButtonsCollection.length - 1 ].onclick();

}

function duplicatePreset( button ) {

    var id = Number(button.id);

    var preset = params.program[ id ];

    var newProgramParams = JSON.parse( JSON.stringify( preset ) );
    
    newProgramParams.alias = preset.alias + "-Copy";

    //params.program.push( newProgramParams );
    //defaultParams.program.push( JSON.parse( JSON.stringify( newProgramParams ) ) );

    //put new preset inline
    // params.program.splice( Number(button.id)+1, 0, newProgramParams );
    // defaultParams.program.splice( Number(button.id)+1, 0, JSON.parse( JSON.stringify( newProgramParams ) ) );


    params.program.push( newProgramParams );

    defaultParams.program.push( JSON.parse( JSON.stringify( newProgramParams ) ) );

    cleanPresetsMenu();

    makePresetsMenu(params.program, currentProgram);

    //makePresetButton( newProgramParams, params.program.length - 1 );

    presetButtonsCollection[ id ].onclick();

    //button.blur();

}

function renamePreset(button) {

    console.log(button.id);

    button.readOnly = false;
    button.focus();

    button.classList.remove( 'cursorPointer' );

    //presetButtonsCollection[ Number(button.id) - 1 ].readOnly = false;
    //presetButtonsCollection[ Number(button.id) - 1 ].focus();
}

function setPresetName( newName, id ) {

    params.program[ id ].alias = newName;

    presetButtonsCollection[ Number(id) - 1 ].value = newName;

    console.log("new preset name submitted!:", newName);

}

function deletePreset(button) {    

    var index = Number(button.id);

    if (presetButtonsCollection.length === 1) return;

    params.program.splice(index, 1);

    // presetButtonsCollection.splice( index - 1, 1 );


    if (index === currentProgram){
        currentProgram--;
    } 
    switchProgram(currentProgram);

    cleanPresetsMenu();
    makePresetsMenu(params.program, currentProgram);
    

 presetButtonsCollection[ index - 2 ].onclick();

}

 //remove deleted preset from buttons block
function cleanPresetsMenu() {

    if (presetButtonsBlock === undefined) return;
    for ( var i = 0; i < presetButtonsBlock.children.length; i++ ) {

        while (presetButtonsBlock.firstChild) {
            presetButtonsBlock.removeChild(presetButtonsBlock.firstChild);
        }
    }

}