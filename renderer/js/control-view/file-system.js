ipcRenderer.on( 'file-load', (event, data) => { 

    console.log("FILE DATAAAAAAAAAAA", data); 

})

ipcRenderer.on( 'file-opened', (event, message) => { 

   console.log("FILEEEE OPENEDDD!!!!", message); 

    var paramsCombined = JSON.parse(message);
    processProjectSettingsLoad(paramsCombined);

})

ipcRenderer.on( 'loadOSCMappings', (event, message) => { 

    //console.log(message); 

    var paramsOSCCombined = JSON.parse(message);
    processOSCSettingsLoad(paramsOSCCombined);

})

ipcRenderer.on( 'get-data-to-save', (event, message) => { 

    console.log('get-data-to-save'); 

    var paramsToSave = {};

    paramsToSave.params = params;
    paramsToSave.params.global.currentProgram = currentProgram;
    paramsToSave.midiMap = midiMap;
    paramsToSave.oscMap = oscMap;
    paramsToSave.oscParams = oscParams;
    paramsToSave.portSettingsCache = portSettingsCache;
    paramsToSave.skeletonBodyTrackingMap = skeletonBodyTrackingMap;
    paramsToSave.logoPath = logoFilePath;

    ipcRenderer.send('return-data-to-save', JSON.stringify(paramsToSave));

})

ipcRenderer.on( 'file-saved', (event, message) => { 

    console.log('file-saved', message); 

})

ipcRenderer.on( 'reset-project', (event) => { 

    console.log('reset-project');

    currentProgram = 0;

    resetAndRestartAllSettings();

    resetAllOSCMappings();
    oscMap = [];

    currentProgram = 1;


    switchProgram(currentProgram);

})

ipcRenderer.on( 'reset-current-preset', (event) => { 

    console.log('reset-current-preset');

    resetProgramParams();

})


ipcRenderer.on( 'reset-osc-mappings', (event) => { 

    console.log('reset-osc-mappings');

    resetAllOSCMappings();
    oscMap = [];

})


