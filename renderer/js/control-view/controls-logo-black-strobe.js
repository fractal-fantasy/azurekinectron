var buttonStrobe, buttonLOGO, buttonBlack, toggleBlack;
var calibrationGridButton, blackoutButton;
var strobeFrequencySlider;

function makeLOGOandSTROBEButtons() {

    //var logoButtonsBlock = document.getElementById("logoButtons");


    //// logo on/off button

    buttonLOGO = document.getElementById("buttonLOGO");

    buttonLOGO.midimapLever = {
        func: "postprocessingParamChange",
        paramKey: "LOGO",
        type: "button"
    }

    buttonLOGO.style.margin = "5px";
    buttonLOGO.style.height = "32px";

    buttonLOGO.setActive = function () {
        this.style.backgroundColor = '#002040';
        this.style.borderColor = '#0062ff'
        this.style.color = '#0062ff';
        //this.disabled = true;
        this.pressed = 1;
    }
    buttonLOGO.setActive.bind(buttonLOGO);

    buttonLOGO.setInactive = function () {
        this.style.backgroundColor = '#000000';
        this.style.borderColor = '#335995';
        this.style.color = '#335995';
        //this.disabled = false;
        this.pressed = 0;
    }
    buttonLOGO.setInactive.bind(buttonLOGO);

    buttonLOGO.setInactive();

    buttonLOGO.onmousedown = function (event) {

        if (midiMappingModeOn || oscMappingModeOn) return;

        //if (this.pressed === 0) this.setActive(); else this.setInactive();

        this.setActive();

        renderview.postMessage({
            type: "postprocessingParamChange",
            message: { effectKey: "LOGO", param: 1 }
        });

    }
    buttonLOGO.onmouseup = function (event) {

        if (midiMappingModeOn || oscMappingModeOn) return;

        this.setInactive();
        renderview.postMessage({
            type: "postprocessingParamChange",
            message: { effectKey: "LOGO", param: 0 }
        });
    }

    buttonLOGO.onmousedownSilent = function () {

        //if (this.pressed === 0) this.setActive(); else this.setInactive();

        renderview.postMessage({
            type: "postprocessingParamChange",
            message: { effectKey: "LOGO", param: 1 }
        });

    }
    buttonLOGO.onmouseupSilent = function () {
        //this.setInactive();
        renderview.postMessage({
            type: "postprocessingParamChange",
            message: { effectKey: "LOGO", param: 0 }
        });
    }


    var logoFileLoaderInput = document.getElementById("logoFileLoaderInput");

    var upfile_overlay = document.getElementById("logoFileLoaderImgOverlay");
    upfile_overlay.onclick = function() {
        logoFileLoaderInput.click();
    }

    logoFileLoaderInput.addEventListener( 'change', setLogoFileProperty );

    function setLogoFileProperty() {

        if (this.files.length === 0) return;

        var file = this.files[0];

        var filename = file.name;
        var filepath = file.path;
        logoFilePath = filepath;

        var message = { type: "setLogoFilePath", message: filepath };

        //come back to this

        renderview.postMessage( message );        

    }




    var flipLogoCheckboxControl = document.getElementById("flipLogoCheckboxControl");

    flipLogoCheckboxControl.checked = params.strobe.flipLogoX.value;
    
    flipLogoCheckboxControl.onchange = function() {

        params.strobe.flipLogoX.value = this.checked;

        var message = { param: params.strobe.flipLogoX, paramKey: "flipLogoX", needUpdateParams: { value: true, parentBlockId: "strobe" } };

        renderview.postMessage({
            type: "paramChange",
            message: message
        });

    }

    flipLogoCheckboxControl.onchange();

    //// Calibration grid button

    calibrationGridButton = document.getElementById("calibrationGridButton");

    calibrationGridButton.setActive = function () {
        this.style.backgroundColor = '#404040';
        this.style.borderColor = 'white';
        this.style.color = 'white';
        this.value = "Hide";
        this.pressed = 1;
    }
    calibrationGridButton.setActive.bind(calibrationGridButton);

    calibrationGridButton.setInactive = function () {
        this.style.backgroundColor = '#000000';
        this.style.borderColor = 'grey';
        this.style.color = 'grey';
        this.value = "Show";
        this.pressed = 0;
    }
    calibrationGridButton.setInactive.bind(calibrationGridButton);

    calibrationGridButton.setInactive();

    calibrationGridButton.onclick = function (event) {

        if (this.pressed === 0) {
            this.setActive();
            renderview.postMessage({ type: "action", message: "showCalibrationGrid" });
        } else {
            this.setInactive();
            renderview.postMessage({ type: "action", message: "hideCalibrationGrid" });
        }
              
    }



    // blackoutButton = document.getElementById("blackoutButton");

    // blackoutButton.setActive = function () {
    //     this.style.backgroundColor = '#404040';
    //     this.style.borderColor = 'white';
    //     this.style.color = 'white';
    //     this.value = "Off";
    //     this.pressed = 1;
    // }
    // blackoutButton.setActive.bind(blackoutButton);

    // blackoutButton.setInactive = function () {
    //     this.style.backgroundColor = '#000000';
    //     this.style.borderColor = 'grey';
    //     this.style.color = 'grey';
    //     this.value = "On";
    //     this.pressed = 0;
    // }
    // blackoutButton.setInactive.bind(blackoutButton);

    // blackoutButton.setInactive();

    // blackoutButton.onclick = function (event) {

    //     if (this.pressed === 0) {
    //         this.setActive();
    //         renderview.postMessage({ type: "action", message: "showBlackout" });
    //     } else {
    //         this.setInactive();
    //         renderview.postMessage({ type: "action", message: "hideBlackout" });
    //     }

    // }





    //// strobe button

    buttonStrobe = document.getElementById("buttonStrobe");

    buttonStrobe.midimapLever = {
        func: "postprocessingParamChange",
        paramKey: "STROBE",
        type: "button"
    }

    buttonStrobe.style.margin = "5px";
    buttonStrobe.style.height = "32px";
    buttonStrobe.style.width = "75px";

    buttonStrobe.setActive = function () {
        this.style.backgroundColor = '#404040';
        this.style.borderColor = 'white';
        this.style.color = 'white';
        //this.disabled = true;
        this.pressed = 1;
    }
    buttonStrobe.setActive.bind(buttonStrobe);

    buttonStrobe.setInactive = function () {
        this.style.backgroundColor = '#000000';
        this.style.borderColor = 'grey';
        this.style.color = 'grey';
        //this.disabled = false;
        this.pressed = 0;
    }
    buttonStrobe.setInactive.bind(buttonStrobe);

    buttonStrobe.setInactive();
    buttonStrobe.onmousedown = function (event) {

        if (midiMappingModeOn || oscMappingModeOn) return;

        this.setActive();
        renderview.postMessage({
            type: "postprocessingParamChange",
            message: { effectKey: "STROBE", param: 1 }
        });
    }
    buttonStrobe.onmouseup = function (event) {

        if (midiMappingModeOn || oscMappingModeOn) return;

        this.setInactive();
        renderview.postMessage({
            type: "postprocessingParamChange",
            message: { effectKey: "STROBE", param: 0 }
        });
    }

    buttonStrobe.onmousedownSilent = function () {        
        renderview.postMessage({
            type: "postprocessingParamChange",
            message: { effectKey: "STROBE", param: 1 }
        });
    }
    buttonStrobe.onmouseupSilent = function () {        
        renderview.postMessage({
            type: "postprocessingParamChange",
            message: { effectKey: "STROBE", param: 0 }
        });
    }

    makeStrobeParamsControls();

    function makeStrobeParamsControls() {

        var strobeParamsBlock = document.getElementById("strobeParamsBlock");

        var paramsList = params.strobe;

        for ( var paramKey in paramsList ) {
            
            if (paramsList[paramKey].type === undefined) continue;
            if (paramsList[paramKey].hidden === true) continue;

            var menuItem = makeMenuItem(paramsList[paramKey], paramKey, "strobe");

            // put checkbox next to button
            if ( paramKey === "strobe_toggle" ) {
                document.getElementById("strobeToggleHolder").appendChild( menuItem );
                menuItem.querySelector("label").style.display = "none";
                continue;
            }

            strobeParamsBlock.appendChild(menuItem);
                         
        }

    }

    //// Black button

    buttonBlack = document.getElementById("buttonBlack");

    buttonBlack.midimapLever = {
        func: "postprocessingParamChange",
        paramKey: "BLACK",
        type: "button"
    }

    buttonBlack.style.margin = "5px";
    buttonBlack.style.height = "32px";
    buttonBlack.style.width = "75px";

    buttonBlack.setActive = function () {
        this.style.backgroundColor = '#404040';
        this.style.borderColor = 'white';
        this.style.color = 'white';
        //this.disabled = true;
        this.pressed = 1;
    }
    buttonBlack.setActive.bind(buttonBlack);

    buttonBlack.setInactive = function () {
        this.style.backgroundColor = '#000000';
        this.style.borderColor = 'grey';
        this.style.color = 'grey';
        //this.disabled = false;
        this.pressed = 0;
    }
    buttonBlack.setInactive.bind(buttonBlack);

    buttonBlack.setInactive();
    
    buttonBlack.onmousedown = function (event) {

        if (midiMappingModeOn || oscMappingModeOn) return;

        this.setActive();
        renderview.postMessage({
            type: "postprocessingParamChange",
            message: { effectKey: "BLACK", param: this.pressed }
        });
    }
    buttonBlack.onmouseup = function (event) {

        if (midiMappingModeOn || oscMappingModeOn) return;

        this.setInactive();
        renderview.postMessage({
            type: "postprocessingParamChange",
            message: { effectKey: "BLACK", param: this.pressed }
        });
    }

    buttonBlack.onmousedownSilent = function () {
        renderview.postMessage({
            type: "postprocessingParamChange",
            message: { effectKey: "BLACK", param: 1 }
        });
    }
    buttonBlack.onmouseupSilent = function () {
        renderview.postMessage({
            type: "postprocessingParamChange",
            message: { effectKey: "BLACK", param: 0 }
        });
    }

   
    toggleBlack = document.getElementById("blackoutCheckboxControl");

    toggleBlack.onchange = function (event) {
        renderview.postMessage({
            type: "postprocessingParamToggle",
            message: { effectKey: "BLACK", param: this.checked }
        });
    }

    


}
