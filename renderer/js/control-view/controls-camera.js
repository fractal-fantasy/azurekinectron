function makeCameraSettingsMenu(paramsList) {

    for (var paramKey in paramsList) {

        paramsList[paramKey].object = "camera";

        var menuItem = makeMenuItem(paramsList[paramKey], paramKey, "camera");

        cameraSettingsBlock.appendChild(menuItem);
    }
}

function resetCameraButtonFunction() {    

    renderview.postMessage({ type: "action", message: "resetOrbitControls" });

    for (var item in params.camera) {

        var control = document.getElementById(item);

        if (params.camera[item].defaultValue !== undefined) {

            var paramValue = params.camera[item].defaultValue;

            params.camera[item].value = paramValue;

            params.program[ currentProgram ][ "camera" ][item] = paramValue;

            if (control.type === "range") {

                control.value = paramValue;
                control.oninput();

            } else
                if (control.type === "checkbox") {

                    control.checked = paramValue;
                    control.onchange();
                }

        }
    }

    

    //renderview.postMessage({ type: "setOrbitControlsState", message: params.program[ currentProgram ].orbitControlsState });

    

    

}

function cleanCameraSettingsMenu() {

    while (cameraSettingsBlock.firstChild) {
        cameraSettingsBlock.removeChild(cameraSettingsBlock.firstChild);
    }
}