
const path = require('path');

var playbackFileDefaultPath = path.join(__dirname, "../images/output.mkv");

// var playbackFileDefaultPath = "C:/Users/FF/Documents/azurekinectron/renderer/images/output.mkv"

var isPlaybackMode = false;
var kinectIsPaused = false;

var playbackFilesOptionsList = [ { path: playbackFileDefaultPath, name: "output.mkv" } ];

function switchPauseMode(button) {

    kinectIsPaused = !kinectIsPaused;

    if (kinectIsPaused === true){
        button.value = "Resume Kinect";
        renderview.postMessage({ type: "action", message: "pauseKinect" });        
    } else {
        button.value = "Pause Kinect";
        renderview.postMessage({ type: "action", message: "resumeKinect" });        
    }

}

function setVideoMode2(button) {

    isPlaybackMode = !isPlaybackMode;

    if (isPlaybackMode) {
        button.value = "Eject";
        renderview.postMessage({ type: "action", message: "startPlayback" });
    } else {
        button.value = "Play";
        renderview.postMessage({ type: "action", message: "stopPlayback" });
        //renderview.postMessage({ type: "action", message: "restartKinect" });
    }

}

function setVideoMode(checkbox) {

    isPlaybackMode = checkbox.checked;

    if (isPlaybackMode) {
        renderview.postMessage({ type: "action", message: "startPlayback" });
    } else {
        renderview.postMessage({ type: "action", message: "stopPlayback" });
        //renderview.postMessage({ type: "action", message: "restartKinect" });
    }

}

function bgSpherePlay() {
    console.log("bgSpherePlay");
    sceneObjects.backgroundSphere.material.map.image.play();
    
    if(topSphereActive){
        sceneObjects.topSphere.material.envMap.image.play();
    }
}



function makeVideoModeControls() {

    var mkvLoaderInput = document.getElementById("mkvLoaderInput");

    var playbackMkvFilesDropdown = document.getElementById("playbackMkvFilesDropdown");

    var upfile_overlay = document.getElementById("fileLoaderImgOverlay");
    upfile_overlay.onclick = function() {
        mkvLoaderInput.click();
    }

    mkvLoaderInput.addEventListener( 'change', setPlaybackFileOption );
    playbackMkvFilesDropdown.addEventListener( 'change', setPlaybackFile );

    function setPlaybackFileOption( event ) {

        if (this.files.length === 0) return;

        var file = this.files[0];

        var filename = file.name;
        var filepath = file.path;        

        playbackFilesOptionsList.push( { path: filepath, name: filename } );

        var newOption = document.createElement("option");
        newOption.text = filename;
        newOption.value = playbackMkvFilesDropdown.options.length;
   
        playbackMkvFilesDropdown.add( newOption );

        playbackMkvFilesDropdown[playbackMkvFilesDropdown.options.length-1].selected = 'selected';

        var message = { type: "setPlaybackMkvFilePath", message: filepath };

        renderview.postMessage( message );

    }
    


    var fileOption = document.createElement("option");
    fileOption.text = playbackFilesOptionsList[0].name;
    fileOption.value = 0;
    playbackMkvFilesDropdown.add( fileOption );


}

function setPlaybackFile() {

    console.log( this.value );

    var message = { type: "setPlaybackMkvFilePath", message: playbackFilesOptionsList[ this.value ].path };

    renderview.postMessage( message );

}