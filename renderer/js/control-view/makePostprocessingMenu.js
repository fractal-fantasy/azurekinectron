

function makePostprocessingMenu(effectsList) {

    postprocessingMenuBlock = document.getElementById("postfx_settings_panel");


    for (var effectKey in effectsList) {

        var effectBlock = document.createElement("div");
        postprocessingMenuBlock.appendChild(effectBlock);
        //effectBlock.id = effectKey;

        var checkboxControlItem = makeCheckboxControl(effectsList, effectKey);

        effectBlock.appendChild(checkboxControlItem);

        var effectParamsBlock = document.createElement("div");
        effectParamsBlock.className = "effectParamsBlock";
        effectBlock.appendChild(effectParamsBlock);

        effectsList[effectKey].childControls = [];

        var paramsList = effectsList[effectKey].params;

        for (var paramKey in paramsList) {

            var sliderControlItem = makeSliderControl(paramsList, paramKey, effectKey);

            effectParamsBlock.appendChild(sliderControlItem);

            effectsList[effectKey].childControls.push(sliderControlItem);

            sliderControlItem.slider.disabled = ! effectsList[effectKey].active;

        }       

    }

    function makeSliderControl(paramsList, paramKey, effectKey) {

        var labelValue;

        var sliderControlItem = document.createElement("div");
        sliderControlItem.className = "controlHolder";

        var label = document.createElement("label");
        label.innerHTML = paramsList[paramKey].name;
        label.className = "labelSliderPostFx";
        
        var input = document.createElement("input");
        input.type = "range";
        input.id = paramKey;
        input.value = paramsList[paramKey].value;
        input.min = paramsList[paramKey].min;
        input.max = paramsList[paramKey].max;
        input.step = paramsList[paramKey].step;

        input.midimapLever = {
            func: "postprocessingParamChange",
            effectKey: effectKey,
            paramKey: paramKey,
            type: "range",
            min: paramsList[paramKey].min,
            max: paramsList[paramKey].max
        }
        
        labelValue = document.createElement("label");
        labelValue.className = "guiLabelValue";
        labelValue.innerHTML = paramsList[paramKey].value;
        input.labelValue = labelValue;

        input.alias = paramKey;

        input.parentBlockId = "postprocessing";

        input.oninput = function (event) {

            var needUpdateParamsRenderview = undefined;

            input.labelValue.innerHTML = this.value;
            
            paramsList[this.id].value = Number(this.value);           
          
            // update preset's super config

            if (event !== undefined) {

                needUpdateParamsRenderview = { value: true, parentBlockId: undefined };

                if (this.parentBlockId !== undefined) {  //if manual input on control

                    if (params.program[ currentProgram ][this.parentBlockId] === undefined) {

                        params.program[ currentProgram ][this.parentBlockId] = {};

                    }

                    params.program[ currentProgram ][this.parentBlockId][paramKey] = Number(this.value);

                    needUpdateParamsRenderview.parentBlockId = this.parentBlockId;

                }
            }

            renderview.postMessage({
                type: "postprocessingParamChange",
                message: { effectKey: effectKey, paramKey: this.id, param: paramsList[this.id], needUpdateParams: needUpdateParamsRenderview }
            });

        };

        sliderControlItem.appendChild(label);
        sliderControlItem.appendChild(labelValue);
        sliderControlItem.appendChild(input);
        sliderControlItem.slider = input;

        return sliderControlItem;

    }

    function makeCheckboxControl(effectsList, effectKey) {

        var checkboxControlItem = document.createElement("div");
        checkboxControlItem.className = "checkboxInputBlock";

        var label = document.createElement("label");
        label.innerHTML = effectKey + "   ";
        label.className = "checkboxInputLabel";

        var input = document.createElement("input");
        input.type = "checkbox";
        input.id = effectKey;
        input.checked = effectsList[effectKey].active;
        input.className = "checkboxInputBox";

        input.midimapLever = {
            func: "postprocessingParamToggle",
            effectKey: effectKey,
            type: "checkbox"
        }

        input.alias = effectKey;

        input.parentBlockId = "postprocessing";

        input.onchange = function (event) {

            var needUpdateParamsRenderview = undefined;

            effectsList[this.id].active = this.checked;   

            
            // update preset's super config

            if (event !== undefined) {

                needUpdateParamsRenderview = { value: true, parentBlockId: undefined };

                if (this.parentBlockId !== undefined) {  //if manual input on control

                    if (params.program[ currentProgram ][this.parentBlockId] === undefined) {

                        params.program[ currentProgram ][this.parentBlockId] = {};

                    }

                    params.program[ currentProgram ][this.parentBlockId][effectKey] = this.checked;

                    needUpdateParamsRenderview.parentBlockId = this.parentBlockId;

                }
            }

            renderview.postMessage({
                type: "postprocessingParamToggle",
                message: { effectKey: this.id, active: this.checked, needUpdateParams: needUpdateParamsRenderview }
            });


            // disable sliders if effect is turned off
            for (var i = 0, il = effectsList[effectKey].childControls.length; i < il; i++) {
                effectsList[effectKey].childControls[i].slider.disabled = !this.checked;
            }

        }

        checkboxControlItem.appendChild(label);
        checkboxControlItem.appendChild(input);

        return checkboxControlItem;

    }

}