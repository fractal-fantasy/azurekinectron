function makeScreenshot(button) {

    renderview.postMessage({ type: "action", message: "makeScreenshot" });

}

function captureRGB() {

    renderview.postMessage({ type: "action", message: "captureRGBenvMap" });

}

function processLoadedImage( file ) {

    var filename = file.name;
    var filepath = file.path;    

    var param = params.program[ currentProgram ].objects[ "depthMesh" ].material.envMapLoader;

    var hasInTheList = false;

    for (var i = 0; i < param.userLoadedEnvMapNameList.length; i++) {

        if (param.userLoadedEnvMapNameList[i].name === filename) hasInTheList = true;

    }

    if (!hasInTheList) {

        param.userLoadedEnvMapNameList.push( { name: filename, path: filepath } );

        // add image asset path into params cache    
        params.envMapTexturePathList[ filename ] = filepath;

        // update envMap property in material category
        params.program[ currentProgram ].objects[ "depthMesh" ].material.envMap.optionsList.push( filename );

        var optValue = params.program[ currentProgram ].objects[ "depthMesh" ].material.envMap.optionsList.length - 1;
        params.program[ currentProgram ].objects[ "depthMesh" ].material.envMap.value = optValue;

    }
    

    var message = { type: "loadNewTexture", message: { param: param, paramKey: "envMapLoader" } };

    renderview.postMessage( message );

}