// All params global. Default preset params

var currentProgram = 0;
var currentPresetButtonIndex = 0;

var permanent_settings_block, permanent_settings_left_panel, permanent_settings_right_panel,
    depth_settings_panel, global_settings_panel,
    depthSettingsBlock, lightSettingsBlock,
    materialSettingsBlock, meshDeformersSettingsBlock,
    cameraSettingsBlock, postprocessingMenuBlock, depthPostprocessingBlock,
    scene_objects_panel;

// new UI
var addableSceneObjectsBlock;

var globalSettingsBlock;

var isControlPressed = false;

var saveProjectButton, loadProjectButton;

var paramsBackup, defaultParams, postprocessingBackup;

var autoswitcherTimer, autoswitcherInterval = 5 * 60 * 1000;  // preset auto switcher interval in ms

var midiMapEventsVisible = true;
var oscMapEventsVisible = true;

var presetButtonsCollection = [];

var lastAddedMenuItem;

var id = function (id) {
    return document.getElementById(id);
}
var newElem = function (elem) {
    return document.createElement(elem);
}

const MATERIAL_MAP_IR = 0,
    MATERIAL_MAP_DEPTH = 1,
    MATERIAL_MAP_RGB = 2,
    MATERIAL_MAP_COLORRAMP = 3,
    MATERIAL_MAP_NORMAL = 4,
    MATERIAL_MAP_SCENEFLOW = 5,
    MATERIAL_MAP_DISPLACEMENT = 6,
    MATERIAL_MAP_NULL = 7

const materialMapOptionsList = [
    "Infrared", "Depth", "RGB", "Color Ramp", "Normal", "Scene Flow","Displacement", "Null"
]

const materialMapOptionsList_Pano = [
    1, 2, 3, 4, 5, 6, 7
]

var videomodeParams = {

}

var keepGlobal = {
                    camera: true,
                    depth: true,
                    displacement: true,
                    flipX: false,
                    midiActive: false
                }

var params = {

    envMapTexturePathList: { "ffpano1.jpg":"images/ffpano1.jpg","ffpano2.jpg":"images/ffpano2.jpg","ffpano3.jpg":"images/ffpano3.jpg","ffpano4.jpg":"images/ffpano4.jpg","ffpano5.jpg":"images/ffpano5.jpg","ffpano6.jpg":"images/ffpano6.jpg" },

    global: {

        camera: { value: false, type: "checkbox", name: "Camera" },
        depth: { value: false, type: "checkbox", name: "Depth" },
        displacement: { value: false, type: "checkbox", name: "Displacement" },
        flipX: { value: false, type: "checkbox", name: "Flip Projection X" },
        currentProgram: { value: 1 },
        midiActive:{ value: false, type: "checkbox", name: "midiActive" },
       // test:{ value: false, type: "checkbox", name: "test" },
       // test2:{ value: false, type: "checkbox", name: "test" },

    },

    camera: {
       // test3:{ value: false, type: "checkbox", name: "test" },
        camDistance: { value: 1500, defaultValue: 1200, min: 300, max: 5000, type: "range", name: "Distance" },
        camFOV: { value: 30, defaultValue: 30, min: 15, max: 90, type: "range", name: "FoV" },
        ortho: { value: false, defaultValue: false, type: "checkbox", name: "Orthographic camera" },
        dome: { value: false, defaultValue: false, type: "checkbox", name: "dome" },
        domeCamZ: { value: 0, defaultValue: 0, min: -500, max: 1500, type: "range", name: "dome camera Z" },
        
        camY: { value: 0, defaultValue: 0, min: -4000, max: 500, type: "range", name: "camera Y" },
        camRotate: { value: 0, defaultValue: 0, min: -3.14, max: 3.14, step: 0.01, type: "range", name: "camera Rotate" },
        camRotateX: { value: 0, defaultValue: 0, min: -3.14, max: 3.14, step: 0.01, type: "range", name: "camera Rotate X" },
        
        // camRotationX: { value: 0, defaultValue: 0, min: -1, max: 1, step: 0.001, type: "range", name: "camera Rotation X" },

        animateCamera: { value: false, defaultValue: false, type: "checkbox", name: "Rotate camera" },
        animateSpeed: { value: 1.5, defaultValue: 0.5, min: 0, max: 5, step: 0.01, type: "range", name: "  Speed" },
        animateZspeed: { value: 0.5, defaultValue: 0.5, min: 0, max: 5, step: 0.01, type: "range", name: "  Animate Z (speed)" },
        animateZamount: { value: 0, defaultValue: 0, min: 0, max: 4000, step: 1, type: "range", name: "  Animate Z (amount)" },

        resetCamera: { value: "Reset", type: "button", name: "", actionFunction: "resetCameraButtonFunction" },

        showStats: { value: false, type: "checkbox", name: "Show stats" },
        limitFPS: { value: false, type: "checkbox", name: "Limit FPS" },
        // antiAlias: { value: false, type: "checkbox", name: "antiAlias" },
        // debugHands: { value: false, type: "checkbox", name: "Debug hands" },

        //keepGlobal: { value: false, type: "checkbox", name: "Keep Global between presets" },
    },

    depth: {

        //depthLimit: { value: 888, min: 500, max: 6000, step: 1, type: "range", name: "depth focus" },
        //halfRange: { value: 400, min: 300, max: 1500, type: "range", name: "half depth range" },
        // oldMaterial: { value: false, type: "checkbox", name: "oldMaterial" },
        colorStart: { hidden: false, value: 0.0, min: 0.0, max: 1.0, step: .01, type: "range", name: "Color Ramp Start" },
        colorEnd: { hidden: false, value: 1.0, min: 0.0, max: 1.0, step: 0.1, type: "range", name: "Color Ramp End" },
        minDepth: { hidden: true, value: 300, min: 100, max: 6000, step: 1, type: "range", name: "minDepth" },
        maxDepth: { hidden: true, value: 1500, min: 100, max: 6000, step: 1, type: "range", name: "maxDepth" },

        depthLimit: { value: 768, defaultValue: 650, min: 500, max: 3000, step: 1, type: "range", name: "Depth focus" },
        halfRange: { value: 432, min: 100, max: 1500, step: 1, type: "range", name: "Half depth range" },

        //minIR: { value: 0, min: 0, max: 1000, step: 1, type: "range", name: "minIR" },
        maxIR: { value: 1500, min: 0, max: 3000, step: 1, type: "range", name: "maxIR" },

        //depthMode: { value: 0, type: 'dropdown', name: "Depth mode", optionsList: [ "depthToColor_WFOV", "depthToColor_NFOV", "colorToDepth_NFOV", "colorToDepth_WFOV", "depthToColor_WFOV_UNBINNED" ] },
        depthMode: { value: 0, type: 'dropdown', name: "Depth mode", optionsList: [ "depthToColor_WFOV", "depthToColor_NFOV", "colorToDepth_NFOV", "depthToColor_WFOV_UNBINNED" ] },
        //meshScale: { value: 1.0, min: 0.0, max: 5.0, step: 0.01, type: "range", name: "Mesh Scale" },


        // depthMode: { value: false, type: "checkbox", name: "WFOV/NFOV" }

        //keepGlobal: { value: false, type: "checkbox", name: "Keep Global between presets" },

        //midiInMessageLimit: { value: 50, min: 10, max: 200, type: "range", name: "Midi In Message Limit (per frame)" }
    },

    strobe: {
        strobe_toggle: { value: false, type: "checkbox", name: "Strobe toggle" },
        strobe_frequency: { value: 30, min: 1, max: 60, step: 1, type: "range", name: "Strobe frequency (FPS)" },        
        //strobe_hold: { value: 10, min: 30, max: 10000, step: 1, type: "range", name: "Strobe hold time (ms)" },
        flipLogoX: { hidden: true, value: false, type: "checkbox", name: "Flip logo X" }  // strobeShader
    },

    displacement:{

        
    },


    postprocessing: {
        

        // liquid: {
        //     active: false,
        //     // params: {
                
        //     // }
        // },

        edgeDetection: {
            active: false,
            params: {
                aspect: { value: 256, min: 256, max: 1024, step: 1, type: "range", name: "aspect" },
            }
        },

        motionBlur: {
            active: false,
            params: {
                motionMixRatio: { value: 0.98, min: 0.80, max: 0.99, step: 0.01, type: "range", name: "mixRatio" }
            }
        },

        

        unrealBloom: {
            active: true,
            params: {
                threshold: { value: 0.87, min: 0.0, max: 0.99, step: 0.01, type: "range", name: "BLOOM Threshold" },
                strength: { value: 1.74, min: 0.1, max: 5, step: 0.02, type: "range", name: "BLOOM Strength" },
                radius: { value: 0.7, min: 0.0, max: 2.0, step: 0.02, type: "range", name: "BLOOM Radius" }
            }
        },

        background: {
            active: true,
            params: {
                brightness: { value: 1.0, min: 0, max: 6, step: 0.01, type: "range", name: "Exposure" },  // postfx brightnessPass uniform
                backColorHue: { value: 120, min: 0, max: 320, type: "range", name: "back color HUE" },
                backColorSaturation: { value: 50, min: 0, max: 100, type: "range", name: "back color SATURATION" },
                backColorLightness: { value: 0, min: 0, max: 100, type: "range", name: "back color LIGHTNESS" }
            }

        },

    },

    depthPostprocessing: {  

        label4: { value: "Displacement Params", type: "label" },
        normalsRatio: { value: 2.0, min: 0.01, max: 10, step: 0.01, type: "range", name: "normals ratio" },
       // normalsRatioOld: { value: 1.69, min: 0.0, max: 10, step: 0.01, type: "range", name: "normals ratio old" },
        normalBias: { value: 0.0, min: -1.0, max: 1.0, step: 0.01, type: "range", name: "normal bias dispacement" },
        normalBiasShading: { value: 0.0, min: -1.0, max: 1.0, step: 0.01, type: "range", name: "normal bias shading" },
        oldNormals: { value: true, type: "checkbox", name: "Old Normals Behaviour" },    
        lookupRadius: { value: 1.90, min: 0.5, max: 10, step: 0.01, type: "range", name: "lookup radius" },
        normalZ: { value: 1.00, min: -20.0, max: 20.0, step: 0.0001, type: "range", name: "normalZ" },
        displacementScaleDepth: { value: 970, min: -5000, max: 5000, type: "range", name: "displacementScale" },
        anisotropy: { value: 1.0, min: 1, max: 2, step: 1.0, type: "range", name: "anisotropy" },
       // fixEdgeNormals: { value: true, type: "checkbox", name: "fixEdgeNormals" }, 
       // fxaaDisplacement: { value: false, type: "checkbox", name: "fxaaDisplacement" },
       // cutSidePixels: { value: true, type: "checkbox", name: "cutSidePixels" },    
       // refine_cutSidePixels: {value: true, type: "checkbox", name: "refine_cutSidePixels" },  
       // resMult: { value: 1.0, min: 0, max: 100, step: 0.1, type: "range", name: "resMult" },

        // reduceMul: { value: 4.0, min: 0, max: 100, step: 0.1, type: "range", name: "reduceMul" },
        // reduceMin: { value: 1.0, min: 0, max: 100, step: 1.0, type: "range", name: "reduceMin" },
       //  encoding: { value: 0, type: 'dropdown', name: "encoding", optionsList: [ "THREE.LinearEncoding", "THREE.sRGBEncoding", "THREE.BasicDepthPacking", "THREE.RGBADepthPacking" ] },
        // magFilter: { value: 0, type: 'dropdown', name: "magFilter", optionsList: [ "THREE.NearestFilter", "THREE.LinearFilter" ] },
         //minFilter: { value: 3, type: 'dropdown', name: "minFilter", optionsList: [ "THREE.NearestFilter", "THREE.NearestMipmapNearestFilter", "THREE.NearestMipmapLinearFilter", "THREE.LinearFilter" , "THREE.LinearMipmapNearestFilter" , "THREE.LinearMipmapLinearFilter"  ] },
         // mipmaps: { value: false, type: "checkbox", name: "mipmaps" },

        label11: { value: "Edge Extrude", type: "label" },
        edgeExtrude:{ value: true, type: "checkbox", name: "edgeExtrude" },
        edgeExtrudeXtra:{ value: true, type: "checkbox", name: "edgeExtrudeXtra" },
        extrudeDepth:{ value: 0.14, min: 0.0, max: 1.0, step: 0.0005, type: "range", name: "extrudeDepth" },
        blurEdgeAmount: { value: 1.0, min: 0, max: 10, step: 0.05, type: "range", name: "blurEdgeAmount" },
        

        label10: { value: "Temporal Smooth", type: "label" },
        temporalSmoothing: { value: true, type: "checkbox", name: "temporalSmoothing" },
        temporalMixRatio: { value: 0.81, min: 0.00, max: 0.99, step: 0.01, type: "range", name: "temporalMixRatio" },
        temporalThreshold: { value: 0.02, min: 0.0, max: 0.3, step: 0.01, type: "range", name: "temporalThreshold" },
        temporalThresholdRange: { value: 4.0, min: 1.0, max: 20.0, step: 1.00, type: "range", name: "temporalThresholdRange" },
        fxaaDepth: { value: false, type: "checkbox", name: "FXAA Depth x 8" },
        //fxaaDepth2: { value: false, type: "checkbox", name: "FXAA Depth x 16" },
        // denoiseDepthNew: { value: false, type: "checkbox", name: "Denoise Depth" },
        // denoiseAmount: { value: 2.0, min: 0, max: 20, step: 1.0, type: "range", name: "denoise amount" },
        // sampleRadius: { value: 1.0, min: 1, max: 10, step: 1.0, type: "range", name: "denoise radius (faster & uglier)" },

        label9: { value: "Blur", type: "label" },
        blurDepth: { value: true, type: "checkbox", name: "Blur Depth" },
        blurAmount: { value: 1.0, min: 0, max: 10, step: 0.05, type: "range", name: "blurAmount" },
        tiltBlurAmount: { value: 0.0, min: 0, max: 10, step: 0.05, type: "range", name: "tiltBlurAmount" },
        tiltBlurRadius: { value: 2.0, min: 0, max: 4, step: 0.001, type: "range", name: "tiltBlurRadius" },

        // holeFillingHorizontal: { value: false, type: "checkbox", name: "Hole Filling Horizontal" },
        // hf_radius: { value: 4, min: 1, max: 20, step: 1, type: "range", name: "HF Radius" },
        // holeFillingCircle: { value: false, type: "checkbox", name: "Hole Filling Circle" },
        label8: { value: "Hole Filling", type: "label" },
        holeFillingRay: { value: false, type: "checkbox", name: "Hole Filling Ray" },
        hf_circleRadius: { value: 50, min: 1, max: 100, step: 1, type: "range", name: "HF Circle Radius" },
        hf_sampleCount: { value: 10, min: 1, max: 20, step: 1, type: "range", name: "HF Sample Count" },
        hf_pixelsPerStep: { value: 10.0, min: 1.0, max: 20.0, step: 1.0, type: "range", name: "hf_pixelsPerStep" },
        
        holeFillingConv: { value: true, type: "checkbox", name: "Hole Filling Conv" },
        hf_min_defined: { value: 12, min: 3, max: 24, step: 1, type: "range", name: "HF AvgPool Min Defined" },
        hf_offset: { value: 0.0, min: 0, max: 24, step: 0.1, type: "range", name: "HF AvgPool Offset" },
        hf_denominator: { value: 5.0, min: 0, max: 50, step: 1, type: "range", name: "HF AvgPool Denominator" },

        // holeFilling: { value: false, type: "checkbox", name: "Hole Filling Basic" },
        // holeFilling5x5: { value: false, type: "checkbox", name: "Hole Filling Basic 5x5" },


        

        

        // label6: { value: "Depth PostFX", type: "label" },


        //EDGECUTTING
        // sharpEdge: { value: false, type: "checkbox", name: "Sharp Edges" },
        // sharpEdgeWidth: { value: 3.65, min: 0.0, max: 10.0, step: 0.01, type: "range", name: "Sharp Edge Width" },
         edge: { value: false, type: "checkbox", name: "Cut Edges FF " },
         edgeThreshold: { value: 0.06, min: 0.0, max: 0.1, step: 0.0005, type: "range", name: "edgeThreshold (bring down the more smooth)" },
         maxThresholdFactor: { value: 4.00, min: 1.0, max: 40.0, step: 0.1, type: "range", name: "maxThresholdFactor (bring up the more smooth)" },
         maxSteps: { value: 4, min: 0, max: 50, step: 1, type: "range", name: "maxSteps (bring up the more smooth)" },


        
        

        
        


        // hf_min_defined: { value: 12, min: 3, max: 24, step: 1, type: "range", name: "HF AvgPool Min Defined" },
        // hf_offset: { value: 0.0, min: 0, max: 24, step: 0.1, type: "range", name: "HF AvgPool Offset" },
        // hf_denominator: { value: 5.0, min: 0, max: 50, step: 1, type: "range", name: "HF AvgPool Denominator" },

             

        // barrelAlpha: { value: 0.0, min: -1.57, max: 1.57, step: 0.01, type: "range", name: "barrelAlpha" },
        // barrelScale: { value: 1.0, min: 0.1, max: 2.0, step: 0.01, type: "range", name: "barrelScale" },  


       
      
    },

    depthFX: {  
        FXScale: { value: 1.0, min: 0.0, max: 5.0, step: 0.001, type: "range", name: "FX Scale" },
        depthFXOnBackground: { value: false, type: "checkbox", name: "No DepthFX On BG" }, 
        
        label13: { value: "Ripple & Fatness", type: "label" },
        // displaceAlongZ: { value: false, type: "checkbox", name: "displaceAlongZ" }, 
         displacementFatness: { value: 0.0, min: -10.0, max: 10.0, step: 0.1, type: "range", name: "displacementFatness" }, //
        rippleDepthAmplitude: { value: 0.0, min: 0.0, max: 100.0, step: 0.1, type: "range", name: "Ripple Depth Amplitude" }, //
        rippleDepthSpeed: { value: 1.6, min: 0.0, max: 10.0, step: 0.001, type: "range", name: "Ripple Depth Speed" }, //
        rippleDepthFrequency: { value: 13.0, min: 0.0, max: 80.0, step: 0.1, type: "range", name: "Ripple Depth Frequency" }, //
        quantizeDepth:{ value: false, type: "checkbox", name: "quantizeDepth" }, //
		quantizeDepthFrequency: { value: 13.0, min: 0.0, max: 80.0, step: 0.1, type: "range", name: "quantizeDepthFrequency" }, //
        
        label14: { value: "Liquid", type: "label" },
        liquid: { value: false, type: "checkbox", name: "Liquid" }, //
        tension: { value: 0.4, min: 0.0, max: 1.0, step: 0.001, type: "range", name: "scene-flow tension" },  //
        sourceVelocityScale: { value: 3.0, min: 0.0, max: 10.0, step: 0.001, type: "range", name: "sourceVelocityScale" }, //
        depthVelocityThreshold: { value: 50.0, min: 0.0, max: 50.0, step: 0.2, type: "range", name: "depthVelocityThreshold (fix hands b4 mesh)" }, //
        mixRatio: { value: 0.98, min: 0.80, max: 0.99, step: 0.01, type: "range", name: "temporal Smooth Amount" }, //
        blurDynamicAmount: { value: 0.5, min: 0.0, max: 10.0, step: 0.1, type: "range", name: "Blur Amount" },   //
        liquidSpeed: { value: 1.0, min: 0.0, max: 2.0, step: 0.001, type: "range", name: "Liquid Speed" }, //
        liquidDamp: { value: 0.024, min: 0.0, max: 0.5, step: 0.001, type: "range", name: "Liquid Damp" },//
        liquidBlur: { value: 4.3, min: 0.0, max: 10.0, step: 0.1, type: "range", name: "Liquid Blur" }, //
        liquidTension: { value: 0.0475, min: 0.0, max: 0.1, step: 0.0005, type: "range", name: "Liquid Tension" }, //
        liquidGravity: { value: 0.0, min: -0.5, max: 0.5, step: 0.001, type: "range", name: "Liquid Gravity" }, //
        maxGravity: { value: 0.0, min: 0.0, max: 20.0, step: 0.001, type: "range", name: "Max Gravity" }, //
        
        label2: { value: "Echo", type: "label" },
        preprocessDepthWithEcho: { value: false, type: "checkbox", name: "Depth" }, //
        preprocess_IR_WithEcho: { value: false, type: "checkbox", name: "IR" }, //
        preprocess_Color_WithEcho: { value: false, type: "checkbox", name: "Color" }, //
        // cleanBuffer: { value: false, type: "checkbox", name: "cleanBuffer (doesn't work)" },
        maxBufferSize: { value: 32, min: 1, max: 128, step: 1, type: "range", name: "max buffer size" }, //
        numSplits: { value: 6, min: 2, max: 8, step: 1, type: "range", name: "number of splits" }, //

        label7: { value: "Dots", type: "label" },
        dotAmplitude: { value: 0.0, min: -0.0, max: 1000.0, step: 0.1, type: "range", name: "Dot Amplitude" }, //
        dotSize: { value: 0.1, min: 0.0, max: 1.0, step: 0.001, type: "range", name: "Dot Size" }, // 
        dotFrequency: { value: 43.0, min: 0.0, max: 460.0, step: 0.1, type: "range", name: "Dot Frequency" }, // 
        dotExponent: { value: 2.0, min: 0.0, max: 2.0, step: 0.001, type: "range", name: "Dot Exponent" }, //
        quantizeDotsNormal: { value: true, type: "checkbox", name: "quantize Dots Normal" }, // 
        staggerDots: { value: true, type: "checkbox", name: "staggerDots" }, // 
        useDotsForDepth:{ value: false, type: "checkbox", name: "useDotsForDepth"}, //
        useNoiseToSmearDots: { value: true, type: "checkbox", name: "Use Noise To Smear Dots" },   //

        label12: { value: "Noise", type: "label" },
        noiseVariations: { value: true, type: "checkbox", name: "Noise Variations" }, //
        noiseAmplitude: { value: 0.0, min: -1000.0, max: 1000.0, step: 0.1, type: "range", name: "Noise Amplitude" }, //
        noiseFrequency: { value: 10.0, min: 0.0, max: 100.0, step: 0.001, type: "range", name: "Noise Frequency" }, //
        noiseSpeed: { value: 0.01, min: 0.0, max: 0.3, step: 0.001, type: "range", name: "Noise Speed" }, //
        noiseWormAmplitude: { value: 0.0, min: -1.0, max: 1.0, step: 0.1, type: "range", name: "Noise Worm Amplitude" }, //
        noiseCellAmplitude: { value: 0.0, min: -1.0, max: 1.0, step: 0.1, type: "range", name: "Noise Cell Amplitude" }, //
        noiseRodAmplitude: { value: 0.0, min: -1.0, max: 1.0, step: 0.1, type: "range", name: "Noise Rod Amplitude" }, //
        smearAmplitude: { value: 0.0, min: 0.0, max: 1000.0, step: 0.1, type: "range", name: "Smear Amplitude" }, //
        smearFrequency: { value: 0.8, min: 0.0, max: 10.0, step: 0.001, type: "range", name: "Smear Frequency" }, //
        noiseMask: { value: 0.8, min: 0.0, max: 10.0, step: 0.001, type: "range", name: "Noise Mask" }, //
        maskThreshold: { value: 0.0, min: 0.0, max: 1.0, step: 0.001, type: "range", name: "Mask Threshold" }, //
        maskFrequency: { value: 2.0, min: 0.0, max: 10.0, step: 0.001, type: "range", name: "Mask Frequency" }, //
        maskSpeed: { value: 0.0, min: 0.0, max: 1.0, step: 0.001, type: "range", name: "Mask Speed" }, //
        
        label3: { value: "Reaction-Diffusion", type: "label" },
        preprocessDepthWithReactionDiffusion: { value: false, type: "checkbox", name: "On/Off" }, //
        // RD_useDepth: { value: true, type: "checkbox", name: "switch IR / depth" },
        RD_numIterations: { value: 2, defaultValue: 2, min: 1, max: 10, step: 1, type: "range", name: "num iterations" }, //
        rd_stimulationStrength: { value: 1, defaultValue: 1, min: 0, max: 10, step: 0.1, type: "range", name: "stimulation strength body" }, //
        rd_stimulationStrengthHandPointers: { value: 0, defaultValue: 0, min: 0, max: 10, step: 0.1, type: "range", name: "simulation strength hands" }, //
        "rd_zoom_offset": { hidden: false, value: 0.003, min: 0, max: 0.03, step: 0.001, type: "range", name: "Zoom" }, //
        "rd_blobSize": { hidden: true, value: 0.035, min: 0.02, max: 0.1, step: 0.005, type: "range", name: "Blob Size" }, //
        "rd_blobBlurSize": { hidden: true, value: 3, min: 1, max: 10, step: 0.05, type: "range", name: "Blob Blur Size" },//
        "rd_directionOn": { value: true, type: "checkbox", name: "feedback direction on" }, //
        "rd_a": { value: 0.125, defaultValue: 0.125, min: -0.5, max: 0.5, step: 0.001, type: "range", name: "a" }, //
        "rd_b": { value: 0.8, defaultValue: 0.8, min: 0.01, max: 1.5, step: 0.005, type: "range", name: "b" }, //
        "rd_delta": { value: 0.02, defaultValue: 0.02, min: -0.02, max: 0.3, step: 0.005, type: "range", name: "delta" }, //
        "rd_eps": { value: 0.021, defaultValue: 0.021, min: 0.006, max: 0.3, step: 0.001, type: "range", name: "eps" }, //
        "rd_dt": { value: 0.0084, defaultValue: 0.0084, min: 0.00, max: 0.01, step: 0.0001, type: "range", name: "dt" }, //
        resetRDparams: { value: "Reset", type: "button", name: "", actionFunction: "resetRDparams" } 
    },

    program: [

        {
            alias: "0. this is a template for a new preset params object / program[i] / not loaded in presets panel",

            objects: {

                depthMesh: {
                    visibility: { value: true, type: "checkbox", name: "1 - Depth Mesh" },
                    material: {
                        doubleSide: { value: false, type: "checkbox", name: "Double Sided" },
                        onlyDepthNormals: { value: 0.0, min: 0.0, max: 1.0, step: 0.001, type: "range", name: "Only Depth Normals" }, 
                        showNormalMap: { value: false, type: "checkbox", name: "showNormalMap" },
                        normalScale: { value: 0.0, min: 0, max: 500, step: 0.1, type: "range", name: "normal scale" },
                                
                        farWall:{ value: 0.0, min: 0, max: 500, step: 0.1, type: "range", name: "farWall" },
                        reverseWall:{ hidden: false, value: false, type: "checkbox", name: "reverseWall" },
                    
                        cutBackground: { hidden: false, value: true, type: "checkbox", name: "Cut background" },
                        edgeThreshold: { value: 0.625, min: 0, max: 1, step: 0.001, type: "range", name: "edgeThreshold" },
                        edgeSoftness: { value: 0.114, min: 0, max: 1, step: 0.001, type: "range", name: "edgeSoftness" },
                        
                        // cutBigPolygons: { hidden: false, value: false, type: "checkbox", name: "cutBigPolygons" },
                        // largePolyThreshold: { value: 20.0, min: 0, max: 100, step: 0.01, type: "range", name: "largePolyThreshold" },
                       //  displacementScale: { value: 512, min: -5000, max: 5000, type: "range", name: "displacementScale" },

                        //cutDepthThreshold: { hidden: false, value: 0.1, min: 0, max: 10.6, step: 0.01, type: "range", name: "cutDepthThreshold" },
                        //cutPosition_W: { hidden: false, value: -1.0, min: -1000, max: 1000, step: 1, type: "range", name: "cutPosition_W" },

                        // edge detection/removal params -------- //
                        oldEdge: { value: false, type: "checkbox", name: "Enable Old Edge Removal" },
                        edgeWidth: { value: 0.0, min: 0, max: 10, step: 0.1, type: "range", name: "edgeWidth" },
                        // edgeThreshold: { value: 0.05, min: 0.05, max: 0.5, step: 0.001, type: "range", name: "edgeThreshold" },
                        edgeAlphaMultiplier: { value: 10.0, min: 0, max: 40, step: 0.1, type: "range", name: "edgeAlphaMultiplier" },
                        // edgeResolutionMultiplier: { value: 1.0, min: 0, max: 10, step: 0.1, type: "range", name: "edgeResolutionMultiplier" },
                        // -------------------------------------- //
                      
                        positionZ: { value: -300, min: -4000, max: 2000, step: 1, type: "range", name: "position on Z axis" },
                        meshRotationX: { value: 0, min: -1, max: 1, step: 0.01, type: "range", name: "rotation on X axis" },
                        //keepGlobal: { value: false, type: "checkbox", name: "      Keep Displacement Global" },
                        
                        map: { value: MATERIAL_MAP_IR, type: 'dropdownColorMap', name: "Color map", optionsList: materialMapOptionsList },
                        
                      
                        diffuse: { value: "ffffff", type: "color", name: "diffuse" },
                        //diffuseColorOffset: { value: "ffffff", type: "color", name: "diffuseColorOffset" },

                        diffuseColorOffset: { value: 0.0, min: 0, max: 1, step: 0.01, type: "range", name: "diffuseColorOffset" },
                        diffuseExposure: { value: 1.0, min: -2, max: 2, step: 0.01, type: "range", name: "diffuseExposure" },
                        roughness: { value: 0.1, min: 0, max: 1, step: 0.05, type: "range", name: "roughness" },
                        metalness: { value: 0.0, min: 0, max: 1, step: 0.05, type: "range", name: "metalness" },
                        opacity: { value: 1.0, min: 0, max: 1, step: 0.05, type: "range", name: "opacity" },

                        envMapLoader: { 
                           type: "file", name: "EnvMap", userLoadedEnvMapNameList: [ { name: "Null", path: "" } ],
                         //   type: "file", name: "EnvMap", userLoadedEnvMapNameList: [ { name: "ffpano6", path: "C:\\Users\\FractalFantasy\\Documents\\azurekinectron\\renderer\\images\\ffpano6.jpg" } ],
                        //    envMap: { value: 0, type: 'dropdownEnvMap', optionsList: [ "Null" ] } 
                        },
                       // envMap: { value: 0, type: 'dropdownEnvMap', name: "    loaded textures:", optionsList: [ "ffpano6" ] },
                        envMap: { value: 0, type: 'dropdownEnvMap', name: "    loaded textures:", optionsList: [ "Null" ] },
                        // captureRGB: { value: "capture RGB", type: "button", name: "", actionFunction: "captureRGB" },

                        refraction: { value: false, type: "checkbox", name: "refraction" },
                        flatEnvMap: { value: false, type: "checkbox", name: "use flat envMap" },

                        refractionRatio: { value: 0.98, min: 0.0, max: 3.0, step: 0.01, type: "range", name: "refraction ratio" },
                        envZoom: { value: 1.68, min: 0.8, max: 2.5, step: 0.01, type: "range", name: "envMap zoom" },
                        // envRotation: { value: 0, min: 0.0, max: Math.PI * 3, step: 0.01, type: "range", name: "envMap rotation" },
                        // envAutoRotation: { value: false, type: "checkbox", name: "envMap auto rotation" },
                        // envRotationSpeed: { value: 0.2, min: 0.0, max: 10, step: 0.02, type: "range", name: "envMap rotation speed" },
                        
                        frenelPower: { value: 1.4, min: 0, max: 3.0, step: 0.01, type: "range", name: "frenelPower" },
                        //frenelScale: { value: 0.0, min: 0, max: 1.0, step: 0.01, type: "range", name: "frenelScale" },
                        // fresnelFixScaleX: { value: 1.0, min: 0.2, max: 1.4, step: 0.01, type: "range", name: "frenel FIX ScaleX" },
                        // fresnelFixScaleY: { value: 1.6, min: 0.2, max: 1.6, step: 0.01, type: "range", name: "frenel FIX ScaleY" },
                        // fresnelFixPosX: { value: 0.5, min: 0.2, max: 1.0, step: 0.01, type: "range", name: "frenel FIX PosX" },
                        // fresnelFixPosY: { value: 0.5, min: 0.2, max: 1.0, step: 0.01, type: "range", name: "frenel FIX PosY" },

                        // label1: { value: "Standard normal map:", type: "label" },

                        // bumpMap: { value: false, type: "checkbox", name: "bumplMap" },
                        // bumpScale: { value: 50, min: 0, max: 100.0, step: 0.1, type: "range", name: "bumpMap Scale" },

                        //IF NORMAL MAP IS SET TO FALSE NULL MAP DOESNT WORK
                        normalMap: { value: true, type: "checkbox", name: "normalMap" },
                       // normalScale: { value: 0.0, min: 0, max: 30.0, step: 0.01, type: "range", name: "normalMap Scale" },
                        // vertexTangents: { value: false, type: "checkbox", name: "vertexTangents" },

                        // label2: { value: "Custom normal sampling:", type: "label" },

                        // useCustomNormal: { value: true, type: "checkbox", name: "On / Off" },
                        // normalsRatio: { value: 1.69, min: 0.0, max: 10, step: 0.01, type: "range", name: "normals ratio" },
                        // lookupRadius: { value: 2.00, min: 0.05, max: 10, step: 0.01, type: "range", name: "lookup radius" },
                        // normalZ: { value: 0.04, min: -0.1, max: 0.1, step: 0.001, type: "range", name: "normal Z" },

                    },
                    properties:{
                        amplitude: { value: 0.0, min: -40.0, max: 40.0, step: 0.1, type: "range", name: "Fatness Amplitude" },
                        rippleAmplitude: { value: 0.0, min: 0.0, max: 80.0, step: 0.1, type: "range", name: "Ripple Amplitude" },
                        rippleSpeed: { value: 0.6, min: 0.0, max: 10.0, step: 0.001, type: "range", name: "Ripple Speed" },
                        rippleFrequency: { value: 13.0, min: 0.0, max: 80.0, step: 0.1, type: "range", name: "Ripple Frequency" },
                        //meshRotation: { value: 0, min: -1, max: 1, step: 0.01, type: "range", name: "rotation on Z axis" },
                        //normalScale: { value: 0.0, min: 0.0, max: 80.0, step: 0.1, type: "range", name: "Normal Scale" },
                        //deformNormalsFix: { value: true, type: "checkbox", name: "deformNormalsFix" },
                       // rippleNormalScale: { value: 0.05, min: 0.01, max: 0.2, step: 0.001, type: "range", name: "Ripple Normal Scale" }, 

                        
                        
                        //xySize: { value: 2.0, min: 0.0, max: 50.0, step: 0.1, type: "range", name: "xySize" },

                        //normalsFF: { value: true, type: "checkbox", name: "normalsFF" },            
                        

                        //// gradient calculation params in raw depth shader (for edge cutting)
                        // depthProc_gradientEnable: { value: false, type: "checkbox", name: "Enable Edge Gradient Detection" },
                        // depthProc_uvScale: { value: 6.90, min: 0, max: 10, step: 0.01, type: "range", name: "Gradient Width" },
                        // depthProc_gradThreshold: { value: 0.6, min: 0.1, max: 4, step: 0.001, type: "range", name: "Gradient Threshold" },
                    }
                },
                pointcloud: {
                    visibility: { value: false, type: "checkbox", name: "2 - Pointcloud" },
                    material: {
                        
                        displacementScale: { value: 1000, min: -5000, max: 5000, type: "range", name: "displacement scale" },
                        positionZ: { value: -300, min: -4000, max: 2000, step: 1, type: "range", name: "position on Z axis" },
                        map: { value: MATERIAL_MAP_DEPTH, type: 'dropdownColorMap', name: "Color map", optionsList: materialMapOptionsList },
                        geometryDensity: { value: 1, min: 1, max: 8, step: 1, type: "range", name: "Density" },
                        pointSize: { value: 1, min: 0.5, max: 10, step: 0.1, type: "range", name: "point size" },
                        depthCutLevel: { value: 0.1, min: 0.01, max: 0.7, step: 0.02, type: "range", name: "depth cut level" },
                        // autorotate: { value: true, type: "checkbox", name: "autorotate" },                        
                        useColorRamp: { value: true, type: "checkbox", name: "useColorRamp" },
                        colorShift: { value: 1.16, min: -3.0, max: 3.0, step: 0.01, type: "range" },
                        colorWidth: { value: 0.12, min: -10.0, max: 10.0, step: 0.01, type: "range" },
                        brightness: { value: 1, min: 0.2, max: 1.0, step: 0.01, type: "range" },
                        saturation: { value: 1, min: 0.0, max: 1.0, step: 0.05, type: "range" }
                    },
                    properties: {
                        // useRawDepth: { value: false, type: "checkbox", name: "useRawDepth" }
                    }
                },
                instanced: {
                    visibility: { value: false, type: "checkbox", name: "3 - Instanced" },
                    material: {

                        displacementScale: { value: 512, min: -5000, max: 5000, type: "range", name: "displacement scale" },
                        positionZ: { value: -300, min: -4000, max: 2000, step: 1, type: "range", name: "position on Z axis" },
                        // map: { value: MATERIAL_MAP_IR, type: 'dropdownColorMap', name: "Color map", optionsList: materialMapOptionsList },
                        diffuse: { value: "ffffff", type: "color", name: "diffuse" },
                        roughness: { value: 0.30, min: 0, max: 1, step: 0.05, type: "range", name: "roughness" },
                        metalness: { value: 0.99, min: 0, max: 1, step: 0.05, type: "range", name: "metalness" },
                        opacity: { value: 1.0, min: 0, max: 1, step: 0.05, type: "range", name: "opacity" },
                        ballRadius: { value: 6, min: 1.0, max: 20, step: 0.1, type: "range", name: "ballRadius" },
                        ballRadiusPower: { value: 1.2, min: 0.75, max: 2.0, step: 0.01, type: "range", name: "ballRadiusPower" },
                        //useGeoMod: { value: false, type: "checkbox", name: "useGeoMod" },
                        envMapLoader: { type: "file", name: "EnvMap",
                            userLoadedEnvMapNameList: [ { name: "Null", path: "" } ]
                        },
                        envMap: { value: 0, type: 'dropdownEnvMap', name: "    loaded textures:", optionsList: [ "Null" ] },
                        refraction: { value: false, type: "checkbox", name: "refraction" },
                        refractionRatio: { value: 0.98, min: 0.0, max: 3.0, step: 0.01, type: "range", name: "refraction ratio" },
                        frenelPower: { value: 0.5, min: 0, max: 2.0, step: 0.01, type: "range", name: "frenelPower" },
                        frenelScale: { value: 0.0, min: 0, max: 1.0, step: 0.01, type: "range", name: "frenelScale" },
                    },
                    properties: {
                    }
                },

                backgroundPlane: {
                    visibility: { value: false, type: "checkbox", name: "4 - Background plane" },
                    material: {
                        map: { value: MATERIAL_MAP_DEPTH, type: 'dropdownColorMap', name: "Color map", optionsList: materialMapOptionsList },
                        roughness: { value: 1.0, min: 0, max: 1, step: 0.05, type: "range", name: "roughness" },
                        metalness: { value: 0.0, min: 0, max: 1, step: 0.05, type: "range", name: "metalness" },
                        opacity: { value: 1.0, min: 0, max: 1, step: 0.05, type: "range", name: "opacity" },
                        envMapLoader: { type: "file", name: "EnvMap",
                            userLoadedEnvMapNameList: [ { name: "Null", path: "" } ]
                        },
                        envMap: { value: 0, type: 'dropdownEnvMap', name: "    loaded textures:", optionsList: [ "Null" ] }
                    }
                },
                
                backgroundSphere: {
                    visibility: { value: false, type: "checkbox", name: "5 - Background sphere" },
                    material: {
                        // map: { value: 0, type: 'dropdown', name: "Pano map", optionsList: materialMapOptionsList_Pano },
                        envMapLoader: { type: "file", name: "EnvMap", userLoadedEnvMapNameList: [ { name: "Null", path: "" } ] },
                        envMap: { value: 0, type: 'dropdownEnvMap', name: "    loaded textures:", optionsList: [ "Null" ] },
                        bgSpherePlay: { value: false, type: "checkbox", name: "Playing" },
                        //opacity: { value: 1.0, min: 0, max: 1, step: 0.05, type: "range", name: "opacity" },
                        // bgSpherePlay: { value: "Play", type: "button", name: "", actionFunction: "bgSpherePlay" } 
                        
                    },
                    properties:{
                        sphereOpacity: { value: 1, min: 0, max: 1, step: 0.01, type: "range", name: "Sphere Opacity" },
                        autoRotation: { value: false, type: "checkbox", name: "auto rotation" },
                        autoRotationSpeed: { value: 0.2, min: 0, max: 4, step: 0.1, type: "range", name: "auto rotation speed" },
                        scale: { value: 1, min: 0, max: 6, step: 0.1, type: "range", name: "Geometry scale" },
                        spherePositionY: { value: 0, min: -800.0, max: 800.0, step: 0.01, type: "range", name: "Position Y" },
                        domeDegrees: { value: 90, min: 0, max: 360, step: 1, type: "range", name: "Dome degrees" },
                        rotationY: { value: 0, min: - 3.14, max: 3.14, step: 0.01, type: "range", name: "Rotation Y" },
                        rotationX: { value: 0, min: - 3.14, max: 3.14, step: 0.01, type: "range", name: "Rotation X" },
                    }
                },
                handGeometry: {
                    visibility: { value: false, type: "checkbox", name: "6 - Hand markers" },
                    material: {
                        //map: { value: MATERIAL_MAP_IR, type: 'dropdown', name: "Color map" },
                        roughness: { value: 0.30, min: 0, max: 1, step: 0.05, type: "range", name: "roughness" },
                        metalness: { value: 0.70, min: 0, max: 1, step: 0.05, type: "range", name: "metalness" },
                        opacity: { value: 1.0, min: 0, max: 1, step: 0.05, type: "range", name: "opacity" },
                    },
                    properties: {
                        enableBT: { value: false, type: "checkbox", name: "Enable Body Tracking"},
                        size: { value: 0.50, min: 0.5, max: 5, step: 0.1, type: "range", name: "size" },
                        positionZ: { value: 50, min: -2000, max: 200, step: 1, type: "range", name: "position Z" },
                        positionX: { value: -276, min: -400, max: 400, step: 1, type: "range", name: "position X" },
                        positionY: { value: -232, min: -400, max: 400, step: 1, type: "range", name: "position Y" },
                        // smoothing: { value: 0.5, min: 0, max: 1, step: 0.01, type: "range", name: "smoothing" },
                    }
                },

                lights: {
                    visibility: { value: true, type: "checkbox", name: "7 - Lights" },
                    material: {},
                    properties: {
                        ambientLightOn: { value: false, type: "checkbox", name: "Ambient light" },
                        ambientLightColor: { value: "ffffff", type: "color", name: "color" },
                        ambientLightIntensity: { value: 1.0, min: 0, max: 1, step: 0.01, type: "range", name: "Ambient intensity" },
                    
                        seperator1: { type: "seperator", value: "---------" },
                        
                        pointLight1On: { value: true, type: "checkbox", name: "Point light 1" },
                        pointLight1Color: { value: "FFFFFF", type: "color", name: "color" },
                        pointLight1Intensity: { value: 1.1, min: 0, max: 4, step: 0.01, type: "range", name: "Point light 1 intensity" },
                        label_position_1: { type: "text", value: "Point light 1 position:" },
                        pointLight1Position_X: { value: -73, min: -500, max: 500, step: 1, type: "range", name: "X" },
                        pointLight1Position_Y: { value: 160, min: -500, max: 500, step: 1, type: "range", name: "Y" },
                        pointLight1Position_Z: { value: 1100, min: -500, max: 2000, step: 1, type: "range", name: "Z" },
                        animatePointLight1: { value: false, type: "checkbox", name: "Animate" },      
                        
                        seperator2: { type: "seperator", value: "---------" },

                        pointLight2On: { value: true, type: "checkbox", name: "Point light 2" },
                        pointLight2Color: { value: "0000FF", type: "color", name: "color" },
                        pointLight2Intensity: { value: 0.7, min: 0, max: 4, step: 0.01, type: "range", name: "Point light 2 intensity" },
                        label_position_2: { type: "text", value: "Point light 2 position:" },
                        pointLight2Position_X: { value: 112, min: -500, max: 500, step: 1, type: "range", name: "X" },
                        pointLight2Position_Y: { value: 200, min: -1000, max: 1000, step: 1, type: "range", name: "Y" },
                        pointLight2Position_Z: { value: 600, min: -2000, max: 2000, step: 1, type: "range", name: "Z" },
                        animatePointLight2: { value: false, type: "checkbox", name: "Animate" },
                        
                        seperator3: { type: "seperator", value: "---------" },

                        animateSpeed: { value: 0.5, min: 0, max: 5, step: 0.01, type: "range", name: "  Animation Speed" },
                        animateRadius: { value: 500, min: 1, max: 5000, step: 1, type: "range", name: "  Radius from center" },
                        animateYspeed: { value: 0.5, min: 0, max: 5, step: 0.01, type: "range", name: "  Animate Y (speed)" },
                        animateYamount: { value: 100, min: 0, max: 4000, step: 1, type: "range", name: "  Animate Y (amount)" },
                        showLightHelpers: { value: false, type: "checkbox", name: "Show light helpers" },

                        seperator4: { type: "seperator", value: "---------" },

                        strobePointLights: { value: false, type: "checkbox", name: "Strobe point lights" },
                        strobeFrequency: { value: 2, min: 1, max: 60, step: 1, type: "range", name: "Frequency (FPS)" },
                        strobeHold: { value: 30, min: 0, max: 60, step: 1, type: "range", name: "Hold (FPS)" },

                    }
                }
            },


            "strobe": { 
                "strobe_frequency": 30, 
                "strobe_hold": 10,
                "strobe_toggle":false,
                "flipLogoX": false
            },

            "postprocessing": {
                "edgeDetection": false,
                "motionBlur": false,
                "unrealBloom": true,
                "threshold": 0.87,
                "strength": 1.74,
                "radius": 0.7,
                "background": true,
                "brightness": 1.0,
                "backColorLightness": 0,
                "backColorSaturation": 100,
                "backColorHue": 0,
                
                
                //"displacementScaleDepth":1.0,
                "displacementScaleDepth":970.0,
              
                // "sceneFlowDynamic":false,
                

                
            },

            "depthPostprocessing": {
                //"fxaaDisplacement": false,
                //"cutSidePixels": false,
                //"refine_cutSidePixels":false,
                //"fixEdgeNormals": false,

                
                // "barrelAlpha": 0.0,
                // "barrelScale": 1.0,
                
                // "cleanBuffer":false,
                


                // "sharpEdge": false,
                // "sharpEdgeWidth": 3.65,
                //"edgeThreshold" :0.05,
                "edge": false,
                "maxThresholdFactor" :4.0,
                "maxSteps" : 4, 


                "temporalSmoothing": true,
                "normalZ": 1.00,
                "normalsRatio":2.0,
                "normalBias":0.0,
                "normalBiasShading": 0.0,
                //"normalsRatioOld":1.69,
                "lookupRadius":1.90,
                
                //"cutSidePixels":false,
                "oldNormals":true,
                
                "edgeExtrude": true,
                "edgeExtrudeXtra": true,
                "extrudeDepth" :0.065,
                "blurEdgeAmount": 0.8,
                // "denoiseDepthNew": false,
                // "denoiseAmount": 2.0,
                // "sampleRadius": 1.0,    
                "fxaaDepth": false,
               // "fxaaDepth2": false,
                //"format":"1",
                //"encoding":"0",
            
                // "minFilter": "3",
                //"mipmaps": false,
                
                "anisotropy": 1.0,
                // "magFilter": "0",
                "blurDepth": true,
                
                // "holeFillingCircle": false,
                // "holeFillingHorizontal": false,
                // "hf_radius": 4,
                "holeFillingRay": false,
                "hf_circleRadius": 50,
                "hf_pixelsPerStep": 10,
                "hf_sampleCount": 10,

                "holeFillingConv": true,
                "hf_min_defined": 12,
                "hf_denominator": 5.0,
                "hf_offset": 0.0,

                // "holeFilling": false,
                // "holeFilling5x5": false,
                    
                "blurAmount": 1.0,
                "tiltBlurRadius": 2.0,
                "tiltBlurAmount": 0.0,



                
                

           
            },

            "depthFX": {
                "FXScale":  1.0,
                 "displacementFatness":0.0,
                "depthFXOnBackground":false,
                // "testNew": true,
                "rippleDepthAmplitude": 0.0,
                "rippleDepthSpeed": 1.6,
                "rippleDepthFrequency": 13.0,
                "quantizeDepth": false,
		        "quantizeDepthFrequency": 13.0,


                "liquid":false,  
                "tension":0.4,
                "sourceVelocityScale":3.0,
                "depthVelocityThreshold":50.0,
                "mixRatio":0.98,
                "blurDynamicAmount":0.5,
                "liquidSpeed":1.0,   
                "liquidDamp":0.024,   
                "liquidBlur":4.3,  
                "liquidTension":0.0575,  
                "liquidGravity":0.0,
                "maxGravity":0.0,

                "preprocessDepthWithEcho": false,
                "preprocess_IR_WithEcho": false,
                "preprocess_Color_WithEcho": false,
                "maxBufferSize": 34,
                "numSplits": 6,

                "dotAmplitude": 0.0,
                "dotSize": 0.1,
                "dotFrequency": 43.0,
                "dotExponent":2.0,

                "quantizeDotsNormal": false,
                "staggerDots": false,
                "useDotsForDepth": false, 
                "useNoiseToSmearDots": false,

		        "noiseVariations":  false,
                "noiseAmplitude":  0.0,
                "noiseFrequency": 10.0, 
                "noiseSpeed":  0.01,
                "noiseWormAmplitude":  0.0,
                "noiseCellAmplitude":  0.0,
                "noiseRodAmplitude":  0.0,
                "smearAmplitude":  0.0,
                "smearFrequency":  0.8, 
                "noiseMask":  0.8, 
                "maskThreshold":  0.0, 
                "maskFrequency":  2.0,
                "maskSpeed":  0.0,

                
                "preprocessDepthWithReactionDiffusion": false,
                "RD_numIterations": 2,
                "rd_stimulationStrength": 3.6,
                "rd_stimulationStrengthHandPointers": 0,
                "rd_zoom_offset": 0.004,
                //"rd_blobSize": 0.035,
               // "rd_blobBlurSize": 3,
                "rd_directionOn": true,
                "rd_a": 0.125,
                "rd_b": 0.805,
                "rd_delta": 0.02,
                "rd_eps": 0.02,
                "rd_dt": 0.0085,
               
               
            },

            "camera": {
                "camDistance": 1500,
                "camFOV": 30,
                "ortho": false, 
                "dome": false,
                "domeCamZ": 0,
                "camY": 0,
                "camRotate": 0,
                "camRotateX": 0,

                "animateCamera": false,
                "animateSpeed": 0.5,
                "animateZspeed": 0.5,
                "animateZamount": 0,
                "limitFPS":false,
                "showStats":false
            },

            "depth": {
                "depthLimit": 768,
                "halfRange": 432,
                "maxIR": 1500,
                "depthMode": "0",
               // "meshScale": "1",
                // "colorStart": 0,
                // "colorEnd:": 0

            }



        }

    ]
}
var paramsDefaultApp = JSON.parse(JSON.stringify(params));
