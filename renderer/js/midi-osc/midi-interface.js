
function setMidiSettingsPopupState(state) {

    var midiSettingsPopup = document.getElementById("midiSettings");

    if (state === "disabled") {
        midiSettingsPopup.style.pointerEvents = "none";
        midiSettingsPopup.style.opacity = "0.8";

    } else
        if (state === "enabled") {
            midiSettingsPopup.style.pointerEvents = "auto";
            midiSettingsPopup.style.opacity = "1.0";
        }
}


function initMidiInterface() {

    midiSettingsControlView = new MidiSettingsView();
    midiSettingsControlView.initMidiInPorts("#midiSettingsControlView");
    // midiSettingsControlView.initMidiOutPorts("#midiSettingsControlView");

    midiSettingsControlView.midiInDevicesList.onchange = function (args) {

        if (args.target.selectedIndex === 0) {
            if (midiInPort !== null) {
                
                midiInPort.onmessagereceived = null;
                midiInPort.close();
                //midiInPort.dispose();
                midiInPort = null;
                var newPortId = { owner: "control", key: 'midiInPortId', value: null };
                updateMidiSettings_PortID(newPortId);
            }
            return;
        }

        var portId = midiSettingsControlView.midiInPortList[args.target.selectedIndex].id;
        var inputPort = midiSettingsControlView.midiInPortList[args.target.selectedIndex];

        // store midi port Id for port owner
        var newPortId = { owner: "control", key: 'midiInPortId', value: portId };

        setMidiInPortByPortId(portId);

        //setMidiInPort(inputPort);

        updateMidiSettings_PortID(newPortId);

        var isMuted = midiInPortMuteCheckboxControl.checked;

        updateMidiSettings_MuteState( { owner: "control", key: "midiInPort_isMuted" }, isMuted );

    }

    // midiSettingsControlView.midiOutDevicesList.onchange = function (args) {

    //     if (args.target.selectedIndex === 0) {
    //         if (midiOutPort !== null) {
    //             midiOutPort.onmessagereceived = null;
    //             midiOutPort = null;

    //             var newPortId = { owner: 'control', key: 'midiOutPortId', value: null };
    //             updateMidiSettings_PortID(newPortId);
    //         }
    //         return;
    //     }

    //     var portId = midiSettingsControlView.midiOutPortList[args.target.selectedIndex].id;

    //     // store midi port Id for port owner
    //     var newPortId = { owner: 'control', key: 'midiOutPortId', value: portId };

    //     setMidiOutPortByPortId(portId);

    //     updateMidiSettings_PortID(newPortId);
    // }

    midiSettingsControlView.midiInChannelList.onchange = function (args) {

        if (args.target.selectedIndex === 0) {
            midiInChannel = 137;  /// alias for omni
        } else {
            midiInChannel = Number(args.target.selectedIndex) - 1;
        }

        var newChannel = { owner: 'control', key: 'midiInChannel', value: midiInChannel };

        updateMidiSettings_Channel(newChannel);

    }

    // midiSettingsControlView.midiOutChannelList.onchange = function (args) {

    //     midiOutChannel = Number(args.target.selectedIndex);

    //     var newChannel = { owner: 'control', key: 'midiOutChannel', value: midiOutChannel };

    //     updateMidiSettings_Channel(newChannel);

    // }
    


    midiSettingsRenderView = new MidiSettingsView();
    //midiSettingsRenderView.initMidiInPorts("#midiSettingsRenderView");
    midiSettingsRenderView.initMidiOutPorts("#midiSettingsRenderView");

    // midiSettingsRenderView.midiInDevicesList.onchange = function (args) {

    //     if (args.target.selectedIndex === 0) {
    //         if (midiInPort !== null) {
    //             midiInPort.onmessagereceived = null;
    //             midiInPort = null;
    //             var newPortId = { owner: "renderer", key: 'midiInPortId', value: null };
    //             updateMidiSettings_PortID(newPortId);
    //         }
    //         return;
    //     }

    //     var portId = midiSettingsRenderView.midiInPortList[args.target.selectedIndex].id;

    //     // store midi port Id for port owner
    //     var newPortId = { owner: "renderer", key: 'midiInPortId', value: portId };
        
    //     // init midi at render view
    //     renderview.postMessage({ type: "setMidiInPortByPortId", message: portId });

    //     updateMidiSettings_PortID(newPortId);

    // }

    midiSettingsRenderView.midiOutDevicesList.onchange = function (args) {

        if (args.target.selectedIndex === 0) {
            if (midiOutPort !== null) {
                midiOutPort.onmessagereceived = null;
                midiOutPort = null;

                var newPortId = { owner: 'renderer', key: 'midiOutPortId', value: null };
                updateMidiSettings_PortID(newPortId);
            }
            return;
        }

        var portId = midiSettingsRenderView.midiOutPortList[args.target.selectedIndex].id;

        // store midi port Id for port owner
        var newPortId = { owner: 'renderer', key: 'midiOutPortId', value: portId };

        // init midi at render view
        renderview.postMessage({ type: "setMidiOutPortByPortId", message: portId });

        updateMidiSettings_PortID(newPortId);
    }

    // midiSettingsRenderView.midiInChannelList.onchange = function (args) {

    //     var renderer_midiInChannel;

    //     if (args.target.selectedIndex === 0) {
    //         renderer_midiInChannel = 137;  /// omni
    //     } else {
    //         renderer_midiInChannel = Number(args.target.selectedIndex) - 1;
    //     }

    //     renderview.postMessage({ type: "setMidiInChannel", message: renderer_midiInChannel });

    //     var newChannel = { owner: 'renderer', key: 'midiInChannel', value: renderer_midiInChannel };

    //     updateMidiSettings_Channel(newChannel);

    // }

    midiSettingsRenderView.midiOutChannelList.onchange = function (args) {

        var renderer_midiOutChannel = Number(args.target.selectedIndex);

        renderview.postMessage({ type: "setMidiOutChannel", message: renderer_midiOutChannel });

        var newChannel = { owner: 'renderer', key: 'midiOutChannel', value: renderer_midiOutChannel };

        updateMidiSettings_Channel(newChannel);

    }
    

    getMidiAccess(  function() {

        requestAvailableMidiDevices_IN( function (collection) {

            midiSettingsControlView.buildInDeviceList(collection);

            // midiSettingsRenderView.buildInDeviceList(collection);

        });

        requestAvailableMidiDevices_OUT( function (collection) {

            // midiSettingsControlView.buildOutDeviceList(collection);

            midiSettingsRenderView.buildOutDeviceList(collection);

        });

    });


    
    
    var midiInPortMuteCheckboxControl = document.getElementById('midiInPortMuteCheckboxControl');
    midiInPortMuteCheckboxControl.onchange = function (args) {


        if (this.checked === true) {

           if (midiInPort) midiInPort.onmidimessage = null;

        } else {

            if (midiInPort) midiInPort.onmidimessage = MidiInPort_MessageReceived;

        }

        updateMidiSettings_MuteState( { owner: "control", key: "midiInPort_isMuted" }, this.checked );

    }

    // unmute control's midi in port by default
    midiInPortMuteCheckboxControl.checked = false;
    midiInPortMuteCheckboxControl.onchange();

}



function updateMidiSettings_PortID(newPortId) {

        portSettingsCache[newPortId.owner] = portSettingsCache[newPortId.owner] || {};

        if (newPortId.value === null) {
            delete portSettingsCache[newPortId.owner][newPortId.key];
        } else {
            portSettingsCache[newPortId.owner][newPortId.key] = newPortId.value;
        }

}



function updateMidiSettings_Channel(newChannel) {
    
    portSettingsCache[newChannel.owner] = portSettingsCache[newChannel.owner] || {};

    if (portSettingsCache[newChannel.owner]) {
        portSettingsCache[newChannel.owner][newChannel.key] = newChannel.value;
    }

}


function updateMidiSettings_MuteState(newPortId, isMuted) {
    
    portSettingsCache[newPortId.owner] = portSettingsCache[newPortId.owner] || {};

        //if (newPortId.value === null) {
        //    delete portSettingsCache[newPortId.owner][newPortId.key];
        //} else {
            portSettingsCache[newPortId.owner][newPortId.key] = isMuted;
        //}

}