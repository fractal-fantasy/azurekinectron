﻿/*
created by mika 03.10.2017
*/

// var enumeration = Windows.Devices.Enumeration;
// var midi = Windows.Devices.Midi;



var midi = null;

// var midiBro = new MIDIBro();

var midiMessageType = {

    noteOn: 144,
    noteOff: 128,
    controlChange: 176,
    programChange: 192

}

function getMidiChannel( decimal ) {

    var hex = decimal.toString(16);

    return parseInt( hex.charAt(1), 16 );

}

function getMidiMessageType( decimal ) {

    var hex = decimal.toString(16);

    return parseInt( hex.charAt(0) + "0", 16 );

}



// var MIDISettingsPopupIsActive = false;

// var midiInDeviceHost, midiOutDeviceHost;


// var midiSettingsControlView;
// var midiSettingsRenderView;

// var midiInDevicesList, midiOutDevicesList;

// var midiInChannelList, midiOutChannelList;

// var midiInLogOutput, midiOutLogOutput;

//var midiMessageType = midi.MidiMessageType;

var midiMessageType = {

    noteOn: 144,
    noteOff: 128,
    controlChange: 176,
    programChange: 192

}

var midiInPort = null, midiOutPort = null;

var midiInChannel = 137;   //   137 === omni

var midiOutChannel = 1;

var portSettingsCache = {};

var midiMap = [];
var midiMappingModeOn = false;
var mappableControlsCollection = [];

var mappingSelectedControl;
var prevMappingSelectedControl;

var midiMappingIncrementalIndex = 0;

var midiOutLogEnabledCheckbox;

// var midiBro_msgStack = [];



var midiInDevicesCollection;
var midiOutDevicesCollection;

function getMidiAccess(callback) {

    function onMIDISuccess( midiAccess ) {

        console.log( "MIDI ready!" );
        midi = midiAccess;  // store in the global (in real usage, would probably keep in an object instance)

        callback();

    }

    function onMIDIFailure(msg) {
        console.log( "Failed to get MIDI access - " + msg );
    }

    navigator.requestMIDIAccess().then( onMIDISuccess, onMIDIFailure );

}




function logMidiPortEvent(message) {

    var portInitLog = document.getElementById("portInitLog");    

    if (portInitLog === null) {
        parentview.postMessage( { type: "logMidiPortEvent", message: message } );
    } else {

        portInitLog.innerHTML = message;

    }   

}


// function onMidiInMessageHandler( message ) {

//     midiBro.processMidiInputMessage( message );

// }



function setMidiInPortByPortId(portId) {

    midiInPort = midi.inputs.get(portId);

    if (midiInPort == null) {
        console.error("unable to get midi in port");
        logMidiPortEvent("kinectWindow: unable to get midi in port: " + portId);
        return;
    }

    midiInPort.onmidimessage = onMidiInMessageHandler;

    console.log("midiRenderer.js: initMidiInPortFromId success", portId, midiInPort);
    logMidiPortEvent("kinectWindow: Midi In Port created: " + portId);

}

/*
function updateMidiInReceivedMessages() {

    midiBro_msgStack = midiBro.getMidiInMsgStack();

    var length = midiBro_msgStack.length;

        
    for (var i = 0; i < length; i++) {

        if (midiBro_msgStack[i] !== undefined && midiBro_msgStack[i] !== null)
            MidiInPort_MessageReceived(midiBro_msgStack[i]);

    }


    midiBro.resetMidiInMessageStack();

}
*/



function setMidiOutPortByPortId(portId) {

    midiOutPort = midi.outputs.get(portId);

    if (midiOutPort === null) {

        logMidiPortEvent("Unable to create Midi Out Port from device");
        console.error("Unable to create Midi Out Port from device");

    } else {

        console.log("Midi Out Port created from port id: ", portId, midiOutPort);
        logMidiPortEvent("kinectWindow: Midi Out Port created: " + portId);

    }
}


function getPortIndexByPortId(array, portId) {

    for (var i = 0; i < array.length; i++) {
        if (array[i].id === portId) return i;
    }
    return 0;
}



function buildMidiStringForLog(message) {

    var type = message.type;
    var channel = message.channel;
    var velocity = message.velocity;
    var note = message.note;

    var typeString = "";

    switch (type) {

        case midiMessageType.noteOn:
            typeString = "noteOn";
            break;

        case midiMessageType.noteOff:
            typeString = "noteOff";
            break;

        case midiMessageType.controlChange:
            typeString = "ControlChange";
            break;

        case midiMessageType.programChange:
            typeString = "ProgramChange";
            break;

        default:
            typeString = type;
    }

    var fullMessage = "type: " + typeString + " ";

    for (var key in message) {
        if (key !== "type" && key !== "rawData" && key !== "timestamp" && key !== "toString") {
            fullMessage += key + ": " + message[key] + " ";
        }
    }

    return fullMessage;

}

var midiMsgCounter = 0, midiMsgCounterMAX = 0;

var midiCC_perFrameCounter_lookupTable = new Uint8Array(127);

for (var i = 0; i < 127; i++) {
    midiCC_perFrameCounter_lookupTable[i] = 0;
}

function reset_midiCC_perFrameCounter_lookupTable() {

    //if (midiInPort && midiInPort.onmessagereceived == null) {
    //    midiInPort.onmessagereceived = MidiInPort_MessageReceived;
    //}

    for (var i = 0; i < 127; i++) {
        midiCC_perFrameCounter_lookupTable[i] = 0;
    }
}

function MidiInPort_MessageReceived( message ) {

    // var messageArgs = {

    //     note: message.data[1],
    //     velocity: message.data[2],
    //     controller: message.data[1],
    //     controlValue: message.data[2],
    //     program: message.data[1]

    // }

    var type = message.type;
    var channel = message.channel;


        //channel filter

        if (midiInChannel !== 137) {
            if (channel !== midiInChannel) return;
        }

        switch ( type ) {

            case midiMessageType.noteOn:
                handleNoteOnMessage( message.note, message.velocity );
                //setImmediate(function () { handleNoteOnMessage(messageArgs.note, messageArgs.velocity); });
                break;

            case midiMessageType.noteOff:
                handleNoteOffMessage( message.note, message.velocity );
                //setImmediate(function () { handleNoteOffMessage(messageArgs.note, messageArgs.velocity); });
                break;

            case midiMessageType.controlChange:

                if ( midiCC_perFrameCounter_lookupTable[ message.controller ] === 0 )
                    handleCCMessage( message.controller, message.controlValue );

                midiCC_perFrameCounter_lookupTable[ message.controller ] = 1;

                //setImmediate(function () { handleCCMessage(messageArgs.controller, messageArgs.controlValue); });
                break;

            case midiMessageType.programChange:
                handlePCMessage( message.program );
                //setImmediate(function () { handlePCMessage(messageArgs.program); });
                break;

            default:

        }

    //}
    

}

function getMidiMappingByController(id) {
    for (var i = 0; i < midiMap.length; i++) {
        if (midiMap[i].midiController === id) {
            return midiMap[i];
        }
    }
    return undefined;
}

function getMidiMappingByUIElementId(id) {
    for (var i = 0; i < midiMap.length; i++) {
        if (midiMap[i].UIcontrolObjectId === id) {
            return midiMap[i];
        }
    }
    return undefined;
}

function processMidiMappingOnMidiInput(controller, value, isNoteOff) {

    //if (midiMappingModeOn) {

        //var existingMidiMapping = getMidiMappingByController(controller);

        //if (existingMidiMapping !== undefined) {

        //    if (mappingSelectedControl !== null && mappingSelectedControl !== undefined) {

        //        resetMidiMappingOnControl(existingMidiMapping);
        //    }
        //}

        //if (mappingSelectedControl !== null && mappingSelectedControl !== undefined) {

        //    setMidiMappingOnControl(controller);
        //}

        //var midiMapping = getMidiMappingByController(controller);

        //if (midiMapping !== undefined) {
        //    midiMapping.triggerMidiEvent(value, isNoteOff);
        //}

    //} else {

        var midiMapping = getMidiMappingByController(controller);

        if (midiMapping !== undefined) {            
            midiMapping.triggerMidiEvent(value, isNoteOff);
        }
    //}
}

function handleNoteOnMessage(note, velocity) {
    
    processMidiMappingOnMidiInput(note, velocity);
}

function handleNoteOffMessage(note, velocity) {
    
    processMidiMappingOnMidiInput(note, velocity, true);
}

function handleCCMessage(controller, controlValue) {
    
    processMidiMappingOnMidiInput(controller, controlValue);
}

function handlePCMessage(program) {
    
    switchProgram(program + 1);

}


// messageTypes: "NoteOn", "NoteOff", "CC"
// usage example: sendMidiMessage("NoteOn", note, velocity);
// usage example: sendMidiMessage("CC", controller, controlValue);

function sendMidiMessage(messageType, val1, val2) {

    if (midiOutPort === null) return;

    var midiMessageToSend = null;

    switch (messageType) {

        case "NoteOff":
            // usage format: MidiNoteOffMessage(channel, note, velocity)
            // midiMessageToSend = midi.MidiNoteOffMessage(midiOutChannel, val1, val2);

            var NoteOff_type_channel = midiMessageType.noteOn + midiOutChannel;

            midiMessageToSend = [ NoteOff_type_channel, val1, val2 ];

            break;

        case "NoteOn":
            // usage format: MidiNoteOnMessage(channel, note, velocity)
            //midiMessageToSend = midi.MidiNoteOnMessage(midiOutChannel, val1, val2);

            var NoteOn_type_channel = midiMessageType.noteOn + midiOutChannel;

            midiMessageToSend = [ NoteOn_type_channel, val1, val2 ];

            break;            

        case "CC":
            // usage format: MidiControlChangeMessage(channel, controller, controlValue)

            // [0x80, 60, 0x40]

            // midiMessageToSend = midi.MidiControlChangeMessage(midiOutChannel, val1, val2);

            var CC_type_channel = midiMessageType.controlChange + midiOutChannel;
            
            midiMessageToSend = [ CC_type_channel, val1, val2 ];

            break;

    }
            

    if (midiMessageToSend !== null && midiOutPort !== null) {

        var midiLogString;
 

        try {
            midiOutPort.send(midiMessageToSend);

        }
        catch (e) {

            console.error(e.message);

            var errorMessage = "midi port error =( Try choosing another";

        }        

    }

}










// function processMidiBodyEvent(message) {

//     sendMidiMessage("CC", message.controller, message.value);

// }

function scale(valueIn, baseMin, baseMax, limitMin, limitMax) {

	if (valueIn < baseMin) return limitMin;
    else
		if (valueIn > baseMax) return limitMax;
        else
			return ((limitMax - limitMin) * (valueIn - baseMin) / (baseMax - baseMin)) + limitMin;
}

