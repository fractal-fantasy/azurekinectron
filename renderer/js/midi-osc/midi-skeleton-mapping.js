const KinectAzure = require("kinect-azure");

const btJointsEnum = {
  PELVIS: KinectAzure.K4ABT_JOINT_PELVIS,
  SPINE_NAVEL: KinectAzure.K4ABT_JOINT_SPINE_NAVEL,
  SPINE_CHEST: KinectAzure.K4ABT_JOINT_SPINE_CHEST,
  NECK: KinectAzure.K4ABT_JOINT_NECK,
  CLAVICLE_LEFT: KinectAzure.K4ABT_JOINT_CLAVICLE_LEFT,
  SHOULDER_LEFT: KinectAzure.K4ABT_JOINT_SHOULDER_LEFT,
  ELBOW_LEFT: KinectAzure.K4ABT_JOINT_ELBOW_LEFT,
  WRIST_LEFT: KinectAzure.K4ABT_JOINT_WRIST_LEFT,
  HAND_LEFT: KinectAzure.K4ABT_JOINT_HAND_LEFT,
  HANDTIP_LEFT: KinectAzure.K4ABT_JOINT_HANDTIP_LEFT,
  THUMB_LEFT: KinectAzure.K4ABT_JOINT_THUMB_LEFT,
  CLAVICLE_RIGHT: KinectAzure.K4ABT_JOINT_CLAVICLE_RIGHT,
  SHOULDER_RIGHT: KinectAzure.K4ABT_JOINT_SHOULDER_RIGHT,
  ELBOW_RIGHT: KinectAzure.K4ABT_JOINT_ELBOW_RIGHT,
  WRIST_RIGHT: KinectAzure.K4ABT_JOINT_WRIST_RIGHT,
  HAND_RIGHT: KinectAzure.K4ABT_JOINT_HAND_RIGHT,
  HANDTIP_RIGHT: KinectAzure.K4ABT_JOINT_HANDTIP_RIGHT,
  THUMB_RIGHT: KinectAzure.K4ABT_JOINT_THUMB_RIGHT,
  HIP_LEFT: KinectAzure.K4ABT_JOINT_HIP_LEFT,
  KNEE_LEFT: KinectAzure.K4ABT_JOINT_KNEE_LEFT,
  ANKLE_LEFT: KinectAzure.K4ABT_JOINT_ANKLE_LEFT,
  FOOT_LEFT: KinectAzure.K4ABT_JOINT_FOOT_LEFT,
  HIP_RIGHT: KinectAzure.K4ABT_JOINT_HIP_RIGHT,
  KNEE_RIGHT: KinectAzure.K4ABT_JOINT_KNEE_RIGHT,
  ANKLE_RIGHT: KinectAzure.K4ABT_JOINT_ANKLE_RIGHT,
  FOOT_RIGHT: KinectAzure.K4ABT_JOINT_FOOT_RIGHT,
  HEAD: KinectAzure.K4ABT_JOINT_HEAD,
  NOSE: KinectAzure.K4ABT_JOINT_NOSE,
  EYE_LEFT: KinectAzure.K4ABT_JOINT_EYE_LEFT,
  EAR_LEFT: KinectAzure.K4ABT_JOINT_EAR_LEFT,
  EYE_RIGHT: KinectAzure.K4ABT_JOINT_EYE_RIGHT,
  EAR_RIGHT: KinectAzure.K4ABT_JOINT_EAR_RIGHT
}

var ui_debugHandLeftX, ui_debugHandRightX, ui_debugHandLeftY, ui_debugHandRightY, ui_debugHandLeftZ, ui_debugHandRightZ;
var sendMidiHandLeftCheckbox, sendMidiHandRightCheckbox;

var selector_HandLeft_Controller_X, selector_HandRight_Controller_X,
    selector_HandLeft_Controller_Y, selector_HandRight_Controller_Y,
    selector_HandLeft_Controller_Z, selector_HandRight_Controller_Z;

var allowSendMIDI = {
    left: false,
    right: false
}

var allowSendOSC = {
    left: false,
    right: false
};

var handController = {
    left: { x: 0, y: 0, z: 0 },
    right: { x: 0, y: 0, z: 0 }
}

var soloModeHandController = {
    left: { x: false, y: false, z: false },
    right: { x: false, y: false, z: false }
}

var btJointType = {
    left: btJointsEnum.HAND_LEFT,
    right: btJointsEnum.HAND_RIGHT
}

var ui_btJointListLeft, ui_btJointListRight, ui_btSpaceType, ui_btTemporalSmoothingInput, ui_btTemporalSmoothingValue, ui_btProcessingMode, ui_btModelPath;

var ui_btJointConstraintBorderX, ui_btJointConstraintBorderYtop, ui_btJointConstraintBorderYbottom;

var jointConstraintBorder = { x: 40, Ytop: 70, Ybottom: 70 };

// body tracking defaults
var skeletonBodyTrackingMap = {
    hands: {
        allowSendMIDI: allowSendMIDI,
        allowSendOSC: allowSendOSC,
        handController: handController,
        soloModeHandController: soloModeHandController,
        btJointType: btJointType
    },
    coordinateSpaceType: 0, // 0 3d, 1 2d 
    temporalSmoothing: 0.5,
    jointConstraintBorder: jointConstraintBorder,
    processingMode: 0, // 0 cuda, 1 DirectML, 2 TensorRT
    modelPath: 0 // 0 normal, 1 lite

}

// ======================================== //

function toggleHandMarkersVisibility() {

    renderview.postMessage({ type: "action", message: "toggleHandMarkersVisibility" });

}

function toggleMIDISettingsPopup() {

    renderview.postMessage({ type: "action", message: "toggleMIDISettingsPopup" });

}

function restoreSkeletonBodyTrackingMapFromFileSettings( newBTSettings ) {
     
    if (newBTSettings === undefined) return;
    if (selector_HandLeft_Controller_X === undefined) return;

    var allowSendMidiHandCached = newBTSettings.hands.allowSendMIDI;
    if (allowSendMIDI.left !== allowSendMidiHandCached.left) {
        sendMidiHandLeftCheckbox.click();
    }
    if (allowSendMIDI.right !== allowSendMidiHandCached.right) {
        sendMidiHandRightCheckbox.click();
    }

    var allowSendOSCHandCached = newBTSettings.hands.allowSendOSC;
    if (allowSendOSC.left !== allowSendOSCHandCached.left) {
        sendOSCHandLeftCheckbox.click();
    }
    if (allowSendOSC.right !== allowSendOSCHandCached.right) {
        sendOSCHandRightCheckbox.click();
    }

    var handControllerCached = newBTSettings.hands.handController;
    
    selector_HandLeft_Controller_X.value = handControllerCached.left.x;
    selector_HandLeft_Controller_X.dispatchEvent(new Event('change'));

    selector_HandLeft_Controller_Y.value = handControllerCached.left.y;
    selector_HandLeft_Controller_Y.dispatchEvent(new Event('change'));

    selector_HandLeft_Controller_Z.value = handControllerCached.left.z;
    selector_HandLeft_Controller_Z.dispatchEvent(new Event('change'));

    selector_HandRight_Controller_X.value = handControllerCached.right.x;
    selector_HandRight_Controller_X.dispatchEvent(new Event('change'));

    selector_HandRight_Controller_Y.value = handControllerCached.right.y;
    selector_HandRight_Controller_Y.dispatchEvent(new Event('change'));

    selector_HandRight_Controller_Z.value = handControllerCached.right.z;
    selector_HandRight_Controller_Z.dispatchEvent(new Event('change'));

    var soloModeHandControllerCached = newBTSettings.hands.soloModeHandController;

    if (soloModeHandController.left.x !== soloModeHandControllerCached.left.x) {
        selector_HandLeft_Controller_X.soloControl.click();
    }
    if (soloModeHandController.left.y !== soloModeHandControllerCached.left.y) {
        selector_HandLeft_Controller_Y.soloControl.click();
    }
    if (soloModeHandController.left.z !== soloModeHandControllerCached.left.z) {
        selector_HandLeft_Controller_Z.soloControl.click();
    }
    if (soloModeHandController.right.x !== soloModeHandControllerCached.right.x) {
        selector_HandRight_Controller_X.soloControl.click();
    }
    if (soloModeHandController.right.y !== soloModeHandControllerCached.right.y) {
        selector_HandRight_Controller_Y.soloControl.click();
    }
    if (soloModeHandController.right.z !== soloModeHandControllerCached.right.z) {
        selector_HandRight_Controller_Z.soloControl.click();
    }

    if (btJointType.left !== newBTSettings.hands.btJointType.left) {
        ui_btJointListLeft.selectedIndex = newBTSettings.hands.btJointType.left;
        ui_btJointListLeft.onchange();
    }
    if (btJointType.right !== newBTSettings.hands.btJointType.right) {
        ui_btJointListRight.selectedIndex = newBTSettings.hands.btJointType.right;
        ui_btJointListRight.onchange();
    }

    if (skeletonBodyTrackingMap.coordinateSpaceType !== newBTSettings.coordinateSpaceType) {
        ui_btSpaceType.selectedIndex = newBTSettings.coordinateSpaceType;
        ui_btSpaceType.onchange();
    }

    if (skeletonBodyTrackingMap.temporalSmoothing !== newBTSettings.temporalSmoothing) {
        ui_btTemporalSmoothingInput.value = newBTSettings.temporalSmoothing;
        ui_btTemporalSmoothingInput.oninput();
    }

    ui_btJointConstraintBorderX.value = newBTSettings.jointConstraintBorder.x;
    ui_btJointConstraintBorderYtop.value = newBTSettings.jointConstraintBorder.Ytop;
    ui_btJointConstraintBorderYbottom.value = newBTSettings.jointConstraintBorder.Ybottom;
    ui_btJointConstraintBorderX.oninput();
    ui_btJointConstraintBorderYtop.oninput();
    ui_btJointConstraintBorderYbottom.oninput();
    
    if (skeletonBodyTrackingMap.processingMode !== newBTSettings.processingMode) {
        ui_btProcessingMode.selectedIndex = newBTSettings.processingMode;
        ui_btProcessingMode.onchange();
    }

    if (skeletonBodyTrackingMap.modelPath !== newBTSettings.modelPath) {
        ui_btModelPath.selectedIndex = newBTSettings.modelPath;
        ui_btModelPath.onchange();
    }

}

function sendSoloModeHandControllerToRenderView(soloHandController) {

    renderview.postMessage({ type: "setSoloModeHandController", message: soloHandController });

}

function sendHandControllerToRenderView(handController) {

    renderview.postMessage({ type: "setHandController", message: handController });

}

function sendSendMidiHandCheckboxToRenderView(object) {

    renderview.postMessage({ type: "setAllowSendMidiHand", message: object });

}

function updateUIMidiHandPositionsLeft(position) {
    position = JSON.parse(position);
    ui_debugHandLeftX.innerHTML = position.x;
    ui_debugHandLeftY.innerHTML = position.y;
    ui_debugHandLeftZ.innerHTML = position.z;
}

function updateUIMidiHandPositionsRight(position) {
    position = JSON.parse(position);    
    ui_debugHandRightX.innerHTML = position.x;    
    ui_debugHandRightY.innerHTML = position.y;    
    ui_debugHandRightZ.innerHTML = position.z;
}

function initSkeletonMidiMappingInterface() {

    function makeControllerSelector(controllerElement) {

        for (var i = 0; i <= 127; i++) {
            var option = document.createElement("option");
            option.value = i;
            option.innerHTML = i;
            controllerElement.appendChild(option);
        }

    }


    function makeSoloControl(controllerElement, hand, coord) {

        var controlContainer = controllerElement.parentElement;
        var xLabel = controlContainer.querySelector(".handDebugInfoLabel");
        xLabel.onclick = function () {
            soloModeHandController[hand][coord] = !soloModeHandController[hand][coord];
            soloModeHandController[hand][coord] ? xLabel.style.backgroundColor = "yellow" : xLabel.style.backgroundColor = "white";

            //send command to render view to updateSoloModeState
            sendSoloModeHandControllerToRenderView(soloModeHandController);
        }

        controllerElement.soloControl = xLabel;
    }  
    

    ui_debugHandLeftX = id("debugHandLeftX");
    ui_debugHandRightX = id("debugHandRightX");
    ui_debugHandLeftY = id("debugHandLeftY");

    ui_debugHandRightY = id("debugHandRightY");
    ui_debugHandLeftZ = id("debugHandLeftZ");
    ui_debugHandRightZ = id("debugHandRightZ");

    // left hand controllers

    selector_HandLeft_Controller_X = id("selector_HandLeft_Controller_X");
    makeControllerSelector(selector_HandLeft_Controller_X);
    //selector_HandLeft_Controller_X.value = "1";
    selector_HandLeft_Controller_X.onchange = function (args) {
        handController.left.x = args.target.selectedIndex;
        sendHandControllerToRenderView(handController);
    }
    makeSoloControl(selector_HandLeft_Controller_X, "left", "x");


    selector_HandLeft_Controller_Y = id("selector_HandLeft_Controller_Y");
    makeControllerSelector(selector_HandLeft_Controller_Y);
    //selector_HandLeft_Controller_Y.value = "2";
    selector_HandLeft_Controller_Y.onchange = function (args) {
        handController.left.y = args.target.selectedIndex;
        sendHandControllerToRenderView(handController);
    }
    makeSoloControl(selector_HandLeft_Controller_Y, "left", "y");


    selector_HandLeft_Controller_Z = id("selector_HandLeft_Controller_Z");
    makeControllerSelector(selector_HandLeft_Controller_Z);
    //selector_HandLeft_Controller_Z.value = "16";
    selector_HandLeft_Controller_Z.onchange = function (args) {
        handController.left.z = args.target.selectedIndex;
        sendHandControllerToRenderView(handController);
    }
    makeSoloControl(selector_HandLeft_Controller_Z, "left", "z");

    // right hand controllers

    selector_HandRight_Controller_X = id("selector_HandRight_Controller_X");
    makeControllerSelector(selector_HandRight_Controller_X);
    //selector_HandRight_Controller_X.value = "4";
    selector_HandRight_Controller_X.onchange = function (args) {
        handController.right.x = args.target.selectedIndex;
        sendHandControllerToRenderView(handController);
    }
    makeSoloControl(selector_HandRight_Controller_X, "right", "x");

    selector_HandRight_Controller_Y = id("selector_HandRight_Controller_Y");
    makeControllerSelector(selector_HandRight_Controller_Y);
    //selector_HandRight_Controller_Y.value = "5";
    selector_HandRight_Controller_Y.onchange = function (args) {
        handController.right.y = args.target.selectedIndex;
        sendHandControllerToRenderView(handController);
    }
    makeSoloControl(selector_HandRight_Controller_Y, "right", "y");

    selector_HandRight_Controller_Z = id("selector_HandRight_Controller_Z");
    makeControllerSelector(selector_HandRight_Controller_Z);
    //selector_HandRight_Controller_Z.value = "17";
    selector_HandRight_Controller_Z.onchange = function (args) {
        handController.right.z = args.target.selectedIndex;
        sendHandControllerToRenderView(handController);
    }
    makeSoloControl(selector_HandRight_Controller_Z, "right", "z");

    //

    sendMidiHandLeftCheckbox = id("sendMidiHandLeftCheckbox");
    sendMidiHandRightCheckbox = id("sendMidiHandRightCheckbox");

    sendMidiHandLeftCheckbox.onchange = function (args) {
        allowSendMIDI.left = args.target.checked;
        sendSendMidiHandCheckboxToRenderView(allowSendMIDI);
    }

    sendMidiHandRightCheckbox.onclick = function (args) {
        allowSendMIDI.right = args.target.checked;
        sendSendMidiHandCheckboxToRenderView(allowSendMIDI);
    }

    // sendMidiHandLeftCheckbox.click();
    // sendMidiHandRightCheckbox.click();


    // send OSC checkboxes
    sendOSCHandLeftCheckbox = id("sendOSCHandLeftCheckbox");
    sendOSCHandRightCheckbox = id("sendOSCHandRightCheckbox");

    sendOSCHandLeftCheckbox.onchange = function (args) {        
        allowSendOSC.left = args.target.checked;
        renderview.postMessage({ type: "allowSendOSC", message: allowSendOSC });
    }
    
    sendOSCHandRightCheckbox.onchange = function (args) {        
        allowSendOSC.right = args.target.checked;
        renderview.postMessage({ type: "allowSendOSC", message: allowSendOSC });
    }

    sendOSCHandLeftCheckbox.click();
    sendOSCHandRightCheckbox.click();

    ui_btSpaceType = id("btSpaceType");
    ui_btSpaceType.selectedIndex = skeletonBodyTrackingMap.coordinateSpaceType;
    ui_btSpaceType.onchange = function() {
        skeletonBodyTrackingMap.coordinateSpaceType = this.selectedIndex;
        renderview.postMessage({ type: "setBTSpaceType", message: this.selectedIndex });
    }
    ui_btSpaceType.onchange();

    ui_btTemporalSmoothingInput = id("btTemporalSmoothingInput");
    ui_btTemporalSmoothingValue = id("btTemporalSmoothingValue");
    ui_btTemporalSmoothingInput.oninput = function(e) {

        skeletonBodyTrackingMap.temporalSmoothing = this.value;
        var message = { type: "setTemporalSmoothing", message: this.value };
        ui_btTemporalSmoothingValue.innerHTML = this.value;
        renderview.postMessage(message);

    }

    ui_btTemporalSmoothingInput.oninput();

    ui_btProcessingMode = id("btProcessingMode");
    ui_btProcessingMode.selectedIndex = skeletonBodyTrackingMap.processingMode;
    ui_btProcessingMode.onchange = function() {
        skeletonBodyTrackingMap.processingMode = this.selectedIndex;
        renderview.postMessage({ type: "setBTProcessingMode", message: this.selectedIndex });
    }

    ui_btModelPath = id("btModelPath");
    ui_btModelPath.selectedIndex = skeletonBodyTrackingMap.modelPath;
    ui_btModelPath.onchange = function() {
        skeletonBodyTrackingMap.modelPath = this.selectedIndex;
        renderview.postMessage({ type: "setBTModelPath", message: this.selectedIndex });
    }

    // BODY TRACKING JOINTS SETTINGS

    ui_btJointListLeft = id("btJointListLeft");
    ui_btJointListRight = id("btJointListRight");

    for (var jointType in btJointsEnum) {

        var option = document.createElement("option");
        option.value = btJointsEnum[jointType];
        option.innerHTML = jointType;           

        ui_btJointListLeft.appendChild(option);
    }
    for (var jointType in btJointsEnum) {

        var option = document.createElement("option");
        option.value = btJointsEnum[jointType];
        option.innerHTML = jointType;           

        ui_btJointListRight.appendChild(option);
    }

    ui_btJointListLeft.selectedIndex = btJointType.left;
    ui_btJointListRight.selectedIndex = btJointType.right;

    ui_btJointListLeft.onchange = function() {
        skeletonBodyTrackingMap.hands.btJointType.left = this.selectedIndex;
        renderview.postMessage({ type: "setBTJointControlLeft", message: this.selectedIndex });
    }

    ui_btJointListRight.onchange = function() {
        skeletonBodyTrackingMap.hands.btJointType.right = this.selectedIndex;
        renderview.postMessage({ type: "setBTJointControlRight", message: this.selectedIndex });
    }

    ui_btJointListLeft.onchange();
    ui_btJointListRight.onchange();

    
    ui_btJointConstraintBorderX = id("btJointConstraintBorderX");
    ui_btJointConstraintBorderYtop = id("btJointConstraintBorderYtop");
    ui_btJointConstraintBorderYbottom = id("btJointConstraintBorderYbottom");

    ui_btJointConstraintBorderX.oninput = function(e) {
        jointConstraintBorder.x = this.value;
        skeletonBodyTrackingMap.jointConstraintBorder.x = this.value;
        var message = { type: "setJointConstraintBorder", message: jointConstraintBorder };
        id("btJointConstraintBorderLabelX").innerHTML = this.value;
        renderview.postMessage(message);
    }

    ui_btJointConstraintBorderYtop.oninput = function(e) {
        jointConstraintBorder.Ytop = this.value;
        skeletonBodyTrackingMap.jointConstraintBorder.Ytop = this.value;
        var message = { type: "setJointConstraintBorder", message: jointConstraintBorder };
        id("btJointConstraintBorderLabelYtop").innerHTML = this.value;
        renderview.postMessage(message);
    }

    ui_btJointConstraintBorderYbottom.oninput = function(e) {
        jointConstraintBorder.Ybottom = this.value;
        skeletonBodyTrackingMap.jointConstraintBorder.Ybottom = this.value;
        var message = { type: "setJointConstraintBorder", message: jointConstraintBorder };
        id("btJointConstraintBorderLabelYbottom").innerHTML = this.value;
        renderview.postMessage(message);
    }

    ui_btJointConstraintBorderX.oninput();
    ui_btJointConstraintBorderYtop.oninput();
    ui_btJointConstraintBorderYbottom.oninput();

}