﻿/**
 * @author mikkamikka / https://mikkamikka.com
 */

var midi = null;

function buildDeviceList(deviceInformationCollection, portList, midiDevicesList, midiChannelList) {

    for (var i = midiDevicesList.options.length - 1 ; i >= 0 ; i--) {
        midiDevicesList.remove(i);
    }

    // If no devices are found, update the ListBox
    if ((deviceInformationCollection == null) || (deviceInformationCollection.count == 0)) {
        // Start with a clean list
        portList = [];
        portList.push("No MIDI ports found");
    }
        // If devices are found, enumerate them and add them to the list
    else {
        // Start with a clean list
        portList = [];

        var option = document.createElement("option");
        option.value = 0;
        option.innerHTML = "None";
        midiDevicesList.appendChild(option);
        portList.push({ name: "dummy" });

        var id = 1;

        for (var i = 0; i < deviceInformationCollection.length; i++) {
            var device = deviceInformationCollection[i];

            portList.push(device);

            var option = document.createElement("option");
            option.value = i;
            option.innerHTML = device.name;           

            id++;

            midiDevicesList.appendChild(option);
        }

    }

    return portList;

}



var MIDISettingsPopupIsActive = false;

var midiInDeviceHost, midiOutDeviceHost;


var midiSettingsControlView;
var midiSettingsRenderView;

var midiInDevicesList, midiOutDevicesList;

var midiInChannelList, midiOutChannelList;

var midiInLogOutput, midiOutLogOutput;

//var midiMessageType = midi.MidiMessageType;

var midiMessageType = {

    noteOn: 144,
    noteOff: 128,
    controlChange: 176,
    programChange: 192

}


var midiInPort = null, midiOutPort = null;

var midiInChannel = 137;   //   137 === omni

var midiOutChannel = 1;

var portSettingsCache = {};

var midiMap = [];
var midiMappingModeOn = false;
var mappableControlsCollection = [];

var mappingSelectedControl;
var prevMappingSelectedControl;

var midiMappingIncrementalIndex = 0;

var midiOutLogEnabledCheckbox;

var midiBro_msgStack = [];

var triggerMidiEvent = function (value, isNoteOff) {

    //if (this.UIcontrolObject === null) this.UIcontrolObject = document.getElementById(this.UIcontrolMappingId);

    //var UIcontrolObject = document.getElementById(this.UIcontrolMappingId);

    var UIcontrolObject = mappableControlsCollection[ this.UIcontrolMappingId ];
    
    if (UIcontrolObject === null || UIcontrolObject === undefined) return;

    switch (UIcontrolObject.type) {

        case "checkbox":

            var isActive = false;

            if (isNoteOff === true) {
                isActive = false;
            } else {
                isActive = true;
            }

            if (isActive) {
                UIcontrolObject.checked = true;
            } else {
                UIcontrolObject.checked = false;
            }

            UIcontrolObject.onchange();

            //if (isNoteOff === undefined) {
            //    UIcontrolObject.disabled = false;
            //    UIcontrolObject.click();
            //}
            break;

        case "button":

            // in midiMappingModeOn disabled for buttons 'coz too laggy
            if (!midiMappingModeOn) {

                if (midiMapEventsVisible === true) {

                    if (UIcontrolObject.onmousedown !== null) {

                        if (isNoteOff === true) {
                            if (UIcontrolObject.onmouseup !== null) UIcontrolObject.onmouseup();
                        } else {
                            UIcontrolObject.onmousedown();
                        }
                    }
                    else {
                        UIcontrolObject.click();
                    }

                } else {

                    if (this.UIcontrolObject.onmousedownSilent !== null) {

                        if (isNoteOff === true) {
                            if (UIcontrolObject.onmouseupSilent !== null) UIcontrolObject.onmouseupSilent();
                        } else {
                            UIcontrolObject.onmousedownSilent();
                        }
                    }
                    else {
                        if (UIcontrolObject.clickSilent !== null) UIcontrolObject.clickSilent();
                    }
                }

                //if (midiMappingModeOn) UIcontrolObject.disabled = true;
            }

            break;

        case "range":

            //var midiRangeAdaptedValue = Math.floor(scale(value, 0, 127, UIcontrolObject.min, UIcontrolObject.max));

            var midiRangeAdaptedValue = scale(Number(value), 0, 127, Number(UIcontrolObject.min), Number(UIcontrolObject.max));                       

            if (midiMapEventsVisible === true) {

                UIcontrolObject.value = midiRangeAdaptedValue;

                if (UIcontrolObject.oninput !== null) {
                    UIcontrolObject.oninput();
                } else
                    if (UIcontrolObject.onchange !== null) {
                        UIcontrolObject.onchange();
                    }

            } else {

                if (UIcontrolObject.oninputSilent !== null) {
                    UIcontrolObject.oninputSilent( midiRangeAdaptedValue );
                } else
                    if (UIcontrolObject.onchangeSilent !== null) {
                        UIcontrolObject.onchangeSilent( midiRangeAdaptedValue );
                    }

            }

            break;

            case "number":    // number box
            
                var midiRangeAdaptedValue = scale(Number(value), 0, 127, Number(UIcontrolObject.min), Number(UIcontrolObject.max));  

                if (midiMapEventsVisible === true) {

                    UIcontrolObject.value = midiRangeAdaptedValue;

                    if (UIcontrolObject.oninput !== null) {
                        UIcontrolObject.oninput();
                    } else
                        if (UIcontrolObject.onchange !== null) {
                            UIcontrolObject.onchange();
                        }

                } else {

                    if (UIcontrolObject.oninputSilent !== null) {
                        UIcontrolObject.oninputSilent( midiRangeAdaptedValue );
                    } else
                        if (UIcontrolObject.onchangeSilent !== null) {
                            UIcontrolObject.onchangeSilent( midiRangeAdaptedValue );
                        }

                }

            break;

        // case "text":    // color picker
            
        //     //var midiRangeAdaptedValue = Math.floor(scale(value, 0, 127, UIcontrolObject.min, UIcontrolObject.max));

        //     var midiRangeAdaptedValue_Hue = scale(Number(value), 0, 127, 0, 360); 

        //     console.log( UIcontrolObject.picker.hsv );

        //     var hsv = UIcontrolObject.picker.hsv;

        //     // h: 0-360
        //     // s: 0-100
        //     // v: 0-100

        //     UIcontrolObject.picker.fromHSV( midiRangeAdaptedValue_Hue, hsv[1], hsv[2] );
            
        //     //var midiRangeAdaptedValue = value;                      

        //     if (midiMapEventsVisible === true) {

        //         //UIcontrolObject.value = midiRangeAdaptedValue;

        //         if (UIcontrolObject.oninput !== null) {
        //             UIcontrolObject.oninput();
        //         } else
        //             if (UIcontrolObject.onchange !== null) {
        //                 UIcontrolObject.onchange();
        //             }

        //     } else {

        //         if (UIcontrolObject.oninputSilent !== null) {
        //             UIcontrolObject.oninputSilent( midiRangeAdaptedValue );
        //         } else
        //             if (UIcontrolObject.onchangeSilent !== null) {
        //                 UIcontrolObject.onchangeSilent( midiRangeAdaptedValue );
        //             }

        //     }

        //     break;
    }

}

var setController = function (controller) {

    this.midiController = controller;
}

var MidiMapInstance = function (midimapLever, mappingId, midiController, elementId) {

    //this.UIcontrolObject = UIcontrolObject;

    this.midimapLever = midimapLever;

    this.UIcontrolMappingId = mappingId;  // index in midiMap array

    this.elementId = elementId;

    this.midiController = midiController;

    this.setController = setController;

    this.triggerMidiEvent = triggerMidiEvent;


}


var midiInDevicesCollection;
var midiOutDevicesCollection;

function getMidiAccess(callback) {

    function onMIDISuccess( midiAccess ) {

        console.log( "MIDI ready!" );
        midi = midiAccess;  // store in the global (in real usage, would probably keep in an object instance)

        callback();

    }

    function onMIDIFailure(msg) {
        console.log( "Failed to get MIDI access - " + msg );
    }

    navigator.requestMIDIAccess().then( onMIDISuccess, onMIDIFailure );

}

function requestAvailableMidiDevices_IN(callback) {

    //midiBro.getMidiInDeviceListAsync();
    //.then(function (collection) {

    //    callback(collection);

    //});

    var collection = [];

    for (var entry of midi.inputs) {
        var input = entry[1];
        console.log( "Input port [type:'" + input.type + "'] id:'" + input.id +
        "' manufacturer:'" + input.manufacturer + "' name:'" + input.name +
        "' version:'" + input.version + "'" );

        collection.push( input );
    }

    callback( collection );

}

function requestAvailableMidiDevices_OUT(callback) {

    var collection = [];

    for (var entry of midi.outputs) {
        var output = entry[1];
        console.log( "Output port [type:'" + output.type + "'] id:'" + output.id +
        "' manufacturer:'" + output.manufacturer + "' name:'" + output.name +
        "' version:'" + output.version + "'" );

        collection.push( output );
    }

    

    callback( collection );

}

var MidiSettingsView = function () {

}

MidiSettingsView.prototype.initMidiInPorts = function (parentDivSelector) {
    
    this.midiInPortList = [];

    this.midiInDevicesList = document.querySelector(parentDivSelector + " #midiInDevicesList");

    this.midiInChannelList = document.querySelector(parentDivSelector + " #midiInChannelList");

    this.initMidiInChannelListUI();

}

MidiSettingsView.prototype.initMidiOutPorts = function (parentDivSelector) {

    this.midiOutPortList = [];

    this.midiOutDevicesList = document.querySelector(parentDivSelector + " #midiOutDevicesList");

    this.midiOutChannelList = document.querySelector(parentDivSelector + " #midiOutChannelList");

    this.initMidiOutChannelListUI();

}

MidiSettingsView.prototype.buildInDeviceList = function (collection) {

    this.midiInPortList = buildDeviceList(collection, this.midiInPortList, this.midiInDevicesList, this.midiInChannelList);

}

MidiSettingsView.prototype.buildOutDeviceList = function (collection) {

    this.midiOutPortList = buildDeviceList(collection, this.midiOutPortList, this.midiOutDevicesList, this.midiOutChannelList);

}


MidiSettingsView.prototype.initMidiInChannelListUI = function () {

    var option = document.createElement("option");
    option.value = 0;
    option.innerHTML = "OMNI";
    this.midiInChannelList.appendChild(option);

    for (var i = 1; i <= 6; i++) {

        var option = document.createElement("option");
        option.value = i;
        option.innerHTML = i;
        this.midiInChannelList.appendChild(option);
    }
}

MidiSettingsView.prototype.initMidiOutChannelListUI = function() {

    for (var i = 1; i <= 6; i++) {

        var option = document.createElement("option");
        option.value = i;
        option.innerHTML = i;
        this.midiOutChannelList.appendChild(option);

    }
    //midiOutChannelList.value = "2";
}


function logMidiPortEvent(message) {

    var portInitLog = document.getElementById("portInitLog");    

    if (portInitLog === null) {
        parentview.postMessage( { type: "logMidiPortEvent", message: message } );
    } else {

        portInitLog.innerHTML = message;

    }   

}


function setMidiInPortByPortId(portId) {

   //var result = midiBro.initMidiInPortFromId(portId).then(function () {

        //midiInPort = midiBro.getMidiInPort();
        midiInPort = midi.inputs.get(portId);

        if (midiInPort == null) {
            console.log("unable to get midi in port");
            logMidiPortEvent("controlWindow: unable to get midi in port: " + portId);
            return;
        }

        if (document.getElementById("midiInPortMuteCheckboxControl").checked === false) {
         
            midiInPort.onmidimessage = MidiInPort_MessageReceived;

        }

        console.log("midi.js: initMidiInPortFromId success", portId, midiInPort);
        logMidiPortEvent("controlWindow: Midi In Port created from port id: " + portId);

    //}); 
}


function setMidiOutPortByPortId(portId) {

    //midi.MidiOutPort.fromIdAsync(portId).then(
        //function (port) {

        midiOutPort = midi.outputs.get(portId);

            if (midiOutPort === null) {

                logMidiPortEvent("Unable to create Midi Out Port from device");

            } else {

                console.log("Midi Out Port created from port id: ", portId, midiOutPort);
                logMidiPortEvent("controlWindow: Midi Out Port created: " + portId);

            }

        //}
    //);
}


function getPortIndexByPortId(array, portId) {

    for (var i = 0; i < array.length; i++) {
        if (array[i].id === portId) return i;
    }
    return 0;
}

//function getPortNameByPortId(array, portId) {

//    for (var i = 0; i < array.length; i++) {
//        if (array[i].id === portId) return array[i].name;
//    }
//    return "";
//}


// read port settings from file and init if available
function initMidiPortsFromFileSettings() {

        if (portSettingsCache.control !== undefined) {
            if (portSettingsCache.control.midiInPortId !== undefined) {

                var portId = portSettingsCache.control.midiInPortId;

                setMidiInPortByPortId(portId);

                midiSettingsControlView.midiInDevicesList.selectedIndex = getPortIndexByPortId(midiSettingsControlView.midiInPortList, portId);

            }
            // if (portSettingsCache.control.midiOutPortId !== undefined) {

            //     var portId = portSettingsCache.control.midiOutPortId;

            //     setMidiOutPortByPortId(portId);

            //     midiSettingsControlView.midiOutDevicesList.selectedIndex = getPortIndexByPortId(midiSettingsControlView.midiOutPortList, portId);

            // }

            if (portSettingsCache.control.midiInChannel !== undefined) {

                midiInChannel = portSettingsCache.control.midiInChannel;

                var optionIndex = 0;
                if (midiInChannel === 137) optionIndex = 0; else optionIndex = midiInChannel + 1;

                midiSettingsControlView.midiInChannelList.selectedIndex = optionIndex;

            }
            // if (portSettingsCache.control.midiOutChannel !== undefined) {

            //     midiOutChannel = portSettingsCache.control.midiOutChannel;

            //     midiSettingsControlView.midiOutChannelList.selectedIndex = midiOutChannel;

            // }

            //set midi in port mute state
            if (portSettingsCache.control.midiInPort_isMuted !== undefined) {

                var isMuted = portSettingsCache.control.midiInPort_isMuted;

                midiInPortMuteCheckboxControl.checked = isMuted;
                midiInPortMuteCheckboxControl.onchange();

            }

        }

        if (portSettingsCache.renderer !== undefined) {
            if (portSettingsCache.renderer.midiInPortId !== undefined) {

                var portId = portSettingsCache.renderer.midiInPortId;

                renderview.postMessage( { type: "setMidiInPortByPortId", message: portId } );

                midiSettingsRenderView.midiInDevicesList.selectedIndex = getPortIndexByPortId(midiSettingsRenderView.midiInPortList, portId);


            }
            if (portSettingsCache.renderer.midiOutPortId !== undefined) {

                var portId = portSettingsCache.renderer.midiOutPortId;
                //setMidiOutDefaultPort(portSettingsCache.renderer.midiOutPortId);
                renderview.postMessage( { type: "setMidiOutPortByPortId", message: portId } );

                midiSettingsRenderView.midiOutDevicesList.selectedIndex = getPortIndexByPortId(midiSettingsRenderView.midiOutPortList, portId);

            }

            if (portSettingsCache.renderer.midiInChannel !== undefined) {

                var renderer_midiInChannel = portSettingsCache.renderer.midiInChannel;
                renderview.postMessage( { type: "setMidiInChannel", message: renderer_midiInChannel } );

                var optionIndex = 0;
                if (renderer_midiInChannel === 137) optionIndex = 0; else optionIndex = renderer_midiInChannel + 1;

                midiSettingsRenderView.midiInChannelList.selectedIndex = optionIndex;
            }

            if (portSettingsCache.renderer.midiOutChannel !== undefined) {

                var renderer_midiOutChannel = portSettingsCache.renderer.midiOutChannel;
                renderview.postMessage( { type: "setMidiOutChannel", message: renderer_midiOutChannel } );

                midiSettingsRenderView.midiOutChannelList.selectedIndex = renderer_midiOutChannel;
            }

        }
   
}


function buildMidiStringForLog(message) {

    var type = message.type;
    var channel = message.channel;
    var velocity = message.velocity;
    var note = message.note;

    var typeString = "";

    switch (type) {

        case midiMessageType.noteOn:
            typeString = "noteOn";
            break;

        case midiMessageType.noteOff:
            typeString = "noteOff";
            break;

        case midiMessageType.controlChange:
            typeString = "ControlChange";
            break;

        case midiMessageType.programChange:
            typeString = "ProgramChange";
            break;

        default:
            typeString = type;
    }

    var fullMessage = "type: " + typeString + " ";

    for (var key in message) {
        if (key !== "type" && key !== "rawData" && key !== "timestamp" && key !== "toString") {
            fullMessage += key + ": " + message[key] + " ";
        }
    }

    return fullMessage;

}

function getMidiChannel( decimal ) {

    var hex = decimal.toString(16);

    return parseInt( hex.charAt(1), 16 );

}

function getMidiMessageType( decimal ) {

    var hex = decimal.toString(16);

    return parseInt( hex.charAt(0) + "0", 16 );

}

function MidiInPort_MessageReceived(message) {

    var messageArgs = {

        note: message.data[1],
        velocity: message.data[2],
        controller: message.data[1],
        controlValue: message.data[2],
        program: message.data[1]

    }

    var type = getMidiMessageType( message.data[0] );
    var channel = getMidiChannel( message.data[0] );
   
    //channel filter

    if (midiInChannel !== 137) {
        if (channel !== midiInChannel) return;
    }

    switch (type) {

        case midiMessageType.noteOn:
            handleNoteOnMessage(messageArgs.note, messageArgs.velocity);
            break;

        case midiMessageType.noteOff:
            handleNoteOffMessage(messageArgs.note, messageArgs.velocity);
            break;

        case midiMessageType.controlChange:
            handleCCMessage(messageArgs.controller, messageArgs.controlValue);
            break;

        case midiMessageType.programChange:
            handlePCMessage(messageArgs.program);
            break;

        default:
            
    }

}

function getMidiMappingByController(controller) {
    for (var i = 0; i < midiMap.length; i++) {
        if (midiMap[i].midiController === controller) {
            return midiMap[i];
        }
    }
    return undefined;
}

function getMidiMappingByUIMappingId(mappingId) {
    for (var i = 0; i < midiMap.length; i++) {
        if (midiMap[i].UIcontrolMappingId === mappingId) {
            return midiMap[i];
        }
    }
    return undefined;
}

// function getElementByMappingId(mappingId) {

//     for (var i = 0; i < mappableControlsCollection[i].length; i++) {
//         if ()
//     }

// }

function processMidiMappingOnMidiInput(controller, value, isNoteOff) {

    if (midiMappingModeOn) {

        var existingMidiMapping = getMidiMappingByController(controller);

        if (existingMidiMapping !== undefined) {

            if (mappingSelectedControl !== null && mappingSelectedControl !== undefined) {

                resetMidiMappingOnControl(existingMidiMapping);
            }
        }

        if (mappingSelectedControl !== null && mappingSelectedControl !== undefined) {

            setMidiMappingOnControl(controller);
        }

        var midiMapping = getMidiMappingByController(controller);

        if (midiMapping !== undefined) {
            midiMapping.triggerMidiEvent(value, isNoteOff);
        }

    } else {
        var midiMapping = getMidiMappingByController(controller);

        if (midiMapping !== undefined) {            
            midiMapping.triggerMidiEvent(value, isNoteOff);
        }
    }
}

function handleNoteOnMessage(note, velocity) {
    
    processMidiMappingOnMidiInput(note, velocity, false);
}

function handleNoteOffMessage(note, velocity) {
    
    processMidiMappingOnMidiInput(note, velocity, true);
}

function handleCCMessage(controller, controlValue) {
    
    processMidiMappingOnMidiInput(controller, controlValue, null);
}

function handlePCMessage(program) {
    
    switchProgramByPresetButton(program + 1);

}

function updateRendererMidiMap() {

    renderview.postMessage( { type: "updateMidiMap", message: JSON.stringify(midiMap) } );
    

}

function setMidiMappingOnControl(controllerOrNote) {

            if (mappingSelectedControl.mapped === true) {

                var midiMapping = getMidiMappingByUIMappingId(mappingSelectedControl.mappingId);

                if (midiMapping !== undefined)
                    midiMapping.setController(controllerOrNote); 
                else
                    console.warn("midi mapping is undefined", mappingSelectedControl.mappingId);

                mappingSelectedControl.cue.innerHTML = controllerOrNote;


            } else {

                var midiMapping = new MidiMapInstance(mappingSelectedControl.midimapLever, mappingSelectedControl.mappingId, controllerOrNote, mappingSelectedControl.id);

                mappingSelectedControl.mapped = true;

                if (midiMapping) midiMap.push(midiMapping);


                if (mappingSelectedControl.cue === undefined) {

                    makeControllerCue(mappingSelectedControl, midiMapping);

                } else {

                    mappingSelectedControl.cue.innerHTML = controllerOrNote;
                }

            }

            updateRendererMidiMap();

}

function resetMidiMappingOnControl(existingMidiMapping) {

    // var mappedUIControl = document.getElementById(existingMidiMapping.UIcontrolMappingId);

    var mappedUIControl = mappableControlsCollection[ existingMidiMapping.UIcontrolMappingId ];

    if (mappedUIControl !== null) {
        mappedUIControl.mapped = false;        

        if (mappedUIControl.cue !== undefined) {
            mappedUIControl.parentElement.removeChild(mappedUIControl.cue);
            mappedUIControl.cue = undefined;            
        }
        
    } else {

        return;

    }

    //remove existing midi mapping instance from midi map list

    var actualIndex = midiMap.indexOf(existingMidiMapping);

    midiMap.splice(actualIndex, 1);

    existingMidiMapping = undefined;

    updateRendererMidiMap();

}

function makeControllerCue(mappingSelectedControl, midiMapping) {

    var cue = document.createElement("div");
    cue.className = "midiMapCue";
    cue.innerHTML = midiMapping.midiController;

    var container = mappingSelectedControl.parentElement;
    container.appendChild(cue);
    mappingSelectedControl.cue = cue;

    cue.addEventListener('contextmenu', function (e) {
        e.preventDefault();
        resetMidiMappingOnControl(midiMapping);
        return false;
    }, false);
}

// messageTypes: "NoteOn", "NoteOff", "CC"
// usage example: sendMidiMessage("NoteOn", note, velocity);
// usage example: sendMidiMessage("CC", controller, controlValue);

function sendMidiMessage(messageType, val1, val2) {

    if (midiOutPort === null) return;

    var midiMessageToSend = null;

    switch (messageType) {

        case "NoteOff":
            // usage format: MidiNoteOffMessage(channel, note, velocity)
            midiMessageToSend = midi.MidiNoteOffMessage(midiOutChannel, val1, val2);
            break;

        case "NoteOn":
            // usage format: MidiNoteOnMessage(channel, note, velocity)
            midiMessageToSend = midi.MidiNoteOnMessage(midiOutChannel, val1, val2);
            break;            

        case "CC":
            // usage format: MidiControlChangeMessage(channel, controller, controlValue)
             midiMessageToSend = midi.MidiControlChangeMessage(midiOutChannel, val1, val2);
            break;

    }
            

    if (midiMessageToSend !== null && midiOutPort !== null) {

        var midiLogString;
 

        try {
            midiOutPort.sendMessage(midiMessageToSend);

        }
        catch (e) {

            console.error(e.message);

            var errorMessage = "midi port error =( Try choosing another";

        }        

    }

}

function enumerateMappableControls() {

    midiMappingIncrementalIndex = 0;
    mappableControlsCollection = [];

    // collect all UI control inputs

    var column2 = document.getElementById("column2");
    var column2Elements = column2.getElementsByTagName("input");

    var column3 = document.getElementById("column3");
    var column3Elements = column3.getElementsByTagName("input");

    var column4 = document.getElementById("column4");
    var column4Elements = column4.getElementsByTagName("input");
    
    for (var i = 0; i < column2Elements.length; i++) {

        if ( column2Elements[i].isColor !== true ) {

            column2Elements[i].mappingId = midiMappingIncrementalIndex;

            midiMappingIncrementalIndex++;

            mappableControlsCollection.push(column2Elements[i]);
            
        }
    }

    for (var i = 0; i < column3Elements.length; i++) {

        if ( column3Elements[i].isColor !== true ) {

            column3Elements[i].mappingId = midiMappingIncrementalIndex;

            midiMappingIncrementalIndex++;

            mappableControlsCollection.push(column3Elements[i]);
            
        }
    }

    

    // const delayEnumeration = setTimeout(function(){

        console.log(column4Elements, column4Elements.length);

        for (var i = 0; i < column4Elements.length; i++) {

            if ( column4Elements[i].isColor !== true ) {
    
                column4Elements[i].mappingId = midiMappingIncrementalIndex;
    
                midiMappingIncrementalIndex++;
    
                mappableControlsCollection.push(column4Elements[i]);
    
                
                
            }
        }

        // add preset switch slider to mappable collection
        var presetSwitchSlider = id( "presetSwitchSlider" );
        presetSwitchSlider.mappingId = midiMappingIncrementalIndex;
        midiMappingIncrementalIndex++;
        mappableControlsCollection.push( presetSwitchSlider );
        


        var resetPresetButton= id( "resetPresetButton" );
        resetPresetButton.mappingId = midiMappingIncrementalIndex;
        midiMappingIncrementalIndex++;
        mappableControlsCollection.push( resetPresetButton );

        var KinectReinitButton= id( "KinectReinitButton" );
        KinectReinitButton.mappingId = midiMappingIncrementalIndex;
        midiMappingIncrementalIndex++;
        mappableControlsCollection.push( KinectReinitButton );



        

        // add preset buttons to mappable collection
        //var column1 = document.getElementById("column1");
        var presetButtonsBlock =  id("preset_buttons_existing");
        var presetButtons = presetButtonsBlock.getElementsByTagName("input");

        for (var i = 0; i < presetButtons.length; i++) {

            presetButtons[i].mappingId = midiMappingIncrementalIndex;

            midiMappingIncrementalIndex++;

            mappableControlsCollection.push(presetButtons[i]);

        }      

        console.log("mappable controls:", mappableControlsCollection.length );

    
    // }, 500);

    

    

}

function enterMidiMappingMode() {

    midiMappingModeOn = true;

    // unmute control view midi in port
    if (midiInPort && midiInPort.onmessagereceived === null) {

        midiInPort.onmidimessage = MidiInPort_MessageReceived;

    }

    // mute renderer view midi in port
    //renderview.postMessage( { type: "setMidiInPortMuteState", message: true } );

    for (var i = 0; i < mappableControlsCollection.length; i++) {


        setHighlightControl( mappableControlsCollection[i] );

        mappableControlsCollection[i].normalDisabledState = mappableControlsCollection[i].disabled;

        mappableControlsCollection[i].disabled = false;

        if (mappableControlsCollection[i].cue !== undefined) {
            mappableControlsCollection[i].cue.style.display = "block";
        }

        mappableControlsCollection[i].onfocus = function (e) {
        
            mappingSelectedControl = this;

            setMappingModeControlSelected(this);

            if (prevMappingSelectedControl !== undefined && prevMappingSelectedControl !== this) {

                setMappingModeControlUnselected(prevMappingSelectedControl);

            }

            prevMappingSelectedControl = mappingSelectedControl;

        }          

    }

}


function setMappingModeControlSelected(control) {

    control.disabled = true;
    control.classList.add("mappingModeControlSelected");

}

function setMappingModeControlUnselected(control) {

    if (control === undefined || control === null) return;

    control.disabled = false;
    control.classList.remove("mappingModeControlSelected");

}

function setHighlightControl(control) {

    control.classList.add("highLightControl");

}

function resetHighlightControl(control) {

    control.classList.remove("highLightControl");

}

function quitMidiMappingMode() {

    midiMappingModeOn = false;

    // mute control view midi in port

    if (midiInPort && midiInPort.onmessagereceived !== null) {

        midiInPort.onmessagereceived = null;

    }

    // mute renderer view midi in port

    renderview.postMessage( { type: "setMidiInPortMuteState", message: false } );


    mappingSelectedControl = undefined;

    setMappingModeControlUnselected(prevMappingSelectedControl);

    for (var i = 0; i < mappableControlsCollection.length; i++) {

        resetHighlightControl(mappableControlsCollection[i]);

        mappableControlsCollection[i].disabled = mappableControlsCollection[i].normalDisabledState;

        mappableControlsCollection[i].onfocus = null;
        //mappableControlsCollection[i].onmousedown = mappableControlsCollection[i].normalOnmousedownHandler;

        if (mappableControlsCollection[i].cue !== undefined) {
            mappableControlsCollection[i].cue.style.display = "none";
        }

    }
}

function resetAllMidiMappings() {    

    for (var i = 0; i < mappableControlsCollection.length; i++) {

        if (mappableControlsCollection[i].mapped === true) {

            mappableControlsCollection[i].mapped = false;   

            if (mappableControlsCollection[i].cue !== undefined) {
                mappableControlsCollection[i].parentElement.removeChild(mappableControlsCollection[i].cue);
                mappableControlsCollection[i].cue = undefined;            
            }

        }
            
    }
}

function restoreMidiMappingsOnLoad() {

    for (var i = 0; i < midiMap.length; i++) {

        midiMap[i].triggerMidiEvent = triggerMidiEvent;
        midiMap[i].setController = setController;

        var mappedElementId = midiMap[i].elementId;

        // console.log( mappedElementId );

        var mappingControl = id( mappedElementId );

        // var mappingControl = mappableControlsCollection[ midiMap[i].UIcontrolMappingId ];

        if (mappingControl == null) continue;

        //midiMap[i].UIcontrolObject = mappingControl;

        midiMap[i].midimapLever = mappingControl.midimapLever;

        mappingControl.mapped = true;

        mappableControlsCollection[ midiMap[i].UIcontrolMappingId ] = mappingControl;

        makeControllerCue(mappingControl, midiMap[i]);

        mappingControl.cue.style.display = "none";

    }

}



function refreshMidiMappingOnProgramSettingsLoad() {

    for (var i = 0; i < midiMap.length; i++) {        

        // // var mappingControl = document.getElementById(midiMap[i].UIcontrolMappingId);

        //var mappingControl = mappableControlsCollection[ midiMap[i].UIcontrolMappingId ];

        

        var mappedElementId = midiMap[i].elementId;

        console.log( mappedElementId );

        var mappingControl = id( mappedElementId );

        if (mappingControl === null || mappingControl === undefined) continue;

        if (mappingControl.mapped !== true) {

            //var cue = mappingControl.cue;

            mappableControlsCollection[ midiMap[i].UIcontrolMappingId ] = mappingControl;

            mappingControl.mapped = true;

            makeControllerCue(mappingControl, midiMap[i]);

            mappingControl.cue.style.display = "none";

            console.log("new midi mapping set:", mappingControl);

        }        

    }

}

// function refreshMidiMappingOnMaterialSettingsLoad() {

//     var materialControlsUI = materialSettingsBlock.getElementsByTagName("input");

//     for (var i = 0; i < materialControlsUI.length; i++ ) {

//         var mappingControl = materialControlsUI[i];

//         for (var j = 0; j < midiMap.length; j++) {      

//             if ( mappingControl.id === midiMap[j].UIcontrolMappingId ) {

//                 if (mappingControl.mapped !== true) {

//                     //var cue = mappingControl.cue;

//                     mappingControl.mapped = true;

//                     makeControllerCue( mappingControl, midiMap[j] );

//                     mappingControl.cue.style.display = "none";

//                 }
//             }
//         }
//     }
// }




function processMidiBodyEvent(message) {

    sendMidiMessage("CC", message.controller, message.value);

}

function scale(valueIn, baseMin, baseMax, limitMin, limitMax) {

	if (valueIn < baseMin) return limitMin;
    else
		if (valueIn > baseMax) return limitMax;
        else
			return ((limitMax - limitMin) * (valueIn - baseMin) / (baseMax - baseMin)) + limitMin;
}


