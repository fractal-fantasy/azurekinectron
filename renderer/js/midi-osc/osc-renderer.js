
const osc = require("osc");

var oscParams = {

    localAddress: "192.168.0.10",
    remoteAddress: "127.0.0.1",
    localPort: 2346,    
    remotePort: 8000

}


// Create an osc.js UDP Port listening on port 2346.
var udpPort = new osc.UDPPort({
    //localAddress: oscParams.localAddress,
    //localPort: oscParams.localPort,
    remoteAddress: oscParams.remoteAddress,
    remotePort: oscParams.remotePort,
    broadcast: true
    // metadata: true
});

// Listen for incoming OSC messages.
udpPort.on("message", function (oscMsg, timeTag, info) {
    console.log( "OSC message: ", "address:", oscMsg.address, "value:", oscMsg.args[0].value, "type:", oscMsg.args[0].type );
    // console.log("OSC message full: ", oscMsg);

    onOSCMessageReceived( oscMsg );

});

// Open the socket.
udpPort.open();

// var allowSendOSC = false;

// When the port is read, send an OSC message to, say, SuperCollider
// udpPort.on("ready", function () {
//     udpPort.send({
//         address: "/s_new",
//         args: [
//             {
//                 type: "s",
//                 value: "default"
//             },
//             {
//                 type: "i",
//                 value: 100
//             }
//         ]
//     }, "127.0.0.1", 57110);
// });


// udpPort.on("error", function (error) {
//     console.log("An error occurred: ", error.message);
// });

var oscMsgAddress = "/kinectron/hand";


function updateUdpPortOptions() {

    udpPort.close();

    udpPort = new osc.UDPPort({
        //localAddress: oscParams.localAddress,
        //localPort: oscParams.localPort,
        remoteAddress: oscParams.remoteAddress,
        remotePort: oscParams.remotePort,
        // metadata: true,
        broadcast: true
    });
    

    udpPort.open();

    udpPort.on("ready", () => {
        var message = {address: "/HELLO"}

        udpPort.send(message)
    })

    udpPort.on("error", (e) => {
        
        console.error(e);
    })

    console.log( udpPort );

}

function processOSCBodyEvent( hand, axis, value ) {

    // if ( !allowSendOSC ) return;

    var adrHand = "";

    switch (hand) {

        case "left":
            adrHand = "/left";
            break;
        case "right":
            adrHand = "/right";
            break;
    }

    var sendAddress = oscMsgAddress + adrHand + "/" + axis;

    udpPort.send({
        address: sendAddress,
        args: [
            {
                type: "f",
                value: value
            }
        ]
    });

    // console.log( sendAddress, value );

}


