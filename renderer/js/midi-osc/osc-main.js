/**
 * @author mikkamikka / https://mikkamikka.com
 */

const osc = require("osc");

const { networkInterfaces } = require('os');

// Get device's local network IP address

const nets = networkInterfaces();
const results = Object.create(null);

for (const name of Object.keys(nets)) {
    for (const net of nets[name]) {
        // Skip over non-IPv4 and internal (i.e. 127.0.0.1) addresses
        if (net.family === 'IPv4' ) {  //&& !net.internal) {
            if (!results[name]) {
                results[name] = [];
            }
            results[name].push(net.address);
        }
    }
}

var localWiFiAddress;

if (results["Wi-Fi"] == undefined){
    
    console.log("no wifi");
   // localWiFiAddress = "192.168.0.1";
   localWiFiAddress = "No Wifi Connection Detected";
} else {
    localWiFiAddress = results["Wi-Fi"][0];
}

// console.log(results, "Your device's Wi-Fi address:", results["Wi-Fi"][0]);
// const localWiFiAddress = results["Wi-Fi"][0];

var oscParams = {

    remoteAddress: "127.0.0.1",
    localAddress: "127.0.0.1",
    localPort: 2346,    
    remotePort: 8000

}

// Create an osc.js UDP Port listening on port 2346.
var udpPort = new osc.UDPPort({
    localAddress: oscParams.localAddress,
    localPort: oscParams.localPort,
    //remoteAddress: oscParams.remoteAddress,
    //remotePort: oscParams.remotePort,
    metadata: true
});

// Listen for incoming OSC messages.
udpPort.on("message", function (oscMsg, timeTag, info) {
//    console.log( "OSC message: ", "address:", oscMsg.address, "value:", oscMsg.args[0].value, "type:", oscMsg.args[0].type );


    onOSCMessageReceived( oscMsg );

});

// Open the socket.
udpPort.open();

function setOcsLocalAddress(input) {
    oscParams.localAddress = input.value;
    updateUdpPortOptions();    
}

function setOcsLocalPort(input) {
    oscParams.localPort = Number(input.value);
    updateUdpPortOptions();
}

function setOcsRemoteAddress(input) {
    oscParams.remoteAddress = input.value;
    updateUdpPortOptions();
}

function setOcsRemotePort(input) {
    oscParams.remotePort = Number(input.value);
    updateUdpPortOptions();
}

function updateUdpPortOptions() {

    udpPort.close();

    udpPort = new osc.UDPPort({
        localAddress: oscParams.localAddress,
        localPort: oscParams.localPort,
        //remoteAddress: oscParams.remoteAddress,
        //remotePort: oscParams.remotePort,
        metadata: true
    });

    // Listen for incoming OSC messages.
    udpPort.on("message", function (oscMsg, timeTag, info) {
      //  console.log( "OSC message: ", "address:", oscMsg.address, "value:", oscMsg.args[0].value, "type:", oscMsg.args[0].type );
        //console.log("OSC message full: ", oscMsg);

        onOSCMessageReceived( oscMsg );

    });

    udpPort.open();

    renderview.postMessage( { type: "updateOSCParams", message: oscParams } )

}




function onOSCMessageReceived(oscMsg) {

    processOscMappingOnOscInput( oscMsg.address, oscMsg.args[0].value );
   
}


var oscMap = [];
var oscMappingModeOn = false;
var oscMappableControlsCollection = [];

var oscMappingSelectedControl;
var prevOscMappingSelectedControl;

var oscMappingIncrementalIndex = 0;




var triggerOscEvent = function (address, value) {

    var UIcontrolObject = mappableControlsCollection[ this.UIcontrolMappingId ];
    
    if (UIcontrolObject === null || UIcontrolObject === undefined) return;

    var isPushButton = address.includes("push"); // || address.includes("button");
    var isToggle = address.includes("toggle");   // || address.includes("button");

    switch (UIcontrolObject.type) {

        case "checkbox":

            //if ( !isToggle ) break;

            var isActive = false;

            if ( value > 0 ) {
                isActive = true;
            } else {
                isActive = false;
            }

            if (isActive) {
                UIcontrolObject.checked = true;
            } else {
                UIcontrolObject.checked = false;
            }

            UIcontrolObject.onchange();

            break;

        case "button":

            //if ( !isPushButton ) break;

            // in midiMappingModeOn disabled for buttons 'coz too laggy
            if (!oscMappingModeOn) {

                if (oscMapEventsVisible === true) {

                    if (UIcontrolObject.onmousedown !== null) {

                        if ( value > 0 ) {
                            if (UIcontrolObject.pressed != 1) UIcontrolObject.onmousedown();
                        } else {
                            if (UIcontrolObject.onmouseup !== null) 
                                UIcontrolObject.onmouseup();
                        }
                    }
                    else {
                        UIcontrolObject.click();
                    }

                } else {

                    if (this.UIcontrolObject.onmousedownSilent !== null) {

                        if ( value > 0 ) {
                            UIcontrolObject.onmousedownSilent();
                            
                        } else {
                            if (UIcontrolObject.onmouseupSilent !== null) UIcontrolObject.onmouseupSilent();
                        }
                    }
                    else {
                        if (UIcontrolObject.clickSilent !== null) UIcontrolObject.clickSilent();
                    }
                }

            }

            break;

        case "range":

            var rangeAdaptedValue = scale(Number(value), 0, 1, Number(UIcontrolObject.min), Number(UIcontrolObject.max));                       

            if (oscMapEventsVisible === true) {

                UIcontrolObject.value = rangeAdaptedValue;

                if (UIcontrolObject.oninput !== null) {
                    UIcontrolObject.oninput();
                } else
                    if (UIcontrolObject.onchange !== null) {
                        UIcontrolObject.onchange();
                    }

            } else {

                if (UIcontrolObject.oninputSilent !== null) {
                    UIcontrolObject.oninputSilent( rangeAdaptedValue );
                } else
                    if (UIcontrolObject.onchangeSilent !== null) {
                        UIcontrolObject.onchangeSilent( rangeAdaptedValue );
                    }

            }

            break;

        case "number":

            var rangeAdaptedValue = scale(Number(value), 0, 1, Number(UIcontrolObject.min), Number(UIcontrolObject.max));                       

            if (oscMapEventsVisible === true) {

                UIcontrolObject.value = rangeAdaptedValue;

                if (UIcontrolObject.oninput !== null) {
                    UIcontrolObject.oninput();
                } else
                    if (UIcontrolObject.onchange !== null) {
                        UIcontrolObject.onchange();
                    }

            } else {

                if (UIcontrolObject.oninputSilent !== null) {
                    UIcontrolObject.oninputSilent( rangeAdaptedValue );
                } else
                    if (UIcontrolObject.onchangeSilent !== null) {
                        UIcontrolObject.onchangeSilent( rangeAdaptedValue );
                    }

            }

            break;

        case "text":  // preset buttons!

            if (!oscMappingModeOn) {

                if (oscMapEventsVisible === true) {

                    if (UIcontrolObject.onmousedown !== null) {

                        if ( value > 0 ) {
                            if (UIcontrolObject.pressed != 1) UIcontrolObject.onmousedown();
                        } else {
                            if (UIcontrolObject.onmouseup !== null) 
                                UIcontrolObject.onmouseup();
                        }
                    }
                    else {
                        if ( value > 0 ) {
                            UIcontrolObject.click();
                        }
                    }

                } else {

                    if (this.UIcontrolObject.onmousedownSilent !== null) {

                        if ( value > 0 ) {
                            UIcontrolObject.onmousedownSilent();
                            
                        } else {
                            if (UIcontrolObject.onmouseupSilent !== null) UIcontrolObject.onmouseupSilent();
                        }
                    }
                    else {
                        if (UIcontrolObject.clickSilent !== null) UIcontrolObject.clickSilent();
                    }
                }

            }

            break;

    }

}

var setAddress = function (address) {
    this.oscAddress = address;
};

var OscMapInstance = function (midimapLever, mappingId, oscAddress, elementId) {

    this.midimapLever = midimapLever;

    this.UIcontrolMappingId = mappingId;  // index in oscMap array

    this.elementId = elementId;    // html tag id

    this.oscAddress = oscAddress;

    this.setAddress = setAddress;

    this.triggerOscEvent = triggerOscEvent;

}


function processOscMappingOnOscInput(address, value) {

    if (oscMappingModeOn) {

        var existingOscMapping = getOscMappingByController(address);

        if (existingOscMapping !== undefined) {

            if (oscMappingSelectedControl !== null && oscMappingSelectedControl !== undefined) {

                resetOscMappingOnControl(existingOscMapping);
            }
        }

        if (oscMappingSelectedControl !== null && oscMappingSelectedControl !== undefined) {

            setOscMappingOnControl(address);
        }

        var mapping = getOscMappingByController(address);

        if (mapping !== undefined) {
            mapping.triggerOscEvent(address, value);
        }

    } else {
        var mapping = getOscMappingByController(address);

        if (mapping !== undefined) {            
            mapping.triggerOscEvent(address, value);
        }
    }
}


function getOscMappingByController(id) {
    for (var i = 0; i < oscMap.length; i++) {
        if (oscMap[i].oscAddress === id) {
            return oscMap[i];
        }
    }
    return undefined;
}

function getOscMappingByUIMappingId(mappingId) {
    for (var i = 0; i < oscMap.length; i++) {
        if (oscMap[i].UIcontrolMappingId === mappingId) {
            return oscMap[i];
        }
    }
    return undefined;
}

function getOscMappingByName(name) {
    
    for (var i = 0; i < oscMap.length+2; i++) {

            if (oscMap[i].elementId === name) {
                //console.log(oscMap[i].elementId);
                // var mappedElementId = oscMap[i].elementId;
                // var mappingControl = id( mappedElementId );

                return oscMap[i];
            }
        

    }

    return undefined;
}


function updateRendererOscMap() {

    renderview.postMessage( { type: "updateOscMap", message: JSON.stringify(oscMap) } );
    

}

function setOscMappingOnControl( address ) {

            if (oscMappingSelectedControl.oscMapped === true) {

               // console.log(oscMappingSelectedControl.mappingId);
                var oscMapping = getOscMappingByUIMappingId(oscMappingSelectedControl.mappingId);

                //var selectedControlId = oscMappingSelectedControl.id;
                //var oscMapping = getOscMappingByName(selectedControlId);

                oscMapping.setAddress( address );

                oscMappingSelectedControl.oscCue.innerHTML = address;


            } else {

                var oscMapping = new OscMapInstance( oscMappingSelectedControl.midimapLever, oscMappingSelectedControl.mappingId, address, oscMappingSelectedControl.id );

                oscMappingSelectedControl.oscMapped = true;

                if (oscMapping) oscMap.push(oscMapping);


                if (oscMappingSelectedControl.oscCue === undefined) {

                    makeOscCue(oscMappingSelectedControl, oscMapping);

                    

                } else {

                    oscMappingSelectedControl.oscCue.innerHTML = address;

                    console.log("osc already mapped");
                }

            }

            updateRendererOscMap();

}

function resetOscMappingOnControl(existingOscMapping) {

    var mappedUIControl = mappableControlsCollection[ existingOscMapping.UIcontrolMappingId ];

    if (mappedUIControl !== null) {
        mappedUIControl.oscMapped = false;        

        if (mappedUIControl.oscCue !== undefined) {
            mappedUIControl.parentElement.removeChild(mappedUIControl.oscCue);
            mappedUIControl.oscCue = undefined;            
        }
        
    } else {

        return;

    }

    //remove existing midi mapping instance from midi map list

    var actualIndex = oscMap.indexOf(existingOscMapping);

    oscMap.splice(actualIndex, 1);

    existingOscMapping = undefined;

    updateRendererOscMap();

}


function makeOscCue(oscMappingSelectedControl, oscMapping) {

    // console.log(oscMappingSelectedControl, oscMapping);

    var cue = document.createElement("div");
    cue.className = "oscMapCue";
    cue.innerHTML = oscMapping.oscAddress;

    var container = oscMappingSelectedControl.parentElement;
    container.appendChild(cue);
    oscMappingSelectedControl.oscCue = cue;

    cue.addEventListener('contextmenu', function (e) {
        console.log(cue, oscMapping);
        e.preventDefault();
        resetOscMappingOnControl(oscMapping);
        return false;
    }, false);
}


function enterOscMappingMode() {

    oscMappingModeOn = true;

    for (var i = 0; i < mappableControlsCollection.length; i++) {
   

        setHighlightControl_OSC( mappableControlsCollection[i] );

        mappableControlsCollection[i].normalDisabledState = mappableControlsCollection[i].disabled;

        mappableControlsCollection[i].disabled = false;

        if (mappableControlsCollection[i].oscCue !== undefined) {
            mappableControlsCollection[i].oscCue.style.display = "block";
        }

        mappableControlsCollection[i].onfocus = function (e) {
        
            oscMappingSelectedControl = this;

            setMappingModeControlSelected_OSC(this);

            if (prevOscMappingSelectedControl !== undefined && prevOscMappingSelectedControl !== this) {

                setMappingModeControlUnselected_OSC(prevOscMappingSelectedControl);

            }

            prevOscMappingSelectedControl = oscMappingSelectedControl;

        }
        

    }

}


function setMappingModeControlSelected_OSC(control) {

    control.disabled = true;
    control.classList.add("mappingModeControlSelected_OSC");

}

function setMappingModeControlUnselected_OSC(control) {

    if (control === undefined || control === null) return;

    control.disabled = false;
    control.classList.remove("mappingModeControlSelected_OSC");

}

function setHighlightControl_OSC(control) {

    control.classList.add("highLightControl_OSC");

}

function resetHighlightControl_OSC(control) {

    control.classList.remove("highLightControl_OSC");

}

function quitOscMappingMode() {

    oscMappingModeOn = false;

    oscMappingSelectedControl = undefined;

    setMappingModeControlUnselected_OSC(prevOscMappingSelectedControl);

    for (var i = 0; i < mappableControlsCollection.length; i++) {

        resetHighlightControl_OSC(mappableControlsCollection[i]);

        mappableControlsCollection[i].disabled = mappableControlsCollection[i].normalDisabledState;

        mappableControlsCollection[i].onfocus = null;

        if (mappableControlsCollection[i].oscCue !== undefined) {
            mappableControlsCollection[i].oscCue.style.display = "none";
        }

    }
}

function resetAllOSCMappings() {

    for (var i = 0; i < mappableControlsCollection.length; i++) {

        if (mappableControlsCollection[i] !== undefined){
            if (mappableControlsCollection[i].oscMapped === true) {

                mappableControlsCollection[i].oscMapped = false;   

                if (mappableControlsCollection[i].oscCue !== undefined) {
                    mappableControlsCollection[i].parentElement.removeChild(mappableControlsCollection[i].oscCue);
                    mappableControlsCollection[i].oscCue = undefined;
                }

            }
        }
              
    }
}

function restoreOSCMappingsOnLoad() {

    for (var i = 0; i < oscMap.length; i++) {

        oscMap[i].triggerOscEvent = triggerOscEvent;
        oscMap[i].setAddress = setAddress;

        // var mappingControl = mappableControlsCollection[ oscMap[i].UIcontrolMappingId ];

        var mappedElementId = oscMap[i].elementId;

        var mappingControl = id( mappedElementId );

        if (mappingControl === null) continue;

        oscMap[i].midimapLever = mappingControl.midimapLever;

        mappingControl.oscMapped = true;

        mappableControlsCollection[ oscMap[i].UIcontrolMappingId ] = mappingControl;

        makeOscCue(mappingControl, oscMap[i]);

        mappingControl.oscCue.style.display = "none";

    }

}


function refreshOSCMappingOnProgramSettingsLoad() {

    for (var i = 0; i < oscMap.length; i++) {        

        //var mappingControl = mappableControlsCollection[ oscMap[i].UIcontrolMappingId ];

        var mappedElementId = oscMap[i].elementId;

        // console.log( mappedElementId );

        var mappingControl = id( mappedElementId );

        if (mappingControl === null || mappingControl === undefined) continue;

        if (mappingControl.oscMapped !== true) {

            mappableControlsCollection[ oscMap[i].UIcontrolMappingId ] = mappingControl;

            mappingControl.oscMapped = true;

            makeOscCue( mappingControl, oscMap[i] );

            mappingControl.oscCue.style.display = "none";

        }

    }

}