/**
 * @author alteredq / http://alteredqualia.com/
 *
 * Blend two textures
 */

 THREE.normalMapShaderFF = {

	uniforms: {

		"processedDepth": { value: null },
		"processedDepthPrev": { value: null },
		"mixRatio": { value: 0.5 },
		"opacity": { value: 1.0 },
		"step": { value: new THREE.Vector2( 1.0 / 1280.0, 1.0 / 720.0 ) }

	},

	vertexShader: [

		"varying vec2 vUv;",

		"void main() {",

		"	vUv = uv;",
		"	gl_Position = projectionMatrix * modelViewMatrix * vec4( position, 1.0 );",

		"}"

	].join( "\n" ),

	fragmentShader: [

		"uniform float opacity;",
		"uniform float mixRatio;",
		"uniform vec2 step;",

		"uniform sampler2D processedDepth;",
		"uniform sampler2D processedDepthPrev;",
		
		"varying vec2 vUv;",

		"void main() {",

		"	float texel1 = texture2D( processedDepth, vUv ).x;",
		"	float texel2 = texture2D( processedDepthPrev, vUv ).x;",
		"   float separation = 2.0;",
		"   float depthScale = -40.0;",
		"   vec2 dx = vec2(separation,0.0);",
		"   vec2 dy = vec2(0.0,separation);",
		"	float px = texture2D( processedDepth, vUv+dx  * step).x;",
		"	float py = texture2D( processedDepth, vUv+dy  * step).x;",
		//"   float sx = depthScale*(px-texel1)/separation;",
		//"   float sy = depthScale*(py-texel1)/separation;",
		//"   vec3 v1 = vec3(separation,0.0,px*depthScale);",
		//"   vec3 v2 = vec3(0.0,separation,py*depthScale);",
		//"   vec3 Norm = cross(v1,v2);",
		"   vec3 Norm = vec3((px-texel1)*depthScale,(py-texel1)*depthScale,1.0);",
		"   Norm = normalize(Norm) * 0.5 + 0.5;",
		
		// "	N.y = 1.0",
		//"   gl_FragColor = texel1;",
		// "	gl_FragColor = opacity * mix( texel1, texel2, mixRatio );",
		"	gl_FragColor = vec4(Norm.x,Norm.y,Norm.z,1.0);",
		"}"

	].join( "\n" )

};
