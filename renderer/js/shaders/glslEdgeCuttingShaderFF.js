/**
 * edge detection
 */

var glslEdgeCuttingShaderFF = {

	uniforms: {

		"tDiffuse": { value: null },
        "sharpEdgeWidth": { value: 1.0 },     
        "edgeThreshold": { value: 0.05 },
        "edgeCleanWidth": { value: 1.0 },
        "maxThresholdFactor": { value: 4.0 },
        "maxSteps": { value: 4 },
        "resolution": { value: new THREE.Vector2(1280, 720) },
	},

	vertexShader: [

		"varying vec2 vUv;",

		"void main() {",

		"	vUv = uv;",
		"	gl_Position = projectionMatrix * modelViewMatrix * vec4( position, 1.0 );",

		"}"

	].join( "\n" ),

	fragmentShader: [

        "uniform sampler2D tDiffuse;",
        "precision highp float;",
		`
        uniform vec2 resolution;
        uniform float edgeThreshold;
        uniform float edgeCleanWidth;
        uniform int maxSteps;
        uniform float maxThresholdFactor;
        
        float edge( in vec2 tc, float thisPix ) {
            float maxThreshold = maxThresholdFactor * edgeThreshold;
            
            vec2 texel = vec2(  edgeCleanWidth / resolution.x, edgeCleanWidth / resolution.y);
            

            float xp = texture2D( tDiffuse, tc + texel * vec2(1.0 , 0.0) ).x;
            float xm = texture2D( tDiffuse, tc + texel * vec2(-1.0 , 0.0) ).x;
            float yp = texture2D( tDiffuse, tc + texel * vec2(0.0 , 1.0) ).x;
            float ym = texture2D( tDiffuse, tc + texel * vec2(0.0 , -1.0) ).x;

            float gradient = max(abs( xp - xm ), abs( yp-ym));
            if ( gradient > maxThreshold){
                thisPix = 0.0;
            } else if( gradient > edgeThreshold ){
                vec2 grad = vec2(xm-xp,ym-yp);
                grad = normalize(grad);
                bool hitZero= false;
                for (int i=0; i < maxSteps; i++){
                    if( !hitZero ){
                        float offset = float(i+2);
                        float p = texture2D( tDiffuse, tc + offset * texel * grad).x;
                        if( p == 0.0 ){
                            hitZero = true;
                        }
                    }
                }
                if( hitZero ){
                    thisPix = 0.0;
                }
            }


            return thisPix;

        }

       
        `,

		`varying vec2 vUv;

		void main() {

            float thisPix = texture2D( tDiffuse, vUv ).x;
            if( thisPix != 0.0 ){
                thisPix = edge( vUv, thisPix );
                
            }
			gl_FragColor = vec4( vec3( thisPix, thisPix, thisPix ), 1.0 );

		}`

	].join( "\n" )

};
