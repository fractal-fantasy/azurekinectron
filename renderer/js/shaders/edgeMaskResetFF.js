/**
 * edge detection
 */

var edgeMaskResetShader = {

	uniforms: {

		"tDiffuse": { value: null },
        "rawDepth": { value: null },     
	},

	vertexShader: [

		"varying vec2 vUv;",

		"void main() {",

		"	vUv = uv;",
		"	gl_Position = projectionMatrix * modelViewMatrix * vec4( position, 1.0 );",

		"}"

	].join( "\n" ),

	fragmentShader: [

        "uniform sampler2D tDiffuse;",
        "precision highp float;",

		`varying vec2 vUv;

        uniform sampler2D rawDepth;

		void main() {

            float thisPix = texture2D( tDiffuse, vUv ).x;
            float thisPixRaw = texture2D( rawDepth, vUv ).x;

            if( thisPixRaw != 0.0 ){
                //thisPix = thisPix;
            } else {
                thisPix *= thisPixRaw;
            }
			gl_FragColor = vec4( vec3( thisPix, thisPix, thisPix ), 1.0 );

		}`

	].join( "\n" )

};
