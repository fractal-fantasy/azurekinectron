THREE.computeLiquidVelocityShader = {

	uniforms: {

		"velocity1": { value: null },
		"displacement": { value: null },
		"smoothDisplacement": { value: null },
		"speed": { value: 0.5 },
		"damp": { value: 0.01 },
		"tension": { value: 0.01 },
		"gravity": { value: 0.0 },
		"maxGravity": { value: 0.0 },

	},

	vertexShader: [

		"varying vec2 vUv;",

		"void main() {",

		"	vUv = uv;",
		"	gl_Position = projectionMatrix * modelViewMatrix * vec4( position, 1.0 );",

		"}"

	].join( "\n" ),

	fragmentShader: [

		`
		uniform sampler2D velocity1;
		uniform sampler2D displacement;
		uniform sampler2D smoothDisplacement; 
		uniform float speed; 
		uniform float tension; 
		uniform float damp; 
		uniform float gravity; 
		uniform float maxGravity; 

		varying vec2 vUv;

		void main() {

			vec4 v = texture2D( velocity1, vUv );
			vec4 d = texture2D( displacement, vUv );
			vec4 ds = texture2D( smoothDisplacement, vUv );

			
			float l = min(length(v),maxGravity);
			v+=vec4(0.0,l*gravity,0.0,0.0);

	

			vec4 d_dif = ds-d;
			float m = 1.0 - damp;
			v = m * (v + speed * d_dif);
			v = v - d * tension;

			gl_FragColor = v;

		}
		`

	].join( "\n" )

};
