

THREE.DenoiseDepthNew = {

    uniforms: {
            "tDiffuse": { value: null },
            "exponent": { value: 0.0 },
            "sampleRadius": { value: 4.0 },
            "amount": { value: 4.0 },
            "resolution": { value: new THREE.Vector2(1280, 720) }

    },

    vertexShader: [

        "varying vec2 vUv;",
        "varying vec3 vNormal;",

        "void main() {",

        "    vUv = uv;",
        "    vNormal = normalMatrix * normal;",
        "   gl_Position = projectionMatrix * modelViewMatrix * vec4( position, 1.0 );",

        "}"

    ].join( "\n" ),

    fragmentShader: [

        "uniform sampler2D tDiffuse;",
        "uniform float exponent;",
        "uniform float amount;",
        "uniform float sampleRadius;",
        "uniform vec2 resolution;",
        "varying vec2 vUv;",

        "void main() {",

        "    vec4 center = texture2D(tDiffuse, vUv);",
        "    vec4 color = vec4(0.0);",
        "    float total = 0.0;",
        "    for (float x = -amount; x <= amount; x += sampleRadius) {",
        "        for (float y = -amount; y <= amount; y +=sampleRadius) {",
        "            vec4 sampleVariable = texture2D(tDiffuse, vUv + vec2(x, y) / resolution);",
        "            float weight = 1.0 - abs(dot(sampleVariable.rgb - center.rgb, vec3(0.25)));",
        "            weight = pow(weight, exponent);",
        "            color += sampleVariable * weight;",
        "            total += weight;",
        "        }",
        "    }",
        "    gl_FragColor = color / total;",
        "}"

    ].join( "\n" ),
}

// var renderTargetParameters = {
//     minFilter: THREE.NearestFilter,
//     magFilter: THREE.NearestFilter,
//     format: THREE.RedFormat,
//     type: THREE.FloatType,
//     stencilBuffer: false
// };




