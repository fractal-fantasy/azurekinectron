/**
 * @author alteredq / http://alteredqualia.com/
 *
 * Blend two textures
 */

 THREE.sceneFlowShader2 = {

	uniforms: {

		"tDiffuse": { value: null },

	},

	vertexShader: [

		"varying vec2 vUv;",

		"void main() {",

		"	vUv = uv;",
		"	gl_Position = projectionMatrix * modelViewMatrix * vec4( position, 1.0 );",

		"}"

	].join( "\n" ),

	fragmentShader: [
		
		"varying vec2 vUv;",
		"uniform sampler2D tDiffuse;",

		"void main() {",

		"	float texel1 = texture2D( tDiffuse, vUv ).x;",
		"	float texel2 = texture2D( tDiffuse, vUv ).y;",
		"	float texel3 = texture2D( tDiffuse, vUv ).z;",
		"	float texel4 = texture2D( tDiffuse, vUv ).w;",
		// "	float alpha = texture2D( tDiffuse, vUv ).x;",
		"	gl_FragColor = vec4(texel2,texel3,texel4,1.0);",
		"}"

	].join( "\n" )

};
