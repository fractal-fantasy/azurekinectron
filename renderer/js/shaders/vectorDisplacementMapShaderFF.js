THREE.vectorDisplacementMapShader = {

	uniforms: {

		"processedDepth": { value: null },
		"precomputedNormals": { value: null },
		"displacementVector": { value: null },
		"depthScale":{ value: null },
		"rippleAmplitude":{ value: 0.0 },
		"rippleSpeed":{ value: 3.0 },
		"rippleFrequency":{ value: 26.0 },
		"time":{ value: null },
		"dotAmplitude":{ value: 0.0 },
		"dotSize":{ value: 0.1 },
		"dotFrequency":{ value: 13.0},
		"dotExponent":{ value: 1.0},
		"noiseFrequency":{ value: 1.0},
		"noiseAmplitude":{ value: 0.0},
		"noiseSpeed":{ value: 0.1},
		"noiseWormAmplitude":{ value: 0.0},
		"noiseCellAmplitude":{ value: 0.0},
		"noiseRodAmplitude":{ value: 0.0},
		"noiseVariations":{ value: false},
		// "fixEdgeNormals":{ value: true},
		"useNoiseToSmearDots":{ value: false},
		"smearAmplitude":{ value: 0.0},
		"smearFrequency":{ value: 0.2},
		"maskThreshold":{ value: 0.0},
		"noiseMask":{ value: 0.0},
		"maskFrequency":{ value: 2.0},
		"FXScale":{ value: 1.0},
		"maskSpeed":{ value: 0.0},
		"quantizeDepth":{ value: false},
		"quantizeDepthFrequency":{ value: 13.0},
		"displacementFatness":{ value: 0.0},
		"normalBias":{ value: 0.0},
		"quantizeDotsNormal":{ value: false},
		"staggerDots":{ value: false},
		"useDotsForDepth":{ value: false},
		"displaceAlongZ":{ value: false},
		"depthFXOnBackground":{ value: false},
		"oldNormals":{ value: false},
		"liquid":{ value: false},
		
		
		"resolution": { value: new THREE.Vector2(1280,720) },

	},

	vertexShader: [

		"varying vec2 vUv;",

		"void main() {",

		"	vUv = uv;",
		"	gl_Position = projectionMatrix * modelViewMatrix * vec4( position, 1.0 );",

		"}"

	].join( "\n" ),

	fragmentShader: [

		`
		//CALCULATE NORMALS (we need it for ripple)
		uniform vec2 resolution;
		uniform float noiseWormAmplitude;
		uniform float noiseCellAmplitude;
		uniform float noiseRodAmplitude;
		uniform bool noiseVariations;
		uniform sampler2D processedDepth;
		uniform sampler2D displacementVector;
		uniform sampler2D precomputedNormals;
		uniform float rippleFrequency, rippleAmplitude, rippleSpeed;
		uniform float time;
		uniform float depthScale;
		uniform bool depthFXOnBackground;
		

		uniform float dotAmplitude;
		uniform bool quantizeDotsNormal;
		uniform bool staggerDots;
		uniform float dotSize;
		uniform float dotFrequency;
		uniform float dotExponent;
		uniform float noiseFrequency;
		uniform float noiseAmplitude;
		uniform float smearFrequency;
		uniform float smearAmplitude;
		uniform float noiseSpeed;
		// uniform bool fixEdgeNormals;
		uniform bool useNoiseToSmearDots;
		uniform float maskFrequency;
		uniform float noiseMask;
		uniform float maskThreshold;
		uniform float maskSpeed;
		uniform float FXScale;
		uniform float normalBias;
		uniform bool oldNormals;

		uniform float displacementFatness;
		uniform bool quantizeDepth;
		uniform float quantizeDepthFrequency;
		uniform bool useDotsForDepth;
		uniform bool displaceAlongZ;
		uniform bool liquid;


		// vec3 getFixedNormal (vec2 uv, sampler2D depthMap, sampler2D normalMap)
        //     {
        //         float d = texture2D( depthMap, uv).x;
        //         vec3  n = texture2D( normalMap, uv).xyz;
        //         if(d > 0.0){
        //             float aspect = resolution.x/resolution.y;
        //             float dx = 2.0 / resolution.x;
        //             float dy = dx * aspect;
            
        //             float d1 = texture2D( depthMap, uv - vec2(dx, 0.0)).x;
        //             float d2 = texture2D( depthMap, uv + vec2(dx, 0.0)).x;
        //             float d3 = texture2D( depthMap, uv - vec2(0.0, dy)).x;
        //             float d4 = texture2D( depthMap, uv + vec2(0.0, dy)).x;

        //             // if any neighbor depth is zero depth then set normal to average of all non-zero neighbors
        //             if( d1 == 0.0 || d2 == 0.0 || d3 == 0.0 || d4 == 0.0 ){
        //                 vec3 norm = vec3(0.0,0.0,0.0);
        //                 float weight = 0.0;
        //                 if( d1 != 0.0 ){
        //                     norm += texture2D( normalMap, uv - vec2(dx, 0.0)).xyz;
        //                     weight += 1.0;
        //                 } else if( d2 != 0.0 ){
        //                     norm += texture2D( normalMap, uv + vec2(dx, 0.0)).xyz;
        //                     weight += 1.0;
        //                 } else if( d3 != 0.0 ){
        //                     norm += texture2D( normalMap, uv - vec2(0.0, dy)).xyz;
        //                     weight += 1.0;
        //                 } else if( d4 != 0.0 ){
        //                     norm += texture2D( normalMap, uv + vec2(0.0, dy)).xyz;
        //                     weight += 1.0;
        //                 }
        //                 if( weight != 0.0 ){
        //                     n = norm/weight;
        //                 }
        //             }         
                
        //         }
        //         return( n );
        //     }


		
		float ripple(vec2 uv){

			return (
				(
					sin ( uv[1] * rippleFrequency + rippleSpeed * time)   //horizontal
				//	+ sin ( uv[0] * rippleFrequency + rippleSpeed * time) //vertical (optional)
					+ sin ( (uv[0] + uv[1]) * rippleFrequency*0.5 + rippleSpeed * time)  //diagonal
				) * rippleAmplitude
			); 
			

		}

		float dots(vec2 uv){
			uv *= dotFrequency;

			//STAGGER THE DOTS

			if(staggerDots && mod(uv[1],2.0) > 1.0){
				uv[0]+=0.5;
			}


			uv[0] = uv[0]-float(int(uv[0]))-0.5;
			uv[1] = uv[1]-float(int(uv[1]))-0.5;
			float d = sqrt(uv[0]*uv[0] + uv[1]*uv[1]);
			if (d < dotSize){
				d = pow(1.0 - d/dotSize,dotExponent*dotExponent);
			}else{
				d = 0.0;
			}
			return d;	
		}

		vec2 quantizeDotUV(vec2 uv){
			uv *= dotFrequency;
			bool didStagger = false;
			if(staggerDots && mod(uv[1],2.0) > 1.0){
				didStagger = true;
				uv[0]+=0.5;
			}
			uv[0] =float(int(uv[0]));
			uv[1] =float(int(uv[1]));
			if(didStagger){
				uv[0]-=0.5;
			}
			uv /= dotFrequency;
			return( uv );
		}
		
		// Fork of "3d simplex noise" by nikat. https://shadertoy.com/view/XsX3zB
		// 2020-06-17 16:05:37

		/* https://www.shadertoy.com/view/XsX3zB
		*
		* The MIT License
		* Copyright © 2013 Nikita Miropolskiy
		* 
		* ( license has been changed from CCA-NC-SA 3.0 to MIT
		*
		*   but thanks for attributing your source code when deriving from this sample 
		*   with a following link: https://www.shadertoy.com/view/XsX3zB )
		*
		* ~
		* ~ if you're looking for procedural noise implementation examples you might 
		* ~ also want to look at the following shaders:
		* ~ 
		* ~ Noise Lab shader by candycat: https://www.shadertoy.com/view/4sc3z2
		* ~
		* ~ Noise shaders by iq:
		* ~     Value    Noise 2D, Derivatives: https://www.shadertoy.com/view/4dXBRH
		* ~     Gradient Noise 2D, Derivatives: https://www.shadertoy.com/view/XdXBRH
		* ~     Value    Noise 3D, Derivatives: https://www.shadertoy.com/view/XsXfRH
		* ~     Gradient Noise 3D, Derivatives: https://www.shadertoy.com/view/4dffRH
		* ~     Value    Noise 2D             : https://www.shadertoy.com/view/lsf3WH
		* ~     Value    Noise 3D             : https://www.shadertoy.com/view/4sfGzS
		* ~     Gradient Noise 2D             : https://www.shadertoy.com/view/XdXGW8
		* ~     Gradient Noise 3D             : https://www.shadertoy.com/view/Xsl3Dl
		* ~     Simplex  Noise 2D             : https://www.shadertoy.com/view/Msf3WH
		* ~     Voronoise: https://www.shadertoy.com/view/Xd23Dh
		* ~ 
		*
		*/

		/* discontinuous pseudorandom uniformly distributed in [-0.5, +0.5]^3 */
		vec3 random3(vec3 c) {
			float j = 4096.0*sin(dot(c,vec3(17.0, 59.4, 15.0)));
			vec3 r;
			r.z = fract(512.0*j);
			j *= .125;
			r.x = fract(512.0*j);
			j *= .125;
			r.y = fract(512.0*j);
			return r-0.5;
			//return normalize(r-0.5)*.5;
		}

		/* skew constants for 3d simplex functions */
		const float F3 =  0.3333333;
		const float G3 =  0.1666667;

		/* 3d simplex noise */
		vec4 simplex3d(vec3 p) {
			/* 1. find current tetrahedron T and it's four vertices */
			/* s, s+i1, s+i2, s+1.0 - absolute skewed (integer) coordinates of T vertices */
			/* x, x1, x2, x3 - unskewed coordinates of p relative to each of T vertices*/
			
			/* calculate s and x */
			vec3 s = floor(p + dot(p, vec3(F3)));
			vec3 x = p - s + dot(s, vec3(G3));
			
			/* calculate i1 and i2 */
			vec3 e = step(vec3(0.0), x - x.yzx);
			vec3 i1 = e*(1.0 - e.zxy);
			vec3 i2 = 1.0 - e.zxy*(1.0 - e);
				
			/* x1, x2, x3 */
			vec3 x1 = x - i1 + G3;
			vec3 x2 = x - i2 + 2.0*G3;
			vec3 x3 = x - 1.0 + 3.0*G3;
			
			/* 2. find four surflets and store them in d */
			vec4 w, d;
			
			/* calculate surflet weights */
			w.x = dot(x, x);
			w.y = dot(x1, x1);
			w.z = dot(x2, x2);
			w.w = dot(x3, x3);
			
			/* w fades from 0.6 at the center of the surflet to 0.0 at the margin */
			w = max(0.6 - w, 0.0);
			
			/* calculate surflet components */
			d.x = dot(random3(s), x);
			d.y = dot(random3(s + i1), x1);
			d.z = dot(random3(s + i2), x2);
			d.w = dot(random3(s + 1.0), x3);
			
			/* multiply d by w^4 */
			w *= w;
			w *= w;
			d *= w;
			
			/* 3. return the four surflets */
			//return dot(d, vec4(52.0));
			return 70.*d;
		}

		/* const matrices for 3d rotation */
		const mat3 rot1 = mat3(-0.37, 0.36, 0.85,-0.14,-0.93, 0.34,0.92, 0.01,0.4);
		const mat3 rot2 = mat3(-0.55,-0.39, 0.74, 0.33,-0.91,-0.24,0.77, 0.12,0.63);
		const mat3 rot3 = mat3(-0.71, 0.52,-0.47,-0.08,-0.72,-0.68,-0.7,-0.45,0.56);
		
		vec4 variations(vec4 n) {
			vec4 an = abs(n);
			vec4 s = vec4(
				dot( n, vec4(1.) ),
				dot( an,vec4(1.) ),
				length(n),
				max(max(max(an.x, an.y), an.z), an.w) );
			
			float t =.45;
			//if(iMouse.xy!=vec2(0))
			//t = iMouse.x/iResolution.x;
			
			return vec4(
				// worms
				max(0., 1.25*( s.y*t-abs(s.x) )/t),
				// cells
				pow( (1.+t)*( (1.-t)+(s.y-s.w/t)*t), 2.), //step( .7, (1.+t)*( (1.-t)+(s.y-s.w/t)*t) ),
				.75*s.y,
				.5+.5*s.x);
		}

		/* directional artifacts can be reduced by rotating each octave */
		vec4 simplex3d_fractal(vec3 m) {
			return   0.5333333*variations( simplex3d(m*rot1) 	 )
					+0.2666667*variations( simplex3d(2.0*m*rot2) )
					+0.1333333*variations( simplex3d(4.0*m*rot3) )
					+0.0666667*variations( simplex3d(8.0*m) 	 );
		}

		float myFractal(vec3 m, float frequency, float amplitude){
			vec4 s;
			if (noiseVariations){
				s=variations( simplex3d(m*frequency) );
			} else {
				s=simplex3d_fractal(m*frequency);
			}
			return (s.x*noiseWormAmplitude+s.y*noiseCellAmplitude+s.z*noiseRodAmplitude)*amplitude;
		} 


		vec3 biasNormal( vec3 n, float bias ){
			if(bias > 0.0){
				n = vec3(n.x,n.y,n.z*(1.0-pow(bias,0.5)));
				 if (!oldNormals){
					n = normalize(n);
				 }
			}else {
				bias = 1.0-pow(-bias, 0.5);
				n = vec3(n.x*bias, n.y*bias, n.z);
				 if (!oldNormals){
					n = normalize(n);
				 }
			}
			return n;
		}




		varying vec2 vUv;

		void main() {

			vec2 depthUv= vUv;
			if( quantizeDepth ){
				depthUv *= quantizeDepthFrequency;
				depthUv.x = float(int(depthUv.x));
				depthUv.y = float(int(depthUv.y));
				depthUv /= quantizeDepthFrequency;
			}



			
			vec4 displacementVectorTexture = vec4(0.0,0.0,0.0,0.0);

			if (liquid){
				displacementVectorTexture = texture2D( displacementVector, vUv );
			}
			


			float depthValue = texture2D( processedDepth, depthUv ).x;

			vec3 deltaNormal;

			deltaNormal = texture2D(precomputedNormals, vUv).xyz;

			// if(fixEdgeNormals){
			// 	deltaNormal = getFixedNormal(vUv, processedDepth, precomputedNormals);
			// }

			vec3 displaceNormal = deltaNormal;
			if( displaceAlongZ ){
				displaceNormal = vec3(0.0,0.0,1.0);
			} else {

				if (normalBias != 0.0){
					displaceNormal = biasNormal( displaceNormal, normalBias );
				}

			}

			vec3 displacement = vec3(0.0,0.0,0.0);
			vec2 uv2 = vUv;
			if (smearAmplitude!=0.0){
				//Offset by fractal noise
				float f = myFractal(vec3(vUv[0],vUv[1],time*noiseSpeed),smearFrequency,smearAmplitude)*0.001;
				uv2 = vUv + vec2(f,f) ;
			}
			if (rippleAmplitude != 0.0){
				displacement+=ripple(uv2)*displaceNormal;
				if( displaceAlongZ ){
					vec2 rUv = vec2(uv2.y, uv2.x);
					displacement+=ripple(rUv)*vec3(1.0,1.0,0.0);
				}
			}

			
			if(useDotsForDepth){
				if( quantizeDotsNormal ){
					depthValue = dots(uv2) * texture2D( processedDepth, quantizeDotUV(vUv) ).x;

				} else {
					depthValue *= dots(uv2);
				}
			} else if (dotAmplitude != 0.0){	
				vec3 dotNormal = displaceNormal;
				if( quantizeDotsNormal ){
					vec2 dotUV = quantizeDotUV(vUv);
					dotNormal = texture2D(precomputedNormals, dotUV).xyz;
				}
				displacement+=dots(uv2)*dotAmplitude*dotNormal;
			}
 

			vec4 depthTexture = vec4(0.0, 0.0, depthValue, 1.0) *depthScale;

			if (noiseAmplitude != 0.0){
				displacement+=myFractal(vec3(uv2[0],uv2[1],time*noiseSpeed),noiseFrequency,noiseAmplitude)*displaceNormal;
			}

			
			if (noiseMask != 0.0){
				float mask = myFractal(vec3(uv2[0],uv2[1],time*maskSpeed),maskFrequency,noiseMask);
				mask -= maskThreshold*mask;
				mask = 1.0 - abs(mask);
				mask = max(mask,0.0);
				displacement *= mask;
			} 

			if( displacementFatness != 0.0){
				displacement+=deltaNormal*displacementFatness;
			}
			displacement *= FXScale;

			gl_FragColor = depthTexture + displacementVectorTexture + vec4(displacement,0.0);


			//Take this out if you want background plane to have displacement (it wont cut background if you take out)
			if (depthFXOnBackground){
				if (depthValue == 0.0){
					gl_FragColor *= 0.0;
				}
			}
			
			gl_FragColor.w = 1.0;


		}
		`

	].join( "\n" )

};
