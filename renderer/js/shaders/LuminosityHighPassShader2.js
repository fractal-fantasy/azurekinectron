/**
 * @author bhouston / http://clara.io/
 *
 * Luminosity
 * http://en.wikipedia.org/wiki/Luminosity
 */

THREE.LuminosityHighPassShader = {

    shaderID: "luminosityHighPass",

    uniforms: {

        "tDiffuse": { type: "t", value: null },
        "luminosityThreshold": { type: "f", value: 1.0 },
        "smoothWidth": { type: "f", value: 1.0 },
        "defaultColor": { type: "c", value: new THREE.Color( 0x000000 ) },
        "defaultOpacity":  { type: "f", value: 0.0 },

    },

    vertexShader: [

        "varying vec2 vUv;",

        "void main() {",

        "vUv = uv;",

        "vec4 mvPosition = modelViewMatrix * vec4( position, 1.0 );",
        "gl_Position = projectionMatrix * mvPosition;",

        "}"

    ].join("\n"),

    fragmentShader: [

        "uniform sampler2D tDiffuse;",
        "uniform vec3 defaultColor;",
        "uniform float defaultOpacity;",
        "uniform float luminosityThreshold;",
        "uniform float smoothWidth;",

        `// expects values in the range of [0,1]x[0,1], returns values in the [0,1] range.
        // do not collapse into a single function per: http://byteblacksmith.com/improvements-to-the-canonical-one-liner-glsl-rand-for-opengl-es-2-0/
        #define PI 3.14159265359
        highp float rand( const in vec2 uv ) {
	        const highp float a = 12.9898, b = 78.233, c = 43758.5453;
	        highp float dt = dot( uv.xy, vec2( a,b ) ), sn = mod( dt, PI );
	        return fract(sin(sn) * c);
        }        
        // based on https://www.shadertoy.com/view/MslGR8
	    vec3 dithering( vec3 color ) {
            //Calculate grid position
		    float grid_position = rand( gl_FragCoord.xy );

            //Shift the individual colors differently, thus making it even harder to see the dithering pattern
            vec3 dither_shift_RGB = vec3( 0.25 / 255.0, -0.25 / 255.0, 0.25 / 255.0 );

            //modify shift acording to grid position.
            dither_shift_RGB = mix( 2.0 * dither_shift_RGB, -2.0 * dither_shift_RGB, grid_position );

            //shift the color by dither_shift
            return color + dither_shift_RGB;
        }`,

        "varying vec2 vUv;",

        "void main() {",

        "vec4 texel = texture2D( tDiffuse, vUv );",

        "vec3 luma = vec3( 0.299, 0.587, 0.114 );",

        "float v = dot( texel.xyz, luma );",

        "vec4 outputColor = vec4( defaultColor.rgb, defaultOpacity );",

        "float alpha = smoothstep( luminosityThreshold, luminosityThreshold + smoothWidth, v );",

        "gl_FragColor = mix( outputColor, texel, alpha );",

        "}"

    ].join("\n")

};
