THREE.computeLiquidDisplacementShader = {

	uniforms: {

		"displacement": { value: null },
		"velocity": { value: null },
		"emission": { value: null },

	},

	vertexShader: [

		"varying vec2 vUv;",

		"void main() {",

		"	vUv = uv;",
		"	gl_Position = projectionMatrix * modelViewMatrix * vec4( position, 1.0 );",

		"}"

	].join( "\n" ),

	fragmentShader: [

		`
		uniform sampler2D displacement;
		uniform sampler2D velocity;
		uniform sampler2D emission; 

		varying vec2 vUv;

		void main() {

			vec4 texel1 = texture2D( displacement, vUv );
			vec4 texel2 = texture2D( velocity, vUv );
			vec4 texel3 = texture2D( emission, vUv );

			gl_FragColor = texel1 + texel2 + texel3;

		}
		`

	].join( "\n" )

};
