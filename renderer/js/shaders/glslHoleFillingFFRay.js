
var glslHoleFillingRayShader = {

    // shoots out rays from empty points to see if they hit anything within a radius
    // if all the rays hit a non-0.0 pixel then we fill with the average of the hit rays. 

    //play with COUNT (raycount)

    ////TODO: add slider for rayCount (tricky: need to dynamically create local array, in fragmentshader)

	uniforms: {

		"tDiffuse": { value: null },
        "hf_circleRadius": { value: 1.0 },
        "hf_sampleCount": { value: 6 },
        "step": { value: new THREE.Vector2( 1.0 / 1280.0, 1.0 / 720.0 ) },
        "hf_pixelsPerStep": { value: 1.0 },


	},

	vertexShader: [

		"varying vec2 vUv;",

		"void main() {",

		"	vUv = uv;",
		"	gl_Position = projectionMatrix * modelViewMatrix * vec4( position, 1.0 );",

		"}"

	].join( "\n" ),

	fragmentShader: [

        "uniform sampler2D tDiffuse;",
		`
        uniform vec2 step;
        uniform float hf_circleRadius;
        uniform int hf_sampleCount;
        uniform float hf_pixelsPerStep;

        float FillHole(sampler2D tex, vec2 uv)
        {
            
            bool blackPixelDetected = false;
            float total = 0.0;
            const int maxRayCount = 100;
            vec2 da[maxRayCount];
            bool hit[maxRayCount];

            //so if you move slider past maxRayCount it wont crash
            int rayCount = min(hf_sampleCount, maxRayCount);

            //INITIALIZE RAY ARRAY
            for ( int i=0; i<rayCount; i++ ){ 
                float u = 2.0*3.14*float(i)/float(rayCount);
                hit[i] = false;    
                da[i].x = sin(u);
                da[i].y = cos(u);
            }

            int steps = int(hf_circleRadius / hf_pixelsPerStep );
            bool allDone = false;
            float totalWeight = 0.0;

            vec2 voff= step * hf_pixelsPerStep;
            for ( int j=0; j<steps; j++ ){
                for ( int i=0; i<rayCount; i++ ){ 
                    if (!hit[i]){               
                        float neighbor = texture2D( tex, uv+da[i] * float(j+1) * voff ).x;
                        if (neighbor != 0.0){
                            hit[i] = true;
                            total+=neighbor;
                            totalWeight+=1.0;
                        }
                    }
                }
            }

            for ( int i=0; i<rayCount; i++ ){ 
                if (hit[i]==false){
                    blackPixelDetected = true;
                } 
            }


            float thisPix = 0.0;
            if(!blackPixelDetected){
                 thisPix = total/totalWeight;

                //thisPix = 1.0;
            }
            return thisPix;
        }

        `,

		`varying vec2 vUv;

		void main() {

            float thisPix = texture2D( tDiffuse, vUv ).x;

             if ( thisPix == 0.0 ) {

                     thisPix = FillHole( tDiffuse, vUv );

             }

			gl_FragColor = vec4( vec3( thisPix ), 1.0 );

		}`

	].join( "\n" )

};
