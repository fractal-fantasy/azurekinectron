/**
 * edge detection
 */

var glslSharpEdgeShader = {

	uniforms: {

		"tDiffuse": { value: null },
        "sharpEdgeWidth": { value: 1.0 },    
        "resolution": { value: new THREE.Vector2(1280, 720) },    
	},

	vertexShader: [

		"varying vec2 vUv;",

		"void main() {",

		"	vUv = uv;",
		"	gl_Position = projectionMatrix * modelViewMatrix * vec4( position, 1.0 );",

		"}"

	].join( "\n" ),

	fragmentShader: [

        "uniform sampler2D tDiffuse;",
        "precision highp float;",
		`
        uniform float sharpEdgeWidth;
        uniform vec2 resolution;

        mat3 G[2];

        const mat3 g0 = mat3( 1.0, 2.0, 1.0, 0.0, 0.0, 0.0, -1.0, -2.0, -1.0 );
        const mat3 g1 = mat3( 1.0, 0.0, -1.0, 2.0, 0.0, -2.0, 1.0, 0.0, -1.0 );

        float edge( in vec2 tc ) {

            mat3 I;
            float cnv[2];
            
            G[0] = g0;
            G[1] = g1;

            vec2 texel = vec2( sharpEdgeWidth * 1.0 / resolution.x, sharpEdgeWidth * 1.0 / resolution.y);

            // fetch the 3x3 neighbourhood and use the RGB vector's length as intensity value 
            for (float i=0.0; i<3.0; i++)
                for (float j=0.0; j<3.0; j++) {
                    
                    float sample_pixel = texture2D( tDiffuse, tc + texel * vec2(i-1.0,j-1.0) ).x;
                    I[int(i)][int(j)] = sample_pixel;
                }

            // calculate the convolution values for all the masks
            for (int i=0; i<2; i++) {
                float dp3 = dot(G[i][0], I[0]) + dot(G[i][1], I[1]) + dot(G[i][2], I[2]);
                
                cnv[i] = dp3 * dp3;
                
            }

            return 0.5 * sqrt(cnv[0] * cnv[0] + cnv[1] * cnv[1]);

        }

        
        `,

		`varying vec2 vUv;

		void main() {

            float thisPix = texture2D( tDiffuse, vUv ).x;

            float edgePix = edge( vUv );

            if ( edgePix > 0.5 ) thisPix = 0.0;

			gl_FragColor = vec4( vec3( thisPix, thisPix, edgePix ), 1.0 );

		}`

	].join( "\n" )

};
