
// fills vertical cracks only, since sensors are side-to-side, vertical felt right.
// big radiused fill between fingers though so only good for small cracks

var glslHoleFillingHorizontalShader = {

	uniforms: {

		"tDiffuse": { value: null },
        "hf_radius": { value: 1.0 },
        "hf_sampleCount": { value: 6 },
        "step": { value: new THREE.Vector2( 1.0 / 1280.0, 1.0 / 720.0 ) }

	},

	vertexShader: [

		"varying vec2 vUv;",

		"void main() {",

		"	vUv = uv;",
		"	gl_Position = projectionMatrix * modelViewMatrix * vec4( position, 1.0 );",

		"}"

	].join( "\n" ),

	fragmentShader: [

        "uniform sampler2D tDiffuse;",
		`
        uniform vec2 step;
        uniform float hf_radius;
        uniform int hf_sampleCount;

        float FillHole(sampler2D tex, vec2 uv)
        {
            
            bool blackPixelDetected = false;
            float total = 0.0;
            vec2 d;
            // CHECK RADIUS FOR BLACK PIXELS
            // for (int i=0; i < hf_sampleCount; i++ ){
            //     if(!blackPixelDetected){
            //         float u = 2.0*3.14*float(i)/float(hf_sampleCount);
            //         d.x = sin(u)*hf_radius;
            //         d.y = cos(u)*hf_radius;
            //         float neighbor = texture2D( tex, uv+d  * step).x;
            //         if( neighbor == 0.0 ){
            //             blackPixelDetected = true;
            //         }
            //         total+= neighbor;
            //     }

            // }

            float thisPix = 0.0;
            d.x = hf_radius;
            d.y = 0.0;

            float neighbor = texture2D( tex, uv+d  * step).x;
            float neighbor2 = texture2D( tex, uv+d  * -step).x;

            if(neighbor != 0.0 && neighbor2 != 0.0){
                thisPix = (neighbor+neighbor2)*0.5;
            }

            // float thisPix = 0.0;
            // if(!blackPixelDetected){
            //      thisPix = total/float(hf_sampleCount);

            //     //thisPix = 1.0;
            // }
            return thisPix;
        }

        `,

		`varying vec2 vUv;

		void main() {

            float thisPix = texture2D( tDiffuse, vUv ).x;

             if ( thisPix == 0.0 ) {

                     thisPix = FillHole( tDiffuse, vUv );

             }

			gl_FragColor = vec4( vec3( thisPix ), 1.0 );

		}`

	].join( "\n" )

};
