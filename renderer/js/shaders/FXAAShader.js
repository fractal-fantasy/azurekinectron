/**
 * NVIDIA FXAA by Timothy Lottes
 * http://timothylottes.blogspot.com/2011/06/fxaa3-source-released.html
 * - WebGL port by @supereggbert
 * http://www.glge.org/demos/fxaa/
 */

THREE.FXAAShader = {

	uniforms: {

		'tDiffuse': { value: null },
		'resolution': { value: new THREE.Vector2( 1 / 1024, 1 / 512 ) },
		'resMult': { value: new THREE.Vector2( 1, 1 ) },
		'fxaaWidth':{ value: 1.0 },
		'reduceMul':{ value: 1.0 },
		'reduceMin':{ value: 1.0 },
		'fxaaRadius':{ value: 1.0 }


	},

	vertexShader: [

		'varying vec2 vUv;',

		'void main() {',

		'	vUv = uv;',
		'	gl_Position = projectionMatrix * modelViewMatrix * vec4( position, 1.0 );',

		'}'

	].join( '\n' ),

	fragmentShader: [

		"precision highp float;",

		"uniform sampler2D tDiffuse;",
		"uniform vec2 resolution;",
		"uniform vec2 resMult;",
		"uniform float fxaaWidth;",
		"uniform float reduceMin;",
		"uniform float reduceMul;",
		"uniform float fxaaRadius;",

		"varying vec2 vUv;",

		"#define FXAA_REDUCE_MIN   (1.0/128.0)",
		"#define FXAA_REDUCE_MUL   (1.0/8.0)",
		"#define FXAA_SPAN_MAX     8.0",

		"void main() {",

			"vec3 rgbNW = texture2D( tDiffuse, ( gl_FragCoord.xy + fxaaRadius * vec2( -1.0, -1.0 )  ) * resolution ).xyz;",
			"vec3 rgbNE = texture2D( tDiffuse, ( gl_FragCoord.xy + fxaaRadius * vec2( 1.0, -1.0 )   ) * resolution ).xyz;",
			"vec3 rgbSW = texture2D( tDiffuse, ( gl_FragCoord.xy + fxaaRadius * vec2( -1.0, 1.0 )   ) * resolution ).xyz;",
			"vec3 rgbSE = texture2D( tDiffuse, ( gl_FragCoord.xy + fxaaRadius * vec2( 1.0, 1.0 )   ) * resolution ).xyz;",
			"vec4 rgbaM  = texture2D( tDiffuse,  gl_FragCoord.xy  * resolution );",
			"vec3 rgbM  = rgbaM.xyz;",
			"float opacity  = rgbaM.w;",

			"vec3 luma = vec3( 0.299, 0.587, 0.114 );",

			"float lumaNW = dot( rgbNW, luma );",
			"float lumaNE = dot( rgbNE, luma );",
			"float lumaSW = dot( rgbSW, luma );",
			"float lumaSE = dot( rgbSE, luma );",
			"float lumaM  = dot( rgbM,  luma );",
			"float lumaMin = min( lumaM, min( min( lumaNW, lumaNE ), min( lumaSW, lumaSE ) ) );",
			"float lumaMax = max( lumaM, max( max( lumaNW, lumaNE) , max( lumaSW, lumaSE ) ) );",

			"vec2 dir;",
			"dir.x = -((lumaNW + lumaNE) - (lumaSW + lumaSE));",
			"dir.y =  ((lumaNW + lumaSW) - (lumaNE + lumaSE));",

			"float dirReduce = max( ( lumaNW + lumaNE + lumaSW + lumaSE ) * ( 0.25 * FXAA_REDUCE_MUL * reduceMul), FXAA_REDUCE_MIN * reduceMin );",

			"float rcpDirMin = 1.0 / ( min( abs( dir.x ), abs( dir.y ) )  * 1.0 + dirReduce );",
			"dir = min( vec2( FXAA_SPAN_MAX  *fxaaWidth,  FXAA_SPAN_MAX  *fxaaWidth),",
				  "max( vec2(-FXAA_SPAN_MAX  *fxaaWidth, -FXAA_SPAN_MAX  *fxaaWidth),",
						"dir * rcpDirMin)) * resolution * resMult;",

			"vec3 rgbA = 0.5 * (",
				"texture2D( tDiffuse, gl_FragCoord.xy  * resolution * resMult + dir * ( 1.0 / 3.0 - 0.5 ) ).xyz +",
				"texture2D( tDiffuse, gl_FragCoord.xy  * resolution * resMult + dir * ( 2.0 / 3.0 - 0.5 ) ).xyz );",

			"vec3 rgbB = rgbA * 0.5 + 0.25 * (",
				"texture2D( tDiffuse, gl_FragCoord.xy  * resolution * resMult + dir * -0.5 ).xyz +",
				"texture2D( tDiffuse, gl_FragCoord.xy  * resolution * resMult + dir * 0.5 ).xyz );",
			
			"float lumaA = dot( rgbA, luma );",
			"float lumaB = dot( rgbB, luma );",
			"opacity = 1.0;",
			"if ( ( lumaB < lumaMin ) || ( lumaB > lumaMax ) ) {",
				"gl_FragColor = vec4( rgbA, opacity );",
				// "if (lumaA > lumaM) {",
				// 	"gl_FragColor = vec4( rgbA, opacity );",
				// "} else {",
				// 	"gl_FragColor = vec4( rgbM, opacity );",
				// "}",
			"} else {",
				"gl_FragColor = vec4( rgbB, opacity );",
				// "if (lumaB > lumaM) {",
				// 	"gl_FragColor = vec4( rgbB, opacity );",
				// "} else {",
				// 	"gl_FragColor = vec4( rgbM, opacity );",
				// "}",
			"}",

		"}"	

	].join( '\n' )

};
