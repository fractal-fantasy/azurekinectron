THREE.precomputeNormalsShader = {

	uniforms: {

		"processedDepth": { value: null },	
		"resolution": { value: new THREE.Vector2(1280,720) },
		"normalZ":{ value: 1.0 },
		"lookupRadius":{ value: 2.0 },
		"normalsRatio":{ value: 2.0 },
		"normalsRatioOld":{ value: 1.0 },
		"cutSidePixels":{ value: false },
		"refine_cutSidePixels":{value:false},
		"oldNormals":{ value: false },
	},

	vertexShader: [

		"varying vec2 vUv;",

		"void main() {",

		"	vUv = uv;",
		"	gl_Position = projectionMatrix * modelViewMatrix * vec4( position, 1.0 );",

		"}"

	].join( "\n" ),

	fragmentShader: [

		`
		uniform vec2 resolution;
		uniform float normalZ;
		uniform sampler2D processedDepth;
		varying vec2 vUv;
		uniform float lookupRadius;
		uniform float normalsRatio;
		uniform float normalsRatioOld;
		uniform bool cutSidePixels;
	    uniform bool refine_cutSidePixels;
		uniform bool oldNormals;


		vec3 extractNormalOG(sampler2D map, vec2 tc) {

        	float aspect = resolution.x/resolution.y;
            float dx = lookupRadius / resolution.x;
            float dy = dx * aspect;

            vec4 dx1 = vec4(0.0);
            vec4 dx2 = vec4(0.0);
            vec4 dy1 = vec4(0.0);
            vec4 dy2 = vec4(0.0);

            //for (int i=1; i < numSteps; i++) {

                 float dxS = float(1) * dx;
                 float dyS = float(1) * dy;

                 dx1 += texture2D(map, tc + vec2(dxS, 0.0));
                 dx2 += texture2D(map, tc + vec2(-dxS, 0.0));

                 dy1 += texture2D(map, tc + vec2(0.0, dyS));
                 dy2 += texture2D(map, tc +vec2(0.0, -dyS));

                 dx1 += texture2D(map, tc +vec2(dxS*1.5, 0.0));
                 dx2 += texture2D(map, tc +vec2(-dxS*1.5, 0.0));

                 dy1 += texture2D(map, tc +vec2(0.0, dyS*1.5));
                 dy2 += texture2D(map, tc +vec2(0.0, -dyS*1.5));

            //}

            float difX = (dx2.r - dx1.r);
            float difY = (dy2.r - dy1.r);


            return vec3(difX, difY, normalZ*0.01);

        }
	

		//CYBERLIGHT NORMALS
		vec3 extractNormal(sampler2D map, vec2 tc) {

            float aspect = resolution.x/resolution.y;
            float dx = lookupRadius / resolution.x;
            float dy = dx * aspect;

			float d = texture2D(map, tc ).x;
			
			vec3 norm = vec3(0.0,0.0,1.0);

			//don't calculate normals for black pixels
			if ( d != 0.0 ){

				float dx1 = texture2D(map, tc + vec2(dx, 0.0)).x;
				float dx2 = texture2D(map, tc + vec2(-dx, 0.0)).x;
				float dy1 = texture2D(map, tc + vec2(0.0, dy)).x;
				float dy2 = texture2D(map, tc + vec2(0.0, -dy)).x;


				float sx = 1.0;
				float sy = 1.0;
			

				//if neighbours are 0.0 make it the same depth as the center
				if (cutSidePixels){
					if ( dx1 == 0.0 ){ dx1 = d; sx = 2.0;}
					if ( dx2 == 0.0 ){ dx2 = d; sx = 2.0;}
					if ( dy1 == 0.0 ){ dy1 = d; sy = 2.0;}
					if ( dy2 == 0.0 ){ dy2 = d; sy = 2.0;}
				}



				if (!refine_cutSidePixels){
					sx = 1.0;
					sy = 1.0;
				 }


				vec3 xDiff = vec3(dx,0.0,(dx1-dx2)*sx);
				vec3 yDiff = vec3(0.0,dy,(dy1-dy2)*sy);

				 float nr = normalsRatio *0.25 ;

				// TOOK THIS LINE OUT OF OUR SHADER BECAUSE IT CAUSES ONLY DEPTH NORMALS TO BEHAVE WEIRD
                // nr/=lookupRadius;

				//cross product of vectors
				norm = cross(normalize(xDiff),normalize(yDiff));
				//norm = cross(xDiff,yDiff);
				norm.z *=normalZ;
				norm *= vec3(nr,nr,1.0);
				norm = normalize(norm);
				
			}
            return norm;

        }

		void main() {

			vec3 deltaNormal;
			
			if(oldNormals){
				//change normalsRatio to normalsRatioOld for proper normals behaviour
				deltaNormal = normalsRatio * normalize( extractNormalOG( processedDepth, vUv ) );
			//	deltaNormal = normalize( extractNormalOG( processedDepth, vUv ) );
			} else {
				//change normalsRatio to normalsRatioOld for proper normals behaviour
				deltaNormal = normalize( extractNormal( processedDepth, vUv ) );
			}

			
			gl_FragColor = vec4(deltaNormal.xyz,1.0);

		}
		`

	].join( "\n" )

};
