/**
 * @author alteredq / https://alteredqualia.com/
 *
 * Barrel distortion correction shader
 * 	- based on: https://www.imaginationtech.com/blog/speeding-up-gpu-barrel-distortion-correction-in-mobile-vr/
 */

THREE.BarrelDistortionCorrectionShader = {

	uniforms: {

		"tDiffuse"	: { value: null },
		"baseColor"	: { value: new THREE.Color( 0x000000) },	/* base color */
		"alpha"		: { value: 0.0 },							/* lens parameter */
		"scale"		: { value: 1.0 }							/* re-scale to get back lost texture size after the correction */

	},

	vertexShader: [

		"varying vec2 vUv;",

		"void main() {",

		"	vUv = uv;",
		"	gl_Position = projectionMatrix * modelViewMatrix * vec4( position, 1.0 );",

		"}"

	].join( "\n" ),

	fragmentShader: [

		"uniform sampler2D tDiffuse;",

		"uniform vec3 baseColor;",
		"uniform float alpha;",
		"uniform float scale;",

		"varying vec2 vUv;",
		
		"void main() {",

			"vec4 color = vec4( baseColor, 1.0 );", 	
		
			/* Normalize to [-1, 1] */
		
			"vec2 p1 = vec2( 2.0 * vUv - 1.0 );",

			/* Rescale */
		
			"p1*= scale;",
		
			/* Transform */
		
			"vec2 p2 = p1 / ( 1.0 - alpha * length( p1 ) );",
		
			/* Back to [0, 1] */

			"p2 = ( p2 + 1.0 ) * 0.5;",
		
			"if ( all( greaterThanEqual( p2, vec2( 0.0 ) ) ) && all( lessThanEqual( p2, vec2( 1.0 ) ) ) ) {",
			
				"color = texture2D( tDiffuse, p2 );",

			"}",

			"gl_FragColor = color;",

		"}"

	].join( "\n" )

};
