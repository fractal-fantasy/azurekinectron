
 THREE.jiggleShader = {

	uniforms: {
		"sourceVelocity": { value: null },
		//"jiggleVelocity": { value: null },
		"jiggleVelocityPrev": { value: null },
		"tension": { value: 0.5 },
		"sourceVelocityScale": { value: 0.5},

	},

	vertexShader: [

		"varying vec2 vUv;",

		"void main() {",

		"	vUv = uv;",
		"	gl_Position = projectionMatrix * modelViewMatrix * vec4( position, 1.0 );",

		"}"

	].join( "\n" ),

	fragmentShader: [

		"uniform float tension;",
		//"uniform  jiggleVelocity;",
		"uniform sampler2D jiggleVelocityPrev;",
		"uniform sampler2D sourceVelocity;",
		"uniform float sourceVelocityScale;",

		"varying vec2 vUv;",

		"void main() {",

		"	vec4 source = texture2D( sourceVelocity, vUv );",
		"	vec4 prevVel = texture2D( jiggleVelocityPrev, vUv );",

		////converting 0-1 color space to -1 - 1 velocity
		// "   prevVel = (prevVel-0.5)*2.0;",
		// "   source = (source-0.5)*2.0;",

		"   prevVel = prevVel*(1.0-tension)+source*sourceVelocityScale;",

		// //TRANSFORM TO COLOR SPACE
		// "	gl_FragColor = prevVel*0.5+0.5;",

		"	gl_FragColor = prevVel;",

		"}"

	].join( "\n" )

};
