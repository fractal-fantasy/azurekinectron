/**
 * 
 * Playing with the Blend Shader to acheive temporalSmooth
 * 
 * @author alteredq / http://alteredqualia.com/
 *
 * Blend two textures
 */

THREE.temporalSmoothShader = {

	uniforms: {

		"rawDepth": { value: null },
		"rawDepthPrev": { value: null },
		"mixRatio": { value: 0.5 },
		"opacity": { value: 1.0 },
		"temporalThreshold": { value: 0.05 },
		"temporalThresholdRange":{ value: 4.0 }

	},

	vertexShader: [

		"varying vec2 vUv;",

		"void main() {",

		"	vUv = uv;",
		"	gl_Position = projectionMatrix * modelViewMatrix * vec4( position, 1.0 );",

		"}"

	].join( "\n" ),

	fragmentShader: [

		//"uniform float opacity;",
		"uniform float mixRatio;",

		"uniform sampler2D rawDepth;",
		"uniform sampler2D rawDepthPrev;",
		"uniform float temporalThreshold;",
		"uniform float temporalThresholdRange;",

		"varying vec2 vUv;",

		"void main() {",

		"	vec4 texel1 = texture2D( rawDepth, vUv );",
		"	vec4 texel2 = texture2D( rawDepthPrev, vUv );",
		"   float maxt = temporalThreshold * temporalThresholdRange;",
		"   float dif = abs(texel1.x-texel2.x);",
		"   gl_FragColor = texel1;",
		"   float mixR = mixRatio;",
		"   if( texel2.x < 0.01 ){",
		"	 	gl_FragColor = texel1;",
		"   }else if( dif < maxt ){",
		"       if(dif > temporalThreshold){",
		"			mixR *= 1.0- (dif - temporalThreshold)/(maxt - temporalThreshold);",
		"		}",
		"		gl_FragColor = mix( texel1, texel2, mixR );",
		"	}",
		// "	}else{ ",
		// "		gl_FragColor =texel1;", 
		// "	}",
		"}"

	].join( "\n" )

};
