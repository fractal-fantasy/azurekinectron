/**
 * edge detection
 */

var edgeMaskShader = {

	uniforms: {

		"tDiffuse": { value: null },   
	},

	vertexShader: [

		"varying vec2 vUv;",

		"void main() {",

		"	vUv = uv;",
		"	gl_Position = projectionMatrix * modelViewMatrix * vec4( position, 1.0 );",

		"}"

	].join( "\n" ),

	fragmentShader: [

        "uniform sampler2D tDiffuse;",
        "precision highp float;",

		`varying vec2 vUv;

		void main() {

            float thisPix = texture2D( tDiffuse, vUv ).x;
            if( thisPix != 0.0 ){
                thisPix = 1.0;
            } else {
                thisPix = 0.0;
            }
			gl_FragColor = vec4( vec3( thisPix, thisPix, thisPix ), 1.0 );

		}`

	].join( "\n" )

};
