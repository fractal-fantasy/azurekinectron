

var echoFX_IR_shader = {

    uniforms: {
        tInput1: { value: undefined },
        tInput2: { value: undefined },
        tInput3: { value: undefined },
        tInput4: { value: undefined },
        tInput5: { value: undefined },
        tInput6: { value: undefined },
        tInput7: { value: undefined },
        tInput8: { value: undefined },
        "minIR": { value: 100 },
        "maxIR": { value: 2000 }

    },

    vertexShader: `

        #version 300 es
                
        varying vec2 vUv;

        void main() {

            vUv = uv;

            gl_Position = projectionMatrix * modelViewMatrix * vec4( position, 1.0 );

        }

    `,

    fragmentShader: `

        #version 300 es

        out vec4 out_FragColor;

        precision mediump float;
        precision mediump int;
        precision mediump usampler2D;

        uniform usampler2D tInput1;
        uniform usampler2D tInput2;
        uniform usampler2D tInput3;
        uniform usampler2D tInput4;
        uniform usampler2D tInput5;
        uniform usampler2D tInput6;
        uniform usampler2D tInput7;
        uniform usampler2D tInput8;

        uniform float minIR;
        uniform float maxIR;

        varying vec2 vUv;

        void main() {

            uvec2 tex1 = texture2D(tInput1, vUv).xy;
            uvec2 tex2 = texture2D(tInput2, vUv).xy;
            uvec2 tex3 = texture2D(tInput3, vUv).xy;
            uvec2 tex4 = texture2D(tInput4, vUv).xy;
            uvec2 tex5 = texture2D(tInput5, vUv).xy;
            uvec2 tex6 = texture2D(tInput6, vUv).xy;
            uvec2 tex7 = texture2D(tInput7, vUv).xy;
            uvec2 tex8 = texture2D(tInput8, vUv).xy;

            uint depth1 = ( tex1.y << 8u ) + tex1.x;
            uint depth2 = ( tex2.y << 8u ) + tex2.x;
            uint depth3 = ( tex3.y << 8u ) + tex3.x;
            uint depth4 = ( tex4.y << 8u ) + tex4.x;
            uint depth5 = ( tex5.y << 8u ) + tex5.x;
            uint depth6 = ( tex6.y << 8u ) + tex6.x;
            uint depth7 = ( tex7.y << 8u ) + tex7.x;
            uint depth8 = ( tex8.y << 8u ) + tex8.x;                

            uint maxdepth = max( max( max( max( max( max( max(depth1, depth2), depth3), depth4), depth5), depth6), depth7), depth8);            

            float irf = float(maxdepth) / maxIR;

            out_FragColor = vec4( vec3( irf ), 1.0 );
                        
        }

    `
}

var echoFX_IR_renderTargetParams = {
    minFilter: THREE.LinearFilter,
    magFilter: THREE.LinearFilter,
    format: THREE.RGBAFormat
};

var echoFX_IR_dataTextureParams = {
    // size: DEPTH_TO_COLOR_WIDTH * DEPTH_TO_COLOR_HEIGHT * 2,
    bytesPerPixel: 2,
    format: THREE.RGIntegerFormat,
    internalFormat: 'RG8UI'
};



