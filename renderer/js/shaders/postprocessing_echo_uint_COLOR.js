

var echoFX_COLOR_shader = {

    uniforms: {
        tInput1: { value: undefined },
        tInput2: { value: undefined },
        tInput3: { value: undefined },
        tInput4: { value: undefined },
        tInput5: { value: undefined },
        tInput6: { value: undefined },
        tInput7: { value: undefined },
        tInput8: { value: undefined },

    },

    vertexShader: `

        #version 300 es
                
        varying vec2 vUv;

        void main() {

            vUv = uv;

            gl_Position = projectionMatrix * modelViewMatrix * vec4( position, 1.0 );

        }

    `,

    fragmentShader: `

        #version 300 es

        out vec4 out_FragColor;

        precision lowp float;
        precision lowp int;
        precision lowp usampler2D;

        uniform usampler2D tInput1;
        uniform usampler2D tInput2;
        uniform usampler2D tInput3;
        uniform usampler2D tInput4;
        uniform usampler2D tInput5;
        uniform usampler2D tInput6;
        uniform usampler2D tInput7;
        uniform usampler2D tInput8;

        varying vec2 vUv;

        void main() {

            uvec3 tex1 = texture2D(tInput1, vUv).zyx;   // Reverse BGR order to RGB
            uvec3 tex2 = texture2D(tInput2, vUv).zyx;
            uvec3 tex3 = texture2D(tInput3, vUv).zyx;
            uvec3 tex4 = texture2D(tInput4, vUv).zyx;
            uvec3 tex5 = texture2D(tInput5, vUv).zyx;
            uvec3 tex6 = texture2D(tInput6, vUv).zyx;
            uvec3 tex7 = texture2D(tInput7, vUv).zyx;
            uvec3 tex8 = texture2D(tInput8, vUv).zyx;

            uvec3 color = max( max( max( max( max( max( max(tex1, tex2), tex3), tex4), tex5), tex6), tex7), tex8);

            out_FragColor = vec4( float(color.x) / 255., float(color.y) / 255., float(color.z) / 255., 1.0 );
                        
        }

    `
}

var echoFX_COLOR_renderTargetParams = {
    minFilter: THREE.LinearFilter,
    magFilter: THREE.LinearFilter,
    format: THREE.RGBAFormat
};

var echoFX_COLOR_dataTextureParams = {
    // size: COLOR_WIDTH * COLOR_HEIGHT * 4,
    bytesPerPixel: 4,
    format: THREE.RGBAIntegerFormat,
    internalFormat: 'RGBA8UI'
};



