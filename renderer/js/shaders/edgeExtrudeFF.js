/**
 * edge detection
 */

var edgeExtrudeShader = {

	uniforms: {

		"tDiffuse": { value: null },
        "extrudeDepth": { value: 0.1 },     
        "rawDepth": { value: null },  
	},

	vertexShader: [

		"varying vec2 vUv;",

		"void main() {",

		"	vUv = uv;",
		"	gl_Position = projectionMatrix * modelViewMatrix * vec4( position, 1.0 );",

		"}"

	].join( "\n" ),

	fragmentShader: [

        "uniform sampler2D tDiffuse;",
        "uniform sampler2D rawDepth;",
        "precision highp float;",

		`varying vec2 vUv;

        uniform float extrudeDepth;

		void main() {

            float thisPix = texture2D( rawDepth, vUv ).x;
            float maskPix = texture2D( tDiffuse, vUv ).x;

            // if( thisPix != 0.0 ){

            //     vec2 xOff = vec2(  1.0 / 1280.0, 0.0);
            //     vec2 yOff = vec2(  0.0, 1.0 / 720.0);

            //     float px = texture2D( rawDepth, vUv + xOff ).x;
            //     float mx = texture2D( rawDepth, vUv - xOff ).x;
            //     float py = texture2D( rawDepth, vUv + yOff ).x;
            //     float my = texture2D( rawDepth, vUv - yOff ).x;

            //     float threshold = 0.000001;

            //     if( px < threshold || mx < threshold || py < threshold || my < threshold ){

            //         thisPix -= extrudeDepth;

            //     }


            // }

            thisPix -= (1.0 - maskPix)*extrudeDepth;


			gl_FragColor = vec4( vec3( thisPix, thisPix, thisPix ), 1.0 );

		}`

	].join( "\n" )

};
