
//fast waves
var a = .1,
    b = .25,
    eps = .025,
    delta = 0.,
    D = 1,
    dt = .0085,
    h = .25;
var dth2 = D * dt / (h * h);


THREE.ReactionDiffusionShader_LiquidWall = {

    uniforms: {

        "tDiffuse1": { value: null },
        "tDiffuse2": { value: null },

        "a": { value: a },
        "b": { value: b },
        "delta": { value: delta },
        "eps": { value: eps },
        "dt": { value: dt },

        "dth2": { value: dth2 },

        "stimulationStrength": { value: 5 },
        "stimulationStrengthHandPointers": { value: 5 },
        "slow": { value: 0.0 },
        "zoom_offset": { value: 0.0 },

        "blobSize": { value: 0.1 },
        "blobBlurSize": { value: 0.1 },

        "mouse_left": { value: new THREE.Vector2(0, 0.5) },
        "mouse_right": { value: new THREE.Vector2(1, 0.5) },

        "directionOn": { value: true },

        "resolution": { value: new THREE.Vector2(512, 424) },
        "time": { value: 0.0 }

    },

    vertexShader: `

		varying vec2 vUv;

		void main() {

			vUv = uv;
			gl_Position = projectionMatrix * modelViewMatrix * vec4( position, 1.0 );

		}
	`
    ,

    //fragmentShader: document.getElementById("fragmentShader_rd1").textContent

    fragmentShader: `

        // reaction-diffusion FitzHugh-Nagumo model. from http://www.thevirtualheart.org/webgl/DS_SIAM/SIAM_webgl_pattern_formation.html

        precision lowp float;
        uniform sampler2D tDiffuse1;
        uniform sampler2D tDiffuse2;
        varying vec2 vUv;

        uniform float a;
        uniform float b;
        uniform float delta;
        uniform float eps;
        uniform float dt;
        uniform float dth2;

        uniform float stimulationStrength;
        uniform float stimulationStrengthHandPointers;
        uniform float slow;

        uniform float zoom_offset;
        uniform float blobSize;
        uniform float blobBlurSize;

        uniform vec2 mouse_left;
        uniform vec2 mouse_right;

        uniform bool directionOn;

        uniform vec2 resolution;
        uniform float time;

        const int mode = 1;

        vec3 rotate(vec3 p, vec3 g){
            float a=cos(g.x),
            b=sin(g.x),
            c=cos(g.y),
            d=sin(g.y),
            e=cos(g.z),
            f=sin(g.z);
            return mat3(c*e+b*d*f,-a*f,b*c*f-d*e,c*f-b*d*e,a*e,-d*f-b*c*e,a*d,b,a*c) * p;
        }


        float point(vec2 st, vec2 pos, float size, float blur) {
	        float dist = distance(st, pos) + 0.02;
            return pow(abs(size / dist), blur);
        }

        float drawBlob( vec2 st, vec2 pos, float size, float blur, float asp ) {
           float blob = 0.0;
           if (mode == 0) {
                vec2 d = ( pos - st ) * vec2( asp, 1.0 );
                if ( length( d ) < size ) {
                    blob = 1.0;
                }
            }
            else if (mode == 1) {
                blob = clamp( point(st * vec2(asp, 1.0), pos * vec2(asp, 1.0), size, blur), 0.0, 1.0 );
            }

            return blob;
        }

        float getStimulationFromMouse(vec2 pos, vec2 st, float asp) {
            float stimul = 0.0;
            stimul = drawBlob( st, pos, blobSize, 10.0 - blobBlurSize, asp );
            return stimul;
        }

        void main(void) {

            //float aspect = resolution.x / resolution.y;  

            vec2 mouse = (mouse_left + mouse_right) / 2.0;

            float mouse_factor = 0.0075 * length( mouse - vUv );
            vec2 mouse_offset = (mouse.xy - 0.5) * mouse_factor;

            float zoom_offset_whands = zoom_offset * distance(mouse_left, mouse_right);

            vec2 mod_uv = directionOn ? (1.0 - zoom_offset_whands) * vUv + zoom_offset_whands * 0.5 - mouse_offset :
                                        (1.0 - zoom_offset) * vUv + zoom_offset * 0.5;

            //float aspect_depth = resolution.x / resolution.y;

            vec2 mouse_left_adapted = (mouse_left - 0.5) + 0.5;
            vec2 mouse_right_adapted = (mouse_right - 0.5) + 0.5;

            float mouseStimulation = getStimulationFromMouse(mouse_left_adapted, vUv, 1.0);
            mouseStimulation += getStimulationFromMouse(mouse_right_adapted, vUv, 1.0);

            vec4 t = texture2D(tDiffuse1, mod_uv);
            float u = t.r,  v = t.g;

            float du = 0.0;
            float vnew = 0.0;

            float d_x = 1.0 / resolution.x / 2.0;
            float d_y = 1.0 / resolution.y / 2.0;

            du = dt * ( u * ( 1.0 - u ) * ( u - a ) - v ) / eps;
            vnew = v + dt * ( b * u - 1.3 * v - delta );


            float dth2_loc = dt / 0.0625;

            du += (   texture2D( tDiffuse1, vec2( vUv.x, vUv.y + d_y ) ).r +
                        texture2D( tDiffuse1, vec2( vUv.x, vUv.y - d_y ) ).r +
                        texture2D( tDiffuse1, vec2( vUv.x + d_x, vUv.y ) ).r +
                        texture2D( tDiffuse1, vec2( vUv.x - d_x, vUv.y ) ).r -
                        4.0 * u ) * dth2_loc;

            if (abs(du) > 1e1) du = a;
            if (abs(vnew) > 1e1) vnew = eps;

            vec2 kinectCoord =  (vUv - 0.5) + 0.5;

            float kinectStimulation = texture2D( tDiffuse2, kinectCoord ).x;


            du += kinectStimulation * stimulationStrength * 0.01;
            du += mouseStimulation * stimulationStrengthHandPointers * 0.01;


            gl_FragColor = vec4(u + du, vnew, t.b, du );

        }`

};
