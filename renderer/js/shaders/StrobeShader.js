/**
 * yo
 */

THREE.StrobeShader = {

	uniforms: {

	    "tDiffuse": { value: null },
	    "logo": { value: null },
	    "state": { value: 0 },
	    "logoON": { value: 0 },
	    "blackoutON": { value: 0 },
	    "flipLogoX" : { value: 0 }
	},

	vertexShader: [

		"varying vec2 vUv;",

		"void main() {",

			"vUv = uv;",
			"gl_Position = projectionMatrix * modelViewMatrix * vec4( position, 1.0 );",

		"}"

	].join( "\n" ),

	fragmentShader: [

		"uniform sampler2D tDiffuse;",
        "uniform sampler2D logo;",
		"varying vec2 vUv;",
		"uniform bool state;",
        "uniform bool logoON;",
        "uniform bool flipLogoX;",
        "uniform bool blackoutON;",

		"void main(void)",
		"{",
			
            "vec4 this_pixel = vec4(0.0);",
            "vec2 logo_vUv = vUv;",
            "if (flipLogoX) logo_vUv = vec2( 1.0-vUv.x, vUv.y );",
            "vec4 logo_pixel = texture2D( logo, logo_vUv );",
            "vec4 mixx = vec4(0.0);",

            "blackoutON ? this_pixel = vec4(0.0) : this_pixel = texture2D(tDiffuse, vUv);",

            "logoON ? mixx = this_pixel + logo_pixel : mixx = this_pixel;",

            "state ? gl_FragColor = vec4(1.0) - mixx : gl_FragColor = mixx;",

		"} "

	].join( "\n" )

};
