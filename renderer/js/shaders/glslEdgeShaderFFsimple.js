/**
 * edge detection
 */

var glslEdgeShader = {

	uniforms: {

		"tDiffuse": { value: null },
        "sharpEdgeWidth": { value: 1.0 },     
        "edgeThreshold": { value: 1.0 },
        "edgeCleanWidth": { value: 1.0 },
        "resolution": { value: new THREE.Vector2(1280, 720) },
	},

	vertexShader: [

		"varying vec2 vUv;",

		"void main() {",

		"	vUv = uv;",
		"	gl_Position = projectionMatrix * modelViewMatrix * vec4( position, 1.0 );",

		"}"

	].join( "\n" ),

	fragmentShader: [

        "uniform sampler2D tDiffuse;",
        "precision highp float;",
		`
        uniform float edgeThreshold;
        uniform float edgeCleanWidth;
        uniform vec2 resolution;
        
        float edge( in vec2 tc ) {

            vec2 texel = vec2(  edgeCleanWidth / resolution.x, edgeCleanWidth / resolution.y);
            
            float xp = texture2D( tDiffuse, tc + texel * vec2(1.0 , 0.0) ).x;
            float xm = texture2D( tDiffuse, tc + texel * vec2(-1.0 , 0.0) ).x;
            float yp = texture2D( tDiffuse, tc + texel * vec2(0.0 , 1.0) ).x;
            float ym = texture2D( tDiffuse, tc + texel * vec2(0.0 , -1.0) ).x;

            float gradient = max(abs( xp - xm ), abs( yp-ym));
            return gradient;

        }

       
        `,

		`varying vec2 vUv;

		void main() {

            float thisPix = texture2D( tDiffuse, vUv ).x;

            float edgePix = edge( vUv );

            if ( edgePix > edgeThreshold ) thisPix = 0.0;

			gl_FragColor = vec4( vec3( thisPix, thisPix, edgePix ), 1.0 );

		}`

	].join( "\n" )

};
