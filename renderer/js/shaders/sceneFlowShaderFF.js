/**
 * @author alteredq / http://alteredqualia.com/
 *
 * Blend two textures
 */

 THREE.sceneFlowShader = {

	uniforms: {

		"processedDepth": { value: null },
		"processedDepthPrev": { value: null },
		"depthVelocityThreshold":{ value: 2.0 },
		"step": { value: new THREE.Vector2( 1.0 / 1280.0, 1.0 / 720.0 ) },

	},

	vertexShader: [

		"varying vec2 vUv;",

		"void main() {",

		"	vUv = uv;",
		"	gl_Position = projectionMatrix * modelViewMatrix * vec4( position, 1.0 );",

		"}"

	].join( "\n" ),

	fragmentShader: [

		"uniform vec2 step;",

		"uniform sampler2D processedDepth;",
		"uniform sampler2D processedDepthPrev;",
		"uniform float depthVelocityThreshold;",
		
		"varying vec2 vUv;",

		"void main() {",
		"	float texel1 = texture2D( processedDepth, vUv ).x;",
		"	float texel2 = texture2D( processedDepthPrev, vUv ).x;",
		"   float separation = 5.0;",
		"   float depthScale = -40.0;",
		"   vec2 dx = vec2(separation,0.0);",
		"   vec2 dy = vec2(0.0,separation);",
		"	float px = texture2D( processedDepth, vUv+dx  * step).x;",
		"	float py = texture2D( processedDepth, vUv+dy  * step).x;",
		"   vec3 Norm = vec3((px-texel1)*depthScale,(py-texel1)*depthScale,1.0);",
		"   Norm = normalize(Norm);",
		"   float dvel = (texel1-texel2)*200.0;",
		
			//depth velocity threshold
		"	if( dvel > depthVelocityThreshold ){",
		"    	dvel = depthVelocityThreshold;",
		" 	} else if( dvel < -depthVelocityThreshold ){",
		"		dvel = -depthVelocityThreshold;",
		"	}",


		"   if(texel1==0.0 || texel2==0.0){ dvel = 0.0;}",
		"   Norm = Norm*dvel;",

		// //Transform to color space
		// "   Norm = Norm * 0.5 + 0.5;",

		"	gl_FragColor = vec4(texel1,Norm.x,Norm.y, Norm.z);",
		//"	gl_FragColor = vec4(texel1,texel1,texel1,1.0);",
		"}"

	].join( "\n" )

};
