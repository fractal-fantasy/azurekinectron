/**
 * glslHoleFilling Shader: https://personal.utdallas.edu/~cxc123730/SPIE2016.pdf
 */

var glslHoleFillingShader = {

	uniforms: {

		"tDiffuse": { value: null },
        "hf_mode5x5": { value: false },
        "step": { value: new THREE.Vector2( 1.0 / 1280.0, 1.0 / 720.0 ) }

	},

	vertexShader: [

		"varying vec2 vUv;",

		"void main() {",

		"	vUv = uv;",
		"	gl_Position = projectionMatrix * modelViewMatrix * vec4( position, 1.0 );",

		"}"

	].join( "\n" ),

	fragmentShader: [

        "uniform sampler2D tDiffuse;",
		`
        uniform vec2 step;
        uniform bool hf_mode5x5;

        float FillHole3x3(sampler2D tex, vec2 uv)
        {
            float radius = 1.0;
    
            float maxNeighbor = 0.0;        

            vec2 d;
            for ( d.x = -radius; d.x <= radius; d.x++ ) {
                
                for ( d.y = -radius; d.y <= radius; d.y++ ) {

                    float neighbor = texture2D( tex, uv + d * step ).x;

                    maxNeighbor = neighbor > maxNeighbor ? neighbor : maxNeighbor;
                    
                }
            }
            return maxNeighbor;
        }

        float FillHole5x5(sampler2D tex, vec2 uv)
        {
            float radius = 2.0;
    
            float maxNeighbor = 0.0;            

            vec2 d;
            for ( d.x = -radius; d.x <= radius; d.x++ ) {
                
                for ( d.y = -radius; d.y <= radius; d.y++ ) {

                    float neighbor = texture2D( tex, uv + d * step ).x;

                    maxNeighbor = neighbor > maxNeighbor ? neighbor : maxNeighbor;
                    
                }
            }
            return maxNeighbor;
        }
        `,

		`varying vec2 vUv;

		void main() {

            float thisPix = texture2D( tDiffuse, vUv ).x;

            if ( thisPix == 0.0 ) {

                if ( hf_mode5x5 ) {

                    thisPix = FillHole5x5( tDiffuse, vUv );

                } else {

                    thisPix = FillHole3x3( tDiffuse, vUv );

                }

            }

			gl_FragColor = vec4( vec3( thisPix ), 1.0 );

		}`

	].join( "\n" )

};
