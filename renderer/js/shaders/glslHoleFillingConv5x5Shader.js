/**

 */

var glslHoleFillingConv5x5Shader = {

	uniforms: {

		"tDiffuse": { value: null },
		"hf_denominator": { value: 5.0 },
        "hf_offset": { value: 0.0 },
        "hf_min_defined": { value: 12.0 },
        "step": { value: new THREE.Vector2( 1.0 / 1280.0, 1.0 / 720.0 ) }
	},

	vertexShader: [

		"varying vec2 vUv;",

		"void main() {",

			"vUv = uv;",
			"gl_Position = projectionMatrix * modelViewMatrix * vec4( position, 1.0 );",

		"}"

	].join( "\n" ),

	fragmentShader: 
        `
		uniform sampler2D tDiffuse;
        uniform float hf_denominator;
        uniform float hf_offset;
        uniform float hf_min_defined;
        uniform vec2 step;

		varying vec2 vUv;


        const float kernel[25] = float[]( 0.250, 0.500, 0.625, 0.500, 0.250,
                                            0.500, 0.750, 1.000, 0.750, 0.500,
                                            0.625, 1.000, 0.000, 1.000, 0.625,
                                            0.500, 0.750, 1.000, 0.750, 0.500,
                                            0.250, 0.500, 0.625, 0.500, 0.250 );

        const float kernel2[25] = float[]( 0.9, 0.94, 0.95, 0.94, 0.9,
                                           0.94, 0.99, 1.0, 0.99, 0.94,
                                           0.95, 1.0, 0.0, 1.0, 0.95,
                                           0.94, 0.99, 1.0, 0.99, 0.94,
                                           0.9, 0.94, 0.95, 0.94, 0.9 );


        vec4 get_pixel(in vec2 coords, in float dx, in float dy) { 

            return texture2D( tDiffuse, coords + vec2(dx, dy) );
            
        }

        float Convolve(in float[25] kernel, in float[25] m, in float offset) {

            float res = 0.0;

            float denom = 0.0;


            for (int i=0; i < 25; i++) {
          
                //res += kernel[i] * m[i];

                res += m[i];
                               
                // increment denominator only if pixel defined
                denom += m[i] > 0.0 ? 1.0 : 0.0;

            }

            // ========== Grant Kot's criteria ========= test =======================
            // linear estimation:  l1 + (l1 - l2) or 2 * l1 - l2

            // res += m[11] + (m[11] - m[10]);  // left
            // res += m[13] + (m[13] - m[14]);  // right
            // res += m[17] + (m[17] - m[22]);  // above
            // res += m[7]  + (m[7]  - m[2] );  // below

            // // res += m[16] + (m[16] - m[20]);  // left-above
            // // res += m[18] + (m[18] - m[24]);  // right-above
            // // res += m[6]  + (m[6]  - m[0] );  // left-below
            // // res += m[8]  + (m[8]  - m[4] );  // right-below

            // res += (m[16] + m[18] + m[6] + m[8]) * 0.9;   //add diagonal neighbors

            // res += (m[15] + m[21] + m[23] + m[19] + m[9] + m[3] + m[1] + m[5]) * 0.8;  //add peripheral sub-neighbors

            //======================================================================


            // don't fill pixel if its immediate neighbor and next one are 0
            // if ( m[11] + m[10] + m[13] + m[14] + m[17] + m[22] + m[7] + m[2] == 0.0 )                
            //     {
            //         res = 0.0;
            //     }

            // don't fill pixel if its immediate neighbors are all 0
            if ( m[11] + m[17] + m[13] + m[7] == 0.0 )                
                {
                    res = 0.0;
                }


            if (denom < hf_min_defined) res = 0.0;

            //denom *= hf_denominator;

            return clamp( res / denom, 0.0, 1.0);
        }


        // float Convolve2(in float[25] kernel, in float[5][5] matrix, in float offset) {

        //     float res = 0.0;

        //     float denom = 0.0;

        //     for (int i= -2; i <= 2; i++) {

        //         for (int j= -2; j <= 2; j++) {



        //             res += matrix[i+2][j+2];

        //             // increment denominator only if pixel defined
        //             denom += matrix[i+2][j+2] > 0.0 ? 1.0 : 0.0;

        //         }
        //     }


        //     if (denom < hf_min_defined) res = 0.0;

        //     return clamp( res / denom, 0.0, 1.0);
        // }

        float[25] GetData(in int channel) {
            float[25] mat;
            int k = -1;
            for (int i= -2; i <= 2; i++) {   
                for(int j= -2; j <= 2; j++) {    
                    k++;    
                    mat[k] = get_pixel( vUv, float(i) * step.x, float(j) * step.y )[channel];
                }
            }
            return mat;
        }

        // float[5][5] GetData2(in int channel) {
        //     float[5][5] mat;
        //     int k = 0;
        //     int l = 0;
        //     for (int i= -2; i <= 2; i++) {   
                
        //         for(int j= -2; j <= 2; j++) {    
                        
        //             mat[k][l] = get_pixel( vUv, float(i) * step.x, float(j) * step.y )[channel];
        //             l++;
        //         }
        //         k++;
        //     }

        //     return mat;
        // }

        // float[25] GetDistances() {

        //     float[25] mat;
        //     int k = 0;
        //     for (int i= -2; i <= 2; i++) {   
        //         for(int j= -2; j <= 2; j++) {    
                       
        //             mat[k] = (8.0 - float(i*i) + float(j*j)) / 7.0;
        //             k++; 
        //         }
        //     }
        //     return mat;
        // }



		void main(void) {


			float matr[25] = GetData(0);

            //float matr2[5][5] = GetData2(0);

            //float distances[25] = GetDistances();


            float thisPix = texture2D( tDiffuse, vUv ).x;

            if ( thisPix == 0.0 ) {

                thisPix = Convolve( kernel2, matr, hf_offset );

                // thisPix = Convolve( distances, matr, hf_offset );

            }

			gl_FragColor = vec4( vec3( thisPix ), 1.0);

		}`
	
};
