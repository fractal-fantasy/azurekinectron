THREE.OrbitControls.prototype.getState = function() {

    return {
        target: this.target.clone(),
        position: this.object.position.clone(),
        zoom: this.object.zoom
    }

}

THREE.OrbitControls.prototype.setState = function(stashedState){

    // // prevent changhing camera position if auto-rotation is turned on
    // if ( this.autoRotate === true ) return;

    if ( stashedState !== undefined ) {

        this.target.copy(stashedState.target);
        this.object.position.copy(stashedState.position);
        this.object.zoom = stashedState.zoom;

    } else {

        this.target.copy( new THREE.Vector3() );
        this.object.position.copy( new THREE.Vector3(0,0,params.camera.camDistance.value) );
        this.object.zoom = 1;

    }


    // this.saveState();
    // this.reset();

    this.update();

    console.log( "orbit controls set state:", this.object );

}

THREE.OrbitControls.prototype.refreshAutoRotationAngle = function () {

    this.autoRotationAngle = 2 * Math.PI / 60 / 60 * this.autoRotateSpeed;

};
    
