function HalfCubeCamera(near, far, cubeResolution, options) {
    THREE.Object3D.call(this);

    this.type = 'HalfCubeCamera';
    this._renderBottom = true; // Private property with initial value true

    // Define getter and setter for renderBottom to control internal state
    Object.defineProperty(this, 'renderBottom', {
        get: function() {
            return this._renderBottom;
        },
        set: function(value) {
            if (this._renderBottom !== value) {
                this._renderBottom = value;
                this.adjustResolution();
            }
        }
    });

    var fov = 90; // Field of view
    var aspect = 1; // Aspect ratio

    // Define cameras
    var cameras = {
        PX: new THREE.PerspectiveCamera(fov, aspect, near, far),
        NX: new THREE.PerspectiveCamera(fov, aspect, near, far),
        PY: new THREE.PerspectiveCamera(fov, aspect, near, far),
        NY: new THREE.PerspectiveCamera(fov, aspect, near, far),
        PZ: new THREE.PerspectiveCamera(fov, aspect, near, far),
        NZ: new THREE.PerspectiveCamera(fov, aspect, near, far)
    };

    // Set up camera orientations
    cameras.PX.up.set(0, -1, 0);
    cameras.PX.lookAt(new THREE.Vector3(1, 0, 0));
    this.add(cameras.PX);

    cameras.NX.up.set(0, -1, 0);
    cameras.NX.lookAt(new THREE.Vector3(-1, 0, 0));
    this.add(cameras.NX);

    cameras.PY.up.set(0, 0, 1);
    cameras.PY.lookAt(new THREE.Vector3(0, 1, 0));
    this.add(cameras.PY);

    cameras.NY.up.set(0, 0, -1);
    cameras.NY.lookAt(new THREE.Vector3(0, -1, 0));
    this.add(cameras.NY);

    cameras.PZ.up.set(0, -1, 0);
    cameras.PZ.lookAt(new THREE.Vector3(0, 0, 1));
    this.add(cameras.PZ);

    cameras.NZ.up.set(0, -1, 0);
    cameras.NZ.lookAt(new THREE.Vector3(0, 0, -1));
    this.add(cameras.NZ);

    options = options || { format: THREE.RGBFormat, magFilter: THREE.LinearFilter, minFilter: THREE.LinearFilter };
    
    // Initial render target with original resolution
    this.renderTarget = new THREE.WebGLCubeRenderTarget(cubeResolution, options);
    this.renderTarget.texture.name = "HalfCubeCameraTexture";

    this.adjustResolution(); // Adjust resolution based on initial renderBottom value

    this.update = function(renderer, scene) {
        if (this.parent === null) {
            this.updateMatrixWorld();
        }

        var currentRenderTarget = renderer.getRenderTarget();
        var renderTarget = this.renderTarget;
        var generateMipmaps = renderTarget.texture.generateMipmaps;

        renderTarget.texture.generateMipmaps = false;

        // Render each camera's view into the corresponding render target
        for (var key in cameras) {
            if (key !== 'NY' || this.renderBottom) {  // Check if rendering bottom
                var index = {PX: 0, NX: 1, PY: 2, NY: 3, PZ: 4, NZ: 5}[key];
                renderer.setRenderTarget(renderTarget, index);
                renderer.render(scene, cameras[key]);
            }
        }

        renderTarget.texture.generateMipmaps = generateMipmaps;
        renderer.setRenderTarget(currentRenderTarget);
    };

    this.clear = function(renderer, color, depth, stencil) {
        var currentRenderTarget = renderer.getRenderTarget();
        var renderTarget = this.renderTarget;

        for (var i = 0; i < 6; i++) {
            if (i !== 3 || this.renderBottom) { // Skip or include bottom based on flag
                renderer.setRenderTarget(renderTarget, i);
                renderer.clear(color, depth, stencil);
            }
        }

        renderer.setRenderTarget(currentRenderTarget);
    };
}

HalfCubeCamera.prototype = Object.create(THREE.Object3D.prototype);
HalfCubeCamera.prototype.constructor = HalfCubeCamera;

HalfCubeCamera.prototype.adjustResolution = function() {
    var adjustedResolution = this._renderBottom ? this.renderTarget.width : Math.ceil(this.renderTarget.width * Math.sqrt(6/5));
    
    // Update the render target with the new resolution
    var options = { format: this.renderTarget.texture.format, magFilter: this.renderTarget.texture.magFilter, minFilter: this.renderTarget.texture.minFilter };
    this.renderTarget.dispose(); // Dispose of the old render target
    this.renderTarget = new THREE.WebGLCubeRenderTarget(adjustedResolution, options);
    this.renderTarget.texture.name = "HalfCubeCameraTexture";
};

// Example usage
var halfCubeCamera = new HalfCubeCamera(0.1, 1000, 1024, {}, false);  // Initialize without rendering bottom camera
