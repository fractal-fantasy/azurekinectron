// Modules to control application life and create native browser window
// const {app, BrowserWindow} = require('electron')
const electron = require('electron');
const {app, BrowserWindow, Menu} = require('electron');

const path = require('path');
const ipc = require('electron').ipcMain;

const dialog = electron.dialog;
const fs = require('fs');

require('./crash-reporter');

const Store = require('electron-store');

const store = new Store();

app.allowRendererProcessReuse = true;
process.env['ELECTRON_DISABLE_SECURITY_WARNINGS'] = 'true';

const prefsWindowDefaultTitle = "Azure Kinectron";


//RELOAD SHORTCUT


//FORCE GPU
// app.commandLine.appendSwitch('high-dpi-support', 'true');
// app.commandLine.appendSwitch('force-device-scale-factor', '1');
app.commandLine.appendSwitch('ignore-gpu-blacklist', 'true');
// app.commandLine.appendSwitch('use-angle', 'gl');  // Or 'd3d11', 'd3d9', depending on which works best
// This specifically tries to force the use of a discrete GPU
// app.commandLine.appendSwitch('use-gl', 'desktop');  // Forces OpenGL on Windows which typically ensures discrete GPU is used


// Keep a global reference of the window object, if you don't, the window will
// be closed automatically when the JavaScript object is garbage collected.

global.kinectWindow = null;
global.prefsWindow = null;

var saveFilePath = undefined;

function createKinectWindow () {
    
    var electronScreen = electron.screen;
    var displays = electronScreen.getAllDisplays();
    var externalDisplay = null;

    var largestSize = 0;
    for (var i in displays) {
        if (displays[i].bounds.x != 0 || displays[i].bounds.y != 0) {
            var size = displays[i].bounds.width * displays[i].bounds.height;
            if (size > largestSize) {
                largestSize = size;
                externalDisplay = displays[i];
            }
        }
    }
    console.log(displays);

    if (externalDisplay) {
        console.log("launching externaldisplay");
        kinectWindow = new BrowserWindow( {
            x: externalDisplay.bounds.x +50,
            y: externalDisplay.bounds.y +50,
            width: externalDisplay.bounds.x,
            height: externalDisplay.bounds.y,
            // fullscreen: true,
            // fullscreenable: true,
            fullscreenable: false,

            //yNYC SETTINGS
            // x: externalDisplay.bounds.x,
            // y: externalDisplay.bounds.y+200,
            // width: 1920,
            // height: 1080,
            // fullscreen: false,
            
          

           
            transparent: true, 
            frame: false,
            autoHideMenuBar: true,
            icon: __dirname + '/renderer/images/icon.ico',
            // icon:'renderer/images/icon.ico',
            // skipTaskbar:true,
            
         
            webPreferences: {
                nodeIntegration: true,
                //webviewTag: true,  
                nodeIntegrationInWorker: true,
                enableRemoteModule: true
            }
        } );
        kinectWindow.maximize();
    }
    else {
        kinectWindow = new BrowserWindow({
        // width: 1600,
        // height: 1200,

    
        autoHideMenuBar: true,
        transparent: true, 
        frame: false,
        icon: __dirname + '/renderer/images/icon.ico',
        x: displays[0].bounds.x ,
        y: displays[0].bounds.y ,
        // width:  2560,
        // height: 1600*0.7,
        width:  1440,
        height: 1440,
        // icon:'renderer/images/icon.ico',
        //fullscreen: true,
        
        webPreferences: {
            nodeIntegration: true,
            //webviewTag: true,
            nodeIntegrationInWorker: true,
            enableRemoteModule: true
        }
        });
    }
  
    
    // and load the index.html of the app.
    
    kinectWindow.loadFile('renderer/render-view.html')

    // kinectWindow.loadFile('app/test.html')


}

function createControlWindow () {


    

    prefsWindow = new BrowserWindow({
        minWidth : 500,
        maxWidth : 2400,
        width: 2200,
        height: 1080,
        autoHideMenuBar: false,
        icon: __dirname + '/renderer/images/icon.ico',
        // fullscreen: true,
        webPreferences: {
            nodeIntegration: true,
            //webviewTag: true,
            enableRemoteModule: true
        }
    })

    //prefsWindow.loadFile('kinectPrefs.html')
    prefsWindow.loadFile('renderer/control-view.html');
    // prefsWindow.loadFile('renderer/control-view.html');

    // store.delete('latestSavePath');

  
    function loadLatestSavedProject() {

                    // var latestPath = store.get('latestPath');
                    var latestSavePath = store.get('latestSavePath');

                    if ( latestSavePath != undefined ){
                        console.log("Loading latest saved project file:", latestSavePath);

                        fs.readFile( latestSavePath,

                            (err, data) => {
                                if (!err) {

                                    prefsWindow.webContents.send('file-opened', data.toString());

                                    store.set( 'latestPath', latestSavePath );

                                    saveFilePath = latestSavePath;

                                    prefsWindow.setTitle( prefsWindowDefaultTitle + "  " + saveFilePath );

                                } else {
                                    console.error(err.toString());
                                }
                            }
                        )

                    } else {
                        console.log("You have no recently saved project files yet");

                        var response = dialog.showMessageBox(null, {
                            message: "You have no recently saved project files yet"
                        });
                    }



    }



    var application_menu = [
        {
        label: 'File',
        submenu: [

            {
                label: 'New project',
                accelerator: 'CmdOrCtrl+N',
                click: () => {

                    // store.delete('latestSavePath');
                    saveFilePath = undefined;

                    prefsWindow.webContents.send( 'reset-project' );

                    prefsWindow.setTitle( prefsWindowDefaultTitle );

                }
            },

            {
                label: 'Open..',
                accelerator: 'CmdOrCtrl+O',
                click: () => {

                    var openDialogPath = store.get('latestPath') || '';

                    electron.dialog.showOpenDialog(
                        {   properties: [ 'openFile' ],
                            filters: [{
                                name: 'Fractal Fantasy File',
                                extensions: ['ff']
                            }],
                            defaultPath: openDialogPath
                        }).then(result => {

                            if (result.canceled == false) {  // proceed to read file only if dialog is not cancelled

                                fs.readFile( result.filePaths[0],

                                    (err, data) => {

                                        if (!err) {

                                            prefsWindow.webContents.send('file-opened', data.toString())

                                            store.set( 'latestPath', result.filePaths[0] );

                                            // store.set( 'latestSavePath', result.filePaths[0] );

                                            saveFilePath = result.filePaths[0];

                                            console.log( "storage set w/ latestPath:", store.get('latestPath') );

                                            console.log(result);

                                            prefsWindow.setTitle( prefsWindowDefaultTitle + "  " + saveFilePath );
                                            
                                        } else 
                                            console.error(err.toString());
                                    }
                                )
                            }

                        }).catch(err => {
                            console.error(err);
                        })
 
                }
            },
            {
                label: 'Open recent',   // opens latest saved project file if the last saved session exist
                accelerator: 'Alt+O',
                click: () => {

                    loadLatestSavedProject();
 
                }
            },

            {
                label: 'Save',
                accelerator: 'CmdOrCtrl+S',
                click: () => {                    

                    var latestSavePath = store.get('latestSavePath');

                    if (saveFilePath == undefined) {  // if project has not been saved yet open save dialog, else save to latest saved filepath

                        var openDialogPath = store.get('latestPath') || '';

                        electron.dialog.showSaveDialog({
                            filters: [{
                                name: 'Fractal Fantasy File',
                                extensions: ['ff']
                            }],
                            defaultPath: openDialogPath
                            }).then(result => {
                                
                                if (result.canceled == false) {   // if dialog not cancelled

                                    console.log("filePath:", result.filePath);

                                    saveFilePath = result.filePath;

                                    prefsWindow.webContents.send( 'get-data-to-save' );       

                                    store.set( 'latestSavePath', saveFilePath );

                                    console.log( "Save | storage set w/ latestSavePath:", store.get('latestSavePath') );    

                                    prefsWindow.setTitle( prefsWindowDefaultTitle + "  " + saveFilePath );

                                } else 
                                    console.log("Dialog is cancelled");

                            }).catch(err => {
                                console.error("Write file error:", err);
                            })

                    } 
                    else
                    {
                        console.log("Saving current project file:", latestSavePath);

                        prefsWindow.webContents.send( 'get-data-to-save' );

                    }
 
                }
            },
            {
                label: 'Save as..',
                accelerator: 'CmdOrCtrl+Shift+S',
                click: () => {

                    var openDialogPath = store.get('latestPath') || '';

                    electron.dialog.showSaveDialog({
                        filters: [{
                            name: 'Fractal Fantasy File',
                            extensions: ['ff']
                        }],
                        defaultPath: openDialogPath
                        }).then(result => {

                            if (result.canceled == false) {   // if dialog not cancelled

                                console.log(result.filePath);

                                saveFilePath = result.filePath;

                                prefsWindow.webContents.send( 'get-data-to-save' );       

                                store.set( 'latestSavePath', saveFilePath );

                                console.log( "Save as.. | storage set w/ latestSavePath:", store.get('latestSavePath') );    

                                prefsWindow.setTitle( prefsWindowDefaultTitle + "  " + saveFilePath );

                            } else 
                                console.log("Dialog is cancelled");

                        }).catch(err => {
                            console.error(err);
                        })
 
                }
            },
            {
                label: 'Reset OSC Mappings',
                click: () => {
                    
                    prefsWindow.webContents.send( 'reset-osc-mappings' );
 
                }
            },


            {
                label: 'Load OSC Mappings..',
                click: () => {

                    var openDialogPath = store.get('latestPath') || '';

                    electron.dialog.showOpenDialog(
                        {   properties: [ 'openFile' ],
                            filters: [{
                                name: 'Fractal Fantasy File',
                                extensions: ['ff']
                            }],
                            defaultPath: openDialogPath
                        }).then(result => {

                            if (result.canceled == false) {  // proceed to read file only if dialog is not cancelled

                                fs.readFile( result.filePaths[0],

                                    (err, data) => {

                                        if (!err) {

                                            prefsWindow.webContents.send('loadOSCMappings', data.toString())

                                            // store.set( 'latestPath', result.filePaths[0] );

                                            // // store.set( 'latestSavePath', result.filePaths[0] );

                                            // saveFilePath = result.filePaths[0];

                                          //  console.log( "storage set w/ latestPath:", store.get('latestPath') );

                                            // console.log(result);

                                            // prefsWindow.setTitle( prefsWindowDefaultTitle + "  " + saveFilePath );
                                            
                                        } else 
                                            console.error(err.toString());
                                    }
                                )
                            }

                        }).catch(err => {
                            console.error(err);
                        })
 
                }
            },
            {
                label: 'DevTools',
                click: () => {
                    
                                prefsWindow.webContents.openDevTools();
                                kinectWindow.webContents.openDevTools();
                            
                }
            },
            
        ]
        }
    ];


    menu = Menu.buildFromTemplate(application_menu);
    Menu.setApplicationMenu(menu);

    
    ipc.on('get-file-data', function(event) {
        
        var data = null
        if (process.platform == 'win32' && process.argv.length >= 2) {
          var openFilePath = process.argv[1];
          data = openFilePath;
        }
        console.log('GET DATA FILE', data);
       //

        if (data != null) {
            if (data.includes(".ff")) {
                if (fs.existsSync(data)) {
                    console.log("opening file");
                    setTimeout(() => {
                        //if (data != "main.js") {
                            fs.readFile( data,
                                
                                    (err, data) => {
                                        if (!err) {
                                        
                                            prefsWindow.webContents.send('file-opened', data.toString());
                        
                                            //store.set( 'latestPath', data );
                        
                                            saveFilePath = data;
                        
                                            prefsWindow.setTitle( prefsWindowDefaultTitle + "  " + saveFilePath );
                        
                                        } else {
                                            console.log(err.toString());
                                        }
                                    }
                            
                            )
                        //}
                    }, 500);
                }
            }
            prefsWindow.webContents.send('file-load', data);
          
         }

        event.returnValue = data;


    });

    ipc.on('return-data-to-save', (event, data) => {

        saveFile( saveFilePath, data );

        //console.log('return-data-to-save:', data) 

    })

    ipc.on('load-latest-project', (event, arg) => {

        loadLatestSavedProject();

    })


  

   

    // ipc.on('save-screenshot', (event, data) => {

    //     electron.dialog.showSaveDialog({
    //         filters: [{
    //             name: 'screenshot',
    //             extensions: ['png']
    //         }] }
    //         ).then(result => {

    //             console.log(result.filePath);

    //             saveFilePath = result.filePath;

    //             var blob = new Blob( [ data ], { type: "image/jpeg" } );

    //             saveFile( saveFilePath, blob );

    //         }).catch(err => {
    //             console.error(err);
    //         })
        

    // })

    // save file
    function saveFile(path, data) {

        fs.writeFile(path, data,

            (err) => {
                if (!err) {

                    prefsWindow.webContents.send( 'file-saved', path );

                    //console.log(data.toString());
                } else 
                    console.error(err);
            })
    }


}


ipc.on('asynchronous-message', (event, arg) => {

    closeWindows();

    console.log('asynchronous-message:', arg)

})

ipc.on('synchronous-message', (event, arg) => {

    closeWindows();

    console.log('synchronous-message:', arg) 

})

ipc.on('start-projection', (event, data) => {

    kinectWindow.show();

})


function closeWindows (){
    prefsWindow = null
    if (kinectWindow != null){
      kinectWindow.close();
    }
}

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.on('ready', function() {
    
    createKinectWindow();
    createControlWindow();
    console.log("App ready");

    //WEB INSPECTOR DEV TOOLS
    // prefsWindow.webContents.openDevTools();
    // kinectWindow.webContents.openDevTools();

    var dmpDir = app.getPath('crashDumps');
    console.log("crashDumps:", dmpDir);

    // kinectWindow.on('hide', function () {

    //     dialog.showMessageBox({
    //         message: "HIDE",
    //         buttons: ["OK"]
    //     });

    //     console.log("kinect window close");
    
    // })

    // kinectWindow.on('close', function () {

    //     dialog.showMessageBox({
    //         message: "Close button has been pressed!",
    //         buttons: ["OK"]
    //     });

    //     console.log("kinect window close");
    
    // })

    // kinectWindow.onbeforeunload = (e) => {
    //     console.log('I do not want to be closed')

    //     // Unlike usual browsers that a message box will be prompted to users, returning
    //     // a non-void value will silently cancel the close.
    //     // It is recommended to use the dialog API to let the user confirm closing the
    //     // application.
    //     e.returnValue = false // equivalent to `return false` but not recommended
    // }

    // prefsWindow.maximize();

    // Open the DevTools.
    // Emitted when the window is closed.
    kinectWindow.on('closed', function () {
        // Dereference the window object, usually you would store windows
        // in an array if your app supports multi windows, this is the time
        // when you should delete the corresponding element.
        //kinectWindow.webContents.send("close-kinect");

        kinectWindow = null;

        console.log("kinect window closed");
    
    })

    prefsWindow.on('closed', function () {
        // Dereference the window object, usually you would store windows
        // in an array if your app supports multi windows, this is the time
        // when you should delete the corresponding element.

        if (kinectWindow != null){
            kinectWindow.webContents.send("close-kinect");
        }

        //setInterval(closeWindows, 1000)
        
    })

})

// Quit when all windows are closed.
app.on('window-all-closed', function () {
    // On macOS it is common for applications and their menu bar
    // to stay active until the user quits explicitly with Cmd + Q
    if (process.platform !== 'darwin') app.quit()
})

app.on('session-created', (session) => {

    var ver = app.getVersion();
    console.log("App version:", ver);
    console.log("Node version:", process.versions.node);
    console.log("Chrome version:", process.versions.chrome);
    console.log("Electron version:", process.versions.electron);

})

// app.on('activate', function () {
//     // On macOS it's common to re-create a window in the app when the
//     // dock icon is clicked and there are no other windows open.
//     if (kinectWindow === null) createKinectWindow();
//     if (prefsWindow === null) createControlWindow();
//     console.log("App activated");
// })

