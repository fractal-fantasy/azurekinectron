const {app, BrowserWindow} = require('electron')
const path = require('path')

let mainWindow

function createWindow () {
  mainWindow = new BrowserWindow({
    width: 800,
    height: 600,
    webPreferences: {
      nodeIntegration: true,
      webviewTag: true
    }
  })

  mainWindow.loadFile('examples-individual/point-cloud-color.html');
  // mainWindow.loadFile('examples-individual/stream-transforms.html');
 // mainWindow.loadFile('examples-individual/body-tracking-skeleton-2d-color.html');
  // mainWindow.loadFile('examples-individual/body-tracking-skeleton-3d.html');

  mainWindow.on('closed', function () {
    mainWindow = null
  })
}

app.on('ready', createWindow)
app.on('window-all-closed', function () {
  if (process.platform !== 'darwin') app.quit()
})

app.on('activate', function () {
  if (mainWindow === null) createWindow()
})
