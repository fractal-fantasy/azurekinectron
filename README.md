
INSTRUCTIONS TO INSTALL DEV ENVIRONMENT

0) Install Azure Kinect Body Tracking SDK & Azure Kinect Sensor SDK
https://www.microsoft.com/en-us/download/confirmation.aspx?id=102901
https://github.com/microsoft/Azure-Kinect-Sensor-SDK/blob/develop/docs/usage.md


1) INSTALL NODE.JS

https://nodejs.org/en/download/

- include required tools when promted
- dont use NVM and check the box at the end of the intallation to install the dependencies.

---

1b) INSTALL GIT

https://git-scm.com/download/win

----

2) INSTALL YARN

npm install --global yarn

https://classic.yarnpkg.com/en/docs/install/#windows-stable

& restart command prompt or powershell

---

3) INSTALL ELECTRON (SYSTEM-WIDE):


cd <user directory>  
npm install --global electron  
npm install --global electron-forge  

---



4) INSTALL DEPENDENCIES:

cd <repository directory>  
yarn install  

YOU may need to install Microsoft Visual Studio with C++ Development and Node.js development checked. 
---

4b) ENABLE SCRIPTING

run PowerShell as administrator
Set-ExecutionPolicy -ExecutionPolicy Unrestricted


5) CLONE AND LINK MIKAS VERSION OF THE "kinect-azure" MODULE:

a) Clone/download mikas kinect-azure repository: 'https://github.com/mikkamikka/kinect-azure.git` to your local dev directory

b) In command-prompt navigate to cloned repo folder: `cd <path to the repo>`
    Enter the following commands:
    `npm install`
    `yarn link`
        (this should return: `success Registered "kinect-azure"`)
        *you might need to unlink manually first by deleting shortcut in "C:\Users\<username>\AppData\Local\Yarn\Data\link"

c)  `cd <your 'azurekinectron' project directory>`
Enter following commands:

`yarn unlink "kinect-azure"`
(this will unlink current remote npm package)

 `yarn link "kinect-azure"`
(this will link yarn/npm to your local version of the kinect-azure module)

--
TO GET MIKAS NODE WORKING:
-move content of:

C:\Program Files\Azure Kinect Body Tracking SDK\tools
&
C:\Program Files\Azure Kinect SDK v1.4.1

to

C:\Users\FF\Documents\kinect-azure-mika
and maybe 'examples' and 'node' folder?

--
FOR LAPTOPS WITH DISCREET GPU
MAKE SURE APP IS USING THE GPU (USE A MUX SWITCH!)
--

If having trouble building mikas node you can download an earlier one prebuilt here:
https://www.dropbox.com/s/y8f00qpsxy37we1/kinect-azure-zora.7z?dl=0

----


6) START APP

cd <repository directory>  
yarn start  

----

7) FINISHED!

------------------------------------------------------------


SOME MORE USEFUL STUFF FOR DEVELOPMENT...



TO CREATE INSTALLER (for deployment)

Will create app one directory back from azureKinectron:
yarn build

Another way using electron-forge:
yarn make

To create a packaged installer:
1) Package The App
yarn build

2) Create Installer with Inno Setup Compiler
download: https://jrsoftware.org/isdl.php
tutorial: https://youtu.be/rxLdBDJjER4

---
---


Notes on building local kinect-azure module project (from mikas 'kinect-azure'):

 - make sure node-gyp is installed globally `npm install -g node-gyp`

 - VC++ build tools should be installed along with VS 2017

 google: visual studio 2017 community .iso (to find offline installer, microsoft doesnt provide it anymore)

 - to build node module run `node-gyp rebuild`


----
----



ELECTRON FORGE

yarn create electron-app my-app

cd my-app
yarn start


---

PLAYING BACK RECORDED MKV FILES IN APP:

If you don't have a kinect then u can use this video file:
https://www.dropbox.com/s/a6g1dzop8w7n05n/output.mkv?dl=0

1) Record Kinect .mkv file
 open commmand prompt: 
 "C:\Program Files\Azure Kinect SDK v1.4.1\tools\k4arecorder.exe" -d WFOV_2X2BINNED -c 720p -r 30  C:\Users\FF\Videos\kinect%date:~-4,4%-%date:~-10,2%-%date:~7,2%--%time:~0,2%.%time:~3,2%.mkv

2) Put output.mkv in renderer/images

3) start app and press "play"



---

firmware update:
navigate to folder in command prompt
AzureKinectFirmwareTool.exe -u "C:\Program Files\Azure Kinect SDK v1.4.1\tools\firmware\AzureKinectDK_Fw_1.6.110079014.bin"

--

DEVTOOLS

open app and press \

--

#### Error: The specified module could not be found

"The specified module could not be found," could occur when the native module is missing a dependency, such as a .dll, 

This module relies on some dll & onnx files from the kinect azure installation. If those files can't be loaded, loading the module will fail as well. You can can find those in the `Azure Kinect Body SDK\tools` directory. Either add that folder (eg C:\Program Files\Azure Kinect Body Tracking SDK\tools) to your PATH or copy the dll and onnx files into the root of your project.

THIS WORKS WHEN YOU JUST COPY paste the contents of the "tools" folder into the root of the app directory.



--


DESKTOP SHORTCUT

to make desktop shortcut:
1. Right click on some empty space in Explorer, and in the context menu go to "New/Shortcut".
2. When prompted to enter a location put:
C:\Windows\System32\cmd.exe /k cd C:\Users\FF\Documents\azurekinectron & yarn start

save it and pin to your taskbar

--


TROUBLESHOOTING MIKA AZURE NODE
installing mika azure node:
- make sure you've installed azure kinect body tracking sdk! lol

To update the whole project:
1. You'll need to rebuild kinect-azure node project: pull the latest commits from https://github.com/mikkamikka/kinect-azure and run node-gyp rebuild
2. Copy directml.dll and  to your ..\azurekinectron\node_modules\electron\dist directory
3. Pull latest commit from azurekinectron repo and run as normal


trying with yarn - didnt work


C:\Users\<username>\AppData\Local\Yarn\Data\link

grabbed it off of zoras computer:
https://www.dropbox.com/s/y8f00qpsxy37we1/kinect-azure-zora.7z?dl=0


If you get "kinect-azure" NODE NOT FOUND while installing, it's because it can't see the linked "kinect-azure" node:
-yarn unlink "kinect-azure", then yarn install, and relink the custom kinect-azure packae
-you may have to delete the kinect-azure link in yarn-link folder, and relink it afterwards (C:\Users\FF\AppData\Local\Yarn\Data\link)

If getting freezes & "Failed to create custom ir image":
link "kinect-azure-mika-working" (seems to work best for now)
https://www.dropbox.com/scl/fo/d4luotnsc3xqzj465z6mh/APoi_XT8ife0fqKWhu2nPo8?rlkey=ni54fgnyrn7hcjot6fjdihg23&dl=0