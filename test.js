// Modules to control application life and create native browser window
// const {app, BrowserWindow} = require('electron')
const electron = require('electron');
const {app, BrowserWindow, Menu} = require('electron');

const path = require('path');
const ipc = require('electron').ipcMain;

const dialog = electron.dialog;
const fs = require('fs');

require('./crash-reporter');

const Store = require('electron-store');

const store = new Store();

app.allowRendererProcessReuse = true;
process.env['ELECTRON_DISABLE_SECURITY_WARNINGS'] = 'true';


//RELOAD SHORTCUT



// Keep a global reference of the window object, if you don't, the window will
// be closed automatically when the JavaScript object is garbage collected.

global.kinectWindow = null;
// global.prefsWindow = null;

var saveFilePath = '';

function createKinectWindow () {
    
    var electronScreen = electron.screen;
    var displays = electronScreen.getAllDisplays();
    var externalDisplay = false;

    for (var i in displays) {
        if (displays[i].bounds.x != 0 || displays[i].bounds.y != 0) {
            externalDisplay = displays[i];
        break;
        }
    }
    kinectWindow = new BrowserWindow({
        width: 1600,
        height: 1200,
        
        //fullscreen: true,
        
        webPreferences: {
            nodeIntegration: true,
            //webviewTag: true,
            nodeIntegrationInWorker: true,
            enableRemoteModule: true
        }
 });
  
    // and load the index.html of the app.
    
    // kinectWindow.loadFile('renderer/main-scene.html')

    kinectWindow.loadFile('renderer/test.html')


}

function createControlWindow () {

    prefsWindow = new BrowserWindow({
        minWidth : 1520,
        maxWidth : 2400,
        width: 1920,
        height: 1080,
        autoHideMenuBar: false,
        // fullscreen: true,
        webPreferences: {
            nodeIntegration: true,
            //webviewTag: true,
            enableRemoteModule: true
        }
    })

    //prefsWindow.loadFile('kinectPrefs.html')
    // prefsWindow.loadFile('renderer/control.html');

    var application_menu = [
        {
        label: 'File',
        submenu: [

            {
                label: 'Load project',
                //accelerator: 'CmdOrCtrl+O',
                click: () => {
                    electron.dialog.showOpenDialog(
                        {   properties: [ 'openFile' ],
                            filters: [{
                                name: 'Fractal Fantasy File',
                                extensions: ['ff']
                            }]
                        }).then(result => {

                            console.log(result.filePaths);

                            fs.readFile(result.filePaths[0],

                                (err, data) => {
                                    if (!err) {
                                        // prefsWindow.webContents.send('file-opened', data.toString())

                                        store.set( 'latestPath', result.filePaths[0] );

                                        console.log( "storage set w/ latestPath:", store.get('latestPath') );

                                        //console.log(data.toString());
                                    } else 
                                        console.error(err.toString());
                                }
                            )

                        }).catch(err => {
                            console.error(err);
                        })
 
                }
            },
            // {
            //     label: 'Load latest project',
            //     click: () => {

            //         var latestPath = store.get('latestPath');

            //         if ( latestPath != undefined ){
            //             console.log("Loading latest loaded project file:", latestPath);

            //             fs.readFile( latestPath,

            //                 (err, data) => {
            //                     if (!err) {
            //                         prefsWindow.webContents.send('file-opened', data.toString());
            //                     } else {
            //                         console.error(err.toString());
            //                     }
            //                 }
            //             )

            //         } else {
            //             console.log("You have not loaded any of project files yet!");
            //         }
 
            //     }
            // },
            {
                label: 'Save project',
                //accelerator: 'CmdOrCtrl+O',
                click: () => {
                    electron.dialog.showSaveDialog({
                        filters: [{
                            name: 'Fractal Fantasy File',
                            extensions: ['ff']
                        }] }
                        //{ properties: [ 'openFile' ]}
                        ).then(result => {

                            console.log(result.filePath);

                            saveFilePath = result.filePath;

                            prefsWindow.webContents.send( 'get-data-to-save' );                            

                        }).catch(err => {
                            console.error(err);
                        })
 
                }
            },
            {
                label: 'Reset project',
                click: () => {
                    
                    prefsWindow.webContents.send( 'reset-project' );
 
                }
            },
            // {
            //     label: 'Reset current preset',
            //     click: () => {
                    
            //         prefsWindow.webContents.send( 'reset-current-preset' );
 
            //     }
            // },
            
        ]
        }
    ];


    menu = Menu.buildFromTemplate(application_menu);
    Menu.setApplicationMenu(menu);

    ipc.on('return-data-to-save', (event, data) => {

        saveFile( saveFilePath, data );

        //console.log('return-data-to-save:', data) 

    })

    ipc.on('load-latest-project', (event, arg) => {

        var latestPath = store.get('latestPath');

        if ( latestPath != undefined ){
            console.log("Loading latest loaded project file:", latestPath);

            fs.readFile( latestPath,

                (err, data) => {
                    if (!err) {
                        prefsWindow.webContents.send('file-opened', data.toString());
                    } else {
                        console.error(err.toString());
                    }
                }
            )

        } else {
            console.log("You have not loaded any of project files yet!");
        }

    })

    // ipc.on('save-screenshot', (event, data) => {

    //     electron.dialog.showSaveDialog({
    //         filters: [{
    //             name: 'screenshot',
    //             extensions: ['png']
    //         }] }
    //         ).then(result => {

    //             console.log(result.filePath);

    //             saveFilePath = result.filePath;

    //             var blob = new Blob( [ data ], { type: "image/jpeg" } );

    //             saveFile( saveFilePath, blob );

    //         }).catch(err => {
    //             console.error(err);
    //         })
        

    // })

    // save file
    function saveFile(path, data) {

        fs.writeFile(path, data,

            (err) => {
                if (!err) {

                    prefsWindow.webContents.send( 'file-saved', path );

                    //console.log(data.toString());
                } else 
                    console.error(err);
            })
    }


}


ipc.on('asynchronous-message', (event, arg) => {

    closeWindows();

    console.log('asynchronous-message:', arg)

})

ipc.on('synchronous-message', (event, arg) => {

    closeWindows();

    console.log('synchronous-message:', arg) 

})

ipc.on('start-projection', (event, data) => {

    kinectWindow.show();

})


function closeWindows (){
    // prefsWindow = null
    if (kinectWindow != null){
      kinectWindow.close();
    }
}

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.on('ready', function() {
    
    createKinectWindow();
    // createControlWindow();
    console.log("App ready");

    // prefsWindow.webContents.openDevTools();
    kinectWindow.webContents.openDevTools();

    var dmpDir = app.getPath('crashDumps');
    console.log("crashDumps:", dmpDir);

    // kinectWindow.on('hide', function () {

    //     dialog.showMessageBox({
    //         message: "HIDE",
    //         buttons: ["OK"]
    //     });

    //     console.log("kinect window close");
    
    // })

    // kinectWindow.on('close', function () {

    //     dialog.showMessageBox({
    //         message: "Close button has been pressed!",
    //         buttons: ["OK"]
    //     });

    //     console.log("kinect window close");
    
    // })

    // kinectWindow.onbeforeunload = (e) => {
    //     console.log('I do not want to be closed')

    //     // Unlike usual browsers that a message box will be prompted to users, returning
    //     // a non-void value will silently cancel the close.
    //     // It is recommended to use the dialog API to let the user confirm closing the
    //     // application.
    //     e.returnValue = false // equivalent to `return false` but not recommended
    // }

    // prefsWindow.maximize();

    // Open the DevTools.
    // Emitted when the window is closed.
    kinectWindow.on('closed', function () {
        // Dereference the window object, usually you would store windows
        // in an array if your app supports multi windows, this is the time
        // when you should delete the corresponding element.
        //kinectWindow.webContents.send("close-kinect");

        kinectWindow = null;

        console.log("kinect window closed");
    
    })

    // prefsWindow.on('closed', function () {
    //     // Dereference the window object, usually you would store windows
    //     // in an array if your app supports multi windows, this is the time
    //     // when you should delete the corresponding element.

    //     if (kinectWindow != null){
    //         kinectWindow.webContents.send("close-kinect");
    //     }

    //     //setInterval(closeWindows, 1000)
        
    // })

})

// Quit when all windows are closed.
app.on('window-all-closed', function () {
    // On macOS it is common for applications and their menu bar
    // to stay active until the user quits explicitly with Cmd + Q
    if (process.platform !== 'darwin') app.quit()
})

app.on('session-created', (session) => {

    var ver = app.getVersion();
    console.log("App version:", ver);
    console.log("Node version:", process.versions.node);
    console.log("Chrome version:", process.versions.chrome);
    console.log("Electron version:", process.versions.electron);

})

// app.on('activate', function () {
//     // On macOS it's common to re-create a window in the app when the
//     // dock icon is clicked and there are no other windows open.
//     if (kinectWindow === null) createKinectWindow();
//     if (prefsWindow === null) createControlWindow();
//     console.log("App activated");
// })

